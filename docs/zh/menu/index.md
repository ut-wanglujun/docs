---
headless: true
---
- [发行说明]({{< relref "./docs/Releasenotes/法律声明.md" >}})
    - [发行说明]({{< relref "./docs/Releasenotes/release_notes.md" >}})
        - [简介]({{< relref "./docs/Releasenotes/简介.md" >}})
        - [用户须知]({{< relref "./docs/Releasenotes/用户须知.md" >}})
        - [帐号清单]({{< relref "./docs/Releasenotes/帐号清单.md" >}})
        - [简介]({{< relref "./docs/Releasenotes/简介.md" >}})
        - [系统安装]({{< relref "./docs/Releasenotes/系统安装.md" >}})
        - [关键特性]({{< relref "./docs/Releasenotes/关键特性.md" >}})
        - [已知问题]({{< relref "./docs/Releasenotes/已知问题.md" >}})
        - [已修复问题]({{< relref "./docs/Releasenotes/已修复问题.md" >}})
        - [CVE漏洞]({{< relref "./docs/Releasenotes/CVE漏洞.md" >}})
        - [源代码]({{< relref "./docs/Releasenotes/源代码.md" >}})
        - [参与贡献]({{< relref "./docs/Releasenotes/参与贡献.md" >}})
        - [致谢]({{< relref "./docs/Releasenotes/致谢.md" >}})
        - [快速入门]({{< relref "./docs/Quickstart/quick-start.md" >}})
- [安装升级](#)
    - [安装指南]({{< relref "./docs/Installation/installation.md" >}})
        - [安装在服务器]({{< relref "./docs/Installation/安装在服务器.md" >}})
            - [安装准备]({{< relref "./docs/Installation/安装准备.md" >}})
            - [安装方式介绍]({{< relref "./docs/Installation/安装方式介绍.md" >}})
            - [安装指导]({{< relref "./docs/Installation/安装指导.md" >}})
            - [使用kickstart自动化安装]({{< relref "./docs/Installation/使用kickstart自动化安装.md" >}})
            - [FAQ]({{< relref "./docs/Installation/FAQ.md" >}})
        - [安装在树莓派]({{< relref "./docs/Installation/安装在树莓派.md" >}})
            - [安装准备]({{< relref "./docs/Installation/安装准备-1.md" >}})
            - [安装方式介绍]({{< relref "./docs/Installation/安装方式介绍-1.md" >}})
            - [安装指导]({{< relref "./docs/Installation/安装指导-1" >}})
            - [FAQ]({{< relref "./docs/Installation/FAQ-1.md" >}})
            - [更多资源]({{< relref "./docs/Installation/更多资源.md" >}})
    - [升级指南]({{< relref "./docs/os_upgrade_and_downgrade/openEuler 22.03 LTS升降级指导.md" >}})
- [系统管理](#)
    - [管理员指南]({{< relref "./docs/Administration/administration.md" >}})
        - [查看系统信息]({{< relref "./docs/Administration/查看系统信息.md" >}})
        - [基础配置]({{< relref "./docs/Administration/基础配置.md" >}})
        - [管理用户和用户组]({{< relref "./docs/Administration/管理用户和用户组.md" >}})
        - [使用DNF管理软件包]({{< relref "./docs/Administration/使用DNF管理软件包.md" >}})
        - [管理服务]({{< relref "./docs/Administration/管理服务.md" >}})
        - [管理进程]({{< relref "./docs/Administration/管理进程.md" >}})
        - [管理内存]({{< relref "./docs/Administration/memory-management.md" >}})
        - [配置网络]({{< relref "./docs/Administration/配置网络.md" >}})
        - [使用LVM管理硬盘]({{< relref "./docs/Administration/使用LVM管理硬盘.md" >}})
        - [使用KAE加速引擎]({{< relref "./docs/Administration/使用KAE加速引擎.md" >}})
        - [搭建服务]({{< relref "./docs/Administration/搭建服务.md" >}})
            - [搭建repo服务器]({{< relref "./docs/Administration/搭建repo服务器.md" >}})
            - [搭建FTP服务器]({{< relref "./docs/Administration/搭建FTP服务器.md" >}})
            - [搭建web服务器]({{< relref "./docs/Administration/搭建web服务器.md" >}})
            - [搭建数据库服务器]({{< relref "./docs/Administration/搭建数据库服务器.md" >}})
        - [可信计算]({{< relref "./docs/Administration/可信计算.md" >}})
        - [FAQ]({{< relref "./docs/Administration/FAQ-54.md" >}})
    - [运维指南]({{< relref "./docs/ops_guide/overview.md" >}})
        - [运维概述]({{< relref "./docs/ops_guide/运维概述.md" >}})
        - [系统资源与性能]({{< relref "./docs/ops_guide/系统资源与性能.md" >}})
        - [信息收集]({{< relref "./docs/ops_guide/信息收集.md" >}})
        - [故障应急处理]({{< relref "./docs/ops_guide/故障应急处理.md" >}})
        - [常用工具]({{< relref "./docs/ops_guide/常用工具.md" >}})
        - [常用技能]({{< relref "./docs/ops_guide/常用技能.md" >}})
- [网络](#)
    - [Gazelle用户指南]({{< relref "./docs/Gazelle/Gazelle.md" >}})
- [维护](#)
    - [Aops用户指南]({{< relref "./docs/A-Ops/overview.md" >}})
        - [AOps部署指南]({{< relref "./docs/A-Ops/AOps部署指南.md" >}})
        - [AOps智能定位框架使用手册]({{< relref "./docs/A-Ops/AOps智能定位框架使用手册.md" >}})
        - [aops-agent部署指南]({{< relref "./docs/A-Ops/aops-agent部署指南.md" >}})
        - [热补丁dnf插件使用手册]({{< relref "./docs/A-Ops/热补丁dnf插件使用手册.md" >}})
        - [配置溯源服务使用手册]({{< relref "./docs/A-Ops/配置溯源服务使用手册.md" >}})
        - [架构感知服务使用手册]({{< relref "./docs/A-Ops/架构感知服务使用手册.md" >}})
        - [gala-gopher使用手册]({{< relref "./docs/A-Ops/gala-gopher使用手册.md" >}})
        - [gala-anteater使用手册]({{< relref "./docs/A-Ops/gala-anteater使用手册.md" >}})
        - [gala-spider使用手册]({{< relref "./docs/A-Ops/gala-spider使用手册.md" >}})
        - [社区热补丁制作发布流程]({{< relref "./docs/A-Ops/社区热补丁制作发布流程.md" >}})
    - [内核热升级指南]({{< relref "./docs/KernelLiveUpgrade/KernelLiveUpgrade.md" >}})
        - [安装与部署]({{< relref "./docs/KernelLiveUpgrade/安装与部署.md" >}})
        - [使用方法]({{< relref "./docs/KernelLiveUpgrade/使用方法.md" >}})
        - [常见问题与解决方法]({{< relref "./docs/KernelLiveUpgrade/常见问题与解决方法.md" >}})
    - [HA 用户指南]({{< relref "./docs/thirdparty_migration/ha.md" >}})
        - [部署 HA]({{< relref "./docs/thirdparty_migration/HA的安装与部署.md" >}})
        - [HA 使用实例]({{< relref "./docs/thirdparty_migration/HA的使用实例.md" >}})
- [安全](#)
    - [安全加固指南]({{< relref "./docs/SecHarden/secHarden.md" >}})
        - [操作系统加固概述]({{< relref "./docs/SecHarden/操作系统加固概述.md" >}})
        - [安全配置规范基线]({{< relref "./docs/SecHarden/安全配置规范基线.md" >}})
        - [加固指导]({{< relref "./docs/SecHarden/加固指导.md" >}})
            - [帐户口令]({{< relref "./docs/SecHarden/帐户口令.md" >}})
            - [授权认证]({{< relref "./docs/SecHarden/授权认证.md" >}})
            - [系统服务]({{< relref "./docs/SecHarden/系统服务.md" >}})
            - [文件权限]({{< relref "./docs/SecHarden/文件权限.md" >}})
            - [内核参数]({{< relref "./docs/SecHarden/内核参数.md" >}})
            - [SELinux配置]({{< relref "./docs/SecHarden/SELinux配置.md" >}})
        - [安全加固工具]({{< relref "./docs/SecHarden/安全加固工具.md" >}})
        - [附录]({{< relref "./docs/SecHarden/附录.md" >}})
    - [secGear开发指南]({{< relref "./docs/secGear/secGear.md" >}})
        - [认识secGear]({{< relref "./docs/secGear/认识secGear.md" >}})
        - [安装与部署]({{< relref "./docs/secGear/安装与部署.md" >}})
        - [使用方法]({{< relref "./docs/secGear/使用方法.md" >}})
- [性能](#)
    - [A-Tune用户指南]({{< relref "./docs/A-Tune/A-Tune.md" >}})
        - [认识A-Tune]({{< relref "./docs/A-Tune/认识A-Tune.md" >}})
        - [安装与部署]({{< relref "./docs/A-Tune/安装与部署.md" >}})
        - [使用方法]({{< relref "./docs/A-Tune/使用方法.md" >}})
        - [native-turbo特性]({{< relref "./docs/A-Tune/native-turbo.md" >}})
        - [常见问题与解决方法]({{< relref "./docs/A-Tune/常见问题与解决方法.md" >}})
        - [附录]({{< relref "./docs/A-Tune/附录.md" >}})
- [桌面](#)
    - [UKUI]({{< relref "./docs/desktop/ukui.md" >}})
        - [安装 UKUI]({{< relref "./docs/desktop/安装UKUI.md" >}})
        - [UKUI 用户指南]({{< relref "./docs/desktop/UKUIuserguide.md" >}})
    - [DDE]({{< relref "./docs/desktop/dde.md" >}})
        - [安装 DDE]({{< relref "./docs/desktop/安装DDE.md" >}})
        - [DDE 用户指南]({{< relref "./docs/desktop/DDE-User-Manual.md" >}})
    - [XFCE]({{< relref "./docs/desktop/xfce.md" >}})
        - [安装 Xfce]({{< relref "./docs/desktop/Install_XFCE.md" >}})
        - [Xfce 用户指南]({{< relref "./docs/desktop/Xfce_userguide.md" >}})
    - [Gnome]({{< relref "./docs/desktop/gnome.md" >}})
        - [安装 GNOME]({{< relref "./docs/desktop/Install_GNOME.md" >}})
        - [Gnome 用户指南]({{< relref "./docs/desktop/Gnome_userguide.md" >}})
- [嵌入式](#)
    - [openEuler Embedded用户指南](https://openeuler.gitee.io/yocto-meta-openeuler/master/index.html)
    - [UniProton用户指南]({{< relref "./docs/Embedded/UniProton/UniProton用户指南-概述.md" >}})
        - [UniProton功能设计]({{< relref "./docs/Embedded/UniProton/UniProton功能设计.md" >}})
        - [UniProton接口说明]({{< relref "./docs/Embedded/UniProton/UniProton接口说明.md" >}})
- [虚拟化](#)
    - [虚拟化用户指南]({{< relref "./docs/Virtualization/virtualization.md" >}})
        - [认识虚拟化]({{< relref "./docs/Virtualization/认识虚拟化.md" >}})
        - [安装虚拟化组件]({{< relref "./docs/Virtualization/安装虚拟化组件.md" >}})
        - [准备使用环境]({{< relref "./docs/Virtualization/准备使用环境.md" >}})
        - [虚拟机配置]({{< relref "./docs/Virtualization/虚拟机配置.md" >}})
        - [管理虚拟机]({{< relref "./docs/Virtualization/管理虚拟机.md" >}})
        - [热迁移虚拟机]({{< relref "./docs/Virtualization/热迁移虚拟机.md" >}})
        - [管理系统资源]({{< relref "./docs/Virtualization/管理系统资源.md" >}})
        - [管理设备]({{< relref "./docs/Virtualization/管理设备.md" >}})
        - [管理虚拟机可维护性]({{< relref "./docs/Virtualization/管理虚拟机可维护性.md" >}})
        - [最佳实践]({{< relref "./docs/Virtualization/最佳实践.md" >}})
        - [工具使用指南]({{< relref "./docs/Virtualization/工具使用指南.md" >}})
            - [vmtop]({{< relref "./docs/Virtualization/vmtop.md" >}})
            - [LibcarePlus]({{< relref "./docs/Virtualization/LibcarePlus.md" >}})
        - [Skylark虚拟机混部]({{< relref "./docs/Virtualization/Skylark.md" >}})
        - [附录]({{< relref "./docs/Virtualization/附录.md" >}})
    - [StratoVirt用户指南]({{< relref "./docs/StratoVirt/StratoVirtGuide.md" >}})
        - [StratoVirt介绍]({{< relref "./docs/StratoVirt/StratoVirt介绍.md" >}})
        - [安装StratoVirt]({{< relref "./docs/StratoVirt/安装StratoVirt.md" >}})
        - [准备使用环境]({{< relref "./docs/StratoVirt/准备使用环境.md" >}})
        - [虚拟机配置]({{< relref "./docs/StratoVirt/虚拟机配置.md" >}})
        - [虚拟机管理]({{< relref "./docs/StratoVirt/虚拟机管理.md" >}})
        - [对接iSula安全容器]({{< relref "./docs/StratoVirt/对接iSula安全容器.md" >}})
        - [对接libvirt]({{< relref "./docs/StratoVirt/对接libvirt.md" >}})
        - [StratoVirt VFIO 使用说明]({{< relref "./docs/StratoVirt/StratoVirt-VFIO使用说明.md" >}})
    - [libvirt直连聚合用户指南]({{< relref "./docs/DPUOffload/libvirt直连聚合环境搭建.md" >}})
        - [qtfs共享文件系统架构及使用手册]({{< relref "./docs/DPUOffload/qtfs共享文件系统架构及使用手册.md" >}})
        - [容器管理面DPU无感卸载指南]({{< relref "./docs/DPUOffload/overview.md" >}})
            - [容器管理面无感卸载]({{< relref "./docs/DPUOffload/容器管理面无感卸载.md" >}})
            - [容器管理面无感卸载部署指导]({{< relref "./docs/DPUOffload/无感卸载部署指导.md" >}})
    - [OpenStack]({{< relref "./docs/thirdparty_migration/openstack.md" >}})
- [云原生](#)
    - [容器用户指南]({{< relref "./docs/Container/container.md" >}})
        - [iSula容器引擎]({{< relref "./docs/Container/iSula容器引擎.md" >}})
            - [安装、升级与卸载]({{< relref "./docs/Container/安装-升级与卸载.md" >}})
                - [安装与配置]({{< relref "./docs/Container/安装与配置.md" >}})
                - [升级]({{< relref "./docs/Container/升级.md" >}})
                - [卸载]({{< relref "./docs/Container/卸载.md" >}})
            - [使用指南]({{< relref "./docs/Container/使用指南.md" >}})
                - [容器管理]({{< relref "./docs/Container/容器管理.md" >}})
                - [支持CNI网络]({{< relref "./docs/Container/支持CNI网络.md" >}})
                - [容器资源管理]({{< relref "./docs/Container/容器资源管理.md" >}})
                - [特权容器]({{< relref "./docs/Container/特权容器.md" >}})
                - [CRI接口]({{< relref "./docs/Container/CRI接口.md" >}})
                - [镜像管理]({{< relref "./docs/Container/镜像管理.md" >}})
                - [容器健康状态检查]({{< relref "./docs/Container/容器健康状态检查.md" >}})
                - [查询信息]({{< relref "./docs/Container/查询信息.md" >}})
                - [安全特性]({{< relref "./docs/Container/安全特性.md" >}})
                - [支持OCI hooks]({{< relref "./docs/Container/支持OCI-hooks.md" >}})
                - [本地卷管理]({{< relref "./docs/Container/本地卷管理.md" >}})
                - [iSulad shim v2 对接 StratoVirt]({{< relref "./docs/Container/iSula-shim-v2对接stratovirt.md" >}})
            - [附录]({{< relref "./docs/Container/附录.md" >}})
        - [系统容器]({{< relref "./docs/Container/系统容器.md" >}})
            - [安装指导]({{< relref "./docs/Container/安装指导.md" >}})
            - [使用指南]({{< relref "./docs/Container/使用指南-1.md" >}})
                - [指定rootfs创建容器]({{< relref "./docs/Container/指定rootfs创建容器.md" >}})
                - [通过systemd启动容器]({{< relref "./docs/Container/通过systemd启动容器.md" >}})
                - [容器内reboot/shutdown]({{< relref "./docs/Container/容器内reboot-shutdown.md" >}})
                - [cgroup路径可配置]({{< relref "./docs/Container/cgroup路径可配置.md" >}})
                - [namespace化内核参数可写]({{< relref "./docs/Container/namespace化内核参数可写.md" >}})
                - [共享内存通道]({{< relref "./docs/Container/共享内存通道.md" >}})
                - [动态加载内核模块]({{< relref "./docs/Container/动态加载内核模块.md" >}})
                - [环境变量持久化]({{< relref "./docs/Container/环境变量持久化.md" >}})
                - [最大句柄数限制]({{< relref "./docs/Container/最大句柄数限制.md" >}})
                - [安全性和隔离性]({{< relref "./docs/Container/安全性和隔离性.md" >}})
                - [容器资源动态管理]({{< relref "./docs/Container/容器资源动态管理.md" >}})
            - [附录]({{< relref "./docs/Container/附录-2.md" >}})
        - [安全容器]({{< relref "./docs/Container/安全容器.md" >}})
            - [安装与配置]({{< relref "./docs/Container/安装与配置-2.md" >}})
            - [使用方法]({{< relref "./docs/Container/使用方法-1.md" >}})
                - [管理安全容器的生命周期]({{< relref "./docs/Container/管理安全容器的生命周期.md" >}})
                - [为安全容器配置资源]({{< relref "./docs/Container/为安全容器配置资源.md" >}})
                - [监控安全容器]({{< relref "./docs/Container/监控安全容器.md" >}})
            - [附录]({{< relref "./docs/Container/附录-3.md" >}})
        - [Docker容器]({{< relref "./docs/Container/Docker容器.md" >}})
            - [安装配置]({{< relref "./docs/Container/安装配置-3.md" >}})
            - [容器管理]({{< relref "./docs/Container/容器管理-3.md" >}})
            - [镜像管理]({{< relref "./docs/Container/镜像管理-3.md" >}})
            - [命令行参考]({{< relref "./docs/Container/命令行参考.md" >}})
                - [容器引擎]({{< relref "./docs/Container/容器引擎-4.md" >}})
                - [容器管理]({{< relref "./docs/Container/容器管理-4.md" >}})
                - [镜像管理]({{< relref "./docs/Container/镜像管理-4.md" >}})
                - [统计信息]({{< relref "./docs/Container/统计信息-4.md" >}})
        - [容器镜像构建]({{< relref "./docs/Container/isula-build构建工具.md" >}})
    - [容器OS升级用户指南]({{< relref "./docs/KubeOS/overview.md" >}})
        - [认识容器OS升级]({{< relref "./docs/KubeOS/认识容器OS升级.md" >}})
        - [安装与部署]({{< relref "./docs/KubeOS/安装与部署.md" >}})
        - [使用方法]({{< relref "./docs/KubeOS/使用方法.md" >}})
        - [容器OS镜像制作指导]({{< relref "./docs/KubeOS/容器OS镜像制作指导.md" >}})
    - [Kubernetes集群部署指南]({{< relref "./docs/Kubernetes/Kubernetes.md" >}})
        - [准备虚拟机]({{< relref "./docs/Kubernetes/准备虚拟机.md" >}})
        - [手动部署集群]({{< relref "./docs/Kubernetes/手动部署集群.md" >}})
            - [安装Kubernetes软件包]({{< relref "./docs/Kubernetes/安装Kubernetes软件包.md" >}})
            - [准备证书]({{< relref "./docs/Kubernetes/准备证书.md" >}})
            - [安装etcd]({{< relref "./docs/Kubernetes/安装etcd.md" >}})
            - [部署控制面组件]({{< relref "./docs/Kubernetes/部署控制面组件.md" >}})
            - [部署Node节点组件]({{< relref "./docs/Kubernetes/部署Node节点组件.md" >}})
        - [自动部署集群]({{< relref "./docs/Kubernetes/eggo自动化部署.md" >}})
            - [工具介绍]({{< relref "./docs/Kubernetes/eggo工具介绍.md" >}})
            - [部署集群]({{< relref "./docs/Kubernetes/eggo部署集群.md" >}})
            - [拆除集群]({{< relref "./docs/Kubernetes/eggo拆除集群.md" >}})
        - [运行测试pod]({{< relref "./docs/Kubernetes/运行测试pod.md" >}})
    - [云原生混合部署rubik用户指南]({{< relref "./docs/rubik/overview.md" >}})
        - [安装与部署]({{< relref "./docs/rubik/安装与部署.md" >}})
        - [http接口文档]({{< relref "./docs/rubik/http接口文档.md" >}})
        - [混部隔离示例]({{< relref "./docs/rubik/混部隔离示例.md" >}})
    - [NestOS用户指南]({{< relref "./docs/NestOS/overview.md" >}})
        - [安装与部署]({{< relref "./docs/NestOS/安装与部署.md" >}})
        - [使用方法]({{< relref "./docs/NestOS/使用方法.md" >}})
        - [功能特性描述]({{< relref "./docs/NestOS/功能特性描述.md" >}})
- [边缘计算](#)
    - [KubeEdge部署指南]({{< relref "./docs/KubeEdge/overview.md" >}})
        - [KubeEdge使用文档]({{< relref "./docs/KubeEdge/KubeEdge使用文档.md" >}})
        - [KubeEdge部署指南]({{< relref "./docs/KubeEdge/KubeEdge部署指南.md" >}})
    - [K3s部署指南]({{< relref "./docs/K3s/K3s部署指南.md" >}})
    - [ROS用户指南]({{< relref "./docs/ROS/ROS用户指南.md" >}})
        - [认识ROS]({{< relref "./docs/ROS/认识ROS.md" >}})
        - [安装与部署]({{< relref "./docs/ROS/安装与部署.md" >}})
        - [使用方法]({{< relref "./docs/ROS/使用方法.md" >}})
        - [常见问题与解决办法]({{< relref "./docs/ROS/常见问题与解决方法.md" >}})
        - [附录]({{< relref "./docs/ROS/附录.md" >}})
- [openEuler DevKit](#)
    - [isocut 使用指南]({{< relref "./docs/TailorCustom/isocut使用指南.md" >}})
    - [imageTailor 使用指南]({{< relref "./docs/TailorCustom/imageTailor 使用指南.md" >}})
    - [GCC插件框架特性用户指南]({{< relref "./docs/Pin/插件框架特性用户指南.md" >}})
- [openEuler DevOps](#)
    - [patch-tracking]({{< relref "./docs/userguide/patch-tracking.md" >}})
    - [pkgship]({{< relref "./docs/userguide/pkgship.md" >}})
- [应用开发](#)
    - [应用开发指南]({{< relref "./docs/ApplicationDev/application-development.md" >}})
        - [开发环境准备]({{< relref "./docs/ApplicationDev/开发环境准备.md" >}})
        - [使用GCC编译]({{< relref "./docs/ApplicationDev/使用GCC编译.md" >}})
        - [使用Clang编译]({{< relref "./docs/ApplicationDev/使用Clang编译.md" >}})
        - [使用make编译]({{< relref "./docs/ApplicationDev/使用make编译.md" >}})
        - [使用JDK编译]({{< relref "./docs/ApplicationDev/使用JDK编译.md" >}})
        - [构建RPM包]({{< relref "./docs/ApplicationDev/构建RPM包.md" >}})
        - [FAQ]({{< relref "./docs/ApplicationDev/FAQ.md" >}})

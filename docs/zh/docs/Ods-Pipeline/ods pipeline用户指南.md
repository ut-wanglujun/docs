# ods pipeline用户指南

## 前言

### 概述

本文档介绍了ods的pipeline,workflow定义，编写规则，功能说明，使用的方法，以及日志结果查询。

### 使用对象

- 社区开发者
- 版本Maitainer
- 性能优化工程师

### 术语说明

|   名称   |                             说明                             |
| :------: | :----------------------------------------------------------: |
|  DevOps  | 是指在一组开发者服务的集合，开发者可以直接使用服务，也可以对服务进行编排。 |
| pipeline |    对于编排好的一系列任务，可被系统执行的语法yaml 文件。     |
| workflow | 为完成一组功能的工作流(如：构建为repo、生成ISO等)，是一个编排好的jobs，可以是被pipeline 引用。 |
|   job    |           在某个执行机器上具体指定的一组执行接口。           |

### 修订记录

| 文档版本 | 发布日期   | 修改说明 |
| -------- | ---------- | -------- |
| 01       | 2023-06-28 |          |

## 工具概述

ods是基于pipeline执行流水线测试任务并输出结果的工具，支持配置化管理pipeline以及workflow，灵活组合workflow，降低人工参与成本。解决任务流中繁琐人工操作，效率低的问题。

ods主要提供以下功能:

- pipeline与workflow管理

对用户上传的pipeline.yaml或者workflow.yaml进行管理,并能生成pipeline的快照。

- 运行pipeline

用户可运行指定的pipeline,并生成日志。

### 应用场景

- 操作系统串行测试 ：构建任务完成后 ，测试在某台机器上完成

```
-提交构建repo任务
-生成镜像(基于多个repo，生成ISO、image)
-iso做成compass-ci做成compass-ci 支持的rootfs
-提交串行测试任务
-汇总测试结果
```

- 操作系统并行测试 ：构建任务完成后 ，测试在不同的台机器上完成

```
-构建repo
-生成镜像(基于多个repo，生成ISO、image)
-iso做成compass-ci做成compass-ci 支持的rootfs
-执行并行测试任务
-汇总测试结果
```

- 操作系统包有更新 ：操作系统本身不变，基于repo 更新rpm，开展测试

```
-构建rpm repo
-加载对应系统的rootfs
-执行测试任务
-汇总测试结果
```

- 场景测试(mysql, ceph, spark   => 构建+部署+冒烟测试)


```
- 构建软件包
- 加载对应系统的rootfs
- 执行测试任务
- 输出测试报告
```

### 使用介绍

本示例中，下列字段均需根据实际部署情况自行配置。部分字段上文提供了生成方法：

${HOST_IP}：ods服务宿主机IP

${ODS_MANAGER_PORT}：ods管理服务对应的端口

${ODS_RUNNER_PORT}：ods任务运行服务对应的端口

${PIPELINE_NAME}: pipeline的名称

${WORKFLOW_NAME}: workflow的名称

${SUBMIT_USER}: 提交的task的用户名

步骤1:编写并提交workflow.yaml
以下图为例:

![build_stage](./image/build_stage.png)

![pre_deploy_stage](./image/pre_deploy_stage.png)

![test_stage](./image/test_stage.png)

分别编写了名为build_stage，pre_test_stage，test_stage的workflow。填写内容不限于图中所示，具体与compass-ci处理job参数保持一致。

调用接口分别提交:

![post_workflow](./image/post_workflow.png)



步骤2:编写并提交pipeline.yaml

以下图为例:

![pipeline](./image/pipeline.png)

编写了名为test的pipeline。填写内容不限于图中所示。

调用接口提交:

![post_pipeline](./image/post_pipeline.png)



说明：pipeline提交后，会根据其下包含的workflow名称去寻找对应workflow信息，进行扩展，生成snapshot，snapshot仅可通过更新pipeline的方式更新，解析job的来源是snapshot。

snapshot示例：

![snapshot](./image/snapshot.png)



步骤3:提交执行pipeline

调用接口执行：

![post_submit](./image/post_submit.png)

说明:

1.对于pipeline下的matrix入参，会做笛卡尔积组合，生成多个task

2.对于单个job的依赖项，其值为workflow.need + job.depends + job.input去重后的合集，若对应的依赖找不到，跳过该依赖，以上图为例，最终job依赖项见下图:

![yaml2jobs](./image/yaml2jobs.png)

3.单个job是否提交，会查看其依赖项是否全部满足，否则不会提交执行。



步骤4:查看执行结果

单个job的执行结果在${HOST_IP}的/result目录下，查找对应的测试套名称，具体日志看其下文件。


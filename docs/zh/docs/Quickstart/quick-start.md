# 快速入门

本文档以TaiShan 200服务器上安装openEuler 21.09 为例，旨在指导用户快速地安装和使用openEuler操作系统，更详细的安装要求和安装方法请参考《[安装指南](./../Installation/installation.html)》。

## 安装要求

- 硬件兼容支持

    支持的服务器类型如[表1](#table14948632047)所示。

    **表 1**  支持的服务器类型

    <a name="table14948632047"></a>
    <table><thead align="left"><tr id="zh-cn_topic_0229291190_row5949431547"><th class="cellrowborder" valign="top" width="26.369999999999997%" id="mcps1.2.4.1.1"><p id="zh-cn_topic_0229291190_p694923843"><a name="zh-cn_topic_0229291190_p694923843"></a><a name="zh-cn_topic_0229291190_p694923843"></a><strong id="zh-cn_topic_0229291190_b1990114912213"><a name="zh-cn_topic_0229291190_b1990114912213"></a><a name="zh-cn_topic_0229291190_b1990114912213"></a>服务器形态</strong></p>
    </th>
    <th class="cellrowborder" valign="top" width="24%" id="mcps1.2.4.1.2"><p id="zh-cn_topic_0229291190_p132705020556"><a name="zh-cn_topic_0229291190_p132705020556"></a><a name="zh-cn_topic_0229291190_p132705020556"></a><strong id="zh-cn_topic_0229291190_b18630171675"><a name="zh-cn_topic_0229291190_b18630171675"></a><a name="zh-cn_topic_0229291190_b18630171675"></a>服务器名称</strong></p>
    </th>
    <th class="cellrowborder" valign="top" width="49.63%" id="mcps1.2.4.1.3"><p id="zh-cn_topic_0229291190_p119491131848"><a name="zh-cn_topic_0229291190_p119491131848"></a><a name="zh-cn_topic_0229291190_p119491131848"></a><strong id="zh-cn_topic_0229291190_b209018491722"><a name="zh-cn_topic_0229291190_b209018491722"></a><a name="zh-cn_topic_0229291190_b209018491722"></a>服务器型号</strong></p>
    </th>
    </tr>
    </thead>
    <tbody><tr id="zh-cn_topic_0229291190_row8949153348"><td class="cellrowborder" valign="top" width="26.369999999999997%" headers="mcps1.2.4.1.1 "><p id="zh-cn_topic_0229291190_p4893192424117"><a name="zh-cn_topic_0229291190_p4893192424117"></a><a name="zh-cn_topic_0229291190_p4893192424117"></a>机架服务器</p>
    </td>
    <td class="cellrowborder" valign="top" width="24%" headers="mcps1.2.4.1.2 "><p id="zh-cn_topic_0229291190_p02706012553"><a name="zh-cn_topic_0229291190_p02706012553"></a><a name="zh-cn_topic_0229291190_p02706012553"></a>TaiShan 200</p>
    </td>
    <td class="cellrowborder" valign="top" width="49.63%" headers="mcps1.2.4.1.3 "><p id="zh-cn_topic_0229291190_p126551941225"><a name="zh-cn_topic_0229291190_p126551941225"></a><a name="zh-cn_topic_0229291190_p126551941225"></a>2280均衡型</p>
    </td>
    </tr>
    <tr id="zh-cn_topic_0229291190_row104064391909"><td class="cellrowborder" valign="top" width="26.369999999999997%" headers="mcps1.2.4.1.1 "><p id="zh-cn_topic_0229291190_p54061539609"><a name="zh-cn_topic_0229291190_p54061539609"></a><a name="zh-cn_topic_0229291190_p54061539609"></a>机架服务器</p>
    </td>
    <td class="cellrowborder" valign="top" width="24%" headers="mcps1.2.4.1.2 "><p id="zh-cn_topic_0229291190_p9937105713311"><a name="zh-cn_topic_0229291190_p9937105713311"></a><a name="zh-cn_topic_0229291190_p9937105713311"></a>FusionServer Pro 机架服务器</p>
    </td>
    <td class="cellrowborder" valign="top" width="49.63%" headers="mcps1.2.4.1.3 "><p id="zh-cn_topic_0229291190_p154063394011"><a name="zh-cn_topic_0229291190_p154063394011"></a><a name="zh-cn_topic_0229291190_p154063394011"></a>FusionServer Pro 2288H V5</p>
    <div class="note" id="zh-cn_topic_0229291190_note2046771016316"><a name="zh-cn_topic_0229291190_note2046771016316"></a><a name="zh-cn_topic_0229291190_note2046771016316"></a><span class="notetitle"> 说明： </span><div class="notebody"><p id="zh-cn_topic_0229291190_p1646710101130"><a name="zh-cn_topic_0229291190_p1646710101130"></a><a name="zh-cn_topic_0229291190_p1646710101130"></a>服务器要求配置Avago 3508 RAID控制卡和启用LOM-X722网卡。</p>
    </div></div>
    </td>
    </tr>
    </tbody>
    </table>

- 最小硬件要求

    最小硬件要求如[表2](#tff48b99c9bf24b84bb602c53229e2541)所示。

    **表 2**  最小硬件要求
    <a name="tff48b99c9bf24b84bb602c53229e2541"></a>
    <table>
        <thead align="left">
            <tr>
                <th width="10%"><p><strong>部件名称</strong></p></th>
                <th width="40%"><p><strong>最小硬件要求</strong></p></th>
                <th width="50%"><p><strong>说明</strong></p></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td width="10%"><p>架构</p></td>
                <td width="40%"><ul><li>AArch64</li><li>x86_64</li></ul></td>
                <td width="50%"><ul><li>支持Arm的64位架构。</li><li>支持Intel的x86 64位架构。</li></ul></td>
            </tr>
            <tr>
                <td width="10%"><p>CPU</p></td>
                <td width="40%"><ul><li>华为鲲鹏920系列CPU</li><li>Intel<sup>&reg;</sup> Xeon<sup>&reg;</sup>处理器</li></ul></td>
                <td width="50%"><p>-</p></td>
            </tr>
            <tr>
                <td width="10%"><p>内存</p></td>
                <td width="40%"><p>不小于4GB（为了获得更好的应用体验，建议不小于8GB）</p></td>
                <td width="50%"><p>-</p></td>
            </tr>
            <tr>
                <td width="10%"><p>硬盘</p></td>
                <td width="40%"><p>为了获得更好的应用体验，建议不小于120GB</p></td>
                <td width="50%"><p><ul><li>支持IDE、SATA、SAS等接口的硬盘。</li><li>用DIF功能的NVME盘，需要对应驱动支持，如果无法使用，请联系硬件厂商。</li></ul></p></td>
            </tr>
        </tbody>
    </table>

## 获取安装源

请按以下步骤获取openEuler的发布包和校验文件：

1. 登录[openEuler社区](https://openeuler.org)网站。
2. 单击“下载”。
3. 单击“社区发行版”，显示版本列表。
4. 在版本列表的“openEuler 22.03 LTS SP2”版本处单击“前往下载”按钮，进入openEuler 22.03_LTS_SP2版本下载列表。
5. 根据实际待安装环境的架构和场景选择需要下载的 openEuler 的发布包和校验文件。
    1. 若为AArch64架构。
        1. 单击“AArch64”。
        2. 若选择本地安装，选择“Offline Standard ISO”或者“Offline Everything ISO”对应的“立即下载”将发布包 “openEuler-22.03-LTS-SP2-aarch64-dvd.iso”下载到本地。
        3. 若选择网络安装，选择“Network Install ISO”将发布包 “openEuler-22.03-LTS-SP2-netinst-aarch64-dvd.iso”下载到本地。
    2. 若为x86_64架构。
        1. 单击“x86_64”。
        2. 若选择本地安装，选择“Offline Standard ISO”或者“Offline Everything ISO”对应的“立即下载”将发布包 “openEuler-22.03-LTS-SP2-x86_64-dvd.iso”下载到本地。
        3. 若选择网络安装，选择“Network Install ISO”将发布包 “openEuler-22.03-LTS-SP2-netinst-x86_64-dvd.iso ”下载到本地。

> ![](./public_sys-resources/icon-note.gif) **说明：**
>
> - 网络安装方式的 ISO 发布包较小，在有网络的安装环境可以选择网络安装方式。
> - AArch64架构的发布包支持UEFI模式，x86\_64架构的发布包支持UEFI模式和Legacy模式。
>

## 发布包完整性校验

> ![](./public_sys-resources/icon-note.gif) **说明：**
> 本章节以AArch64架构的发布包完整性校验为例，x86\_64架构的发布包完整性校验的操作方法相同。

### 简介

为了确认软件包在传输过程中由于网络原因或者存储设备原因是否出现下载不完整的问题，在获取到软件包后，需要对软件包的完整性进行校验，通过了校验的软件包才能部署。

这里通过对比校验文件中记录的校验值和手动方式计算的iso文件校验值，判断软件包是否完整。若两个值相同，说明iso文件完整，否则，iso完整性被破坏，请重新获取iso发布包。

### 前提条件

在校验发布包完整性之前，需要准备如下文件：

iso文件：openEuler-22.03-LTS-SP2-aarch64-dvd.iso

校验文件：ISO对应完整性校验值，复制保存对应的ISO值

### 操作指导

文件完整性校验操作步骤如下：

1. 计算文件的sha256校验值。执行命令如下：

    ```sh
    # sha256sum openEuler-22.03-LTS-SP2-aarch64-dvd.iso
    ```

    命令执行完成后，输出校验值。

2. 对比步骤1计算的校验值与对刚刚复制的SHA256的值是否一致。

    如果校验值一致说明iso文件完整性没有破坏，如果校验值不一致则可以确认文件完整性已被破坏，需要重新获取。

## 启动安装

1. 登录服务器iBMC Web界面。具体方法请参考《[TaiShan 200 服务器 用户指南 (型号 2280)](https://support.huawei.com/enterprise/zh/doc/EDOC1100088652)》。
2. 在上方标题栏中，选择“配置”，在左侧导航树中选择“系统启动项”，显示“系统启动项”界面。

    将“优先引导介质”设置为“光驱”，选择“单次有效”，并单击“保存”以保存配置。如[图1](#fig1011938131018)所示。

    **图 1**  设置系统启动项<a name="fig1011938131018"></a>  
    ![](./figures/Setting_the_System_Boot_Option.png)

3. 在上方标题栏中，选择“远程控制”，在左侧导航树中选择“远程控制”，显示“远程控制”界面。

    根据实际情况选择一个集成远程控制台以进入远程虚拟控制台，如选择“Java集成远程控制台\(共享\)”。

4. 在虚拟界面工具栏中，单击虚拟光驱工具如下图所示。

    **图 2**  光驱图标<a name="zh-cn_topic_0229291223_zh-cn_topic_0151920806_f6ff7658b349942ea87f4521c0256c32e"></a>  
    ![](./figures/CD-ROM_drive_icon.png)

    弹出镜像对话框，如下图所示。

    **图 3**  镜像对话框<a name="zh-cn_topic_0229291223_zh-cn_topic_0151920806_fb74fb37f86cd423aacf34bddedd6841a"></a>  
    ![](./figures/Image_dialog_box.png)

5. 在镜像对话框中，选择“镜像文件”， 并单击“浏览”。弹出“打开”对话框。
6. 选择镜像文件，单击“打开”。然后在镜像对话框中，单击“连接”。当“连接”显示为“断开”后，表示虚拟光驱已连接到服务器。
7. 在工具栏中，单击重启工具重启设备，如下图所示。

    **图 4**  重启图标<a name="zh-cn_topic_0229291223_zh-cn_topic_0151920806_f0d1f4f5f96de47b48c64b3535b2b60d1"></a>  
    ![](./figures/restarticon.png)

8. 设备重启后进入到openEuler操作系统安装引导界面，如[图5](#fig1648754873314)所示。

    > ![](./public_sys-resources/icon-note.gif) **说明：**
    >
    > - 如果60秒内未按任何键，系统将从默认选项“Test this media & install openEuler 21.09”自动进入安装界面。
    > - 安装物理机时，如果使用键盘上下键无法选择启动选项，按“Enter”键无响应，可以单击BMC界面上的鼠标控制图标“![](./figures/zh-cn_image_0229420473.png)”，设置“键鼠复位”。
    >

    **图 5**  安装引导界面<a name="fig1648754873314"></a>  
    ![](./figures/Installation_wizard.png)

9. 在安装引导界面，按“Enter”，进入默认选项“Test this media & install openEuler 21.09”的图形化安装界面。

## 安装

进入图形化安装界面后，按如下步骤进行安装。

1. 设置安装语言，默认为英语，用户可根据实际情况进行调整，如[图6](#fig874344811484)所示，选择“中文”。

    **图 6**  选择语言<a name="fig874344811484"></a>  
    ![](./figures/selectlanguage.png)

2. 在安装概览界面，根据实际情况设置各配置项。

    > ![](./public_sys-resources/icon-note.gif) **说明：**
    >
    > - 配置项有告警符号的，表示用户必须完成该选项配置后，告警符号消失，才能进行下一步操作。
    > - 配置项无告警符号的，表示该配置项已有默认配置。
    > - 所有配置项均无告警符号时用户才能单击“开始安装”进行系统安装。
    >

    **图 7**  安装概览<a name="fig24261457656"></a>  
    ![](./figures/Installation_Overview.png)

    1. 选择“软件选择”，设置“软件选择”配置项。

        用户需要根据实际的业务需求，在左侧选择一个“最小安装”，在右侧选择安装环境的附加选项，如[图8](#fig1133717611109)所示。

        **图 8**  软件选择<a name="fig1133717611109"></a>  
        ![](./figures/choosesoftware.png)

        > ![](./public_sys-resources/icon-note.gif) **说明：**
        >
        > - 在最小安装的环境下，并非安装源中所有的包都会安装。如果用户需要使用的包未安装，可将安装源挂载到本地制作repo源，通过DNF工具单独安装。
        > - 选择“虚拟化主机”时会默认安装虚拟化组件qemu、libvirt、edk2，且可在附加选项处选择是否安装ovs等组件。
        >

        设置完成后，请单击左上角“完成”返回“安装概览”页面。

    2. 选择“安装目的地”，设置“安装目的地”配置项。

        在安装位置页面中，您可以选择计算机中的本地可用存储设备。

        > ![](./public_sys-resources/icon-notice.gif) **须知：**
        >
        > - 由于很多服务器BIOS内置NVMe驱动程序版本较低，不支持NVMe的数据保护特性（数据保护：将磁盘扇区格式化为512+N或4096+N字节）。所以，在选择合适的存储介质时，建议不要选择开启数据保护特性的NVMe SSD存储介质作为系统盘，否则可能出现操作系统无法引导等问题。
        > - 用户可以选择优先咨询服务器厂商关于BIOS是否支持开启数据保护特性的NVMe磁盘作为系统盘。如果您无法确认BIOS是否支持，则不推荐使用NVMe安装操作系统，或者选择关闭NVMe盘的数据保护功能实现操作系统安装。
        >

        您还需要进行存储配置以便对系统分区。您可以手动配置分区，也可以选择让安装程序自动分区。如果是在未使用过的存储设备中执行全新安装，或者不需要保留该存储设备中任何数据，建议选择“自动”进行自动分区。如[图9](#fig153381468101)所示。

        **图 9**  安装目标位置<a name="fig153381468101"></a>  
        ![](./figures/Target_installation_position.png)

        > ![](./public_sys-resources/icon-note.gif) **说明：**
        >
        > - 在进行分区时，出于系统性能和安全的考虑，建议您划分如下单独分区：/boot、/var、/var/log 、/var/log/audit、/home、/tmp。
        > - 系统如果配置了swap分区，当系统的物理内存不够用时，会使用swap分区。虽然 swap分区可以增大物理内存大小的限制，但是如果由于内存不足使用到swap分区，会增加系统的响应时间，性能变差。因此在物理内存充足或者性能敏感的系统中，不建议配置swap分区。
        > - 如果需要拆分逻辑卷组则需要选择“自定义”进行手动分区，并在“手动分区”界面单击“卷组”区域中的“修改”按钮重新配置卷组。
        >

        设置完成后，请单击左上角“完成”返回“安装概览”页面。

    3. 选择“根密码”，设置“根密码”配置项。

        在如图10所示的“ROOT密码”页面中，根据密码复杂度输入密码并再次输入密码进行确认。

        > ![](./public_sys-resources/icon-note.gif) **说明：**
        >
        > - root帐户是用来执行关键系统管理任务，不建议您在日常工作及系统访问时使用root帐户。
        > - 在“ROOT密码”界面若选择“锁定root帐户”则root帐户将禁用。
        >

        **密码复杂度** <a name = "密码复杂度"></a>

        用户设置的root用户密码或新创建用户的密码均需要满足密码复杂度要求，否则会导致密码设置或用户创建失败。设置密码的复杂度的要求如下：

        1. 口令长度至少8个字符。
        2. 口令至少包含大写字母、小写字母、数字和特殊字符中的任意3种。
        3. 口令不能和帐号一样。
        4. 口令不能使用字典词汇。

            > ![](./public_sys-resources/icon-note.gif) **说明：**
            > 在已装好的openEuler环境中，可以通过`cracklib-unpacker /usr/share/cracklib/pw_dict > dictionary.txt`命令导出字典库文件dictionary.txt，用户可以查询密码是否在该字典中。

        **图 10**  root密码<a name="zh-cn_topic_0186390266_zh-cn_topic_0122145909_fig1323165793018"></a>  
        ![](./figures/root_password.png)

        设置完成后，单击左上角的“完成”返回“安装概览”页面。

    4. 选择“创建用户”，设置“创建用户”配置项。

        在创建用户的界面如[图11](#zh-cn_topic_0186390266_zh-cn_topic_0122145909_fig1237715313319)所示。输入用户名，并设置密码，其中密码复杂度要求与root密码复杂度要求一致。另外您还可以通过“高级”选项设置用户主目录、用户组等，如[图12](#zh-cn_topic_0186390266_zh-cn_topic_0122145909_fig128716531312)所示。

        **图 11**  创建用户<a name="zh-cn_topic_0186390266_zh-cn_topic_0122145909_fig1237715313319"></a>  
        ![](./figures/createuser.png)

        **图 12**  高级用户配置<a name="zh-cn_topic_0186390266_zh-cn_topic_0122145909_fig128716531312"></a>  
        ![](./figures/Advanced_User_Configuration.png "高级用户配置")

        完成设置后，单击左上角的“完成”返回“安装概览”页面。

    5. 设置其他配置项，其他配置项可以使用默认配置。

3. 单击“开始安装”进行系统安装，如[图13](#fig1717019357392)所示。

    **图 13**  开始安装<a name="fig1717019357392"></a>  
    ![](./figures/Installation_Procedure.png)
4. 安装完成后重启系统。

    openEuler完成安装后，单击“重启”按钮，系统将重新启动。

## 查看系统信息

系统安装完成并重启后直接进入系统命令行登录界面，输入安装过程中设置的用户和密码，进入openEuler操作系统，查看如下系统信息。若需要进行系统管理和配置操作，请参考《[管理员指南](https://openeuler.org/zh/docs/21.09/docs/Administration/administration.html)》。

- 查看系统信息，命令如下：

    ```sh
    cat /etc/os-release
    ```

- 查看系统相关的资源信息。

    查看CPU信息，命令如下：

    ```sh
    # lscpu
    ```

    查看内存信息，命令如下：

    ```sh
    # free
    ```

    查看磁盘信息，命令如下：

    ```sh
    # fdisk -l
    ```

- 查看IP地址，命令如下：

    ```sh
    # ip addr
    ```

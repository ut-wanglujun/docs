# K3s部署指南

### 什么是K3s
K3s 是一个轻量级的 Kubernetes 发行版，它针对边缘计算、物联网等场景进行了高度优化。K3s 有以下增强功能：
- 打包为单个二进制文件。
- 使用基于 sqlite3 的轻量级存储后端作为默认存储机制。同时支持使用 etcd3、MySQL 和 PostgreSQL 作为存储机制。
- 封装在简单的启动程序中，通过该启动程序处理很多复杂的 TLS 和选项。
- 默认情况下是安全的，对轻量级环境有合理的默认值。
- 添加了简单但功能强大的batteries-included功能，例如：本地存储提供程序，服务负载均衡器，Helm controller 和 Traefik Ingress controller。
- 所有 Kubernetes control-plane 组件的操作都封装在单个二进制文件和进程中，使 K3s 具有自动化和管理包括证书分发在内的复杂集群操作的能力。
- 最大程度减轻了外部依赖性，K3s 仅需要 kernel 和 cgroup 挂载。

### 适用场景
K3s 适用于以下场景：

- 边缘计算-Edge
- 物联网-IoT
- CI
- Development
- ARM
- 嵌入 K8s

由于运行 K3s 所需的资源相对较少，所以 K3s 也适用于开发和测试场景。在这些场景中，如果开发或测试人员需要对某些功能进行验证，或对某些问题进行重现，那么使用 K3s 不仅能够缩短启动集群的时间，还能够减少集群需要消耗的资源。

### 部署K3s

#### 准备工作：

- 确保server节点及agent节点主机名不一致：

可以通过 hostnamectl set-hostname “主机名”    进行主机名的修改。

![1661829534335](./figures/set-hostname.png)

- 在各节点yum 安装 K3s：

  K3s官网采用下载对应架构二进制可执行文件的格式，通过install.sh脚本进行离线安装，openEuler社区将该二进制文件的编译过程移植到社区中，并编译出RPM包。此处可通过yum命令直接进行下载安装。

![1661830441538](./figures/yum-install.png)

#### 部署server节点

如需在单个服务器上安装 K3s，可以在 server 节点上执行如下操作：
```
INSTALL_K3S_SKIP_DOWNLOAD=true k3s-install.sh
```

![1661825352724](./figures/server-install.png)

#### 检查server部署情况

![1661825403705](./figures/check-server.png)

#### 部署agent节点

首先查询server节点的token值，该token可在server节点的/var/lib/rancher/k3s/server/node-token查到。

> **注意**:
>
> 后续我们只用到该token的后半部分。

![1661825538264](./figures/token.png)

选择添加其他 agent，请在每个 agent 节点上执行以下操作。

```
INSTALL_K3S_SKIP_DOWNLOAD=true K3S_URL=https://myserver:6443 K3S_TOKEN=mynodetoken k3s-install.sh
```

> **注意**
> 
> 将 myserver 替换为 server 的 IP 或有效的 DNS，并将 mynodetoken 替换 server 节点的 token：

![1661829392357](./figures/agent-install.png)

#### 检查agent节点是否部署成功

安装完毕后，回到 **server** 节点，执行 `kubectl get nodes`，可以看到agent节点已注册成功。

![1661826797319](./figures/check-agent.png)

至此，一个基础的k3s集群搭建完成。

#### 更多用法

K3s的更多用法可以参考K3s官网，https://rancher.com/docs/k3s/latest/en/ ，https://docs.rancher.cn/k3s/ 

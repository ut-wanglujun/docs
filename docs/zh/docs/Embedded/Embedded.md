# 嵌入式

本文档主要包含以下内容：

- openEuler Embedded用户指南：介绍openEuler Embedded如何使用、如何构建，如何开发的相关内容。

- UniProton用户指南：介绍UniProton的基本功能和接口说明。

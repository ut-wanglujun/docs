# Memory Fabric用户文档

-   [简介](简介.md)
-   [部署](部署.md)
-   [启动](启动.md)
-   [接口](接口.md)

# 简介

内存池套件是基于内存型介质和新型网络技术，构建高性能的分布式内存资源池，并通过BigMemory、MemPlog、MemKV等语义对应用提供服务。

内存池套件使用和管理的资源来自计算节点或者单独资源节点提供的可共享的DRAM/Persistent Memory/内存盘等（不支持普通disk直接互联池化），结合新型互联协议（RDMA，CXL，UBUS）us级传输时延，期望内存池套件通过极低的软件时延和节点CPU开销带来整体性能提升。

# 部署

Memory Fabric需要根据节点类型、资源分布情况和应用需求做到多场景应用集成部署，具体描述如下：

-   计算和MF资源共节点时支持client和server同节点部署，如[图1](#fig17349154610267)node1、node2所示。
-   资源独立提供时也支持client、server分离节点部署，如[图1](#fig17349154610267)node3、node4所示。
-   支持同节点部署和分离节点部署两种场景混合部署。
-   节点内支持多client，也支持多server模式部署，如[图1](#fig17349154610267)node2所示。
-   client与同节点的server通信使用IPC，配置支持连接远端server时使用RPC通信。
-   server端接入集群管理服务中，用于统一的节点编号和资源信息广播。

**图 1**  集成部署<a name="fig17349154610267"></a>  
![](images/IntegratedDeployment.png)

# 启动

Memory Fabric启动分为server和client两个部分。在节点上优先启动server端，完成集群注册、监控流程，然后启动本地资源注册\(总容量通过配置文件获取\)和通信建链流程，初始化完成后支持本地多client与server建立连接，可对外启动业务服务。

# 接口

<a name="table104mcpsimp"></a>

<table><tbody><tr id="row109mcpsimp"><td class="cellrowborder" valign="top" width="77%"><p id="p111mcpsimp"><a name="p111mcpsimp"></a><a name="p111mcpsimp"></a>接口</p>
</td>
<td class="cellrowborder" valign="top" width="23%"><p id="p113mcpsimp"><a name="p113mcpsimp"></a><a name="p113mcpsimp"></a>说明</p>
</td>
</tr>
<tr id="row114mcpsimp"><td class="cellrowborder" valign="top" width="77%"><p id="p116mcpsimp"><a name="p116mcpsimp"></a><a name="p116mcpsimp"></a>int BM_Init(char *ockPath, int flags);</p>
<p id="p117mcpsimp"><a name="p117mcpsimp"></a><a name="p117mcpsimp"></a>int BM_InitWithExtAttr(char *ockPath, int flags, void *attr); int BM_Exit(void);</p>
</td>
<td class="cellrowborder" valign="top" width="23%"><p id="p119mcpsimp"><a name="p119mcpsimp"></a><a name="p119mcpsimp"></a>初始化</p>
<p id="p120mcpsimp"><a name="p120mcpsimp"></a><a name="p120mcpsimp"></a>带属性的初始化</p>
<p id="p121mcpsimp"><a name="p121mcpsimp"></a><a name="p121mcpsimp"></a>退出</p>
</td>
</tr>
<tr id="row122mcpsimp"><td class="cellrowborder" valign="top" width="77%"><p id="p124mcpsimp"><a name="p124mcpsimp"></a><a name="p124mcpsimp"></a>int BM_CreateNSSec(const char *poolName, const char *ns, int attribute, const char *secId);</p>
<p id="p125mcpsimp"><a name="p125mcpsimp"></a><a name="p125mcpsimp"></a>int BM_DestroyNSSec(const char *poolName, const char *ns, const char *secId);</p>
<p id="p126mcpsimp"><a name="p126mcpsimp"></a><a name="p126mcpsimp"></a>int BM_QueryNSCache(QueryNsCache *buff, size_t buffLen);</p>
</td>
<td class="cellrowborder" valign="top" width="23%"><p id="p128mcpsimp"><a name="p128mcpsimp"></a><a name="p128mcpsimp"></a>Namespace创建和销毁、查询</p>
</td>
</tr>
<tr id="row129mcpsimp"><td class="cellrowborder" valign="top" width="77%"><p id="p131mcpsimp"><a name="p131mcpsimp"></a><a name="p131mcpsimp"></a>int BM_AllocSec(BmInfo *bminfo, size_t size, int flags, BmAffinity* affinity);</p>
<p id="p132mcpsimp"><a name="p132mcpsimp"></a><a name="p132mcpsimp"></a>int BM_FreeSec(BmInfo *bminfo, int flags);</p>
<p id="p133mcpsimp"><a name="p133mcpsimp"></a><a name="p133mcpsimp"></a>int BM_BatchFreeSec(char *bmIdArray[], int num, int flags, const char *ns, const char *secId);</p>
</td>
<td class="cellrowborder" valign="top" width="23%"><p id="p135mcpsimp"><a name="p135mcpsimp"></a><a name="p135mcpsimp"></a>Bigmemory对象申请和释放</p>
</td>
</tr>
<tr id="row136mcpsimp"><td class="cellrowborder" valign="top" width="77%"><p id="p138mcpsimp"><a name="p138mcpsimp"></a><a name="p138mcpsimp"></a>int BM_ExpandSec(BmInfo *bminfo, size_t size, int flags);</p>
</td>
<td class="cellrowborder" valign="top" width="23%"><p id="p140mcpsimp"><a name="p140mcpsimp"></a><a name="p140mcpsimp"></a>bigMemory扩容</p>
</td>
</tr>
<tr id="row141mcpsimp"><td class="cellrowborder" valign="top" width="77%"><p id="p143mcpsimp"><a name="p143mcpsimp"></a><a name="p143mcpsimp"></a>int BM_MapSec(BmInfo *bminfo, int prot, int flags, void **ptr);</p>
<p id="p144mcpsimp"><a name="p144mcpsimp"></a><a name="p144mcpsimp"></a>int BM_Unmap(char *bmId, void *ptr);</p>
</td>
<td class="cellrowborder" valign="top" width="23%"><p id="p146mcpsimp"><a name="p146mcpsimp"></a><a name="p146mcpsimp"></a>bigMemory到连续虚拟空间map和unmap</p>
</td>
</tr>
<tr id="row147mcpsimp"><td class="cellrowborder" valign="top" width="77%"><p id="p149mcpsimp"><a name="p149mcpsimp"></a><a name="p149mcpsimp"></a>int BM_CopySec(BmInfo *srcBM, size_t srcOffset, BmInfo *dstBM, size_t dstOffset, size_t length, int flags);</p>
</td>
<td class="cellrowborder" valign="top" width="23%"><p id="p151mcpsimp"><a name="p151mcpsimp"></a><a name="p151mcpsimp"></a>bigMemory对象间拷贝</p>
</td>
</tr>
<tr id="row152mcpsimp"><td class="cellrowborder" valign="top" width="77%"><p id="p154mcpsimp"><a name="p154mcpsimp"></a><a name="p154mcpsimp"></a>int BM_SpecificObjSwapInSec(DiskFileDesc *file, BmInfo *bminfo, int flags, size_t offset, size_t length);</p>
</td>
<td class="cellrowborder" valign="top" width="23%"><p id="p156mcpsimp"><a name="p156mcpsimp"></a><a name="p156mcpsimp"></a>文件内容换入bigmemory对象</p>
</td>
</tr>
<tr id="row157mcpsimp"><td class="cellrowborder" valign="top" width="77%"><p id="p159mcpsimp"><a name="p159mcpsimp"></a><a name="p159mcpsimp"></a>int BM_ReadSec(BmInfo *bminfo, size_t offset, size_t length, void *buffer, int flags);</p>
<p id="p160mcpsimp"><a name="p160mcpsimp"></a><a name="p160mcpsimp"></a>int BM_WriteSec(BmInfo *bminfo, size_t offset, size_t length, const void *buffer, int flags);</p>
</td>
<td class="cellrowborder" valign="top" width="23%"><p id="p162mcpsimp"><a name="p162mcpsimp"></a><a name="p162mcpsimp"></a>Bigmemory读写</p>
</td>
</tr>
<tr id="row163mcpsimp"><td class="cellrowborder" valign="top" width="77%"><p id="p165mcpsimp"><a name="p165mcpsimp"></a><a name="p165mcpsimp"></a>int BM_GatherSec(intptr_t *inputInfo, BmOperatorCode operatorCode, int flags, BmInfo *bminfo, size_t *bmLen);</p>
</td>
<td class="cellrowborder" valign="top" width="23%"><p id="p167mcpsimp"><a name="p167mcpsimp"></a><a name="p167mcpsimp"></a>聚合操作接口</p>
</td>
</tr>
</tbody>
</table>


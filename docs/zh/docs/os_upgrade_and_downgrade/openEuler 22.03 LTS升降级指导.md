本文档以openEuler-22.03-LTS到openEuler-22.03-LTS-SP1的升降级为例，其他版本同理

## **1**. **系统安装**
获取openEuler-22.03-LTS镜像，参考安装指南，完成openEuler操作系统的安装。

查看当前环境openEuler、kernel版本

![LTS_version](./images/LTS_version.png)


## **2**. **升级操作**

### 2.1 新增openEuler-22.03-LTS-SP1 repo源 (openEuler-22.03-LTS-SP1.repo)

```bash
vi /etc/yum.repos.d/openEuler-22.03-LTS-SP1.repo
```

 输入以下openEuler-22.03-LTS-SP1 repo源信息并保存退出 

```
SP1_OS、SP1_everything、SP1_EPOL、SP1_debuginfo、SP1_source、SP1_update 
```

![SP1_repo](./images/SP1_repo.png)

### 2.2 执行升级

```
dnf update | tee update_log
```

```
补充说明：     
1、安装报错时，通过执行 dnf update --skip-broken -x conflict_pkg1 |tee update_log 规避安装冲突问题，如果有多个包冲突，添加多个-x conflict_pkg1 -x conflict_pkg2 -x conflict_pkg3，待升级完成后，对跳过的软件包单独进行分析、验证、升级;  
2、参数释义：
--allowerasing  通过卸载已安装的软件包解决依赖关系 
--skip-broken 通过跳过软件包解决依赖问题
-x 跟--skip-broken配合使用，后边跟需要跳过的软件包名
```


### 2.4 重启系统

```
reboot
```


## **3**. **升级结果验证**

查看当前环境的openEuler、kernel版本

![SP1_version](./images/SP1_version.png)


## **4**. **降级操作**

### 4.1 执行降级

```
dnf downgrade | tee downgrade_log
```

### 4.2 重启系统

```
reboot
```

## **5**. **降级结果验证**

查看当前环境的openEuler、kernel版本

![LTS_version](./images/LTS_version.png)

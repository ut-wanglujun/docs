# 认识secGear

## 概述

随着云计算的快速发展，越来越多的企业把计算业务部署到云上，面对第三方云基础设施，云上用户数据安全面临着巨大的挑战。机密计算是一种基于硬件可信执行环境的隐私保护技术，旨在依赖最底层硬件，构建最小信任依赖，将操作系统、Hypervisor、基础设施、系统管理员、服务提供商等都从信任实体列表中删除，视为未经授权的实体，从而减少潜在的风险，保护可信执行环境中数据的机密性、完整性。然而随着机密计算技术的兴起，业界机密计算技术种类繁多（如主流的Intel SGX、ARM Trustzone、RSIC-V keystone等），各技术SDK也千差万别，给开发者带来较大的开发维护成本，长远考虑，还造成了机密计算应用生态隔离。为方便开发者快速构建保护云上数据安全的机密计算解决方案，openEuler推出机密计算统一开发框架secGear。

## 架构介绍

![](./figures/secGear_arch.png) 

secGear机密计算统一开发框架技术架构如图所示，主要包括三层，共同组成openEuler机密计算软件生态底座。

- Base Layer：机密计算SDK统一层，屏蔽TEE及SDK差异，实现不同架构共源码。
- Middleware Layer：通用组件层，机密计算软件货架，无需从头造轮子，帮助用户快速构建机密计算解决方案。
- Server Layer：机密计算服务层，提供典型场景机密计算解决方案。



## 关键特性

### 零切换

#### 客户痛点
传统应用做机密计算拆分改造后，REE侧逻辑存在频繁调用TEE侧逻辑时或REE与TEE存在频繁大块数据交互时。由于REE与TEE之间的每次调用，都需要经过REE用户态 、REE内核态、驱动、TEE内核态、TEE用户态之间的上下文切换，调用传递的大块数据也要经过多次拷贝，并且驱动底层数据块大小限制等因素，频繁的REE与TEE交互性能直线下降，严重影响机密计算应用的落地。

#### 解决方案
[零切换](https://gitee.com/openeuler/secGear#switchless%E7%89%B9%E6%80%A7)是一种通过共享内存减少REE与TEE上下文切换及数据拷贝次数，优化REE与TEE交互性能的技术。 

#### 使用方法

1. 创建enclave时启用零切换

  零切换配置项及说明如下。

  ```c
  typedef struct {
  	uint32_t num_uworkers;
  	uint32_t num_tworkers;
  	uint32_t switchless_calls_pool_size;
  	uint32_t retries_before_fallback;
  	uint32_t retries_before_sleep;
  	uint32_t parameter_num;
  	uint32_t workers_policy;
  	uint32_t rollback_to_common;
  } cc_sl_config_t;
  ```

  | 配置项                     | 说明                                                         |
  | -------------------------- | ------------------------------------------------------------ |
  | num_uworkers               | 非安全侧代理工作线程数，用于执行switchless OCALL，当前该字段仅在SGX平台生效，ARM平台可以配置，但是因ARM平台暂不支持OCALL，所以配置后不会生效。 <br />规格： <br />ARM：最大值：512；最小值：1；默认值：8（配置为0时）<br />SGX：最大值：4294967295；最小值：1 |
  | num_tworkers               | 安全侧代理工作线程数，用于执行switchless ECALL。 <br />规格： <br />ARM：最大值：512；最小值：1；默认值：8（配置为0时） <br />SGX：最大值：4294967295；最小值：1 |
  | switchless_calls_pool_size | switchless调用任务池的大小，实际可容纳switchless_calls_pool_size * 64个switchless调用任务（例：switchless_calls_pool_size=1，可容纳64个switchless调用任务）。 <br />规格： <br />ARM：最大值：8；最小值：1；默认值：1（配置为0时）<br />SGX：最大值：8；最小值：1；默认值：1（配置为0时） |
  | retries_before_fallback    | 执行retries_before_fallback次汇编pause指令后，若switchless调用仍没有被另一侧的代理工作线程执行，就回退到switch调用模式，该字段仅在SGX平台生效。 <br />规格：SGX：最大值：4294967295；最小值：1；默认值：20000（配置为0时） |
  | retries_before_sleep       | 执行retries_before_sleep次汇编pause指令后，若代理工作线程一直没有等到有任务来，则进入休眠状态，该字段仅在SGX平台生效。 <br />规格： SGX：最大值：4294967295；最小值：1；默认值：20000（配置为0时） |
  | parameter_num              | switchless函数支持的最大参数个数，该字段仅在ARM平台生效。<br />规格： ARM：最大值：16；最小值：0 |
  | workers_policy             | switchless代理线程运行模式，该字段仅在ARM平台生效。 <br />规格： ARM： WORKERS_POLICY_BUSY：代理线程一直占用CPU资源，无论是否有任务需要处理，适用于对性能要求极高且系统软硬件资源丰富的场景； WORKERS_POLICY_WAKEUP：代理线程仅在有任务时被唤醒，处理完任务后进入休眠，等待再次被新任务唤醒 |
  | rollback_to_common         | 异步switchless调用失败时是否回退到普通调用，该字段仅在ARM平台生效。 <br />规格： ARM：0：否，失败时仅返回相应错误码；其他：是，失败时回退到普通调用，此时返回普通调用的返回值 |

2. 定义EDL文件中接口时添加零切换标识transition_using_threads

  ```ocaml
  enclave {
      include "secgear_urts.h"
      from "secgear_tstdc.edl" import *;
      from "secgear_tswitchless.edl" import *;
      trusted {
          public int get_string([out, size=32]char *buf);
          public int get_string_switchless([out, size=32]char *buf) transition_using_threads;
      };
  };
  ```



### 安全通道
#### 客户痛点
数据拥有者在请求云上机密计算服务时，需要把待处理数据上传到云上TEE环境中处理，由于TEE没有网络，用户数据需要经过网络先传输到REE，REE接收到数据的明文后，再传入TEE中。用户数据的明文暴露在REE内存中，存在安全风险。

#### 解决方案
安全通道是一种结合机密计算远程证明，实现数据拥有者与云上TEE之间安全的密钥协商技术，协商出仅数据拥有者与云上TEE拥有的sessionkey，再通过sessionkey加密用户数据，网络传输的是sessionkey加密后的数据，REE接收到密文数据，再传入TEE中解密，处理。

#### 使用方法
安全通道以lib库方式提供，分为客户端、服务端host、服务端enclave三部分，分别由业务程序的客户端、服务端CA、服务端TA调用。
| 模块         | 头文件                      | 库文件                   | 依赖      |
|------------|--------------------------|-----------------------|---------|
| 客户端        | secure_channel_client.h  | libcsecure_channel.so | openssl |
| 服务端host    | secure_channel_host.h    | libusecure_channel.so | openssl |
| 服务端enclave | secure_channel_enclave.h | libtsecure_channel.so | TEE及TEE软件栈     |

##### 接口列表
| 接口名                                                                                                                                          | 所属头文件、库                   | 功能           | 备注 |
|----------------------------------------------------------------------------------------------------------------------------------------------|-----------------------|--------------|----|
| cc_sec_chl_client_init                                                 | secure_channel_client.h libcsecure_channel.so | 安全通道客户端初始化   | 调用前需初始化参数ctx中网络连接和消息发送钩子函数   |
| cc_sec_chl_client_fini                                                                                         | secure_channel_client.h libcsecure_channel.so | 安全通道客户端销毁    | 通知服务端销毁本客户端的信息，销毁本地安全通道信息   |
| cc_sec_chl_client_callback                                              | secure_channel_client.h libcsecure_channel.so | 安全通道协商消息处理函数 | 处理安全通道协商过程中，服务端发送给客户端的消息。在客户端消息接收处调用   |
| cc_sec_chl_client_encrypt | secure_channel_client.h libcsecure_channel.so | 安全通道客户端的加密接口     |  无  |
| cc_sec_chl_client_decrypt | secure_channel_client.h libcsecure_channel.so | 安全通道客户端的解密接口     |  无   |
|  int (*cc_conn_opt_funcptr_t)(void *conn, void *buf, size_t count);                                                                                                                                            |    secure_channel.h                    |    消息发送钩子函数原型          | 由用户客户端和服务端实现，实现中指定安全通道协商消息类型，负责发送安全通道协商消息到对端   |
|  cc_sec_chl_svr_init                                                                                                                                            |  secure_channel_host.h  libusecure_channel.so                    |  安全通道服务端初始化            | 调用前需初始化ctx中enclave_ctx   |
|  cc_sec_chl_svr_fini                                                                                                                                            |   secure_channel_host.h  libusecure_channel.so                    |  安全通道服务端销毁            |  销毁安全通道服务端以及所有客户端信息  |
|  cc_sec_chl_svr_callback                                                                                                                                            |  secure_channel_host.h  libusecure_channel.so                     |  安全通道协商消息处理函数            | 处理安全通道协商过程中，客户端发送给服务端的消息。在服务端消息接收处调用，调用前需初始化与客户端的网络连接和发送消息函数，详见[样例](https://gitee.com/openeuler/secGear/blob/master/examples/secure_channel/host/server.c#:~:text=conn_ctx.conn_kit.send)。   |
| cc_sec_chl_enclave_encrypt                                                                                                                                             |    secure_channel_enclave.h libtsecure_channel.so                   | 安全通道enclave中的加密接口             | 无   |
|   cc_sec_chl_enclave_decrypt                                                                                                                                           |   secure_channel_enclave.h libtsecure_channel.so                    | 安全通道enclave中的解密接口             |  无 |
##### 注意事项
安全通道仅封装密钥协商过程、加解密接口，不建立网络连接，协商过程复用业务的网络连接。其中客户端和服务端的网络连接由业务建立和维护，在安全通道客户端和服务端初始化时传入消息发送钩子函数和网络连接指针。
详见[安全通道样例](https://gitee.com/openeuler/secGear/tree/master/examples/secure_channel)。
## 缩略语

| 缩略语 | 英文全名                      | 中文解释         |
| ------ | ----------------------------- | ---------------- |
| REE    | Rich Execution Environment    | 富执行环境       |
| TEE    | Trusted Execution Environment | 可信执行环境     |
| EDL    | Enclave Description Language  | 安全应用描述语言 |

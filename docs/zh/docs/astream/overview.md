# astream用户指南

astream是一款延长磁盘使用寿命的工具。它基于Linux提供的inotify机制进行目录监控，同时配合用户针对应用场景定义的流分配规则，为匹配到的文件在创建时设置流信息，而后通过内核透传流信息至已使能多流特性的NVMe SSD磁盘，最终使得文件的存储能够根据流信息的标识进行更优的分类存储，降低磁盘垃圾回收的工作量，从而降低磁盘的写放大水平，最终达到延长磁盘使用寿命。专注于具有明显具有相同或相似生命周期的workload特征的数据库场景应用，如MySQL。
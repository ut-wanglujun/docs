# 磁盘加密

## 概述

磁盘加密是对重要数据存储机密性进行保护，按照给定加密算法对数据进行加密后写入磁盘，从而保障重要数据的机密性。该特性主要涉及用户态工具cryptsetup和内核态的dm-crypt模块。当前openEuler操作系统提供的磁盘加密特性已支持商密算法。相关参数如下：

- 加密模式：支持luks2和plain两种模式；
- 密钥长度：支持256位；
- 摘要算法：支持商密SM3算法；
- 加密算法：支持商密sm4-xts-plain64算法。

## 前置条件

1. 内核大于或等于5.10.0-106版本：

```
$ rpm -qa kernel
kernel-5.10.0-106.1.0.55.oe2209.x86_64
```

2. cryptsetup大于或等于2.4.1-1版本：

```
$ rpm -qa cryptsetup
cryptsetup-2.4.1-1.oe2209.x86_64
```

## 如何使用

通过将磁盘格式化成指定加密模式的磁盘，然后映射到/dev/mapper下作为dm设备，后续对磁盘的读写都通过该dm设备进行，数据的加解密过程由内核态完成，用户态不感知。参考步骤如下：

1. 格式化磁盘，将磁盘映射为dm设备：

a. luks2模式

加密模式使用luks2，加密算法使用sm4-xts-plain64，密钥大小为256位，摘要算法使用sm3：

```
# cryptsetup luksFormat /dev/sdd -c sm4-xts-plain64 --key-size 256 --hash sm3
```

b. plain模式

加密模式使用plain，加密算法使用sm4-xts-plain64，密钥大小为256位，摘要算法使用sm3：

```
# cryptsetup plainOpen /dev/sdd crypt1 -c sm4-xts-plain64 --key-size 256 --hash sm3
```

2. 映射成功后可通过lsblk查看设备信息：

```
# lsblk
NAME             MAJ:MIN RM  SIZE RO TYPE  MOUNTPOINTS
......
sdd                8:48   0   50G  0 disk
└─crypt1         253:3    0   50G  0 crypt
......
```

3. 对加密后的设备进行IO读写：

直接对裸盘下发IO：

```
# dd if=/dev/random of=/dev/mapper/crypt1 bs=4k count=10240
```

通过文件系统下发IO：

```
# mkfs.ext4 /dev/mapper/crypt1
# mount /dev/mapper/crypt1 /mnt/crypt/
# dd if=/dev/random of=/mnt/crypt/tmp bs=4k count=10240
```

4. 关闭设备映射:

如果挂载了文件系统，需要先卸载：

```
# umount /mnt/crypt
```

关闭设备：

```
# cryptsetup luksClose crypt1
```
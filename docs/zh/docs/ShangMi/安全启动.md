# 安全启动

安全启动是UEFI规范的标准功能，通过对系统启动中的各个组件进行逐级签名验证，实现启动过程的完整性和真实性保证。Linux系统的UEFI安全启动主要包括以下三个流程：

1. BIOS通过内置证书验证shim组件的签名信息；
2. shim组件通过内置证书验证grub组件的签名信息；
3. grub组件通过shim组件提供的接口验证kernel组件的签名信息。

openEuler对EFI签名工具（pesign及其算法库nss）和shim进行了商密算法扩展支持，即支持使用SM3算法进行EFI文件的哈希计算，使用SM2数字签名算法进行EFI文件的签名/验签，从而建立基于商密算法的操作系统启动安全保障。

## 约束限制

- openEuler shim组件支持商密安全启动，即支持shim验签grub，grub验签kernel流程。shim组件的验签依赖BIOS支持；
- 需要基于支持UEFI安全启动的arm64/x86物理机运行；
- pesign工具最多支持二级证书签名;
- pesign工具当前仅支持生成签名，不支持验证签名。

## 前置条件

1. 系统中安装下列软件包且高于指定版本：

```shell
openssl-1.1.1m-15.oe2203.aarch64
nss-3.72.0-4.oe2203.aarch64
pesign-115-2.oe2203.aarch64
shim-15.6-7.oe2203.aarch64
crypto-policies-20200619-3.git781bbd4.oe2203.noarch
```

2. 下载openEuler shim组件源码，注意需要检查spec文件中的版本号大于15.6-7：

```shell
git clone https://gitee.com/src-openeuler/shim.git -b openEuler-22.03-LTS-SP1 --depth 1
```

3. 安装编译shim组件所需要的软件包：

```shell
yum install elfutils-libelf-devel gcc gnu-efi gnu-efi-devel openssl-devel make git rpm-build
```

4. 检查nss是否使能SM3算法，如果未使能则按照如下所示修改：

```shell
cat /usr/share/crypto-policies/DEFAULT/nss.txt | grep SM3
config="disallow=ALL allow=HMAC-SHA256:HMAC-SHA1:HMAC-SHA384:HMAC-SHA512:CURVE25519:SECP256R1:SECP384R1:SECP521R1:aes256-gcm:chacha20-poly1305:aes256-cbc:aes128-gcm:aes128-cbc:SHA256:SHA384:SHA512:SHA224:SHA1:ECDHE-RSA:ECDHE-ECDSA:RSA:DHE-RSA:ECDSA:RSA-PSS:RSA-PKCS:tls-version-min=tls1.0:dtls-version-min=dtls1.0:DH-MIN=1023:DSA-MIN=2048:RSA-MIN=2048:SM3"
```

## 生成密钥和证书

1. 生成用于签名shim组件的密钥和证书，shim组件由BIOS校验签名，由于当前大部分BIOS不支持商密算法，此处选择使用RSA算法，如支持商密算法，可按照步骤2生成SM2密钥和证书：

```shell
openssl genrsa -out rsa.key 4096
openssl req -new -key rsa.key -out rsa.csr -subj '/C=AA/ST=BB/O=CC/OU=DD/CN=secure boot BIOS'
openssl x509 -req -days 365 -in rsa.csr -signkey rsa.key -out rsa.crt
openssl x509 -in rsa.crt -out rsa.der -outform der
```

2. 生成用签名grub和kernel组件的SM2密钥和证书：

```shell
openssl ecparam -genkey -name SM2 -out sm2.key
openssl req -new -sm3 -key sm2.key -out sm2.csr -subj '/C=AA/ST=BB/O=CC/OU=DD/CN=secure boot shim'
openssl x509 -req -days 3650 -signkey sm2.key -in sm2.csr -out sm2.crt
openssl x509 -in sm2.crt -out sm2.der -outform der
```

3. 建立NSS数据库，并将以上两步骤生成的密钥和证书导入NSS数据库：

```shell
# NSS数据库以目录形式组织，存放位置可自定义
mkdir certdb
certutil -N -d certdb
# 将SM2证书和RSA证书导入NSS数据库，分别命名为sm2和rsa
certutil -A -n sm2 -d certdb -t CT,CT,CT -i sm2.crt
certutil -A -n rsa -d certdb -t CT,CT,CT -i rsa.crt
# 将SM2密钥和RSA密钥导入NSS数据库，需要先打包成pkcs12文件
openssl pkcs12 -export -out rsa.p12 -inkey rsa.key -in rsa.crt
openssl pkcs12 -export -out sm2.p12 -inkey sm2.key -in sm2.crt
pk12util -d certdb -i rsa.p12
pk12util -d certdb -i sm2.p12
```

## 编译shim组件

1. 进入shim源码目录，修改shim.spec中的配置变量，开启商密算法支持，并指定内置SM2证书：

```shell
%global enable_sm 1
%global vendor_cert /path/to/sm2.der
```

2. 编译shim软件包：

```shell
rpmbuild -ba shim.spec --define "_sourcedir $PWD"
```

3. 安装编译完成的shim软件包：

```shell
rpm -Uvh ~/rpmbuild/RPMS/aarch64/shim-xxx.rpm
```

## UEFI文件商密签名

1. 使用RSA密钥和证书签名shim组件并替换：

```shell
# arm64架构
pesign -n certdb -c rsa -s -i /boot/efi/EFI/openEuler/shimaa64.efi -o shimaa64.efi.signed
cp shimaa64.efi.signed /boot/efi/EFI/openEuler/shimaa64.efi
# x86架构
pesign -n certdb -c rsa -s -i /boot/efi/EFI/openEuler/shimx64.efi -o shimx64.efi.signed
cp shimx64.efi.signed /boot/efi/EFI/openEuler/shimx64.efi
```

2. 使用SM2密钥和证书签名grub组件并替换：

```shell
# arm64架构
pesign -n certdb -c sm2 -s -i /boot/efi/EFI/openEuler/grubaa64.efi -o grubaa64.efi.signed -d sm3
cp grubaa64.efi.signed /boot/efi/EFI/openEuler/grubaa64.efi
# x86架构
pesign -n certdb -c sm2 -s -i /boot/efi/EFI/openEuler/grubx64.efi -o grubx64.efi.signed -d sm3
cp grubx64.efi.signed /boot/efi/EFI/openEuler/grubx64.efi
```

3. 使用SM2密钥和证书签名kernel组件并替换（注意文件名包含实际的版本号）：

```shell
# arm64架构，需要解压、签名、重新压缩
cp /boot/vmlinuz-5.10.0-126.0.0.66.oe2203.aarch64 vmlinuz-5.10.0-126.0.0.66.oe2203.aarch64.gz
gzip -d vmlinuz-5.10.0-126.0.0.66.oe2203.aarch64.gz
pesign -n certdb -c sm2 -s -i vmlinuz-5.10.0-126.0.0.66.oe2203.aarch64 -o vmlinuz-5.10.0-126.0.0.66.oe2203.aarch64.signed -d sm3
gzip vmlinuz-5.10.0-126.0.0.66.oe2203.aarch64.signed
cp vmlinuz-5.10.0-126.0.0.66.oe2203.aarch64.signed.gz /boot/vmlinuz-5.10.0-126.0.0.66.oe2203.aarch64
# x86架构
pesign -n certdb -c sm2 -s -i /boot/vmlinuz-5.10.0-126.0.0.66.oe2203.x86_64 -o vmlinuz-5.10.0-126.0.0.66.oe2203.x86_64.signed -d sm3
cp vmlinuz-5.10.0-126.0.0.66.oe2203.x86_64.signed /boot/vmlinuz-5.10.0-126.0.0.66.oe2203.x86_64
```

4. 检查签名信息，以shim和grub为例：

```shell
pesign -S -i /boot/efi/EFI/openEuler/grubaa64.efi 
pesign -S -i /boot/efi/EFI/openEuler/shimaa64.efi 
```

## 安全启动

进入BIOS，导入shim组件的签名证书，并开启安全启动选项。不同的BIOS操作方法不同，以鲲鹏2280 v2服务器为例：

1. 将签名shim组件的RSA证书放入/boot/efi/EFI/openEuler目录下：

```shell
cp rsa.der /boot/efi/EFI/openEuler
```

2. 重启系统；
   
3. 进入BIOS配置界面开启安全启动，配置路径：

```shell
Setup->安全->安全启动->启用
```

4. 配置安全启动为自定义模式，配置路径：

```shell
Setup->安全->安全启动证书配置->安全启动模式->自定义模式
```

5. 导入安全启动证书，配置路径：

```shell
Setup->安全->安全启动证书配置->安全启动自定义模式相关选项->DB相关选项->导入签名->通过文件添加签名->选择rsa.der->保存并退出
```

6. 保存配置后重启系统，系统启动成功，查询安全启动状态为开启：

```shell
mokutil --sb-state
SecureBoot enabled
```

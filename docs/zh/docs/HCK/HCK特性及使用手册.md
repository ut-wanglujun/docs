# 特性说明
HCK(High-performance Computing Kit)是HPC应用的软件底座，是在通用Linux平台上通过定制系统调用，调度优化等手段，为HPC应用提供隔离/低底噪的运行环境，同时兼容Linux生态，达到提高HPC应用性能的目的。在构建内核时需要将选项`CONFIG_PURPOSE_BUILT_KERNEL`开启来使能HCK特性。通过在系统启动阶段隔离出一部分CPU用来运行HPC应用，内核线程和其他用户进程则运行在非隔离核上。

# 使用说明
在内核支持HCK特性前提下，需要在内核启动阶段增加启动参数`pbk_cpus`设置预留的CPU资源，默认不预留。可以通过修改grub的配置文件`grub.cfg`或grub的启动界面设置此参数。
启动参数的格式为： pbk_cpus=<cpulist>，cpulist支持`","`和`"-"`连接，例如`1,3-4`表示预留CPU1/CPU3和CPU4。HCK特性暂不支持x86架构，需要注意的是，在aarch64架构下不支持预留0核。
当配置好启动参数后，内核会自动完成CPU资源的隔离预留，系统启动后不需要额外的部署步骤。

# 用户态launcher工具使用

HCK特性有一个用户态的launcher工具，通过其可以指定程序在特定的隔离核上运行，该工具需要单独安装。

## 基本选项

参见 `launcher -?`：
```
Usage: launcher [OPTION...]

launcher: launch process to pbk domain

EXAMPLES:
   launcher -c 1,2 prog        # alloc CPU 1,2 from pbk root domain to run prog
   launcher -n 2 prog          # alloc 2 CPUs from pbk root domain to run prog
   launcher -v prog            # prog will only touch pbk 
   launcher -c 1 prog          # alloc CPU 1 and 1M memory

  -c, --cpulist=CPULIST      cpulist to run prog
  -n, --nr_cpu=NR_CPU        nr_cpus to run prog
  -v, --pbk_view             run prog with pbk view
  -?, --help                 Give this help list
      --usage                Give a short usage message

Mandatory or optional arguments to long options are also mandatory or optional
for any corresponding short options.
```
## 选项详解

### launcher -？

显示该命令的使用说明。

### launcher -c

指定`cpulist`运行程序，例如：

```
launcher -c 1,2 ./test
```

launcher将会申请CPU1和CPU2用于执行test程序，如果CPU1和CPU2不是隔离核或者已经被其他程序申请，那么返回失败。

存在一种例外情况，例如执行以下操作：

```
launcher -c 1,2 ./test1
launcher -c 1,2 ./test2
```

执行test2时，指定了相同的`cpulist`，此时不会返回失败，而是将test2也运行到CPU1和CPU2上。

### launcher -n

指定CPU数量运行程序，例如

```
launcher -n 1 ./test
```

launcher将会从隔离核中申请一个CPU来运行test程序，如果剩余的隔离核不足，则返回失败。

### launcher -v

在CPU隔离后，CPU资源划分为Linux资源和隔离核资源。为了适配mpirun等具备资源识别分配功能的程序，在launcher中提供修改资源视角的功能，例如：

```
launcher -v cat /proc/cpuinfo
```

此时只会显示隔离核而非所有的CPU。

此选项还可以结合`-n`和`-c`选项使用，例如

```
launcher -c 1,2 -v cat /proc/cpuinfo
```

此时只会显示CPU1和CPU2的信息。

受影响的procfs和sysfs的接口如下：

procfs:

```
/proc/cpuinfo
```

sysfs:

```
/sys/devices/system/cpu/online
/sys/devices/system/cpu/present
/sys/devices/system/cpu/possible
/sys/devices/system/cpu/cpu<N>/online
/sys/devices/system/node/node<N>/cpumap
/sys/devices/system/node/node<N>/cpulist
/sys/fs/cgroup/cpuset/cpuset.cpus
/sys/fs/cgroup/cpuset/cpuset.effective_cpus
```

注：除了cgroup相关的两个接口默认仅显示Linux资源外（这是因为隔离核没有加入到cgroup的cpuset中），其他接口默认都会显示Linux资源+隔离核资源，不受隔离影响。


# 查看系统信息

- 查看系统信息，命令如下：

    ```shell
    cat /etc/os-release
    ```

    例如，命令和输出如下：

    ```shell
    $ cat /etc/os-release
    NAME="openEuler"
    VERSION="23.09"
    ID="openEuler"
    VERSION_ID="23.09"
    PRETTY_NAME="openEuler 23.09"
    ANSI_COLOR="0;31"
    ```

- 查看系统相关的资源信息。

    查看CPU信息，命令如下：

    ```shell
    # lscpu
    ```

    查看内存信息，命令如下：

    ```shell
    free
    ```

    查看磁盘信息，命令如下：

    ```shell
    fdisk -l
    ```

- 查看系统资源实时信息，命令如下：

    ```shell
    top
    ```

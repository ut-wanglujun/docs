# HA使用实例

本章介绍如何快速使用HA高可用集群，以及添加一个实例。若不了解怎么安装，请参考[HA的安装与部署文档](./HA的安装与部署.md\)。

## 快速使用指南

- 以下操作均以社区新开发的管理平台为例。

### 登录页面

用户名为`hacluster`，密码为该用户在主机上设置的密码。

![](./figures/HA-api.png)

### 主页面

登录系统后显示主页面，主页面由四部分组成：侧边导航栏、顶部操作区、资源节点列表区以及节点操作浮动区。

以下将详细介绍这四部分的特点与使用方法。

![](./figures/HA-home-page.png)

#### 导航栏

侧边导航栏由两部分组成：高可用集群软件名称和 logo 以及系统导航。系统导航由三项组成：【系统】、【集群配置】和【工具】。【系统】是默认选项，也是主页面的对应项，主要展示系统中所有资源的相关信息以及操作入口；【集群配置】下设【首选项配置】和【心跳配置】两项；【工具】下设【日志下载】和【集群快捷操作】两项，点击后以弹出框的形式出现。

#### 顶部操作区

登录用户是静态显示，鼠标滑过用户图标，出现操作菜单项，包括【刷新设置】和【退出登录】两项，点击【刷新设置】，弹出【刷新设置】对话框，包含【刷新设置】选项，可以设置系统的自动刷新模式，包括【不自动刷新】、【每 5 秒刷新】和【每 10 秒刷新】三种选择，默认选择【不自动刷新】、【退出登录】即可注销本次登录，系统将自动跳到登录页面，此时，如果希望继续访问系统，则需要重新进行登录。

![](./figures/HA-refresh.png)

#### 资源节点列表区

资源节点列表集中展现系统中所有资源的【资源名】、【状态】、【资源类型】、【服务】、【运行节点】等资源信息，以及系统中所有的节点和节点的运行情况等节点信息。同时提供资源的【添加】、【编辑】、【启动】、【停止】、【清理】、【迁移】、【回迁】、【删除】和【关系】操作。

#### 节点操作浮动区

节点操作浮动区域默认是收起的状态，每当点击资源节点列表表头中的节点时，右侧会弹出节点操作扩展区域，如图所示，该区域由收起按钮、节点名称、停止和备用四个部分组成，提供节点的【停止】和【备用】操作。点击区域左上角的箭头，该区域收起。

### 首选项配置

以下操作均可用命令行配置，现只做简单示例，若想使用更多命令可以使用``pcs --help``进行查询。

```sh
# pcs property set stonith-enabled=false
# pcs property set no-quorum-policy=ignore
```

``pcs property``查看全部设置

![](./figures/HA-firstchoice-cmd.png)

- 点击侧边导航栏中的【首选项配置】按钮，弹出【首选项配置】对话框。将No Quorum Policy和Stonith Enabled由默认状态改为如下对应状态；修改完成后，点击【确定】按钮完成配置。

![](./figures/HA-firstchoice.png)

#### 添加资源

##### 添加普通资源

鼠标点击【添加普通资源】，弹出【创建资源】对话框，其中资源的所有必填配置项均在【基本】页面内，选择【基本】页面内的【资源类型】后会进一步给出该类资源的其他必填配置项以及选填配置项。填写资源配置信息时，对话框右侧会出现灰色文字区域，对当前的配置项进行解释说明。全部必填项配置完毕后，点击【确定】按钮即可创建普通资源，点击【取消】按钮，取消本次添加动作。【实例属性】、【元属性】或者【操作属性】页面中的选填配置项为选填项，不配置不会影响资源的创建过程，可以根据场景需要可选择修改，否则将按照系统默认值处理。

下面以apache为例，添加apache资源

```sh
# pcs resource create httpd ocf:heartbeat:apache
```

查看资源运行状态

```sh
# pcs status
```

![](./figures/HA-pcs-status.png)

- 添加apache资源

![](./figures/HA-add-resource.png)

- 若回显为如下，则资源添加成功

![](./figures/HA-apache-suc.png)

- 资源创建成功并启动，运行于其中一个节点上，例如ha1；成功访问apache界面。

![](./figures/HA-apache-show.png)

##### 添加组资源

添加组资源时，集群中需要至少存在一个普通资源。鼠标点击【添加组资源】，弹出【创建资源】对话框。【基本】页面内均为必填项，填写完毕后，点击【确定】按钮，即可完成资源的添加，点击【取消】按钮，取消本次添加动作。

- **注：组资源的启动是按照子资源的顺序启动的，所以选择子资源时需要注意按照顺序选择。**

![](./figures/HA-group.png)

若回显为如下，则资源添加成功

![](./figures/HA-group-suc.png)

##### 添加克隆资源

鼠标点击【添加克隆资源】，弹出【创建资源】对话框。【基本】页面内填写克隆对象，资源名称会自动生成，填写完毕后，点击【确定】按钮，即可完成资源的添加，点击【取消】按钮，取消本次添加动作。

![](./figures/HA-clone.png)

若回显为如下，则资源添加成功

![](./figures/HA-clone-suc.png)

#### 编辑资源

- 启动资源：资源节点列表中选中一个目标资源，要求：该资源处于非运行状态。对该资源执行启动动作。
- 停止资源：资源节点列表中选中一个目标资源，要求：该资源处于运行状态。对该资源执行停止操作。
- 清理资源：资源节点列表中选中一个目标资源，对该资源执行清理操作。
- 迁移资源：资源节点列表中选中一个目标资源，要求：该资源为处于运行状态的普通资源或者组资源，执行迁移操作可以将资源迁移到指定节点上运行。
- 回迁资源：资源节点列表中选中一个目标资源，要求：该资源已经完成迁移动作，执行回迁操作，可以清除该资源的迁移设置，资源重新迁回到原来的节点上运行。
点击按钮后，列表中该资源项的变化状态与启动资源时一致。
- 删除资源：资源节点列表中选中一个目标资源，对该资源执行删除操作。

#### 设置资源关系

资源关系即为目标资源设定限制条件，资源的限制条件分为三种：资源位置、资源协同和资源顺序。

- 资源位置：设置集群中的节点对于该资源的运行级别，由此确定启动或者切换时资源在哪个节点上运行，运行级别按照从高到低的顺序依次为：Master Node、Slave 1。
- 资源协同：设置目标资源与集群中的其他资源是否运行在同一节点上，同节点资源表示该资源与目标资源必须运行在相同节点上，互斥节点资源表示该资源与目标资源不能运行在相同的节点上。
- 资源顺序：设置目标资源与集群中的其他资源启动时的先后顺序，前置资源是指目标资源运行之前，该资源必须已经运行；后置资源是指目标资源运行之后，该资源才能运行。

## 高可用mysql实例配置

- 先单独配置三个普通资源，待成功后添加为组资源。

### 配置虚拟IP

在首页中点击添加-->添加普通资源，并按如下进行配置。

![](./figures/HA-vip.png)

- 资源创建成功并启动，运行于其中一个节点上，例如ha1；可以ping通并连接，登录后可正常执行各种操作；资源切换到ha2运行；能够正常访问。
- 若回显为如下，则资源添加成功

![](./figures/HA-vip-suc.png)

### 配置NFS存储

- 另外找一台机器作为nfs服务端进行配置

安装软件包

```sh
# yum install -y nfs-utils rpcbind
```

关闭防火墙

```sh
# systemctl stop firewalld && systemctl disable firewalld
```

修改/etc/selinux/config文件中SELINUX状态为disabled

```Conf
SELINUX=disabled
```

启动服务

```sh
# systemctl start rpcbind && systemctl enable rpcbind
# systemctl start nfs-server && systemctl enable nfs-server
```

服务端创建一个共享目录

```sh
# mkdir -p /test
```

修改NFS配置文件

```sh
# vim /etc/exports
# /test *(rw,no_root_squash)
```

重新加载服务

```sh
# systemctl reload nfs
```

客户端安装软件包，先把mysql安装上，为了把下面nfs挂载到mysql数据路径

```sh
# yum install -y nfs-utils mariadb-server
```

在首页中点击添加-->添加普通资源，并按如下进行配置NFS资源。

![](./figures/HA-nfs.png)

- 资源创建成功并启动，运行于其中一个节点上，例如ha1；nfs成功挂载到/var/lib/mysql路径下。资源切换到ha2运行；nfs从ha1节点取消挂载，并自动在ha2节点上挂载成功。
- 若回显为如下，则资源添加成功

![](./figures/HA-nfs-suc.png)

### 配置mysql

在首页中点击添加-->添加普通资源，并按如下进行配置mysql资源。

![](./figures/HA-mariadb.png)

- 若回显为如下，则资源添加成功

![](./figures/HA-mariadb-suc.png)

### 添加上述资源为组资源

- 按资源启动顺序添加三个资源

在首页中点击添加-->添加组资源，并按如下进行配置组资源。

![](./figures/HA-group-new.png)

- 组资源创建成功并启动，若回显与上述三个普通资源成功现象一致，则资源添加成功

![](./figures/HA-group-new-suc.png)

- 将ha1节点备用，成功迁移到ha2节点，运行正常

![](./figures/HA-group-new-suc2.png)

## 仲裁设备配置

选择一台新的机器，作为仲裁设备。当前软件包不支持使用systemct进行服务测试，启停服务使用pcs操作，以下附上详细操作步骤。

### 安装仲裁所需软件包

- 安装corosync-qdevice在现有集群的节点上

```sh
[root@node1:~]# yum install corosync-qdevice 
[root@node2:~]# yum install corosync-qdevice
```

- 安装pcs和corosync-qnetd在仲裁设备主机上

```sh
[root@qdevice:~]# yum install pcs corosync-qnetd
```

- 在仲裁设备主机上启动pcsd服务并在系统启动时启用pcsd

```sh
[root@qdevice:~]# systemctl start pcsd.service 
[root@qdevice:~]# systemctl enable pcsd.service
```

### 修改主机名称及/etc/hosts文件

**注：三台主机均需要进行以下操作，现以其中一台为例。**

在仲裁功能使用前，需要确认修改主机名并将所有主机名写入/etc/hosts文件中，设置hacluster用户密码。

- 修改主机名

```sh
# hostnamectl set-hostname node1
```

- 编辑/etc/hosts文件并写入IP，主机名字段

```Conf
10.1.167.105 ha1
10.1.167.105 ha2
10.1.167.106 qdevice
```

- 设置hacluster用户密码

```sh
# passwd hacluster 
```

### 配置仲裁设备并添加到集群

以下过程为配置仲裁设备并将该仲裁设备添加到集群中。

- 用于仲裁设备的节点是qdevice
- 仲裁设备的model为net
- 集群节点是node1和node2

#### 配置仲裁设备

在将用于托管仲裁设备的节点上，使用以下命令配置仲裁设备。此命令配置和启动的仲裁设备model为net，并将设备配置为在引导时启动。

```sh
[root@qdevice:~]# pcs qdevice setup model net --enable --start 
Quorum device 'net' initialized 
quorum device enabled 
Starting quorum device... 
quorum device started
```

配置仲裁设备后，可以查看其状态。当前状态表明corosync-qnetd守护程序正在运行，此时没有客户端连接到它。使用--full命令可以展示详细的输出内容。

```sh
[root@qdevice:~]# pcs qdevice status net --full 
QNetd address: *:5403 
TLS: Supported (client certificate required) 
Connected clients: 0 
Connected clusters: 0 
Maximum send/receive size: 32768/32768 bytes
```

#### 关闭防火墙

```sh
# systemctl stop firewalld && systemctl disable firewalld
```

- 修改 /etc/selinux/config 文件中SELINUX状态为disabled

```Conf
SELINUX=disabled
```

#### 进行身份认证

从现有集群中的一个hacluster节点，对托管仲裁设备的节点上的用户进行身份验证。这允许pcs集群上连接到pcs主机上qdevice，但不允许主机pcs上qdevice连接到pcs集群上。

```sh
[root@node1:~] # pcs host auth qdevice
Username: hacluster
Password:
qdevice: Authorized
```

#### 将仲裁设备添加到集群

在添加仲裁设备之前，可以通过 pcs quorum config 命令查看仲裁设备的当前配置，以便之后进行比较。

```sh
[root@node1:~]# pcs quorum config
Options:
```

通过 pcs quorum status 命令查看仲裁设备的当前状态，输出结果表明集群尚未使用仲裁设备，并且Qdevice每个节点的成员身份状态为NR（未注册）。

```sh
[root@node1:~]# pcs quorum status
Quorum information
------------------
Date:             Wed Jun 29 13:15:36 2016
Quorum provider:  corosync_votequorum
Nodes:            2
Node ID:          1
Ring ID:          1/8272
Quorate:          Yes

Votequorum information
----------------------
Expected votes:   2
Highest expected: 2
Total votes:      2
Quorum:           1
Flags:            2Node Quorate

Membership information
----------------------
    Nodeid      Votes    Qdevice Name
         1          1         NR node1 (local)
         2          1         NR node2
```

以下命令将之前创建的仲裁设备添加到集群中。注意不能在一个集群中同时使用多个仲裁设备。但是，一个仲裁设备可以同时被多个集群使用。此示例将仲裁设备配置为ffsplit算法。

```sh
[root@node1:~]# pcs quorum device add model net host=qdevice algorithm=ffsplit
Setting up qdevice certificates on nodes...
node2: Succeeded
node1: Succeeded
Enabling corosync-qdevice...
node1: corosync-qdevice enabled
node2: corosync-qdevice enabled
Sending updated corosync.conf to nodes...
node1: Succeeded
node2: Succeeded
Corosync configuration reloaded
Starting corosync-qdevice...
node1: corosync-qdevice started
node2: corosync-qdevice started
```

#### 检查仲裁设备的配置状态

在集群端，执行以下命令来查看配置的变化情况。通过 pcs quorum config 命令显示已配置的仲裁设备信息。

```sh
[root@node1:~]# pcs quorum config
Options:
Device:
  Model: net
    algorithm: ffsplit
    host: qdevice
```

pcs quorum status命令显示仲裁运行时状态，表明仲裁设备正在使用中。每个集群节点的成员信息状态值的含义Qdevice如下:

- A/NA— quorum device 是否存活，表示qdevice和之间是否有心跳corosync。这应始终表明仲裁设备处于活动状态。
- V/NV—V当仲裁设备给一个节点投票时设置。在此示例中，两个节点都设置为，V因为它们可以相互通信。如果将集群拆分为两个单节点集群，则其中一个节点将设置为V，另一个节点将设置为NV。
- MW/NMW— 内部仲裁设备标志已设置 ( MW) 或未设置 ( NMW)。默认情况下，未设置标志，值为NMW。

```sh
[root@node1:~]# pcs quorum status
Quorum information
------------------
Date:             Wed Jun 29 13:17:02 2016
Quorum provider:  corosync_votequorum
Nodes:            2
Node ID:          1
Ring ID:          1/8272
Quorate:          Yes

Votequorum information
----------------------
Expected votes:   3
Highest expected: 3
Total votes:      3
Quorum:           2
Flags:            Quorate Qdevice

Membership information
----------------------
    Nodeid      Votes    Qdevice Name
         1          1    A,V,NMW node1 (local)
         2          1    A,V,NMW node2
         0          1            Qdevice
```

通过pcs quorum device status命令显示仲裁设备运行时状态。

```sh
[root@node1:~]# pcs quorum device status
Qdevice information
-------------------
Model:                  Net
Node ID:                1
Configured node list:
    0   Node ID = 1
    1   Node ID = 2
Membership node list:   1, 2

Qdevice-net information
----------------------
Cluster name:           mycluster
QNetd host:             qdevice:5403
Algorithm:              ffsplit
Tie-breaker:            Node with lowest node ID
State:                  Connected
```

在仲裁设备端，执行以下命令，显示corosync-qnetd守护程序的状态。

```sh
[root@qdevice:~]# pcs qdevice status net --full
QNetd address:                  *:5403
TLS:                            Supported (client certificate required)
Connected clients:              2
Connected clusters:             1
Maximum send/receive size:      32768/32768 bytes
Cluster "mycluster":
    Algorithm:          ffsplit
    Tie-breaker:        Node with lowest node ID
    Node ID 2:
        Client address:         ::ffff:192.168.122.122:50028
        HB interval:            8000ms
        Configured node list:   1, 2
        Ring ID:                1.2050
        Membership node list:   1, 2
        TLS active:             Yes (client certificate verified)
        Vote:                   ACK (ACK)
    Node ID 1:
        Client address:         ::ffff:192.168.122.121:48786
        HB interval:            8000ms
        Configured node list:   1, 2
        Ring ID:                1.2050
        Membership node list:   1, 2
        TLS active:             Yes (client certificate verified)
        Vote:                   ACK (ACK)
```

### 管理仲裁设备服务

PCS 提供了在本地主机 (corosync-qnetd)上管理仲裁设备服务的能力，如以下示例命令所示。请注意，这些命令仅影corosync-qnetd服务。

```sh
[root@qdevice:~]# pcs qdevice start net
[root@qdevice:~]# pcs qdevice stop net
[root@qdevice:~]# pcs qdevice enable net
[root@qdevice:~]# pcs qdevice disable net
[root@qdevice:~]# pcs qdevice kill net
```

当执行 pcs qdevice stop net 命令时可以看到 state 从成功变为失败，再次执行pcs qdevice start net 时，状态变为成功。
![](./figures/HA-qdevice.png)  

### 管理集群中的仲裁设备

可以使用多种pcs命令来更改集群中的仲裁设备设置、禁用仲裁设备和删除仲裁设备。

#### 更改仲裁设备设置

**注意：要更改host中 quorum device model 的选项net，请使用 pcs quorum device remove 和 pcs quorum device add 命令正确设置配置，除非旧主机和新主机是同一台机器。**

- 以下命令将仲裁设备算法更改为lms

```sh
[root@node1:~]# pcs quorum device update model algorithm=lms
Sending updated corosync.conf to nodes...
node1: Succeeded
node2: Succeeded
Corosync configuration reloaded
Reloading qdevice configuration on nodes...
node1: corosync-qdevice stopped
node2: corosync-qdevice stopped
node1: corosync-qdevice started
node2: corosync-qdevice started
```

#### 删除仲裁设备

- 以下命令删除集群节点上配置的仲裁设备

```sh
[root@node1:~]# pcs quorum device remove
Sending updated corosync.conf to nodes...
node1: Succeeded
node2: Succeeded
Corosync configuration reloaded
Disabling corosync-qdevice...
node1: corosync-qdevice disabled
node2: corosync-qdevice disabled
Stopping corosync-qdevice...
node1: corosync-qdevice stopped
node2: corosync-qdevice stopped
Removing qdevice certificates from nodes...
node1: Succeeded
node2: Succeeded
```

删除仲裁设备后，在显示仲裁设备状态时应该会看到以下错误消息。

```sh
[root@node1:~]# pcs quorum device status 
Error: Unable to get quorum status: corosync-qdevice-tool: Can't connect to QDevice socket (is QDevice running?): No such file or directory
```

#### 销毁仲裁设备

- 以下命令禁用和停止仲裁设备主机上的仲裁设备并删除其所有配置文件

```sh
[root@qdevice:~]# pcs qdevice destroy net
Stopping quorum device...
quorum device stopped
quorum device disabled
Quorum device 'net' configuration files removed
```

# 已知问题

| 序号 | 问题单号 | 问题简述 | 问题级别 | 影响分析 | 规避措施 | 历史发现场景 |
| ---- | ------- | -------- | -------- | ------- | -------- | --------- | 
| 1 | [I5LZXD](https://gitee.com/src-openEuler/openldap/issues/I5LZXD) | openldap build problem in openEuler:22.09 | 次要 | 构建过程中，用例执行失败。为用例设计问题，影响可控，通过sleep的方式等待操作执行完成，在高负载下偶先失败 | skip相关用力，并持续跟踪上游社区解决 | |
| 2 | [I5NLZI](https://gitee.com/src-openEuler/dde/issues/I5NLZI) | 【openEuler 22.09 rc2】启动器中个别应用图标显示异常 | 次要 | 仅为DDE桌面启动器的图标显示异常，无功能影响，易用性问题整体影响可控 | 建议切换主题规避 | |
| 3 | [I5P5HM](https://gitee.com/src-openEuler/afterburn/issues/I5P5HM) | 【22.09_RC3_EPOL】【arm/x86】卸载afterburn提示Failed to stop afterburn-sshkeys@.service | 次要 | | | |
| 4 | [I5PQ3O](https://gitee.com/src-openEuler/openmpi/issues/I5PQ3O) | 【openEuler-22.09-RC3】ompi-clean -v  -d参数执行报错 | 主要 | 该包为NestOS使用软件包，使用范围较为局限，默认为 NestOS 中的“core”用户启用，对服务器版本影响较小 | sig暂未提供规避手段 | |
| 5 | [I5Q2FE](https://gitee.com/src-openEuler/udisks2/issues/I5Q2FE) | udisks2 build problem in openEuler:22.09 | 次要 | 构建过程中，用例执行失败。环境未保留，长期本地构建未复现 | 持续跟踪社区构建成功率 | |
| 6 | [I5SJ0R](https://gitee.com/src-openEuler/podman/issues/I5SJ0R) | [22.09RC5 arm/x86]podman create --blkio-weight-device /dev/loop0:123:15 fedora ls 执行报错 | 次要 | blkio-weight为4.xx版本内核特性。5.10版本不支持 | 需跟进升级podman组件 | |
# 用户须知<a name="ZH-CN_TOPIC_0232332818"></a>

-   openEuler版本号计数规则由openEuler x.x变更为以年月为版本号，以便用户了解版本发布时间，例如openEuler 21.03表示发布时间为2021年3月。
-   [Python核心团队](https://www.python.org/dev/peps/pep-0373/#update)已经于2020年1月停止对Python 2的维护。2021年，openEuler 21.03版本仅修复Python 2的致命CVE。
-   openEuler 22.03-LTS版本开始，停止支持和维护Python 2,仅支持Python 3，请您切换和使用Python 3。
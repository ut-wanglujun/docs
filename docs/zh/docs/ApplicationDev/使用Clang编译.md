# 使用LLVM/Clang编译

本章介绍LLVM/Clang编译的一些基本知识，并通过示例进行实际演示。更多Clang使用方法请通过**clang --help**命令查询。
<!-- TOC -->

- [使用LLVM/Clang编译](#使用llvmclang编译)
    - [简介](#简介)
    - [LLVM/Clang安装](#llvmclang安装)
    - [多版本共存支持](#多版本共存支持)
    - [示例](#示例)

<!-- /TOC -->

## 简介

LLVM是一种涵盖多种编程语言和目标处理器的编译器，使用Clang作为C和C++的编译和驱动程序。Clang不仅仅可以将C, C++程序编译为LLVM中间表示的IR，也会调用所有以代码生成为目标的LLVM优化遍，直到生成最终的二进制文件。

## LLVM/Clang安装

openEuler操作系统中通过yum源安装Clang和LLVM软件包。默认安装llvm-12

```shell
yum install llvm
yum install clang
yum install lld // openEuler yum源中目前没有lld-12版本
```

验证安装是否成功

```shell
clang -v
```

若返回结果已包含clang版本信息，说明安装成功。

## 多版本共存支持

openEuler LLVM/Clang 多版本支持的方式如下：

```text
yum包名：
llvm<ver>{-*}
clang<ver>{-*}
lld<ver>{-*}
bolt<ver>{-*}
例如：
clang15
llvm15-devel

安装路径：
/usr/lib64/llvm<ver>
例如：
/usr/lib64/llvm15

/usr/bin目录下会安装带有-<ver>后缀的可执行文件
例如:
/usr/bin/clang-15
/usr/bin/lld-15
```

目前支持的LLVM/Clang版本有

```text
llvm //默认llvm12
llvm-15
```

通过yum安装其他版本

```shell
yum install llvm15
yum install clang15
yum install lld15
```

验证安装是否成功

```shell
clang-15 -v
```

## 示例

编译运行C/C++程序

```shell
clang  [command line flags]  hello.c  -o  hello.o 
./hello.o
```

```shell
clang++  [command line flags]  hello.cpp  -o  hello.o 
./hello.o
```

指定的链接器为LLVM的lld，若不指定它则使用默认的ld。

```shell
clang  [command line flags]  -fuse-ld=lld  hello.c  -o  hello.o 
./hello.o
```

其他通用信息请参考LLVM的[用户指导](https://llvm.org/docs/UserGuides.html)

# 开发环境准备
<!-- TOC -->

- [开发环境准备](#开发环境准备)
    - [环境要求](#环境要求)
        - [操作系统要求](#操作系统要求)
    - [配置openEuler yum源](#配置openeuler-yum源)
        - [通过直接获取在线的openEuler repo源配置在线yum源](#通过直接获取在线的openeuler-repo源配置在线yum源)
        - [通过挂载ISO创建本地openEuler repo源配置本地yum源](#通过挂载iso创建本地openeuler-repo源配置本地yum源)
    - [安装软件包](#安装软件包)
        - [安装JDK软件包](#安装jdk软件包)
        - [安装rpm-build软件包](#安装rpm-build软件包)
    - [使用IDE进行Java开发](#使用ide进行java开发)
        - [简介](#简介)
        - [使用MobaXterm登录服务器](#使用mobaxterm登录服务器)
        - [设置JDK环境](#设置jdk环境)
        - [下载安装GTK库](#下载安装gtk库)
        - [设置X11 Forwarding](#设置x11-forwarding)
        - [下载并运行IntelliJ IDEA](#下载并运行intellij-idea)

<!-- /TOC -->

## 环境要求

- 若使用的是物理机，则开发环境所需的最小硬件要求如[表1](#table154419352610)所示。

    **表 1**  最小硬件要求

    <a name="table154419352610"></a>
    <table><thead align="left"><tr id="row4446359618"><th class="cellrowborder" valign="top" width="11.19111911191119%" id="mcps1.2.4.1.1"><p id="p14417351063"><a name="p14417351063"></a><a name="p14417351063"></a><strong id="b1345735965"><a name="b1345735965"></a><a name="b1345735965"></a>部件名称</strong></p>
    </th>
    <th class="cellrowborder" valign="top" width="40.06400640064006%" id="mcps1.2.4.1.2"><p id="p13451335867"><a name="p13451335867"></a><a name="p13451335867"></a><strong id="b7456351569"><a name="b7456351569"></a><a name="b7456351569"></a>最小硬件要求</strong></p>
    </th>
    <th class="cellrowborder" valign="top" width="48.74487448744874%" id="mcps1.2.4.1.3"><p id="p184573513616"><a name="p184573513616"></a><a name="p184573513616"></a><strong id="b44553512615"><a name="b44553512615"></a><a name="b44553512615"></a>说明</strong></p>
    </th>
    </tr>
    </thead>
    <tbody><tr id="row1545193517615"><td class="cellrowborder" valign="top" width="11.19111911191119%" headers="mcps1.2.4.1.1 "><p id="p6457351868"><a name="p6457351868"></a><a name="p6457351868"></a>架构</p>
    </td>
    <td class="cellrowborder" valign="top" width="40.06400640064006%" headers="mcps1.2.4.1.2 "><a name="ul262164044016"></a><a name="ul262164044016"></a><ul id="ul262164044016"><li>AArch64</li><li>x86_64</li></ul>
    </td>
    <td class="cellrowborder" valign="top" width="48.74487448744874%" headers="mcps1.2.4.1.3 "><a name="ul1830173916388"></a><a name="ul1830173916388"></a><ul id="ul1830173916388"><li>支持Arm的64位架构。</li><li>支持Intel的x86_64位架构。</li></ul>
    </td>
    </tr>
    <tr id="row124517351664"><td class="cellrowborder" valign="top" width="11.19111911191119%" headers="mcps1.2.4.1.1 "><p id="p134573512615"><a name="p134573512615"></a><a name="p134573512615"></a>CPU</p>
    </td>
    <td class="cellrowborder" valign="top" width="40.06400640064006%" headers="mcps1.2.4.1.2 "><a name="ul97131912175915"></a><a name="ul97131912175915"></a><ul id="ul97131912175915"><li>华为鲲鹏920系列CPU</li><li>Intel<sup id="sup487664501416"><a name="sup487664501416"></a><a name="sup487664501416"></a>&reg;</sup> Xeon<sup id="sup10571950171416"><a name="sup10571950171416"></a><a name="sup10571950171416"></a>&reg;</sup>处理器</li></ul>
    </td>
    <td class="cellrowborder" valign="top" width="48.74487448744874%" headers="mcps1.2.4.1.3 "><p id="p4455351613"><a name="p4455351613"></a><a name="p4455351613"></a>-</p>
    </td>
    </tr>
    <tr id="row11457358618"><td class="cellrowborder" valign="top" width="11.19111911191119%" headers="mcps1.2.4.1.1 "><p id="p1545163512616"><a name="p1545163512616"></a><a name="p1545163512616"></a>内存</p>
    </td>
    <td class="cellrowborder" valign="top" width="40.06400640064006%" headers="mcps1.2.4.1.2 "><p id="p1453351269"><a name="p1453351269"></a><a name="p1453351269"></a>不小于4GB（为了获得更好的应用体验，建议不小于8GB）</p>
    </td>
    <td class="cellrowborder" valign="top" width="48.74487448744874%" headers="mcps1.2.4.1.3 "><p id="p74519351563"><a name="p74519351563"></a><a name="p74519351563"></a>-</p>
    </td>
    </tr>
    <tr id="row445113512611"><td class="cellrowborder" valign="top" width="11.19111911191119%" headers="mcps1.2.4.1.1 "><p id="p24514350612"><a name="p24514350612"></a><a name="p24514350612"></a>硬盘</p>
    </td>
    <td class="cellrowborder" valign="top" width="40.06400640064006%" headers="mcps1.2.4.1.2 "><p id="p3451035464"><a name="p3451035464"></a><a name="p3451035464"></a>为了获得更好的应用体验，建议不小于120GB）</p>
    </td>
    <td class="cellrowborder" valign="top" width="48.74487448744874%" headers="mcps1.2.4.1.3 "><p id="p1845133516618"><a name="p1845133516618"></a><a name="p1845133516618"></a>支持IDE、SATA、SAS等接口的硬盘。</p>
    </td>
    </tr>
    </tbody>
    </table>

- 若使用的是虚拟机，则开发环境所需的最小虚拟化空间要求如[表2](#table780410493819)所示。

    **表 2**  最小虚拟化空间要求

    <a name="table780410493819"></a>
    <table><thead align="left"><tr id="row138041049682"><th class="cellrowborder" valign="top" width="11.511151115111511%" id="mcps1.2.4.1.1"><p id="p98041649182"><a name="p98041649182"></a><a name="p98041649182"></a><strong id="b188046491883"><a name="b188046491883"></a><a name="b188046491883"></a>部件名称</strong></p>
    </th>
    <th class="cellrowborder" valign="top" width="39.74397439743974%" id="mcps1.2.4.1.2"><p id="p380418492812"><a name="p380418492812"></a><a name="p380418492812"></a><strong id="b1804164917815"><a name="b1804164917815"></a><a name="b1804164917815"></a>最小虚拟化空间要求</strong></p>
    </th>
    <th class="cellrowborder" valign="top" width="48.74487448744874%" id="mcps1.2.4.1.3"><p id="p580474913816"><a name="p580474913816"></a><a name="p580474913816"></a><strong id="b1580410491682"><a name="b1580410491682"></a><a name="b1580410491682"></a>说明</strong></p>
    </th>
    </tr>
    </thead>
    <tbody><tr id="row180415492085"><td class="cellrowborder" valign="top" width="11.511151115111511%" headers="mcps1.2.4.1.1 "><p id="p10804164916818"><a name="p10804164916818"></a><a name="p10804164916818"></a>架构</p>
    </td>
    <td class="cellrowborder" valign="top" width="39.74397439743974%" headers="mcps1.2.4.1.2 "><a name="ul12618156163"></a><a name="ul12618156163"></a><ul id="ul12618156163"><li>AArch64</li><li>x86_64</li></ul>
    </td>
    <td class="cellrowborder" valign="top" width="48.74487448744874%" headers="mcps1.2.4.1.3 "><p id="p5804849987"><a name="p5804849987"></a><a name="p5804849987"></a>-</p>
    </td>
    </tr>
    <tr id="row18804349380"><td class="cellrowborder" valign="top" width="11.511151115111511%" headers="mcps1.2.4.1.1 "><p id="p080410498820"><a name="p080410498820"></a><a name="p080410498820"></a>CPU</p>
    </td>
    <td class="cellrowborder" valign="top" width="39.74397439743974%" headers="mcps1.2.4.1.2 "><p id="p847135012587"><a name="p847135012587"></a><a name="p847135012587"></a>2个CPU</p>
    </td>
    <td class="cellrowborder" valign="top" width="48.74487448744874%" headers="mcps1.2.4.1.3 "><p id="p580454919810"><a name="p580454919810"></a><a name="p580454919810"></a>-</p>
    </td>
    </tr>
    <tr id="row13804849380"><td class="cellrowborder" valign="top" width="11.511151115111511%" headers="mcps1.2.4.1.1 "><p id="p1580454913817"><a name="p1580454913817"></a><a name="p1580454913817"></a>内存</p>
    </td>
    <td class="cellrowborder" valign="top" width="39.74397439743974%" headers="mcps1.2.4.1.2 "><p id="p1180518499810"><a name="p1180518499810"></a><a name="p1180518499810"></a>不小于4GB（为了获得更好的应用体验，建议不小于8GB）</p>
    </td>
    <td class="cellrowborder" valign="top" width="48.74487448744874%" headers="mcps1.2.4.1.3 "><p id="p180594918817"><a name="p180594918817"></a><a name="p180594918817"></a>-</p>
    </td>
    </tr>
    <tr id="row13805549588"><td class="cellrowborder" valign="top" width="11.511151115111511%" headers="mcps1.2.4.1.1 "><p id="p4805144913815"><a name="p4805144913815"></a><a name="p4805144913815"></a>硬盘</p>
    </td>
    <td class="cellrowborder" valign="top" width="39.74397439743974%" headers="mcps1.2.4.1.2 "><p id="p28051499810"><a name="p28051499810"></a><a name="p28051499810"></a>不小于32GB（为了获得更好的应用体验，建议不小于120GB）</p>
    </td>
    <td class="cellrowborder" valign="top" width="48.74487448744874%" headers="mcps1.2.4.1.3 "><p id="p48057494818"><a name="p48057494818"></a><a name="p48057494818"></a>-</p>
    </td>
    </tr>
    </tbody>
    </table>

### 操作系统要求

操作系统要求为openEuler操作系统。

openEuler操作系统具体安装方法请参考[安装指南](./../Installation/Installation.md)，其中“软件选择”页面的“已选环境的附加选项”中将“开发工具”勾选。

## 配置openEuler yum源

通过直接获取在线的openEuler repo源配置在线yum源或通过挂载ISO创建本地openEuler repo源配置本地yum源。

### 通过直接获取在线的openEuler repo源配置在线yum源

>![](./public_sys-resources/icon-note.gif) **说明：**
>openEuler提供了多种repo源供用户在线使用，各repo源含义可参考[系统安装](./../Releasenotes/系统安装.md)。本操作以AArch64架构的OS repo源为例将其配置为yum源。

1. 进入到yum源目录并查看目录下的.repo配置文件。

    ```shell
    # cd /etc/yum.repos.d
    # ls
    openEuler.repo
    ```

2. 在root权限下编辑openEuler.repo文件，将在线的openEuler repo源配置为yum源。

    ```shell
    # vi openEuler.repo
    ```

    编辑openEuler.repo文件的内容如下：

    ```text
        [osrepo]
        name=osrepo
        baseurl=http://repo.openeuler.org/openEuler-23.09/OS/aarch64/
        enabled=1

        gpgcheck=1
        gpgkey=http://repo.openeuler.org/openEuler-23.09/OS/aarch64/RPM-GPG-KEY-openEuler
    ```

    >![](./public_sys-resources/icon-note.gif) **说明：**
 >
 > - \[*repoid*\]中的repoid为软件仓库（repository）的ID号，所有.repo配置文件中的各repoid不能重复，必须唯一。示例中repoid设置为**osrepo**。
    > - name为软件仓库描述的字符串。
    > - baseurl为软件仓库的地址。
    > - enabled为是否启用该软件源仓库，可选值为1和0。默认值为1，表示启用该软件源仓库。
    > - gpgcheck可设置为1或0，1表示进行gpg（GNU Private Guard）校验，0表示不进行gpg校验，gpgcheck可以确定rpm包的来源是有效和安全的。  
    > - gpgkey为验证签名用的公钥。

### 通过挂载ISO创建本地openEuler repo源配置本地yum源

>![](./public_sys-resources/icon-note.gif) **说明：**
>openEuler提供了多种ISO发布包，各ISO发布包含义可参考[系统安装](./../Releasenotes/系统安装.md)。本操作以openEuler-23.09-aarch64-dvd.iso发布包和sha256校验文件为例，请根据实际需要的ISO发布包和校验文件进行修改。  

1. 下载ISO发布包。
    - 通过跨平台文件传输工具下载ISO镜像。
        1. 登录[openEuler社区](https://openeuler.org/zh/)网站。
        2. 单击“下载”。
        3. 单击“社区发行版”，显示版本列表。
        4. 在版本列表的“openEuler 23.09”版本处单击“前往下载”按钮，进入openEuler-23.09版本下载列表。
        5. 在对应的系统下载列表中，可以查看系统镜像架构和应用场景。

            架构分类如下：

            - AArch64：AArch64架构的ISO。
            - x86\_64：x86\_64架构的ISO。
            - ARM32：嵌入式的Image。

            场景分类如下：

            - 服务器：服务器场景的ISO。
            - 边缘计算：边缘计算场景的ISO。
            - 云计算：云计算场景的ISO。
            - 嵌入式：嵌入式的Image（只有选择AArch64或ARM32架构时，才可选该场景）。

        6. 单击“AArch64”，选择AArch64架构。
        7. 单击“服务器”，选择服务器场景。
        8. 在“软件包类型”栏选择需要的ISO，单击“立即下载”。
        9. 单击该ISO对应的完整性校验文件“SHA256”，复制校验值到本地。
        10. 登录openEuler操作系统，新建用于存放发布包的目录，如“~/iso”。

             ```shell
             mkdir ~/iso
             ```

        11. 使用跨平台文件传输工具（如WinSCP）将本地的openEuler发布包上传到openEuler操作系统。

    - 通过wget命令下载ISO镜像。
        1. 登录[openEuler社区](https://openeuler.org/zh/)网站。
        2. 单击“下载”。
        3. 单击“社区发行版”，显示版本列表。
        4. 在版本列表的“openEuler 23.09”版本处单击“前往下载”按钮，进入openEuler-23.09版本下载列表。
        5. 在对应的系统下载列表中，可以查看系统镜像架构和应用场景。

            架构分类如下：

            - AArch64：AArch64架构的ISO。
            - x86\_64：x86\_64架构的ISO。
            - ARM32：嵌入式的Image。

            场景分类如下：

            - 服务器：服务器场景的ISO。
            - 边缘计算：边缘计算场景的ISO。
            - 云计算：云计算场景的ISO。
            - 嵌入式：嵌入式的Image（只有选择AArch64或ARM32架构时，才可选该场景）。

        6. 单击“AArch64”，选择AArch64架构。
        7. 单击“服务器”，选择服务器场景。
        8. 在“软件包类型”栏选择需要的ISO，右键单击“立即下载”，单击“复制链接地址”，将openEuler发布包地址记录好。
        9. 单击复制该ISO对应的完整性校验文件“SHA256”，将openEuler校验文件记录好。
        10. 登录openEuler操作系统，新建用于存放发布包和检验文件的目录，如“~/iso”，并切换到该目录。

             ```shell
             mkdir ~/iso
             cd ~/iso
             ```

        11. 使用**wget**命令远程下载发布包，命令中的  *ipaddriso*  为openEuler发布包地址。

             ```shell
             wget ipaddriso
             ```

2. 发布包完整性校验。
    1. 计算openEuler发布包的sha256校验值。

        ```shell
        sha256sum openEuler-23.09-aarch64-dvd.iso
        ```

        命令执行完成后，输出校验值。

    2. 对比步骤1和复制的校验值是否一致。

        如果校验值一致说明iso文件完整性没有破坏，如果校验值不一致则可以确认文件完整性已被破坏，需要重新获取。

3. <a name="li6236932222"></a>挂载ISO并创建为repo源。

    在root权限下使用mount命令挂载镜像文件。

    示例如下：

    ```shell
    # mount /home/iso/openEuler-23.09-aarch64-dvd.iso /mnt/
    ```

    挂载好的mnt目录如下：

    ```text
    .
    │── boot.catalog
    │── docs
    │── EFI
    │── images
    │── Packages
    │── repodata
    │── TRANS.TBL
    └── RPM-GPG-KEY-openEuler
    ```

    其中，Packages为rpm包所在的目录，repodata为repo源元数据所在的目录，RPM-GPG-KEY-openEuler为openEuler的签名公钥。

4. 进入到yum源目录并查看目录下的.repo配置文件。

    ```shell
    # cd /etc/yum.repos.d
    # ls
    openEuler.repo
    ```

5. 在root权限下编辑openEuler.repo文件，将[3](#li6236932222)中创建的repo源配置为本地yum源。

    ```shell
    # vi openEuler.repo
    ```

    编辑openEuler.repo文件的内容如下：

    ```text
    [localosrepo]
    name=localosrepo
    baseurl=file:///mnt
    enabled=1
    gpgcheck=1
    gpgkey=file:///mnt/RPM-GPG-KEY-openEuler
    ```

## 安装软件包

安装开发过程中需要用到的软件。不同的开发需要的软件不一样，但安装方法相同，本章以安装常用的几个软件包（JDK，rpm-build）为例。有些开发软件openEuler操作系统已默认自带，如GCC、GNU make。

### 安装JDK软件包

1. 执行**dnf list installed | grep jdk**  查询JDK软件是否已安装。

    ```shell
    dnf list installed | grep jdk
    ```

    查看命令打印信息，若打印信息中包含“jdk”，表示该软件已经安装了，则不需要再安装。若无任何打印信息，则表示该软件未安装。

2. 清除缓存。

    ```shell
    dnf clean all
    ```

3. 创建缓存。

    ```shell
    dnf makecache
    ```

4. 查询可安装的JDK软件包。

    ```shell
    dnf search jdk | grep jdk
    ```

    查看命令打印信息，选择安装java-x.x.x-openjdk-devel.aarch64软件包。其中x.x.x为版本号。

5. 在root权限下安装JDK软件包，以安装java-1.8.0-openjdk-devel软件包为例。

    ```shell
    # dnf install java-1.8.0-openjdk-devel.aarch64
    ```

6. 查询JDK软件版本。

    ```shell
    java -version
    ```

    查看打印信息，若打印信息中包括“openjdk version "1.8.0\_232"”信息，表示已正确安装，其中1.8.0\_232为版本号。

### 安装rpm-build软件包

1. 执行**dnf list installed | grep rpm-build**  查询rpm-build软件是否已安装。

    ```shell
    dnf list installed | grep rpm-build
    ```

    查看命令打印信息，若打印信息中包含“rpm-build”，表示该软件已经安装了，则不需要再安装。若无任何打印信息，则表示该软件未安装。

2. 清除缓存。

    ```shell
    dnf clean all
    ```

3. 创建缓存。

    ```shell
    dnf makecache
    ```

4. 在root权限下安装rpm-build软件包。

    ```shell
    # dnf install rpm-build
    ```

5. 查询rpm-build软件版本。

    ```shell
    rpmbuild --version
    ```

## 使用IDE进行Java开发

对于小型的Java程序，可以直接参考“使用JDK编译”章节得到可运行Java应用。但是对于大中型Java应用，这种方式已经无法满足开发者的需求。因此您可以参考如下步骤安装IDE并进行使用，以方便您在openEuler系统上的Java开发工作。

### 简介

IntelliJ IDEA是一款非常流行的Java IDE，其社区版可以免费下载使用。目前openEuler支持使用IntelliJ IDEA集成开发环境（IDE）进行Java程序的开发，从而可以提升开发人员的工作效率。

### 使用MobaXterm登录服务器

MobaXterm是一款非常优秀的SSH客户端，其自带X Server，可以轻松解决远程GUI显示问题。

您需要提前下载安装好MobaXterm并打开，然后SSH登录您的服务器并进行以下操作。

### 设置JDK环境

在设置JAVA\_HOME之前您需要先找到JDK的安装路径。在“开发环境准备 \> 安装软件包 \> 安装JDK软件包”章节中您已经学会了如何安装JDK，如果您还没安装好JDK，请提前安好。

查看java路径，命令如下：

```shell
# which java
/usr/bin/java
```

查看软链接的实际指向目录，命令如下：

```shell
# ls -la /usr/bin/java
lrwxrwxrwx. 1 root root  22 Mar 6 20:28 /usr/bin/java -> /etc/alternatives/java
# ls -la /etc/alternatives/java
lrwxrwxrwx. 1 root root  83 Mar 6 20:28 /etc/alternatives/java -> /usr/lib/jvm/java-1.8.0-openjdk-1.8.0.232.b09-1.h2.aarch64/jre/bin/java
```

发现JDK的真实路径为/usr/lib/jvm/java-1.8.0-openjdk-1.8.0.232.b09-1.h2.aarch64，设置JAVA\_HOME和PATH，命令如下：

```shell
export JAVA_HOME=/usr/lib/jvm/java-1.8.0-openjdk-1.8.0.232.b09-1.h2.aarch64
export PATH=$JAVA_HOME/bin:$PATH
```

### 下载安装GTK库

运行如下命令:

```shell
dnf list installed | grep gtk
```

如果显示gtk2或者gtk3，则表示您已安装该库，可以直接跳过进入下一步，否则在root权限下运行如下命令自动下载安装gtk库。

```shell
# dnf -y install gtk2 libXtst libXrender  xauth
```

### 设置X11 Forwarding

切换到sshd配置目录

```shell
cd ~/.ssh
```

如果该目录不存在，则创建目录后再进行切换，创建目录命令如下：

```shell
mkdir ~/.ssh
```

然后在.ssh目录下编辑config文件并保存：

1. 使用vim打开config文件

    ```shell
    vim config
    ```

2. 将以下内容添加到文件末尾并保存：

    ```text
    Host *
        ForwardAgent yes
        ForwardX11 yes
    ```

### 下载并运行IntelliJ IDEA

在执行如上环境配置后，您就可以下载使用IntelliJ IDEA了。鉴于最新版的IntelliJ IDEA和openEuler系统在部分功能上有兼容性问题，建议您从此[链接](https://www.jetbrains.com/idea/download/other.html)下载2018版本linux压缩包。下载好后把压缩包移到您想要安装该软件的目录，对压缩包进行解压：

```shell
tar -xf ideaIC-2018.3.tar.gz
```

解压后切换到IntelliJ IDEA的目录下并运行。

```shell
cd ./idea-IC-183.4284.148
bin/idea.sh &
```

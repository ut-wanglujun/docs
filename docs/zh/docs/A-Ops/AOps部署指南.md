# A-Ops部署指南

## 一、环境要求

- 2台openEuler 23.09机器

  分别用于部署check模块的两种模式：调度器，执行器。其他服务如mysql、elasticsearch、aops-manager等可在任意一台机器独立部署，为便于操作，将这些服务部署在机器A。

- 内存尽量为8G+

## 二、配置部署环境

### 机器A

机器A需部署的aops服务有：aops-tools、aops-manager、aops-check、aops-web、aops-agent、gala-gopher。

需部署的第三方服务有：mysql、elasticsearch、zookeeper、kafka、prometheus。

具体部署步骤如下：

#### 2.1 关闭防火墙

关闭本节点防火墙

```shell
systemctl stop firewalld
systemctl disable firewalld
systemctl status firewalld
```

#### 2.2 部署aops-tools

安装aops-tools：

```shell
yum install aops-tools
```

#### 2.3 部署数据库[mysql、elasticsearch]

##### 2.3.1 部署mysql

使用安装aops-tools时安装的aops-basedatabase脚本进行安装

```shell
cd /opt/aops/aops_tools
./aops-basedatabase mysql
```

修改mysql配置文件

```shell
vim /etc/my.cnf
```

新增bind-address, 值为本机ip

![1662346986112](./figures/修改mysql配置文件.png)

重启mysql服务

```shell
systemctl restart mysqld
```

连接数据库，设置权限：

```shell
mysql
show databases;
use mysql;
select user,host from user;//出现user为root，host为localhost时，说明mysql只允许本机连接，外网和本地软件客户端则无法连接。
update user set host = '%' where user='root';
flush privileges;//刷新权限
exit
```

##### 2.3.2 部署elasticsearch

使用安装aops-tools时安装的aops-basedatabase脚本进行安装

```shell
cd /opt/aops/aops_tools
./aops-basedatabase elasticsearch
```

修改配置文件：

修改elasticsearch配置文件：

```shell
vim /etc/elasticsearch/elasticsearch.yml
```

![1662370718890](./figures/elasticsearch配置2.png)

![1662370575036](./figures/elasticsearch配置1.png)

![1662370776219](./figures/elasticsearch3.png)

重启elasticsearch服务：

```shell
systemctl restart elasticsearch
```

#### 2.4 部署aops-manager

安装aops-manager

```shell
yum install aops-manager
```

修改配置文件：

```shell
vim /etc/aops/manager.ini
```

将配置文件中各服务的地址修改为真实地址，由于将所有服务都部署在机器A，故需把IP地址配为机器A的地址。

```shell
[manager]
ip=192.168.1.1  // 此处及后续服务ip修改为机器A真实ip
port=11111
host_vault_dir=/opt/aops
host_vars=/opt/aops/host_vars

[uwsgi]
wsgi-file=manage.py
daemonize=/var/log/aops/uwsgi/manager.log
http-timeout=600
harakiri=600

[elasticsearch]
ip=192.168.1.1  // 此处及后续服务ip修改为机器A真实ip
port=9200
max_es_query_num=10000000

[mysql]
ip=192.168.1.1  // 此处及后续服务ip修改为机器A真实ip
port=3306
database_name=aops
engine_format=mysql+pymysql://@%s:%s/%s
pool_size=10000
pool_recycle=7200

[aops_check]
ip=192.168.1.1  // 此处及后续服务ip修改为机器A真实ip
port=11112
```

启动aops-manager服务：

```shell
systemctl start aops-manager
```

#### 2.5 部署aops-web

安装aops-web

```shell
yum install aops-web
```

修改配置文件，由于将所有服务都部署在机器A，故需将web访问的各服务地址配置成机器A的真实ip。

```shell
vim /etc/nginx/aops-nginx.conf
```

部分服务配置截图：

![1662378186528](./figures/配置web.png)

开启aops-web服务：

```shell
systemctl start aops-web
```

#### 2.6 部署kafka

##### 2.6.1 部署zookeeper

安装：

```shell
yum install zookeeper
```

启动服务：

```shell
systemctl start zookeeper
```

##### 2.6.2 部署kafka

安装：

```shell
yum install kafka
```

修改配置文件：

```shell
vim /opt/kafka/config/server.properties
```

将listener 改为本机ip

![1662381371927](./figures/kafka配置.png)

启动kafka服务:

```shell
cd /opt/kafka/bin
nohup ./kafka-server-start.sh ../config/server.properties &
tail -f ./nohup.out  # 查看nohup所有的输出出现A本机ip 以及 kafka启动成功INFO；
```

#### 2.7 部署aops-check

安装aops-check：

```shell
yum install aops-check
```

修改配置文件：

```shell
vim /etc/aops/check.ini
```

将配置文件中各服务的地址修改为真实地址，由于将所有服务都部署在机器A，故需把IP地址配为机器A的地址。

```shell
[check]
ip=192.168.1.1  // 此处及后续服务ip修改为机器A真实ip
port=11112
mode=configurable  // 该模式为configurable模式，用于常规诊断模式下的调度器。
timing_check=on

[default_mode]
period=30
step=30

[elasticsearch]
ip=192.168.1.1  // 此处及后续服务ip修改为机器A真实ip
port=9200

[mysql]
ip=192.168.1.1  // 此处及后续服务ip修改为机器A真实ip
port=3306
database_name=aops
engine_format=mysql+pymysql://@%s:%s/%s
pool_size=10000
pool_recycle=7200

[prometheus]
ip=192.168.1.1  // 此处及后续服务ip修改为机器A真实ip
port=9090
query_range_step=15s

[agent]
default_instance_port=8888

[manager]
ip=192.168.1.1  // 此处及后续服务ip修改为机器A真实ip
port=11111

[consumer]
kafka_server_list=192.168.1.1:9092  // 此处及后续服务ip修改为机器A真实ip
enable_auto_commit=False
auto_offset_reset=earliest
timeout_ms=5
max_records=3
task_name=CHECK_TASK
task_group_id=CHECK_TASK_GROUP_ID
result_name=CHECK_RESULT
[producer]
kafka_server_list = 192.168.1.1:9092  // 此处及后续服务ip修改为机器A真实ip
api_version = 0.11.5
acks = 1
retries = 3
retry_backoff_ms = 100
task_name=CHECK_TASK
task_group_id=CHECK_TASK_GROUP_ID
```

启动aops-check服务（configurable模式）：

```shell
systemctl start aops-check
```

#### 2.8 部署客户端服务

客户端机器的服务需要部署aops-agent及gala-gopher，具体可参考[aops-agent部署指南](aops-agent部署指南.md)。

注意：主机注册时需要先在前端添加主机组操作，确保该主机所属的主机组存在。此处只对机器A做部署、纳管。

#### 2.9 部署prometheus

安装prometheus:

```shell
yum install prometheus2
```

修改配置文件：

```shell
vim /etc/prometheus/prometheus.yml
```

将所有客户端的gala-gopher地址新增到prometheus的监控节点中。

![1662377261742](./figures/prometheus配置.png)

启动服务：

```shell
systemctl start prometheus
```

#### 2.10 部署gala-ragdoll

A-Ops配置溯源功能依赖gala-ragdoll实现，通过Git实现配置文件的变动监测。

安装gala-ragdoll：

```shell
yum install gala-ragdoll # A-Ops 配置溯源
```

修改配置文件：

```shell
vim /etc/ragdoll/gala-ragdoll.conf
```

将collect节点collect_address中IP地址修改为机器A的地址，collect_api与collect_port修改为实际接口地址。

```text
[git]
git_dir = "/home/confTraceTest"
user_name = "user_name"
user_email = "user_email"

[collect]
collect_address = "http://192.168.1.1"    //此处修改为机器A的真实IP
collect_api = "/manage/config/collect"    //此处修改为配置文件采集的实际接口
collect_port = 11111                      //此处修改为服务的实际端口

[sync]
sync_address = "http://0.0.0.0"
sync_api = "/demo/syncConf"
sync_port = 11114


[ragdoll]
port = 11114

```

启动gala-ragdoll服务

```shell
systemctl start gala-ragdoll
```

### 机器B

机器B只需部署aops-check作为执行器。

#### 2.11 部署aops-check

安装aops-check：

```shell
yum install aops-check
```

修改配置文件：

```shell
vim /etc/aops/check.ini
```

将配置文件中各服务的地址修改为真实地址，除check服务为机器B的地址外，其他服务都部署在机器A，故需把IP地址配置为机器A的地址即可。

```shell
[check]
ip=192.168.1.2  // 此处ip改为机器B真实ip
port=11112
mode=executor  // executor，用于常规诊断模式下的执行器
timing_check=on

[default_mode]
period=30
step=30

[elasticsearch]
ip=192.168.1.1  // 此处及后续服务ip修改为机器A真实ip
port=9200

[mysql]
ip=192.168.1.1  // 此处及后续服务ip修改为机器A真实ip
port=3306
database_name=aops
engine_format=mysql+pymysql://@%s:%s/%s
pool_size=10000
pool_recycle=7200

[prometheus]
ip=192.168.1.1  // 此处及后续服务ip修改为机器A真实ip
port=9090
query_range_step=15s

[agent]
default_instance_port=8888

[manager]
ip=192.168.1.1  // 此处及后续服务ip修改为机器A真实ip
port=11111

[consumer]
kafka_server_list=192.168.1.1:9092  // 此处及后续服务ip修改为机器A真实ip
enable_auto_commit=False
auto_offset_reset=earliest
timeout_ms=5
max_records=3
task_name=CHECK_TASK
task_group_id=CHECK_TASK_GROUP_ID
result_name=CHECK_RESULT
[producer]
kafka_server_list = 192.168.1.1:9092  // 此处及后续服务ip修改为机器A真实ip
api_version = 0.11.5
acks = 1
retries = 3
retry_backoff_ms = 100
task_name=CHECK_TASK
task_group_id=CHECK_TASK_GROUP_ID
```

启动aops-check服务（executor模式）：

```shell
systemctl start aops-check
```

至此，两台机器的服务部署完成。
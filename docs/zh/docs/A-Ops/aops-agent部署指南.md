# aops-agent部署指南

## 一、环境要求

1台openEuler机器，建议openEuler-20.03及以上版本运行。

## 二、配置环境部署

#### 1. 关闭防火墙

```shell
systemctl stop firewalld
systemctl disable firewalld
systemctl status firewalld
```

#### 2. aops-agent部署

1. 基于yum源安装：yum install aops-agent

2. 修改配置文件：将agent节点下IP标签值修改为本机IP，

   vim  /etc/aops/agent.conf，以IP地址为192.168.1.47为例

   ```ini
   [agent]
   ;启动aops-agent时,绑定的IP与端口
   ip=192.168.1.47
   port=12000
    
   [gopher]
   ;gala-gopher默认配置文件路径,如需修改请确保文件路径的准确性
   config_path=/opt/gala-gopher/gala-gopher.conf
    
   ;aops-agent采集日志配置
   [log]
   ;采集日志级别,可设置为DEBUG,INFO,WARNING,ERROR,CRITICAL
   log_level=INFO
   ;采集日志存放位置
   log_dir=/var/log/aops
   ;日志文件最大值
   max_bytes=31457280
   ;备份日志的数量
   backup_count=40
   ```

3. 启动服务：systemctl start aops-agent
#### 3. 向aops-manager注册

为了辨别使用者的身份，避免接口被随意调用，aops-agent采用token验证身份，以减轻所部署主机的压力。

基于安全性考虑，项目采用主动注册的方式去获取token。注册前，须在agent侧准备好需要注册的信息，调用register命令向aops-manager注册。由于agent未配置数据库，注册成功后，自动将token保存到指定文件内，并在前台展示注册结果。同时将本机相关信息存入到aops-manager侧的数据库中，以便后续管理。

1. 准备register.json 文件

   在aops-agent侧准备好注册所需信息以json格式存入文件中，数据结构如下：

```JSON
{
    // 前端登录用户名
    "web_username":"admin",
    // 用户密码
    "web_password": "changeme",
    // 主机名称
    "host_name": "host1",
    // 主机所在组名称
    "host_group_name": "group1",
    // aops-manager运行主机IP地址
    "manager_ip":"192.168.1.23",
    // 是否注册为管理机器
    "management":false,
    // aops-manager运行对外端口
    "manager_port":"11111",
    // agent运行端口
    "agent_port":"12000"
}
```

`注意：确保aops-manager已在目标主机运行，如192.168.1.23，且注册的主机组要存在。`

2. 执行：aops_agent register -f register.json，
3. 前台展示注册结果，注册成功时，保存token字符串至指定文件；注册失败时，根据提示以及日志内容了解具体原因。（/var/log/aops/aops.log）

注册结果示例：

`注册成功`

```shell
[root@localhost ~]# aops_agent register -f register.json
Agent Register Success
```

`注册失败,以aops-manager未启动为示例`

```shell
[root@localhost ~]# aops_agent register -f register.json
Agent Register Fail
[root@localhost ~]#
```

`对应日志内容`

```shell
2022-09-05 16:11:52,576 ERROR command_manage/register/331: HTTPConnectionPool(host='192.168.1.23', port=11111): Max retries exceeded with url: /manage/host/add (Caused by NewConnectionError('<urllib3.connection.HTTPConnection object at 0x7ff0504ce4f0>: Failed to establish a new connection: [Errno 111] Connection refused'))
[root@localhost ~]#
```

## 三、插件支持

#### 3.1 gala-gopher

##### 3.1.1 介绍

gala-gopher是基于eBPF的低负载探针框架，可用于对主机的CPU，内存，网络等状态的监控以及数据采集服务。可根据实际业务需求对已有采集探针采集状态进行配置。

##### 3.1.2 部署

1. 基于yum源安装：yum install gala-gopher
2. 基于实际的业务需求，选择需要探针进行开启，探针信息可在/opt/gala-gopher/gala-gopher.conf下查看。
3. 启动服务：systemctl start gala-gopher

##### 3.1.3 其它

gala-gopher更多信息可参考文档https://gitee.com/openeuler/gala-gopher/blob/master/README.md

## 四、接口支持

### 4.1 对外接口清单

| 序号 | 接口名称                       | 类型 | 说明                           |
| ---- | ------------------------------ | ---- | ----------------------|
| 1    | /v1/agent/plugin/start         | POST | 启动插件                       |
| 2    | /v1/agent/plugin/stop          | POST | 停止插件                       |
| 3    | /v1/agent/application/info     | GET  | 采集目标应用集内正在运行的应用 |
| 4    | /v1/agent/host/info           | GET  | 获取主机信息                   |
| 5    | /v1/agent/plugin/info          | GET  | 获取agent中插件运行信息    |
| 6    | /v1/agent/file/collect         | POST | 采集配置文件内容               |
| 7    | /v1/agent/collect/items/change | POST | 改变插件采集项的运行状态       |

#### 4.1.1、/v1/agent/plugin/start

+ 描述：启动已安装但未运行的插件，目前仅支持gala-gopher插件。

+ HTTP请求方式：POST

+ 数据提交方式：query

+ 请求参数：

  | 参数名      | 必选 | 类型 | 说明   |
  | ----------- | ---- | ---- | ------ |
  | plugin_name | True | str  | 插件名 |

+ 请求参数示例

  | 参数名      | 参数值      |
  | ----------- | ----------- |
  | plugin_name | gala-gopher |

+ 返回体参数

  | 参数名 | 类型 | 说明             |
  | ------ | ---- | ---------------- |
  | code   | int/ | 返回码           |
  | msg    | str  | 状态码对应的信息 |

+ 返回示例

  ```json
  {
      "code": 200,
      "msg": "xxxx"
  }
  ```


#### 4.1.2、/v1/agent/plugin/stop

+ 描述：使正在运行的插件停止，目前仅支持gala-gopher插件。

+ HTTP请求方式：POST

+ 数据提交方式：query

+ 请求参数：

    | 参数名      | 必选 | 类型 | 说明   |
    | ----------- | ---- | ---- | ------ |
    | plugin_name | True | str  | 插件名 |

+ 请求参数示例：

  | 参数名      | 参数值      |
  | ----------- | ----------- |
  | plugin_name | gala-gopher |

+ 返回体参数：

    | 参数名 | 类型 | 说明             |
    | ------ | ---- | ---------------- |
    | code   | int  | 返回码           |
    | msg    | str  | 状态码对应的信息 |

+ 返回示例：

    ```json
    {
        "code": 200,
        "msg": "xxxx"
    }
    ```


#### 4.1.3、/v1/agent/application/info

+ 描述：采集目标应用集内正在运行的应用，当前目标应用集包含mysql, kubernetes, hadoop, nginx, docker, gala-gopher。

+ HTTP请求方式：GET

+ 数据提交方式：query

+ 请求参数：

  | 参数名 | 必选 | 类型 | 说明 |
  | ------ | ---- | ---- | ---- |
  |        |      |      |      |

+ 请求参数示例：

  | 参数名 | 参数值 |
  | ------ | ------ |
  |        |        |

+ 返回体参数：

  | 参数名 | 类型 | 说明             |
  | ------ | ---- | ---------------- |
  | code   | int  | 返回码           |
  | msg    | str  | 状态码对应的信息 |
  | resp   | dict | 响应数据主体     |

  + resp

    | 参数名  | 类型      | 说明                       |
    | ------- | --------- | -------------------------- |
    | running | List[str] | 包含正在运行应用名称的系列 |

+ 返回示例：

  ```json
  {
      "code": 200,
      "msg": "xxxx",
      "resp": {
          "running": [
              "mysql",
              "docker"
          ]
      }
  }
  ```


#### 4.1.4、/v1/agent/host/info

+ 描述：获取安装agent主机的信息，包含系统版本，BIOS版本，内核版本，CPU信息以及内存信息。

+ HTTP请求方式：POST

+ 数据提交方式：application/json

+ 请求参数：

  | 参数名    | 必选 | 类型      | 说明                                             |
  | --------- | ---- | --------- | ------------------------------------------------ |
  | info_type | True | List[str] | 需采集信息的名称,目前仅支持cpu、disk、memory、os |

+ 请求参数示例：

  ```json
  ["os", "cpu","memory", "disk"]
  ```

+ 返回体参数：

  | 参数名 | 类型 | 说明             |
  | ------ | ---- | ---------------- |
  | code   | int  | 返回码           |
  | msg    | str  | 状态码对应的信息 |
  | resp   | dict | 响应数据主体     |

  resp

    | 参数名 | 类型       | 说明     |
    | ------ | ---------- | -------- |
    | cpu    | dict       | cpu信息  |
    | memory | dict       | 内存信息 |
    | os     | dict       | OS信息   |
    | disk   | List[dict] | 硬盘信息 |

    cpu

    | 参数名       | 类型 | 说明            |
    | ------------ | ---- | --------------- |
    | architecture | str  | CPU架构         |
    | core_count   | int  | 核心数          |
    | l1d_cache    | str  | 1级数据缓存大小 |
    | l1i_cache    | str  | 1级指令缓存大小 |
    | l2_cache     | str  | 2级缓存大小     |
    | l3_cache     | str  | 3级缓存大小     |
    | model_name   | str  | 模式名称        |
    | vendor_id    | str  | 厂商ID          |

    memory

    | 参数名 | 类型       | 说明           |
    | ------ | ---------- | -------------- |
    | size   | str        | 总内存大小     |
    | total  | int        | 内存条数量     |
    | info   | List[dict] | 所有内存条信息 |

      info

      | 参数名       | 类型 | 说明     |
      | ------------ | ---- | -------- |
      | size         | str  | 内存大小 |
      | type         | str  | 类型     |
      | speed        | str  | 速度     |
      | manufacturer | str  | 厂商     |

    os

    | 参数名       | 类型 | 说明     |
    | ------------ | ---- | -------- |
    | bios_version | str  | bios版本 |
    | os_version   | str  | 系统名称 |
    | kernel       | str  | 内核版本 |

+ 返回示例：

  ```json
  {
      "code": 200,
      "msg": "operate success",
      "resp": {
          "cpu": {
              "architecture": "aarch64",
              "core_count": "128",
              "l1d_cache": "8 MiB (128 instances)",
              "l1i_cache": "8 MiB (128 instances)",
              "l2_cache": "64 MiB (128 instances)",
              "l3_cache": "128 MiB (4 instances)",
              "model_name": "Kunpeng-920",
              "vendor_id": "HiSilicon"
          },
          "memory": {
              "info": [
                  {
                      "manufacturer": "Hynix",
                      "size": "16 GB",
                      "speed": "2933 MT/s",
                      "type": "DDR4"
                  },
                  {
                      "manufacturer": "Hynix",
                      "size": "16 GB",
                      "speed": "2933 MT/s",
                      "type": "DDR4"
                  }
              ],
              "size": "32G",
              "total": 2
          },
          "os": {
              "bios_version": "1.82",
              "kernel": "5.10.0-60.18.0.50",
              "os_version": "openEuler 22.03 LTS"   
          },
          "disk": [
              {
                  "capacity": "xxGB",
                  "model": "xxxxxx"
              }
              ]
      }
  }
  ```

#### 4.1.5、/v1/agent/plugin/info

+ 描述：获取主机的插件运行情况，目前仅支持gala-gopher插件。

+ HTTP请求方式：GET

+ 数据提交方式：query

+ 请求参数：

  | 参数名 | 必选 | 类型 | 说明 |
  | ------ | ---- | ---- | ---- |
  |        |      |      |      |

+ 请求参数示例：

  | 参数名 | 参数值 |
  | ------ | ------ |
  |        |        |

+ 返回体参数：

  | 参数名 | 类型       | 说明             |
  | ------ | ---------- | ---------------- |
  | code   | int        | 返回码           |
  | msg    | str        | 状态码对应的信息 |
  | resp   | List[dict] | 响应数据主体     |

  resp

  | 参数名        | 类型       | 说明               |
  | ------------- | ---------- | ------------------ |
  | plugin_name   | str        | 插件名称           |
  | collect_items | list       | 插件采集项运行情况 |
  | is_installed  | str        | 状态码对应的信息   |
  | resource      | List[dict] | 插件资源使用情况   |
  | status        | str        | 插件运行状态       |

    resource

    | 参数名        | 类型 | 说明       |
    | ------------- | ---- | ---------- |
    | name          | str  | 资源名称   |
    | current_value | str  | 资源使用值 |
    | limit_value   | str  | 资源限制值 |

+ 返回示例：

  ```json
  {
      "code": 200,
      "msg": "operate success",
      "resp": [
          {
              "collect_items": [
                  {
                      "probe_name": "system_tcp",
                      "probe_status": "off",
                      "support_auto": false
                  },
                  {
                      "probe_name": "haproxy",
                      "probe_status": "auto",
                      "support_auto": true
                  },
                  {
                      "probe_name": "nginx",
                      "probe_status": "auto",
                      "support_auto": true
                  },
              ],
              "is_installed": true,
              "plugin_name": "gala-gopher",
              "resource": [
                  {
                      "current_value": "0.0%",
                      "limit_value": null,
                      "name": "cpu"
                  },
                  {
                      "current_value": "13 MB",
                      "limit_value": null,
                      "name": "memory"
                  }
              ],
              "status": "active"
          }
      ]
  }
  ```

#### 4.1.6、/v1/agent/file/collect

+ 描述：采集目标配置文件内容、文件权限、文件所属用户等信息。当前仅支持读取小于1M，无执行权限，且支持UTF8编码的文本文件

+ HTTP请求方式：POST

+ 数据提交方式：application/json

+ 请求参数：

  | 参数名          | 必选 | 类型      | 说明                     |
  | --------------- | ---- | --------- | ------------------------ |
  | configfile_path | True | List[str] | 需采集文件完整路径的序列 |

+ 请求参数示例：

  ```json
  [ "/home/test.conf", "/home/test.ini", "/home/test.json"]
  ```

+ 返回体参数：

  | 参数名        | 类型       | 说明             |
  | ------------- | ---------- | ---------------- |
  | infos         | List[dict] | 文件采集信息     |
  | success_files | List[str]  | 采集成功文件列表 |
  | fail_files    | List[str]  | 采集失败文件列表 |

  infos

  | 参数名    | 类型 | 说明     |
  | --------- | ---- | -------- |
  | path      | str  | 文件路径 |
  | content   | str  | 文件内容 |
  | file_attr | dict | 文件属性 |

    file_attr

    | 参数名 | 类型 | 说明         |
    | ------ | ---- | ------------ |
    | mode   | str  | 文件类型权限 |
    | owner  | str  | 文件所属用户 |
    | group  | str  | 文件所属群组 |

+ 返回示例：

  ```json
  {
      "infos": [
          {
              "content": "this is a test file",
              "file_attr": {
                  "group": "root",
                  "mode": "0644",
                  "owner": "root"
              },
              "path": "/home/test.txt"
          }
      ],
      "success_files": [
          "/home/test.txt"
      ],
      "fail_files": [
          "/home/test.txt"
      ]
  }
  ```

#### 4.1.7、/v1/agent/collect/items/change

+ 描述：更改插件采集项的采集状态，当前仅支持对gala-gopher采集项的更改，gala-gopher采集项可在配置文件中查看`/opt/gala-gopher/gala-gopher.conf`

+ HTTP请求方式：POST

+ 数据提交方式：application/json

+ 请求参数：

  | 参数名      | 必选 | 类型 | 说明                       |
  | ----------- | ---- | ---- | -------------------------- |
  | plugin_name | True | dict | 插件采集项预期修改结果数据 |

  plugin_name

  | 参数名       | 必选 | 类型   | 说明               |
  | ------------ | ---- | ------ | ------------------ |
  | collect_item | True | string | 采集项预期修改结果 |

+ 请求参数示例：

  ```json
  {
      "gala-gopher":{
          "redis":"auto",
          "system_inode":"on",
          "tcp":"on",
          "haproxy":"auto"
      }
  } 
  ```

+ 返回体参数：

  | 参数名 | 类型       | 说明             |
  | ------ | ---------- | ---------------- |
  | code   | int        | 返回码           |
  | msg    | str        | 状态码对应的信息 |
  | resp   | List[dict] | 响应数据主体     |

  + resp

    | 参数名      | 类型 | 说明               |
    | ----------- | ---- | ------------------ |
    | plugin_name | dict | 对应采集项修改结果 |

    + plugin_name

      | 参数名  | 类型      | 说明             |
      | ------- | --------- | ---------------- |
      | success | List[str] | 修改成功的采集项 |
      | failure | List[str] | 修改失败的采集项 |

+ 返回示例：

  ```json
  {
      "code": 200,
      "msg": "operate success",
      "resp": {
          "gala-gopher": {
              "failure": [
                  "redis"
              ],
              "success": [
                  "system_inode",
                  "tcp",
                  "haproxy"
              ]
          }
      }
  }
  ```

## FAQ

1. 若有报错，请查看日志/var/log/aops/aops.log，根据日志中相关报错提示解决问题，并重启服务。

2. 建议项目在Python3.7以上环境运行，安装Python依赖库时需要注意其版本。

3. access_token值可在注册完成后，从`/etc/aops/agent.conf`文件中获取。

4. 对于插件CPU，以及内存的资源限制目前通过在插件对应service文件中的Service节点下添加MemoryHigh和CPUQuota标签实现。

   如对gala-gopher内存限制为40M，CPU限制为20%。

   ```ini
   [Unit]
   Description=a-ops gala gopher service
   After=network.target
   
   [Service]
   Type=exec
   ExecStart=/usr/bin/gala-gopher
   Restart=on-failure
   RestartSec=1
   RemainAfterExit=yes
   ;尽可能限制该单元中的进程最多可以使用多少内存，该限制允许突破，但突破限制后，进程运行速度会收到限制，并且系统会尽可能回收超出的内存
   ;选项值可以是以字节为单位的 绝对内存大小(可以使用以1024为基数的 K, M, G, T 后缀)， 也可以是以百分比表示的相对内存大小
   MemoryHigh=40M
   ;为此单元的进程设置CPU时间限额，必须设为一个以"%"结尾的百分数， 表示该单元最多可使用单颗CPU总时间的百分之多少
   CPUQuota=20%
   
   [Install]
   WantedBy=multi-user.target
   ```

# gala-gopher使用手册

gala-gopher作为数据采集模块提供OS级的监控能力，支持动态加 /卸载探针，可无侵入式地集成第三方探针，快速扩展监控范围。

本文介绍如何部署和使用gala-gopher服务。

## 安装

挂载repo源：

```basic
[oe-2309]      # openEuler 2309 官方发布源
name=oe2309
baseurl=http://119.3.219.20:82/openEuler:/23.09/standard_x86_64
enabled=1
gpgcheck=0
priority=1

[oe-2309:Epol] # openEuler 2309：Epol 官方发布源
name=oe2309_epol
baseurl=http://119.3.219.20:82/openEuler:/23.09:/Epol/standard_x86_64/
enabled=1
gpgcheck=0
priority=1
```

安装gala-gopher：

```bash
# yum install gala-gopher
```

## 配置

### 配置介绍

gala-gopher配置文件为`/opt/gala-gopher/gala-gopher.conf`，该文件配置项说明如下(省略无需用户配置的部分)。

如下配置可以根据需要进行修改：

- global：gala-gopher全局配置信息。
    - log_directory：gala-gopher日志文件名。
    - pin_path：ebpf探针共享map存放路径（建议维持默认配置）。
- metric：指标数据metrics输出方式配置。
    - out_channel：metrics输出通道，支持配置web_server|kafka，配置为空则输出通道关闭。
    - kafka_topic：若输出通道为kafka，此为topic配置信息。
- event：异常事件event输出方式配置。
    - out_channel：event输出通道，支持配置logs|kafka，配置为空则输出通道关闭。
    - kafka_topic：若输出通道为kafka，此为topic配置信息。
- meta：元数据metadata输出方式配置。
    - out_channel：metadata输出通道，支持logs|kafka，配置为空则输出通道关闭。
    - kafka_topic：若输出通道为kafka，此为topic配置信息。
- imdb：cache缓存规格配置。
    - max_tables_num：最大的cache表个数，/opt/gala-gopher/meta目录下每个meta对应一个表。
    - max_records_num：每张cache表最大记录数，通常每个探针在一个观测周期内产生至少1条观测记录。
    - max_metrics_num：每条观测记录包含的最大的metric指标个数。
    - record_timeout：cache表老化时间，若cache表中某条记录超过该时间未刷新则删除记录，单位为秒。
- web_server：输出通道web_server配置。
    - port：监听端口。
- kafka：输出通道kafka配置。
    - kafka_broker：kafka服务器的IP和port。
- logs：输出通道logs配置。
    - metric_dir：metrics指标数据日志路径。
    - event_dir：异常事件数据日志路径。
    - meta_dir：metadata元数据日志路径。
    - debug_dir：gala-gopher运行日志路径。
- probes：native探针配置。
    - name：探针名称，要求与native探针名一致，如example.probe 探针名为example。
    - param ：探针启动参数，支持的参数详见[启动参数介绍表](#启动参数介绍)。
    - switch：探针是否启动，支持配置 on | off。
- extend_probes ：第三方探针配置。
    - name：探针名称。
    - command：探针启动命令。
    - param：探针启动参数，支持的参数详见[启动参数介绍表](#启动参数介绍)。
    - start_check：switch为auto时，需要根据start_check执行结果判定探针是否需要启动。
    - switch：探针是否启动，支持配置on | off | auto，auto会根据start_check判定结果决定是否启动探针。

### 启动参数介绍

| 参数项 | 含义                                                         |
| ------ | ------------------------------------------------------------ |
| -l     | 是否开启异常事件上报                                         |
| -t     | 采样周期，单位为秒，默认配置为探针5s上报一次数据             |
| -T     | 延迟时间阈值，单位为ms，默认配置为0ms                        |
| -J     | 抖动时间阈值，单位为ms，默认配置为0ms                        |
| -O     | 离线时间阈值，单位为ms，默认配置为0ms                        |
| -D     | 丢包阈值，默认配置为0(个)                                    |
| -F     | 配置为`task`表示按照`task_whitelist.conf`过滤，配置为具体进程的pid表示仅监控此进程 |
| -P     | 指定每个探针加载的探测程序范围，目前tcpprobe、taskprobe探针涉及 |
| -U     | 资源利用率阈值(上限)，默认为0%                               |
| -L     | 资源利用率阈值(下限)，默认为0%                               |
| -c     | 指示探针(tcp)是否标识client_port，默认配置为0(否)            |
| -N     | 指定探针(ksliprobe)的观测进程名，默认配置为NULL              |
| -p     | 指定待观测进程的二进制文件路径，比如nginx_probe，通过 -p /user/local/sbin/nginx指定nginx文件路径，默认配置为NULL |
| -w     | 筛选应用程序监控范围，如-w  /opt/gala-gopher/task_whitelist.conf，用户可将需要监控的程序名写入task_whitelist.conf中，默认配置为NULL表示不筛选 |
| -n     | 指定某个网卡挂载tc ebpf，默认配置为NULL表示所有网卡均挂载，示例： -n eth0 |

### 配置文件示例

- 配置选择数据输出通道：

  ```yaml
  metric =
  {
      out_channel = "web_server";
      kafka_topic = "gala_gopher";
  };
  
  event =
  {
      out_channel = "kafka";
      kafka_topic = "gala_gopher_event";
  };
  
  meta =
  {
      out_channel = "kafka";
      kafka_topic = "gala_gopher_metadata";
  };
  ```

- 配置kafka和webServer：

  ```yaml
  web_server =
  {
      port = 8888;
  };
  
  kafka =
  {
      kafka_broker = "<kafka服务器IP>:9092";
  }；
  ```

- 选择开启的探针，示例如下：

  ```yaml
  probes =
  (
      {
          name = "system_infos";
          param = "-t 5 -w /opt/gala-gopher/task_whitelist.conf -l warn -U 80";
          switch = "on";
      },
  );
  extend_probes =
  (
      {
          name = "tcp";
          command = "/opt/gala-gopher/extend_probes/tcpprobe";
          param = "-l warn -c 1 -P 7";
          switch = "on";
      }
  );
  ```

## 启动

配置完成后，执行如下命令启动gala-gopher。

```bash
# systemctl start gala-gopher.service
```

查询gala-gopher服务状态。

```bash
# systemctl status gala-gopher.service
```

若显示结果如下，说明服务启动成功。需要关注开启的探针是否已启动，如果探针线程不存在，请检查配置文件及gala-gopher运行日志文件。

![gala-gopher成功启动状态](./figures/gala-gopher成功启动状态.png)

> 说明：gala-gopher部署和运行均需要root权限。

## 使用方法

### 外部依赖软件部署

![gopher软件架构图](./figures/gopher软件架构图.png)

如上图所示，绿色部分为gala-gopher的外部依赖组件。gala-gopher会将指标数据metrics输出到promethous，将元数据metadata、异常事件event输出到kafka，灰色部分的gala-anteater和gala-spider会从promethous和kafka获取数据。

> 说明：安装kafka、promethous软件包时，需要从官网获取安装包进行部署。

### 输出数据

- **指标数据metrics**

  Promethous Server内置了Express Browser UI，用户可以通过PromQL查询语句查询指标数据内容。详细教程参见官方文档：[Using the expression browser](https://prometheus.io/docs/prometheus/latest/getting_started/#using-the-expression-browser)。示例如下：

  指定指标名称为`gala_gopher_tcp_link_rcv_rtt`，UI显示的指标数据为：

  ```basic
  gala_gopher_tcp_link_rcv_rtt{client_ip="x.x.x.165",client_port="1234",hostname="openEuler",instance="x.x.x.172:8888",job="prometheus",machine_id="1fd3774xx",protocol="2",role="0",server_ip="x.x.x.172",server_port="3742",tgid="1516"} 1
  ```

- **元数据metadata**

  可以直接从kafka消费topic为`gala_gopher_metadata`的数据来看。示例如下：

  ```bash
  # 输入请求
  ./bin/kafka-console-consumer.sh --bootstrap-server x.x.x.165:9092 --topic gala_gopher_metadata
  # 输出数据
  {"timestamp": 1655888408000, "meta_name": "thread", "entity_name": "thread", "version": "1.0.0", "keys": ["machine_id", "pid"], "labels": ["hostname", "tgid", "comm", "major", "minor"], "metrics": ["fork_count", "task_io_wait_time_us", "task_io_count", "task_io_time_us", "task_hang_count"]}
  ```

- **异常事件event**

  可以直接从kafka消费topic为`gala_gopher_event`的数据来看。示例如下：

  ```bash
  # 输入请求
  ./bin/kafka-console-consumer.sh --bootstrap-server x.x.x.165:9092 --topic gala_gopher_event
  # 输出数据
  {"timestamp": 1655888408000, "meta_name": "thread", "entity_name": "thread", "version": "1.0.0", "keys": ["machine_id", "pid"], "labels": ["hostname", "tgid", "comm", "major", "minor"], "metrics": ["fork_count", "task_io_wait_time_us", "task_io_count", "task_io_time_us", "task_hang_count"]}
  ```

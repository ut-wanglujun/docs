# dnf插件命令使用手册

将A-ops apollo安装部署完成后，可使用dnf命令调用A-ops apollo中的热补丁操作，命令包含热补丁扫描（dnf hot-updateinfo），热补丁状态设置及查询（dnf hotpatch ），热补丁应用（dnf hotupgrade），本文将介绍上述命令的具体使用方法。

## 热补丁扫描

`hot-updateinfo`命令支持扫描热补丁并指定cve查询相关热补丁，命令使用方式如下：

```shell
dnf hot-updateinfo list cves [--cve [cve_id]]

General DNF options:
  -h, --help, --help-cmd
                        show command help
  --cve CVES, --cves CVES
                        Include packages needed to fix the given CVE, in updates

```

- `--list`

1. 查询主机所有可修复的cve和对应的冷/热补丁。

```shell
[root@localhost dnf]# dnf hot-updateinfo list cves
# cve-id   level    cold-patch   hot-patch
Last metadata expiration check: 0:54:46 ago on 2023年03月16日 星期四 09时40分27秒.
CVE-2022-3080  Important/Sec.  bind-libs-9.16.23-10.oe2203.aarch64   patch-bind-libs-9.16.23-09-name-1-111.aarch64
CVE-2021-25220 Moderate/Sec.   bind-9.16.23-10.oe2203.aarch64        -
CVE-2022-1886  Critical/Sec.   vim-common-8.2-39.oe2203.aarch64      patch-vim-common-8.2-38-name-1-233.aarch64
CVE-2022-1725  Low/Sec.        vim-minimal-8.2-58.oe2203.aarch64     patch-vim-minimal-8.2-57-name-2-11.aarch64
```

2. 指定cve查询对应的冷/热补丁。

```shell
[root@localhost dnf]# dnf hot-updateinfo list cves --cve CVE-2022-3080
# cve-id   level    cold-patch   hot-patch
Last metadata expiration check: 0:54:46 ago on 2023年03月16日 星期四 09时40分27秒.
CVE-2022-3080  Important/Sec.  bind-libs-9.16.23-10.oe2203.aarch64   patch-bind-libs-9.16.23-09-name-1-111.aarch64
```

3. cve不存在时列表为空。

```shell
[root@localhost dnf]# dnf hot-updateinfo list cves --cve CVE-2022-3089
# cve-id   level    cold-patch   hot-patch
Last metadata expiration check: 0:54:46 ago on 2023年03月16日 星期四 09时40分27秒.
```

## 热补丁状态及转换图

- 热补丁状态图

  NOT-APPLIED: 热补丁尚未安装。

  DEACTIVED: 热补丁已被安装。

  ACTIVED: 热补丁已被激活。

  ACCEPT: 热补丁已被接受，后续重启后会被自动应用。

  ![热补丁状态转换图](./figures/热补丁状态图.png)

## 热补丁状态查询和切换

`hotpatch`hotptach命令支持查询、切换热补丁的状态，命令使用方式如下：

```shell
dnf hotpatch 

General DNF options:
  -h, --help, --help-cmd
                        show command help
  --cve CVES, --cves CVES
                        Include packages needed to fix the given CVE, in updates

Hotpatch command-specific opetions:
  --list [{cve, cves}]  show list of hotpatch
  --apply APPLY_NAME apply hotpatch
  --remove REMOVE_NAME remove hotpatch
  --active ACTIVE_NAME action hotpatch
  --deactive DEACTIVE_NAME
                       deactive hotpatch
  --accept ACCEPT_NAME accept hotpatch
```

1. 使用`dnf hotpatch --list`命令查询当前系统中可使用的热补丁状态并展示。

    ```shell
    [root@localhost dnf]# dnf hotpatch --list
    Last metadata expiration check: 0:54:46 ago on 2023年03月16日 星期四 09时40分27秒.
    base-pkg/hotpatch   status
    redis-6.2.5-1/HP001 NOT-APPLIED
    redis-6.2.5-1/HP001 NOT-APPLIED
    redis-6.2.5-1/HP002 ACTIVED
    redis-6.2.5-1/HP002 ACTIVED
    ```

2. 使用`dnf hotpatch --list cves`查询漏洞（CVE-id）对应热补丁及其状态并展示。

    ```shell
    [root@localhost dnf]# dnf hotpatch --list cves
    Last metadata expiration check: 0:54:46 ago on 2023年03月16日 星期四 09时40分27秒.
    CVE-id         base-pkg/hotpatch   status
    CVE-2023-1111  redis-6.2.5-1/HP001 NOT-APPLIED
    CVE-2023-1112  redis-6.2.5-1/HP001 NOT-APPLIED
    CVE-2023-2221  redis-6.2.5-1/HP002 ACTIVED
    CVE-2023-2222  redis-6.2.5-1/HP002 ACTIVED
    ```

3. 使用`dnf hotpatch --list cves --cve <CVE-id>`筛选指定CVE对应的热补丁及其状态并展示。

    ```shell
    [root@localhost dnf]# dnf hotpatch --list cves --cve CVE-2023-1111
    Last metadata expiration check: 0:54:46 ago on 2023年03月16日 星期四 09时40分27秒.
    CVE-id         base-pkg/hotpatch   status
    CVE-2023-1111  redis-6.2.5-1/HP001 NOT-APPLIED
    ```

4. 使用`dnf hotpatch --list cves --cve <CVE-id>`查询无结果时展示为空。

    ```shell
    [root@localhost dnf]# dnf hotpatch --list cves --cve CVE-2023-1
    Last metadata expiration check: 0:54:46 ago on 2023年03月16日 星期四 09时40分27秒.
    ```

5. 使用`dnf hotpatch --apply <patch name>`命令应用热补丁，可使用`syscare list`查询应用后的状态变化，变化逻辑见上文的热补丁状态转换图。

    ```shell
    [root@openEuler dnf-plugins]# dnf hotpatch --apply redis-6.2.5-1/HP2
    Last metadata expiration check: 2:38:51 ago on 2023年05月25日 星期四 13时49分28秒.
    Gonna apply this hot patch: redis-6.2.5-1/HP2
    apply hot patch 'redis-6.2.5-1/HP2' succeed
    [root@openEuler dnf-plugins]# syscare list
    Uuid                                     Name                                     Status      
    25209ddc-b1e4-48e0-b715-e759ec8db401     redis-6.2.5-1/HP2                        ACTIVED
    ```

6. 使用`dnf hotpatch --deactive <patch name>`停用热补丁，可使用`syscare list`查询停用后的状态变化，变化逻辑见上文的热补丁状态转换图。

    ```shell
    [root@openEuler dnf-plugins]# dnf hotpatch --deactive  redis-6.2.5-1/HP2
    Last metadata expiration check: 2:39:10 ago on 2023年05月25日 星期四 13时49分28秒.
    Gonna deactive this hot patch: redis-6.2.5-1/HP2
    deactive hot patch 'redis-6.2.5-1/HP2' succeed
    [root@openEuler dnf-plugins]# syscare list
    Uuid                                     Name                                     Status      
    25209ddc-b1e4-48e0-b715-e759ec8db401     redis-6.2.5-1/HP2                        DEACTIVED
    ```

7. 使用`dnf hotpatch --remove <patch name>`删除热补丁，可使用`syscare list`查询删除后的状态变化，变化逻辑见上文的热补丁状态转换图。

    ```shell
    [root@openEuler dnf-plugins]# dnf hotpatch --remove  redis-6.2.5-1/HP2
    Last metadata expiration check: 2:53:25 ago on 2023年05月25日 星期四 13时49分28秒.
    Gonna remove this hot patch: redis-6.2.5-1/HP2
    remove hot patch 'redis-6.2.5-1/HP2' succeed
    [root@openEuler dnf-plugins]# syscare list
    Uuid                                     Name                                     Status      
    25209ddc-b1e4-48e0-b715-e759ec8db401     redis-6.2.5-1/HP2                        NOT-APPLIED
    ```

8. 使用`dnf hotpatch --active <patch name>`激活热补丁，可使用`syscare list`查询激活后的状态变化，变化逻辑见上文的热补丁状态转换图。

    ```shell
    [root@openEuler dnf-plugins]# dnf hotpatch --active  redis-6.2.5-1/HP2
    Last metadata expiration check: 2:53:37 ago on 2023年05月25日 星期四 13时49分28秒.
    Gonna active this hot patch: redis-6.2.5-1/HP2
    active hot patch 'redis-6.2.5-1/HP2' failed, remain original status.
    [root@openEuler dnf-plugins]# syscare list
    Uuid                                     Name                                     Status      
    25209ddc-b1e4-48e0-b715-e759ec8db401     redis-6.2.5-1/HP2                        ACTIVED
    ```

9. 使用`dnf hotpatch --accept <patch name>`接收热补丁，可使用`syscare list`查询接收后的状态变化，变化逻辑见上文的热补丁状态转换图。

    ```shell
    [root@openEuler dnf-plugins]# dnf hotpatch --accept  redis-6.2.5-1/HP2
    Last metadata expiration check: 2:53:25 ago on 2023年05月25日 星期四 13时49分28秒.
    Gonna accept this hot patch: redis-6.2.5-1/HP2
    remove hot patch 'redis-6.2.5-1/HP2' succeed
    [root@openEuler dnf-plugins]# syscare list
    Uuid                                     Name                                     Status      
    25209ddc-b1e4-48e0-b715-e759ec8db401     redis-6.2.5-1/HP2                        ACCEPTED
    ```

## 热补丁应用

`hotupgrade`命令根据cve id和热补丁名称进行热补丁修复，同时也支持全量修复。命令使用方式如下：

```shell
dnf hotupgrade [--cve [cve_id]] [SPEC ...]

General DNF options:
  -h, --help, --help-cmd
                        show command help
  --cve CVES, --cves CVES
                        Include packages needed to fix the given CVE, in updates

command-specific options:
  SPEC            Hotpatch specification
```

- Case1： 当热补丁已经安装时，使用`dnf hotupgrade`安装所有存在的热补丁。

  ```shell
  [root@openEuler aops-ceres]# dnf hotupgrade
  Last metadata expiration check: 4:04:34 ago on 2023年06月02日 星期五 06时33分41秒.
  Gonna apply these hot patches:['patch-redis-6.2.5-1-HP001-1-1.x86_64', 'patch-redis-6.2.5-1-HP002-1-1.x86_64']
  The target package 'redis-6.2.5-1' has a hotpatch 'HP001' applied
  Gonna remove these hot patches: ['redis-6.2.5-1/HP001']
  Remove hot patch redis-6.2.5-1/HP001.
  Package patch-redis-6.2.5-1-HP001-1-1.x86_64 is already installed.
  Package patch-redis-6.2.5-1-HP002-1-1.x86_64 is already installed.
  Dependencies resolved.
  Nothing to do.
  Complete!
  Applying hot patch
  Apply hot patch succeed: redis-6.2.5-1/HP001.
  Apply hot patch failed: redis-6.2.5-1/HP002.
  ```

- Case2： 热补丁未安装时，使用`dnf hotupgrade`命令安装存在的所有热补丁，将显示安装信息。
  
  ```shell
  [root@openEuler A-ops]# dnf hotupgrade 
  Last metadata expiration check: 4:13:16 ago on 2023年06月02日 星期五 06时33分41秒.
  Gonna apply these hot patches:['patch-redis-6.2.5-1-HP002-1-1.x86_64', 'patch-redis-6.2.5-1-HP001-1-1.x86_64']
  Package patch-redis-6.2.5-1-HP002-1-1.x86_64 is already installed.
  Dependencies resolved.
  xxxx(Install messgaes)
  Is this ok [y/N]: y
  Downloading Packages:
  xxxx(Install process)
  Complete!                                                                                                                                        

  Applying hot patch
  Apply hot patch succeed: redis-6.2.5-1/HP001.
  ```

- Case3： 使用`dnf hotupgrade <patch_name>`升级指定热补丁包。
  
  ```shell
  [root@openEuler ~]# dnf hotupgrade patch-redis-6.2.5-1-HP001-1-1.x86_64
  Last metadata expiration check: 0:07:49 ago on 2023年06月08日 星期四 12时03分46秒.
  Package patch-redis-6.2.5-1-HP001-1-1.x86_64 is already installed.
  Dependencies resolved.
  Nothing to do.
  Complete!
  Applying hot patch
  Apply hot patch succeed: redis-6.2.5-1/HP001.
  ```

- `--cve`

    - Case1：使用`dnf hotupgrade --cve <cve_id>`指定cve_id安装指定CVE对应的热补丁。

    ```shell
    [root@localhost dnf]# dnf hotupgrade --cve CVE-2021-11
    Last metadata expiration check: xxx
    Dependencies resolved.
    xxxx(Install messgaes)
    Is this ok [y/N]: y
    Downloading Packages:
    xxxx(Install process)
    Complete!
    Applying hot patch
    Apply hot patch succeed: redis-6.2.5-1/HP001
    ```

    - Case2：使用`dnf hotupgrade --cve <cve_id>`指定cve_id安装时cve不存在。

      ```shell
      [root@localhost dnf]# dnf hotupgrade --cve CVE-2021-11
      Last metadata expiration check: xxx
      The cve doesnt exist: CVE-2021-11
      Error: No hot patches marked for install.
      ```

    - Case3：使用`dnf hotupgrade --cve <cve_id>`指定cve_id安装时，该CVE对应的低版本热补丁已安装时，删除低版本热补丁包，安装高版本热补丁包。

    ```shell
    [root@localhost dnf]# dnf hotupgrade --cve CVE-2021-22
    Last metadata expiration check: xxx
    The target package 'redis-6.2.5-1' has a hotpatch 'HP001' applied
    Gonna remove these hot patches: ['redis-6.2.5-1/HP001']
    Is this ok [y/N]: y
    Remove hot patch redis-6.2.5-1/HP001
    xxxx (install messages and process install)
    Apply hot patch
    apply hot patch succeed: redis-6.2.5-1/HP002
    ```

    - Case4：使用`dnf hotupgrade --cve <cve_id>`指定cve_id安装时，该CVE对应的最高版本热补丁包已存在。

    ```shell
    [root@localhost dnf]# dnf hotupgrade --cve CVE-2021-22
    Package patch -redis-6.2.5-1-HP002-1-1.x86_64 is already installed.
    Dependencies resolved.
    Nothing to do.
    Complete!
    Applying hot patch
    Apply hot patch succeed: redis-6.2.5-1/HP002
    ```

- `SPEC`

  ```shell
  [root@localhost dnf]# dnf hotupgrade bind-libs-hotpatch
  ```

子命令的输出根据不同的情况与"--cve"命令相同。

## 使用场景说明

本段落介绍上述命令的使用场景及顺序介绍，需要提前确认本机的热补丁repo源和相应冷补丁repo源已开启。

使用热补丁扫描命令查看本机待修复cve。

```shell
[root@openEuler aops-apollo_src]# dnf hot-updateinfo list cves
Last metadata expiration check: 0:00:38 ago on 2023年03月25日 星期六 11时53分46秒.
CVE-2023-22995 Important/Sec. python3-perf-5.10.0-136.22.0.98.oe2203sp1.x86_64       -
CVE-2023-26545 Important/Sec. python3-perf-5.10.0-136.22.0.98.oe2203sp1.x86_64       -
CVE-2022-40897 Important/Sec. python3-setuptools-59.4.0-5.oe2203sp1.noarch           -
CVE-2021-1     Important/Sec. redis-6.2.5-2.x86_64                                   patch-redis-6.2.5-1-HP001-1-1.x86_64
CVE-2021-11    Important/Sec. redis-6.2.5-2.x86_64                                   patch-redis-6.2.5-1-HP001-1-1.x86_64
CVE-2021-2     Important/Sec. redis-6.2.5-3.x86_64                                   patch-redis-6.2.5-1-HP002-1-1.x86_64
CVE-2021-22    Important/Sec. redis-6.2.5-3.x86_64                                   patch-redis-6.2.5-1-HP002-1-1.x86_64
CVE-2021-33    Important/Sec. redis-6.2.5-4.x86_64                                   -
CVE-2021-3     Important/Sec. redis-6.2.5-4.x86_64                                   -
CVE-2022-38023 Important/Sec. samba-client-4.17.2-5.oe2203sp1.x86_64                 -
CVE-2022-37966 Important/Sec. samba-client-4.17.2-5.oe2203sp1.x86_64                 -
```

找到提供热补丁的相应cve，发现CVE-2021-1、CVE-2021-11、CVE-2021-2和CVE-2021-22可用热补丁修复。

在安装补丁前测试功能，基于redis.conf配置文件启动redis服务。

```shell
[root@openEuler redis_patch]# sudo redis-server ./redis.conf &
[1] 285075
[root@openEuler redis_patch]# 285076:C 25 Mar 2023 12:09:51.503 # oO0OoO0OoO0Oo Redis is starting oO0OoO0OoO0Oo
285076:C 25 Mar 2023 12:09:51.503 # Redis version=255.255.255, bits=64, commit=00000000, modified=0, pid=285076, just started
285076:C 25 Mar 2023 12:09:51.503 # Configuration loaded
285076:M 25 Mar 2023 12:09:51.504 * Increased maximum number of open files to 10032 (it was originally set to 1024).
285076:M 25 Mar 2023 12:09:51.504 * monotonic clock: POSIX clock_gettime
                _._                                                  
           _.-``__ ''-._                                             
      _.-``    `.  `_.  ''-._           Redis 255.255.255 (00000000/0) 64 bit
  .-`` .-```.  ```\/    _.,_ ''-._                                  
 (    '      ,       .-`  | `,    )     Running in standalone mode
 |`-._`-...-` __...-.``-._|'` _.-'|     Port: 6380
 |    `-._   `._    /     _.-'    |     PID: 285076
  `-._    `-._  `-./  _.-'    _.-'                                   
 |`-._`-._    `-.__.-'    _.-'_.-'|                                  
 |    `-._`-._        _.-'_.-'    |           https://redis.io       
  `-._    `-._`-.__.-'_.-'    _.-'                                   
 |`-._`-._    `-.__.-'    _.-'_.-'|                                  
 |    `-._`-._        _.-'_.-'    |                                  
  `-._    `-._`-.__.-'_.-'    _.-'                                   
      `-._    `-.__.-'    _.-'                                       
          `-._        _.-'                                           
              `-.__.-'                                               

285076:M 25 Mar 2023 12:09:51.505 # Server initialized
285076:M 25 Mar 2023 12:09:51.505 # WARNING overcommit_memory is set to 0! Background save may fail under low memory condition. To fix this issue add 'vm.overcommit_memory = 1' to /etc/sysctl.conf and then reboot or run the command 'sysctl vm.overcommit_memory=1' for this to take effect.
285076:M 25 Mar 2023 12:09:51.506 * Ready to accept connections

```

安装前测试功能。

```shell
[root@openEuler ~]# telnet 127.0.0.1 6380
Trying 127.0.0.1...
Connected to 127.0.0.1.
Escape character is '^]'.

*100

-ERR Protocol error: expected '$', got ' '
Connection closed by foreign host.
```

指定修复CVE-2021-1，确认关联到对应的热补丁包，显示安装成功。

```shell
[root@openEuler aops-apollo_src]# dnf hotupgrade --cve CVE-2021-1
Last metadata expiration check: 0:05:19 ago on 2023年03月25日 星期六 11时53分46秒.
Package patch-redis-6.2.5-1-HP001-1-1.x86_64 is already installed.
Dependencies resolved.
Nothing to do.
Complete!
Applying hot patch
Apply hot patch succeed: redis-6.2.5-1/HP001.
```

使用syscare确认该热补丁是否安装成功，确认Status为ACTIVED。

```shell
[root@openEuler ~]# syscare list
Uuid                                     Name                                     Status      
cf47649c-b370-4f5a-a914-d2ca4d8f1f3a     redis-6.2.5-1/HP001                      ACTIVED
```

确认该cve是否已被修复，由于CVE-2021-1所使用的热补丁包patch-redis-6.2.5-1-HP001-1-1.x86_64同样修复CVE-2021-11，CVE-2021-1和CVE-2021-11都不予显示。

```shell
[root@openEuler dnf-plugins]# dnf hot-updateinfo list cves
Last metadata expiration check: 0:08:48 ago on 2023年03月25日 星期六 11时53分46秒.
CVE-2023-22995 Important/Sec. python3-perf-5.10.0-136.22.0.98.oe2203sp1.x86_64       -
CVE-2023-1076  Important/Sec. python3-perf-5.10.0-136.22.0.98.oe2203sp1.x86_64       -
CVE-2023-26607 Important/Sec. python3-perf-5.10.0-136.22.0.98.oe2203sp1.x86_64       -
CVE-2022-40897 Important/Sec. python3-setuptools-59.4.0-5.oe2203sp1.noarch           -
CVE-2021-22    Important/Sec. redis-6.2.5-3.x86_64                                   patch-redis-6.2.5-1-HP002-1-1.x86_64
CVE-2021-2     Important/Sec. redis-6.2.5-3.x86_64                                   patch-redis-6.2.5-1-HP002-1-1.x86_64
CVE-2021-33    Important/Sec. redis-6.2.5-4.x86_64                                   -
CVE-2021-3     Important/Sec. redis-6.2.5-4.x86_64                                   -
CVE-2022-38023 Important/Sec. samba-client-4.17.2-5.oe2203sp1.x86_64                 -
CVE-2022-37966 Important/Sec. samba-client-4.17.2-5.oe2203sp1.x86_64                 -
```

激活后测试功能，对比激活前回显内容。

```shell
[root@openEuler ~]# telnet 127.0.0.1 6380
Trying 127.0.0.1...
Connected to 127.0.0.1.
Escape character is '^]'.

*100

-ERR Protocol error: unauthenticated multibulk length
Connection closed by foreign host.
```

由于热补丁还未开发完卸载功能，使用syscare指定Name手动卸载。

```shell
[root@openEuler ~]# syscare remove redis-6.2.5-1/HP001
[root@openEuler ~]# syscare list
Uuid                                     Name                                     Status      
cf47649c-b370-4f5a-a914-d2ca4d8f1f3a     redis-6.2.5-1/HP001                      NOT-APPLIED
```

使用热补丁扫描命令查看本机待修复cve，确认CVE-2021-1和CVE-2021-11正常显示。

```shell
[root@openEuler aops-apollo_src]# dnf hot-updateinfo list cves
Last metadata expiration check: 0:00:38 ago on 2023年03月25日 星期六 11时53分46秒.
CVE-2023-22995 Important/Sec. python3-perf-5.10.0-136.22.0.98.oe2203sp1.x86_64       -
CVE-2023-26545 Important/Sec. python3-perf-5.10.0-136.22.0.98.oe2203sp1.x86_64       -
CVE-2022-40897 Important/Sec. python3-setuptools-59.4.0-5.oe2203sp1.noarch           -
CVE-2021-1     Important/Sec. redis-6.2.5-2.x86_64                                   patch-redis-6.2.5-1-HP001-1-1.x86_64
CVE-2021-11    Important/Sec. redis-6.2.5-2.x86_64                                   patch-redis-6.2.5-1-HP001-1-1.x86_64
CVE-2021-2     Important/Sec. redis-6.2.5-3.x86_64                                   patch-redis-6.2.5-1-HP002-1-1.x86_64
CVE-2021-22    Important/Sec. redis-6.2.5-3.x86_64                                   patch-redis-6.2.5-1-HP002-1-1.x86_64
CVE-2021-33    Important/Sec. redis-6.2.5-4.x86_64                                   -
CVE-2021-3     Important/Sec. redis-6.2.5-4.x86_64                                   -
CVE-2022-38023 Important/Sec. samba-client-4.17.2-5.oe2203sp1.x86_64                 -
CVE-2022-37966 Important/Sec. samba-client-4.17.2-5.oe2203sp1.x86_64                 -
```

- case 1

指定安装热补丁包patch-redis-6.2.5-1-HP002-1-1.x86_64。

```shell
[root@openEuler aops-apollo_src]# dnf hotupgrade patch-redis-6.2.5-1-HP002-1-1.x86_64
Last metadata expiration check: 0:05:19 ago on 2023年03月25日 星期六 11时53分46秒.
Package patch-redis-6.2.5-1-HP002-1-1.x86_64 is already installed.
Dependencies resolved.
Nothing to do.
Complete!
Applying hot patch
Apply hot patch succeed: redis-6.2.5-1/HP002.
```

使用热补丁扫描命令查看本机待修复cve，由于patch-redis-6.2.5-1-HP002-1-1.x86_64对应的冷补丁redis-6.2.5-3.x86_64比redis-6.2.5-2.x86_64版本高，redis-6.2.5-2.x86_64对应的CVE-2021-1和CVE-2021-11，以及CVE-2021-2和CVE-2021-22都被修复。

```shell
[root@openEuler aops-apollo_src]# dnf hot-updateinfo list cves
Last metadata expiration check: 0:00:38 ago on 2023年03月25日 星期六 11时53分46秒.
CVE-2023-22995 Important/Sec. python3-perf-5.10.0-136.22.0.98.oe2203sp1.x86_64       -
CVE-2023-26545 Important/Sec. python3-perf-5.10.0-136.22.0.98.oe2203sp1.x86_64       -
CVE-2022-40897 Important/Sec. python3-setuptools-59.4.0-5.oe2203sp1.noarch           -
CVE-2021-33    Important/Sec. redis-6.2.5-4.x86_64                                   -
CVE-2021-3     Important/Sec. redis-6.2.5-4.x86_64                                   -
CVE-2022-38023 Important/Sec. samba-client-4.17.2-5.oe2203sp1.x86_64                 -
CVE-2022-37966 Important/Sec. samba-client-4.17.2-5.oe2203sp1.x86_64                 -
```

- case 2

查看热补丁repo源中repodata目录下的xxx-updateinfo.xml.gz，确认文件中的CVE-2021-33、CVE-2021-3相关信息。

```xml
<update from="openeuler.org" type="security" status="stable">
          <id>openEuler-SA-2022-3</id>
          <title>An update for mariadb is now available for openEuler-22.03-LTS</title>
          <severity>Important</severity>
          <release>openEuler</release>
          <issued date="2022-04-16"></issued>
          <references>
                  <reference href="https://nvd.nist.gov/vuln/detail/CVE-2021-46658" id="CVE-2021-3" title="CVE-2021-3" type="cve"></reference>
                  <reference href="https://nvd.nist.gov/vuln/detail/CVE-2021-46658" id="CVE-2021-33" title="CVE-2021-33" type="cve"></reference>
          </references>
          <description>patch-redis-6.2.5-2-HP001.(CVE-2022-24048)</description>
          <pkglist>
               <hot_patch_collection>
                    <name>openEuler</name>
                    <package id="CVE-2021-3" arch="aarch64" name="patch-redis-6.2.5-2-HP001" release="1" version="1">
                         <filename>patch-redis-6.2.5-2-HP001-1-1.aarch64.rpm</filename>
                    </package>
                    <package id="CVE-2021-3" arch="x86_64" name="patch-redis-6.2.5-2-HP001" release="1" version="1">
                         <filename>patch-redis-6.2.5-2-HP001-1-1.x86_64.rpm</filename>
                    </package>
               </hot_patch_collection>
          </pkglist>
  </update>
```

package中的name字段"patch-redis-6.2.5-2-HP001"的组成部分为：patch-源码包名-源码包版本-源码包release-热补丁包名，该热补丁包需要本机安装redis-6.2.5-2源码版本，检查本机redis安装版本。

```shell
[root@openEuler ~]# rpm -qa | grep redis
redis-6.2.5-1.x86_64
```

由于本机安装版本不匹配，该热补丁包名不显示，以'-'显示。

```shell
[root@openEuler aops-apollo_src]# dnf hot-updateinfo list cves
Last metadata expiration check: 0:00:38 ago on 2023年03月25日 星期六 11时53分46秒.
CVE-2023-22995 Important/Sec. python3-perf-5.10.0-136.22.0.98.oe2203sp1.x86_64       -
CVE-2023-26545 Important/Sec. python3-perf-5.10.0-136.22.0.98.oe2203sp1.x86_64       -
CVE-2022-40897 Important/Sec. python3-setuptools-59.4.0-5.oe2203sp1.noarch           -
CVE-2021-33    Important/Sec. redis-6.2.5-4.x86_64                                   -
CVE-2021-3     Important/Sec. redis-6.2.5-4.x86_64                                   -
CVE-2022-38023 Important/Sec. samba-client-4.17.2-5.oe2203sp1.x86_64                 -
CVE-2022-37966 Important/Sec. samba-client-4.17.2-5.oe2203sp1.x86_64                 -
```

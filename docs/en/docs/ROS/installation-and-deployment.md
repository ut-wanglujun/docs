# Installation and Deployment
## Software
* OS: openEuler 22.03 LTS SP2

## Hardware
* x86_64

## Preparing the Environment
* Install the openEuler OS by referring to the *openEuler 22.03 LTS SP2 Installation Guide*.

## 1. ROS2
### 1. ros-humble
#### 1. Installing ros-humble
1. Install ros-humble software package
```shell
[root@openEuler ~]# yum install openeuler-ros
[root@openEuler ~]# yum install ros-humble-ros-base ros-humble-xxx e.g. ros-humble-turtlesim
```
2. Run the following command to check whether the installation is successful
```shell
[root@openEuler ~]# rpm -q ros-humble
```

#### 2. Test ros-humble
##### Run turtlesim
1. Run turtlesim
```shell
[root@openEuler ~]# source /opt/ros/humble/setup.bash
[root@openEuler ~]# ros2 run turtlesim turtlesim_node
```
2. Open turtlesim terminal
```shell
[root@openEuler ~]# source /opt/ros/humble/setup.bash
[root@openEuler ~]# ros2 run turtlesim turtle_teleop_key
```
3. Use the arrow keys to control the movement of the turtle
![ros-humble](./figures/ros-humble.png)

### 2. ros-foxy
#### 1. Installing ros-foxy-ros-base
1. Download the software package
```shell
[root@openEuler ~]# wget http://121.36.3.168:82/home:/Chenjy3_22.03/openEuler_22.03_LTS_standard_x86_64/x86_64/ros-foxy-ros-base-0.9.2-2.oe2203.x86_64.rpm
```
2. Install the rpm package
```shell
[root@openEuler ~]# rpm -ivh --nodeps --force ros-foxy-ros-base-0.9.2-2.oe2203.x86_64.rpm
```
3. Dependence installation
```shell
[root@openEuler ~]# sh /opt/ros/foxy/install_dependence.sh
```
4. Run the following command to check whether the installation is successful
```shell
[root@openEuler ~]# rpm -q ros-foxy-ros-base
```


#### 2. Test ros-foxy-ros-base
##### Run turtlesim
1. Run turtlesim
```shell
[root@openEuler ~]# source /opt/ros/foxy/setup.bash
[root@openEuler ~]# ros2 run turtlesim turtlesim_node
```
2. Open turtlesim terminal
```shell
[root@openEuler ~]# source /opt/ros/foxy/setup.bash
[root@openEuler ~]# ros2 run turtlesim turtle_teleop_key
```
3. Use the arrow keys to control the movement of the turtle

![ROS2-turtlesim](./figures/turtlesim.png)
## 2. ROS
### 1. ros-noetic
#### 1. Installing ros-noetic-ros-comm
1. Download the software package
```shell
[root@openEuler ~]# wget http://121.36.3.168:82/home:/davidhan:/branches:/openEuler:/22.03:/LTS:/SP1:/Epol/standard_x86_64/x86_64/ros-noetic-ros-comm-1.15.11-2.oe2203.x86_64.rpm

```
2. Install the rpm package
```shell
[root@openEuler ~]# rpm -ivh --nodeps --force ros-noetic-ros-comm-1.15.11-2.oe2203.x86_64.rpm
```
3. Dependence installation
```shell
[root@openEuler ~]# sh /opt/ros/noetic/install_dependence.sh
```
4. Run the following command to check whether the installation is successful
```shell
[root@openEuler ~]# rpm -q ros-noetic-ros-comm
```
#### 2. Test ros-noetic-ros-comm
#####  Run topic_demo
1. Create and compile workspace
```shell
[root@openEuler ~]# mkdir -p catkin_ws/src
[root@openEuler ~]# cd catkin_ws/src/
[root@openEuler ~]# source /opt/ros/noetic/setup.bash
[root@openEuler ~]# catkin_init_workspace
[root@openEuler ~]# git clone https://gitee.com/davidhan008/topic_demo.git
[root@openEuler ~]# cd ..
[root@openEuler ~]# catkin_make
```
2. run roscore
```shell
[root@openEuler ~]# source /opt/ros/noetic/setup.bash
[root@openEuler ~]# roscore
```
3. run topic_demo talker
```shell
[root@openEuler ~]# source /opt/ros/noetic/setup.bash
[root@openEuler ~]# cd catkin_ws
[root@openEuler ~]# source devel/setup.bash
[root@openEuler ~]# rosrun topic_demo talker
```
4. run topic_demo listener
```shell
[root@openEuler ~]# source /opt/ros/noetic/setup.bash
[root@openEuler ~]# cd catkin_ws
[root@openEuler ~]# source devel/setup.bash
[root@openEuler ~]# rosrun topic_demo listener
```
![ROS2-turtlesim](./figures/ROS-demo.png)


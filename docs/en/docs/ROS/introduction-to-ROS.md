# Introduction to ROS

## Introduction

ROS is an open source meta-operating system for robotics. It provides the services that an operating system should have, including hardware abstraction, low-level device control, implementation of common functions, inter-process message passing, and package management. It also provides the tools and library functions needed to fetch, compile, write, and run code across computers.

ROS's operating architecture is a processing architecture that uses ROS communication modules to implement loosely coupled network connections between modules [P2P](https://en.wikipedia.org/wiki/Peer-to-peer), which implements several types of communications, including:

1. Service-based synchronous [RPC](https://en.wikipedia.org/wiki/Remote_procedure_call) (remote procedure call) communication;
2. Topic-based asynchronous data flow communication, as well as data storage on the parameter server.

Since the beginning of ROS in 2007, along with the great development of robot technology, the core ideas and basic software packages of ROS have been gradually improved and different ROS distributions have been released. Below is a list of current and historical ROS releases, the rows marked in green in the table are the currently supported releases.

![ROS 发行版](./figures/ROS-release.png)

Although ROS is still a powerful development tool in the field of robotics, due to the limitations of the initial design, many problems have gradually been exposed. For example: poor real-time performance, high system overhead, unfriendly support for Python3, no encryption mechanism and low security. Many developers and research institutions have also made improvements to address the limitations of ROS, but these local function improvements are often difficult to bring about overall performance improvements. At ROSCon 2014, the design architecture of the new generation of ROS (Next-generation ROS: Building on DDS) was officially announced. On August 31, 2015, the first alpha version of ROS2.0 was launched, and different release versions were released later. Below is a list of current and historical ROS2 releases, the rows marked in green in the table are currently supported releases.

![ROS2 发行版](./figures/ROS2-release.png)

## Architecture

The overall architecture of ROS is shown in the figure below:

![ROS 架构](./figures/ROS-ROS2.png)

1. OS Layer
   - ROS1 is mainly built on the Linux system, and ROS2 has brought changes. The systems supported include Linux, Windows, Mac, RTOS, and even bare metal without an operating system.
2. Middleware Layer
   - One of the most important concepts in ROS is the "node" based on the publish/subscribe model, which allows developers to develop low-coupling functional modules in parallel and facilitates secondary reuse. The communication system of ROS1 is based on TCPROS/UDPROS, while the communication system of ROS2 is based on DDS. DDS is a standard solution for data publishing/subscribing in distributed real-time systems, which will be explained in detail in the next section. ROS2 provides an abstract layer implementation of DDS internally, and users do not need to pay attention to the underlying DDS provider.
   - In the ROS1 architecture, Nodelet and TCPROS/UDPROS are parallel layers, providing a more optimized data transmission method for multiple nodes in the same process. This data transmission method is also retained in ROS2, but it is called "Intra-process", which is also independent of DDS.
3. Application Layer

   - ROS1 is strongly dependent on the ROS Master, and one can imagine what kind of dilemma the entire system will face once the Master goes down. But from the architecture of ROS2 on the right, we can find that the Master, which was a worry before, has finally disappeared, and nodes use a discovery mechanism called "Discovery" to obtain each other's information.

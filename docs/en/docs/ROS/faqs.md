# FAQs

## Question 1:

![](./figures/problem.png)

Reason: The reason for this warning is that there are both ROS1 and ROS2 in the environment variable. Solution: Modify the environment variable to avoid the conflict between the two versions.

```shell
[root@openEuler ~]# vim /opt/ros/foxy/share/ros_environment/environment/1.ros_distro.sh
```

```shell
# generated from ros_environment/env-hooks/1.ros_distro.sh.in

#export ROS_DISTRO=foxy
```

Just annotate everything inside.


# ROS User Guide

This document introduces the installation, deployment and usage of ROS (English: Robot Operating System, generally translated as robot operating system) on the openEuler system, so as to guide users to quickly understand and use ROS.

This document is suitable for community developers, open source enthusiasts and related partners who use the openEuler system and want to understand and use ROS. Users need to have basic knowledge of Linux operating system.
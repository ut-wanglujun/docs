# Usage

## Using ROS

ROS provides some useful command line tools, which can be used to obtain various information of different nodes. Commonly used commands are as follows:

- rosnode : operation node
- rostopic : operation topic
- rosservice : operation service
- rosmsg : operation msg
- rossrv : operation srv msg
- rosparam : operation param

Please also refer to:

http://wiki.ros.org/ROS/CommandLineTools

## Using  ROS2
```shell
# 命令help
[root@openEuler ~]# ros2 --help
usage: ros2 [-h] Call `ros2 <command> -h` for more detailed usage. ...

ros2 is an extensible command-line tool for ROS 2.

optional arguments:
  -h, --help            show this help message and exit

Commands:
  action     Various action related sub-commands
  bag        Various rosbag related sub-commands
  component  Various component related sub-commands
  daemon     Various daemon related sub-commands
  doctor     Check ROS setup and other potential issues
  interface  Show information about ROS interfaces
  launch     Run a launch file
  lifecycle  Various lifecycle related sub-commands
  multicast  Various multicast related sub-commands
  node       Various node related sub-commands
  param      Various param related sub-commands
  pkg        Various package related sub-commands
  run        Run a package specific executable
  security   Various security related sub-commands
  service    Various service related sub-commands
  test       Run a ROS2 launch test
  topic      Various topic related sub-commands
  trace      Trace ROS nodes to get information on their execution
  wtf        Use `wtf` as alias to `doctor`

  Call `ros2 <command> -h` for more detailed usage.
```

## Precautions

* The newly opened terminal needs to execute the “source /opt/ros/foxy/local_setup.bash” or “source /opt/ros/noetic/setup.bash” command.

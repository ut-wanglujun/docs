# KubeEdge Deployment Guide

## Description

### KubeEdge

KubeEdge is an open source system dedicated to solving problems in edge scenarios. It extends the capabilities of containerized application orchestration and device management to edge devices. Based on Kubernetes, KubeEdge provides core infrastructure support for networks, application deployment, and metadata synchronization between the cloud and the edge. KubeEdge supports MQTT and allows for custom logic to enable communication for the resource-constrained devices at the edge. KubeEdge consists of components deployed on the cloud and edge nodes. The components are now open source.

> https://kubeedge.io/

### iSulad

iSulad is a lightweight container runtime daemon designed for IoT and cloud infrastructure. It is lightweight, fast, and is not restricted by hardware specifications or architectures. It is suitable for wide application in various scenarios, such as cloud, IoT, and edge computing.

> https://gitee.com/openeuler/iSulad

## Cluster Overview

### Component Versions

| Component  | Version                           |
| ---------- | --------------------------------- |
| OS         | openEuler 22.03                   |
| Kubernetes | 1.20.2-4                          |
| iSulad     | 2.0.11                            |
| KubeEdge   | v1.8.0                            |

### Node Planning Example

| Node           | Location | Components                       |
| -------------- | -------- | -------------------------------- |
| cloud.kubeedge | Cloud    | Kubernetes (Master), iSulad, CloudCore |
| edge.kubeedge  | Edge     | iSulad, EdgeCore                 |

> Note: You can run the `hostnamectl set-hostname [cloud,edge].kubeedge` command to set the cloud and edge node names in advance.

## Preparations

### Tool Package Download

[kubeedge-tools](https://gitee.com/Poorunga/kubeedge-tools) provides complete offline installation packages and deployment scripts for easy and quick KubeEdge cluster deployment even if the node cannot access the Internet.

```bash
# Download and decompress the kubeedge-tools package on both the cloud and edge nodes.
$ wget -O kubeedge-tools.zip https://gitee.com/Poorunga/kubeedge-tools/repository/archive/master.zip
$ unzip kubeedge-tools.zip

# Go to the kubeedge-tools directory for all the subsequent operations.
$ cd kubeedge-tools-master
```

### Kubernetes Deployment

Perform the following operations on the cloud node only.

#### Initializing the Cloud Environment

```bash
$ ./setup-cloud.sh
```

#### Deploying Kubernetes

Deploy Kubernetes by referring to the [Kubernetes Cluster Deployment Guide](https://docs.openeuler.org/en/docs/21.09/docs/Kubernetes/Kubernetes.html).

> Note: Preferentially, use `kubeadm` to deploy Kubernetes if the cloud node has access to the Internet. The procedure is as follows:

```bash
$ kubeadm init --apiserver-advertise-address=[cloud_node_IP_address] --kubernetes-version v1.20.15 --pod-network-cidr=10.244.0.0/16 --upload-certs --cri-socket=/var/run/isulad.sock
...
Your Kubernetes control-plane has initialized successfully!
...

# After Kubernetes is installed, copy the specified file to the directory as prompted.
#   mkdir -p $HOME/.kube
#   sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
#   sudo chown $(id -u):$(id -g) $HOME/.kube/config
```

#### Configuring Network for the Cloud Container

Container Network Interface (CNI) software that provides network for Kubernetes nodes include [flannel](https://github.com/flannel-io/flannel), [Calico](https://github.com/projectcalico/calico), [Cilium](https://github.com/cilium/cilium), and more. If you have not decided which CNI software to use, run the following command to configure network for the cloud container:

```bash
$ ./install-flannel-cloud.sh
```

#### Checking Deployment Status

```bash
# Check whether the node status is normal (Ready)
$ kubectl get nodes
NAME             STATUS   ROLES                  AGE   VERSION
cloud.kubeedge   Ready    control-plane,master   12m   v1.20.2

# Check whether the Kubernetes components are normal (Running)
$ kubectl get pods -n kube-system
NAME                                     READY   STATUS    RESTARTS   AGE
coredns-74ff55c5b-4ptkh                  1/1     Running   0          15m
coredns-74ff55c5b-zqx5n                  1/1     Running   0          15m
etcd-cloud.kubeedge                      1/1     Running   0          15m
kube-apiserver-cloud.kubeedge            1/1     Running   0          15m
kube-controller-manager-cloud.kubeedge   1/1     Running   0          15m
kube-flannel-cloud-ds-lvh4n              1/1     Running   0          13m
kube-proxy-2tcnn                         1/1     Running   0          15m
kube-scheduler-cloud.kubeedge            1/1     Running   0          15m
```

## Deployment

### CloudCore Deployment

Perform the following operations on the cloud node only.

#### Initializing the Cluster

```bash
# Set --advertise-address to the IP address of the cloud node.
$ keadm init --advertise-address="cloud_node_IP_address" --kubeedge-version=1.8.0
...
CloudCore started
```

#### Configuring CloudCore

```bash
$ ./patch-cloud.sh
```

#### Checking Deployment Status

```bash
# active (running) indicates a normal status
$ systemctl status cloudcore | grep running
     Active: active (running) since Fri 2022-03-04 10:54:30 CST; 5min ago
```

CloudCore has been deployed on the cloud node. Then, deploy EdgeCore on the edge node.

### EdgeCore Deployment

Perform the following operations only on the edge node unless otherwise specified.

#### Initializing the Edge Environment

```bash
$ ./setup-edge.sh
```

#### Managing the Edge Node

```bash
# Run the keadm gettoken command on the cloud node.
$ keadm gettoken
96058ab80ffbeb87fe58a79bfb19ea13f9a5a6c3076a17c00f80f01b406b4f7c.eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2NDY0NDg4NzF9.1mJegWB7SUVjgf-OvAqILgbZXeMHR9eOzMxpNFc42SI
# Save this token for subsequent steps.


# Run the keadm join command on the edge node.
# Set --cloudcore-ipport to the IP address and port number (10000) of the cloud node. Set --token to the token saved in the previous step.
$ keadm join --cloudcore-ipport=clou_node_IP_address:10000 --kubeedge-version=1.8.0 --token=96058ab80ffbeb87fe58a79bfb19ea13f9a5a6c3076a17c00f80f01b406b4f7c.eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2NDY0NDg4NzF9.1mJegWB7SUVjgf-OvAqILgbZXeMHR9eOzMxpNFc42SI
...
KubeEdge edgecore is running...
```

#### Configuring EdgeCore

```bash
$ ./patch-edge.sh
```

#### Configuring Network for the Edge Container

If you have not decided which CNI software to use, run the following command to configure network for the edge container:

```bash
# Run the command on the cloud node.
$ ./install-flannel-edge.sh
```

#### Checking Whether the Edge Node is Managed

```bash
# Run the command on the cloud node. You can see that the edge node is added.
$ kubectl get nodes
NAME             STATUS   ROLES                  AGE     VERSION
cloud.kubeedge   Ready    control-plane,master   1h      v1.20.2
edge.kubeedge    Ready    agent,edge             10m     v1.19.3-kubeedge-v1.8.0
```

The KubeEdge cluster has been deployed. Next, let's test the task delivery from the cloud to the edge.

### Application Deployment

Perform the following operations on the cloud node only.

#### Deploying Nginx

```bash
$ kubectl apply -f yamls/nginx-deployment.yaml
deployment.apps/nginx-deployment created

# Check whether Nginx is deployed on the edge node and running.
$ kubectl get pod -owide | grep nginx
nginx-deployment-84b99f4bf-jb6sz   1/1     Running   0          30s   10.244.1.2   edge.kubeedge   <none>           <none>
```

#### Testing the Function

```bash
# Access the IP address of Nginx on the edge node.
$ curl 10.244.1.2:80
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
html { color-scheme: light dark; }
body { width: 35em; margin: 0 auto;
font-family: Tahoma, Verdana, Arial, sans-serif; }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>
```

The deployment of KubeEdge is complete.

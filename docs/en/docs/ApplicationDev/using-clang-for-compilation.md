# Using LLVM/Clang for Compilation

This chapter describes the basic knowledge of LLVM/Clang compilation and provides examples for demonstration. For more information about how to use Clang, run the **clang --help** command.

- [Using LLVM/Clang for Compilation](#using-llvmclang-for-compilation)
  - [Overview](#overview)
  - [LLVM/Clang Installation](#llvmclang-installation)
  - [Coexistence of Multiple Versions](#coexistence-of-multiple-versions)
  - [Example](#example)

## Overview

LLVM is a compiler that covers multiple programming languages and target processors. It uses Clang as the compiler and driver of C and C++. Clang can not only compile C and C++ programs into the LLVM intermediate representations (IRs), but also invoke all LLVM optimization passes for code generation until the final binary file is generated.

## LLVM/Clang Installation

Install the Clang and LLVM software packages using the Yum source in the openEuler OS. llvm12 is installed by default.

```shell
yum install llvm
yum install clang
yum install lld // The openEuler Yum source does not contain LLD12.
```

Check whether the installation is successful.

```shell
clang -v
```

If the command output contains the Clang version information, the installation is successful.

## Coexistence of Multiple Versions

Configure the openEuler LLVM/Clang multi-version support as follows:

```shell
Yum package name:
llvm<ver>{-*}
clang<ver>{-*}
lld<ver>{-*}
bolt<ver>{-*}
Example:
clang15
llvm15-devel

Installation path:
/usr/lib64/llvm<ver>
Example:
/usr/lib64/llvm15

Executable files with the -<ver> suffix are installed in the /usr/bin directory.
Example:
/usr/bin/clang-15
/usr/bin/lld-15
```

Currently, the following LLVM/Clang versions are supported:

```shell
llvm //By default, llvm12 is installed.
llvm-15
```

Install other versions using Yum.

```shell
yum install llvm15
yum install clang15
yum install lld15
```

Check whether the installation is successful.

```shell
clang-15 -v
```

## Example

Compile and run C and C++ programs.

```shell
clang  [command line flags]  hello.c  -o  hello.o 
./hello.o
```

```shell
clang++  [command line flags]  hello.cpp  -o  hello.o 
./hello.o
```

Specify the LLD of LLVM as the linker. If it is not specified, the default LLD is used.

```shell
clang  [command line flags]  -fuse-ld=lld  hello.c  -o  hello.o 
./hello.o
```

For more information, see the [LLVM User Guides](https://llvm.org/docs/UserGuides.html).

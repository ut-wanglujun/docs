# Imperceptible DPU Offload User Guide

This document describes the container management plane DPU offload function of openEuler, as well as how to install and deploy it. Through the unified abstraction layer provided by the OS, this function masks the differences in how the container management plane accesses resources across hosts. This makes it possible to offload services from the container management plane to the DPU.

This document is intended for community developers, open source enthusiasts, and partners who use the openEuler OS and want to learn and use the OS kernel and containers. Users must:

- Know basic Linux operations.

- Be familiar with the fundamental mechanisms of the Linux kernel file system.

- Understand Kubernetes and Docker, as well as how to deploy and use them.

# Deploying A-Ops

## 1. Environment Requirements

- Two hosts running on openEuler 22.09

  These two hosts are used to deploy two modes of the check module: scheduler and executor. Other services, such as MySQL, Elasticsearch, and aops-manager, can be independently deployed on any host. To facilitate operations, deploy these services on host A.

- More than 8 GB memory

## 2. Configuring the Deployment Environment

### Host A:

Deploy the following A-Ops services on host A: aops-tools, aops-manager, aops-check, aops-web, aops-agent, and gala-gopher.

Deploy the following third-party services on host A: MySQL, Elasticsearch, ZooKeeper, Kafka, and Prometheus.

The deployment procedure is as follows:

#### 2.1 Disabling the Firewall

Disable the firewall on the local host.

```
systemctl stop firewalld
systemctl disable firewalld
systemctl status firewalld
```

#### 2.2 Deploying aops-tools

Install aops-tools.

```
yum install aops-tools
```

#### 2.3 Deploying Databases MySQL and Elasticsearch

##### 2.3.1 Deploying MySQL

Use the **aops-basedatabase** script installed during aops-tools installation to install MySQL.

```
cd /opt/aops/aops_tools
./aops-basedatabase mysql
```

Modify the MySQL configuration file.

```
vim /etc/my.cnf
```

Add **bind-address** and set it to the IP address of the local host.

![1662346986112](./figures/修改mysql配置文件.png)

Restart the MySQL service.

```
systemctl restart mysqld
```

Connect to the database and set the permission.

```
mysql
show databases;
use mysql;
select user,host from user;// If the value of user is root and the value of host is localhost, the MySQL database can be connected only by the local host but cannot be connected from the external network and by the local software client.
update user set host = '%' where user='root';
flush privileges;// Refresh the permission.
exit
```

##### 2.3.2 Deploying Elasticsearch

Use the **aops-basedatabase** script installed during aops-tools installation to install Elasticsearch.

```
cd /opt/aops/aops_tools
./aops-basedatabase elasticsearch
```

Modify the configuration file.

Modify the Elasticsearch configuration file.

```
vim /etc/elasticsearch/elasticsearch.yml
```

![1662370718890](./figures/elasticsearch配置2.png)

![1662370575036](./figures/elasticsearch配置1.png)

![1662370776219](./figures/elasticsearch3.png)

Restart the Elasticsearch service.

```
systemctl restart elasticsearch
```

#### 2.4 Deploying aops-manager

Install aops-manager.

```
yum install aops-manager
```

Modify the configuration file.

```
vim /etc/aops/manager.ini
```

Change the IP address of each service in the configuration file to the actual IP address. Because all services are deployed on host A, you need to set their IP addresses to the actual IP address of host A.

```
[manager]
ip=192.168.1.1  // Change the service IP address to the actual IP address of host A.
port=11111
host_vault_dir=/opt/aops
host_vars=/opt/aops/host_vars

[uwsgi]
wsgi-file=manage.py
daemonize=/var/log/aops/uwsgi/manager.log
http-timeout=600
harakiri=600

[elasticsearch]
ip=192.168.1.1  // Change the service IP address to the actual IP address of host A.
port=9200
max_es_query_num=10000000

[mysql]
ip=192.168.1.1  // Change the service IP address to the actual IP address of host A.
port=3306
database_name=aops
engine_format=mysql+pymysql://@%s:%s/%s
pool_size=10000
pool_recycle=7200

[aops_check]
ip=192.168.1.1  // Change the service IP address to the actual IP address of host A.
port=11112
```

Start the aops-manager service.

```
systemctl start aops-manager
```

#### 2.5 Deploying aops-web

Install aops-web.

```
yum install aops-web
```

Modify the configuration file. Because all services are deployed on host A, set the IP address of each service accessed by aops-web to the actual IP address of host A.

```
vim /etc/nginx/aops-nginx.conf
```

The following figure shows the configuration of some services.

![1662378186528](./figures/配置web.png)

Enable the aops-web service.

```
systemctl start aops-web
```

#### 2.6 Deploying Kafka

##### 2.6.1 Deploying ZooKeeper

Install ZooKeeper.

```
yum install zookeeper
```

Start the ZooKeeper service.

```
systemctl start zookeeper
```

##### 2.6.2 Deploying Kafka

Install Kafka.

```
yum install kafka
```

Modify the configuration file.

```
vim /opt/kafka/config/server.properties
```

Change the value of **listeners** to the IP address of the local host.

![1662381371927](./figures/kafka配置.png)

Start the Kafka service.

```
cd /opt/kafka/bin
nohup ./kafka-server-start.sh ../config/server.properties &
tail -f ./nohup.out  # Check all the outputs of nohup. If the IP address of host A and the Kafka startup success INFO are displayed, Kafka is started successfully.
```

#### 2.7 Deploying aops-check

Install aops-check.

```
yum install aops-check
```

Modify the configuration file.

```
vim /etc/aops/check.ini
```

Change the IP address of each service in the configuration file to the actual IP address. Because all services are deployed on host A, you need to set their IP addresses to the actual IP address of host A.

```
[check]
ip=192.168.1.1  // Change the service IP address to the actual IP address of host A.
port=11112
mode=configurable  // Configurable mode, which means aops-check is used as the scheduler in common diagnosis mode.
timing_check=on

[default_mode]
period=30
step=30

[elasticsearch]
ip=192.168.1.1  // Change the service IP address to the actual IP address of host A.
port=9200

[mysql]
ip=192.168.1.1  // Change the service IP address to the actual IP address of host A.
port=3306
database_name=aops
engine_format=mysql+pymysql://@%s:%s/%s
pool_size=10000
pool_recycle=7200

[prometheus]
ip=192.168.1.1  // Change the service IP address to the actual IP address of host A.
port=9090
query_range_step=15s

[agent]
default_instance_port=8888

[manager]
ip=192.168.1.1  // Change the service IP address to the actual IP address of host A.
port=11111

[consumer]
kafka_server_list=192.168.1.1:9092  // Change the service IP address to the actual IP address of host A.
enable_auto_commit=False
auto_offset_reset=earliest
timeout_ms=5
max_records=3
task_name=CHECK_TASK
task_group_id=CHECK_TASK_GROUP_ID
result_name=CHECK_RESULT
[producer]
kafka_server_list = 192.168.1.1:9092  // Change the service IP address to the actual IP address of host A.
api_version = 0.11.5
acks = 1
retries = 3
retry_backoff_ms = 100
task_name=CHECK_TASK
task_group_id=CHECK_TASK_GROUP_ID
```

Start the aops-check service in configurable mode.

```
systemctl start aops-check
```

#### 2.8 Deploying the Client Services

aops-agent and gala-gopher must be deployed on the client. For details, see the [Deploying aops-agent](deploying-aops-agent.md).

Note: Before registering a host, you need to add a host group to ensure that the host group to which the host belongs exists. In this example, only host A is deployed and managed.

#### 2.9 Deploying Prometheus

Install Prometheus.

```
yum install prometheus2
```

Modify the configuration file.

```
vim /etc/prometheus/prometheus.yml
```

Add the gala-gopher addresses of all clients to the monitoring host of Prometheus.

![1662377261742](./figures/prometheus配置.png)

Start the Prometheus service:

```
systemctl start prometheus
```

#### 2.10 Deploying gala-ragdoll

The configuration source tracing function of A-Ops depends on gala-ragdoll. Git is used to monitor configuration file changes.

Install gala-ragdoll.

```shell
yum install gala-ragdoll # A-Ops configuration source tracing
```

Modify the configuration file.

```shell
vim /etc/ragdoll/gala-ragdoll.conf
```

Change the IP address in **collect_address** of the **collect** section to the IP address of host A, and change the values of **collect_api** and **collect_port** to the actual API and port number.

```
[git]
git_dir = "/home/confTraceTest"
user_name = "user_name"
user_email = "user_email"

[collect]
collect_address = "http://192.168.1.1"    // Change the IP address to the actual IP address of host A.
collect_api = "/manage/config/collect"    // Change the API to the actual API for collecting configuration files.
collect_port = 11111                      // Change the port number to the actual port number of the service.

[sync]
sync_address = "http://0.0.0.0"
sync_api = "/demo/syncConf"
sync_port = 11114


[ragdoll]
port = 11114

```

Start the gala-ragdoll service.

```shell
systemctl start gala-ragdoll
```

### Host B:

Only aops-check needs to be deployed on host B as the executor.

#### 2.11 Deploying aops-check

Install aops-check.

```
yum install aops-check
```

Modify the configuration file.

```
vim /etc/aops/check.ini
```

Change the IP address of each service in the configuration file to the actual IP address. Change the IP address of the aops-check service deployed on host B to the IP address of host B. Because other services are deployed on host A, change the IP addresses of those services to the IP address of host A.

```
[check]
ip=192.168.1.2  // Change the IP address to the actual IP address of host B.
port=11112
mode=executor  // Executor mode, which means aops-check is used as the executor in normal diagnosis mode.
timing_check=on

[default_mode]
period=30
step=30

[elasticsearch]
ip=192.168.1.1  // Change the service IP address to the actual IP address of host A.
port=9200

[mysql]
ip=192.168.1.1  // Change the service IP address to the actual IP address of host A.
port=3306
database_name=aops
engine_format=mysql+pymysql://@%s:%s/%s
pool_size=10000
pool_recycle=7200

[prometheus]
ip=192.168.1.1  // Change the service IP address to the actual IP address of host A.
port=9090
query_range_step=15s

[agent]
default_instance_port=8888

[manager]
ip=192.168.1.1  // Change the service IP address to the actual IP address of host A.
port=11111

[consumer]
kafka_server_list=192.168.1.1:9092  // Change the service IP address to the actual IP address of host A.
enable_auto_commit=False
auto_offset_reset=earliest
timeout_ms=5
max_records=3
task_name=CHECK_TASK
task_group_id=CHECK_TASK_GROUP_ID
result_name=CHECK_RESULT
[producer]
kafka_server_list = 192.168.1.1:9092  // Change the service IP address to the actual IP address of host A.
api_version = 0.11.5
acks = 1
retries = 3
retry_backoff_ms = 100
task_name=CHECK_TASK
task_group_id=CHECK_TASK_GROUP_ID
```

Start the aops-check service in executor mode.

```
systemctl start aops-check
```



The service deployment on the two hosts is complete.

# secGear Developer Guide

This document describes the architecture, features, installation, developer guide, and application scenarios of secGear, a unified confidential computing programming framework provided by openEuler.

This document is intended for developers, open source enthusiasts, and partners who use the openEuler operating system (OS) and want to learn and use secGear. Users must have basic knowledge of the Linux OS.

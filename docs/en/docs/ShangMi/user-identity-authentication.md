# User Identity Authentication

Operating systems usually use the password mechanism to authenticate users. openEuler provides user password management components, such as PAM, passwd, shadow, and libuser. After a user password is set, it needs to be encrypted for storage. Generally, the hash algorithm is used for encryption. The user password management component provided by openEuler supports the ShangMi 3 (SM3) cryptographic hash algorithm.

## Using PAM to Encrypt User Passwords

### Overview

PAM is a pluggable authentication module of the system that provides an authentication mechanism for upper-layer applications. PAM released by openEuler supports user password encryption using the SM3 algorithm.

### Prerequisites

1. PAM 1.5.2-2 or later

```
$ rpm -qa pam
pam-1.5.2-2.oe2209.x86_64
```

2. libxcrypt 4.4.26-2 or later

```
$ rpm -qa libxcrypt
pam-4.4.26-2.oe2209.x86_64
```

### How to Use

1. Open the **/etc/pam.d/password-auth** and **/etc/pam.d/system-auth** files, locate the line starting with **password sufficient pam_unix.so**, and change the algorithm field in the line to **sm3**.

```
$ cat /etc/pam.d/password-auth
......
password    sufficient    pam_unix.so sm3 shadow nullok try_first_pass use_authtok
......

$ cat /etc/pam.d/system-auth
......
password    sufficient    pam_unix.so sm3 shadow nullok try_first_pass use_authtok
......
```

2. After the configuration is modified, the password changed by running the **passwd** command or the password created by a new user is encrypted using the SM3 algorithm. The encryption result starts with **sm3** and is stored in **/etc/shadow**.

```
$ passwd testuser 
Changing password for user testuser.
New password: 
Retype new password: 
passwd: all authentication tokens updated successfully.
$ cat /etc/shadow | grep testuser
testuser:$sm3$wnY86eyUlB5946gU$99LlMr0ddeZNDqnB2KRxn9f30SFCCvMv1WN1cFdsKJ2:19219:0:90:7:35::
```

### Notes

1. By default, the SHA512 algorithm is used. After the SM3 algorithm is used, the existing user passwords are not affected. The cryptographic algorithm takes effect only after the passwords are changed.
2. If PAM and libxcrypt need to be downgraded to non-SM versions and existing user passwords are encrypted using the SM3 algorithm, modify the configuration first.
   Set the algorithm to a non-SM algorithm, change the user passwords, and downgrade the software to a non-SM version. Otherwise, these users cannot log in to the system.


## Using Shadow to Encrypt User Passwords

### Overview

Shadow is a common user management component in Linux. It provides commands such as **chpasswd**, **chgpasswd**, and **newusers**. The shadow component provided by openEuler supports the SM3 algorithm in user management. By default, Shadow uses the PAM security authentication mechanism. Therefore, the SM3 algorithm supported by Shadow affects only the **chpasswd** and **chgpasswd** commands.

### Prerequisites

Shadow 4.9-4 or later

```
$ rpm -qa shadow
shadow-4.9-4.oe2209.x86_64
```

### How to Use

1. By default, **chpasswd** uses the PAM configuration. Use **-c**  to specify the SM3 algorithm. The encryption result starts with **sm3** and is stored in **/etc/shadow**.

```
$ echo testuser:testPassword |chpasswd -c SM3
$ cat /etc/shadow | grep testuser
testuser:$sm3$moojQQeBfdGOrL14$NqjckLHlk3ICs1cx.0rKZwRHafjVlqksdSJqfx9eYh6:19220:0:99999:7:::

```
2. By default, **chgpasswd** uses the PAM configuration. Use **-c**  to specify the SM3 algorithm. The encryption result starts with **sm3** and is stored in **/etc/shadow**.

```
$ echo testGroup:testPassword |chpasswd -c SM3
$ cat /etc/gshadow | grep testGroup
testGroup:$sm3$S3h3X6U6KsXg2Gkc$LFCAnKbi6JItarQz4Y/Aq9/hEbEMQXq9nQ4rY1j9BY9::
```

### Notes

By default, Shadow uses the PAM security authentication mechanism. When **-c** is used to specify the encryption algorithm in related commands, the PAM mechanism is not used.

## Using libuser to Encrypt User Passwords

### Overview

The libuser library implements a standardized interface for operating and managing users and group accounts. This library is encapsulated and provides the command line interface (CLI) and Python interface for managing users and groups. User passwords can be encrypted using algorithms such as DES, MD5, Blowfish, SHA256, and SHA512. libuser released by openEuler supports the SM3 algorithm.

### Prerequisites

libuser 0.63-3 or later

```
$ rpm -qa libuser
libuser-0.63-3.oe2209.x86_64
```

### How to Use

1. Configure **crypt_style=sm3** in the **[defaults]** section in **/etc/libuser.conf**.

```
$ cat /etc/libuser.conf
......
[defaults]
crypt_style = sm3
......
```

2. When you run the **lusermod**, **lpasswd**, or **luseradd** command to set a user password, the default password encryption algorithm is SM3. The encryption result starts with **sm3** and is stored in **/etc/shadow**.

```
# luseradd  testuser -P Test@123
# cat /etc/shadow | grep testuser
testuser:$sm3$1IJtoN6zlBDCiPKC$5oxscBTgiquPAEmZWGNDVqTPrboHJw3fFSohjF6sONB:18862:0:90:7:35::
```

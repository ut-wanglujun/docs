# Secure Boot

Secure Boot is a standard feature defined in the UEFI specification. It verifies the signature of each component level by level during system startup to ensure the integrity and authenticity of the boot sequence. The UEFI Secure Boot of the Linux system includes the following three procedures:

1. The BIOS uses its built-in certificate to verify the signature of the shim component.
2. The shim component uses its built-in certificate to verify the signature of the grub component.
3. The grub component verifies the signature of the kernel component through the interface provided by the shim component.

openEuler adds support for ShangMi (SM) algorithms to the pesign EFI signature tool and its nss algorithm library. That is, openEuler supports SM3 for hash calculation, SM2 for signing and verifying EFI files. In this way, the secure boot of openEuler can be implemented using SM algorithms.

## Constraints

- The openEuler shim component supports SM-based Secure Boot, including verification of the GRUB signature by shim, and verification of the kernel signature by GRUB. The verification of the shim signature depends on the BIOS.
- An ARM64/x86 physical machine that supports UEFI Secure Boot is required.
- The pesign tool can sign signatures with a maximum level of 2.
- Currently, the pesign tool can only generate signatures but cannot verify signatures.

## Preparations

1. The following software packages (or their later versions) must be installed:

```
openssl-1.1.1m-15.oe2203.aarch64
nss-3.72.0-4.oe2203.aarch64
pesign-115-2.oe2203.aarch64
shim-15.6-7.oe2203.aarch64
crypto-policies-20200619-3.git781bbd4.oe2203.noarch
```

2. Download the source code of the openEuler shim component. Ensure that the version in the spec file is later than 15.6-7.

```
git clone https://gitee.com/src-openeuler/shim.git -b openEuler-22.03-LTS-SP1 --depth 1
```

3. Install software packages required for building the shim component:

```
yum install elfutils-libelf-devel gcc gnu-efi gnu-efi-devel openssl-devel make git rpm-build
```

4. Check whether the SM3 algorithm is enabled for nss. If not, modify the file content as follows:

```
cat /usr/share/crypto-policies/DEFAULT/nss.txt | grep SM3
config="disallow=ALL allow=HMAC-SHA256:HMAC-SHA1:HMAC-SHA384:HMAC-SHA512:CURVE25519:SECP256R1:SECP384R1:SECP521R1:aes256-gcm:chacha20-poly1305:aes256-cbc:aes128-gcm:aes128-cbc:SHA256:SHA384:SHA512:SHA224:SHA1:ECDHE-RSA:ECDHE-ECDSA:RSA:DHE-RSA:ECDSA:RSA-PSS:RSA-PKCS:tls-version-min=tls1.0:dtls-version-min=dtls1.0:DH-MIN=1023:DSA-MIN=2048:RSA-MIN=2048:SM3"
```

## Generation of Keys and Certificates

1. Generate the key and certificate for signing the shim component. The shim signature is verified by the BIOS. As most BIOSs do not support SM algorithms, the RSA algorithm is used. For BIOSs that support SM algorithms you can generate the SM2 key and certificate by referring to the next step.

```
openssl genrsa -out rsa.key 4096
openssl req -new -key rsa.key -out rsa.csr -subj '/C=AA/ST=BB/O=CC/OU=DD/CN=secure boot BIOS'
openssl x509 -req -days 365 -in rsa.csr -signkey rsa.key -out rsa.crt
openssl x509 -in rsa.crt -out rsa.der -outform der
```

2. Generate the SM2 key and certificate for signing the GRUB and kernel components.

```
openssl ecparam -genkey -name SM2 -out sm2.key
openssl req -new -sm3 -key sm2.key -out sm2.csr -subj '/C=AA/ST=BB/O=CC/OU=DD/CN=secure boot shim'
openssl x509 -req -days 3650 -signkey sm2.key -in sm2.csr -out sm2.crt
openssl x509 -in sm2.crt -out sm2.der -outform der
```

3. Create an NSS database and import the keys and certificates generated in the preceding two steps to the NSS database.

```
# The NSS database is organized in the form of directories. The storage location can be customized.
mkdir certdb
certutil -N -d certdb
# Import the SM2 and RSA certificates to the NSS database and name them sm2 and rsa respectively.
certutil -A -n sm2 -d certdb -t CT,CT,CT -i sm2.crt
certutil -A -n rsa -d certdb -t CT,CT,CT -i rsa.crt
# To import the SM2 and RSA keys to the NSS database, compress them into PKCS 12 files.
openssl pkcs12 -export -out rsa.p12 -inkey rsa.key -in rsa.crt
openssl pkcs12 -export -out sm2.p12 -inkey sm2.key -in sm2.crt
pk12util -d certdb -i rsa.p12
pk12util -d certdb -i sm2.p12
```

## shim Component Building

1. Go to the shim source code directory, modify the configuration variables in shim.spec to enable the support for SM algorithms, and specify the built-in SM2 certificate.

```
%global enable_sm 1
%global vendor_cert /path/to/sm2.der
```

2. Build the shim software package.

```
rpmbuild -ba shim.spec --define "_sourcedir $PWD"
```

3. Install the built shim software package.

```
rpm -Uvh ~/rpmbuild/RPMS/aarch64/shim-xxx.rpm
```

## SM Signature for UEFI Files

1. Sign the shim component with the RSA key and certificate and replace the original one.

```
# ARM64
pesign -n certdb -c rsa -s -i /boot/efi/EFI/openEuler/shimaa64.efi -o shimaa64.efi.signed
cp shimaa64.efi.signed /boot/efi/EFI/openEuler/shimaa64.efi
# x86
pesign -n certdb -c rsa -s -i /boot/efi/EFI/openEuler/shimx64.efi -o shimx64.efi.signed
cp shimx64.efi.signed /boot/efi/EFI/openEuler/shimx64.efi
```

2. Sign the GRUB component with the SM2 key and certificate and replace the original one.

```
# ARM64
pesign -n certdb -c sm2 -s -i /boot/efi/EFI/openEuler/grubaa64.efi -o grubaa64.efi.signed -d sm3
cp grubaa64.efi.signed /boot/efi/EFI/openEuler/grubaa64.efi
# x86
pesign -n certdb -c sm2 -s -i /boot/efi/EFI/openEuler/grubx64.efi -o grubx64.efi.signed -d sm3
cp grubx64.efi.signed /boot/efi/EFI/openEuler/grubx64.efi
```

3. Sign the kernel component with the SM2 key and certificate and replace the original one. (Note that the file name contains the actual version number.)

```
# For the ARM64 architecture,you need to decompress and sign the component, and compress it again.
cp /boot/vmlinuz-5.10.0-126.0.0.66.oe2203.aarch64 vmlinuz-5.10.0-126.0.0.66.oe2203.aarch64.gz
gzip -d vmlinuz-5.10.0-126.0.0.66.oe2203.aarch64.gz
pesign -n certdb -c sm2 -s -i vmlinuz-5.10.0-126.0.0.66.oe2203.aarch64 -o vmlinuz-5.10.0-126.0.0.66.oe2203.aarch64.signed -d sm3
gzip vmlinuz-5.10.0-126.0.0.66.oe2203.aarch64.signed
cp vmlinuz-5.10.0-126.0.0.66.oe2203.aarch64.signed.gz /boot/vmlinuz-5.10.0-126.0.0.66.oe2203.aarch64
# x86
pesign -n certdb -c sm2 -s -i /boot/vmlinuz-5.10.0-126.0.0.66.oe2203.x86_64 -o vmlinuz-5.10.0-126.0.0.66.oe2203.x86_64.signed -d sm3
cp vmlinuz-5.10.0-126.0.0.66.oe2203.x86_64.signed /boot/vmlinuz-5.10.0-126.0.0.66.oe2203.x86_64
```

4. Check the signature information. The following uses shim and GRUB as examples:

```
pesign -S -i /boot/efi/EFI/openEuler/grubaa64.efi 
pesign -S -i /boot/efi/EFI/openEuler/shimaa64.efi 
```

## Secure Boot

Enter the BIOS, import the certificate for signing the shim component, and enable the Secure Boot option. The operation method varies depending on the BIOS. The following uses the Kunpeng 2280 v2 server as an example:

1. Place the RSA certificate for signing the shim component in the **/boot/efi/EFI/openEuler** directory.

```
cp rsa.der /boot/efi/EFI/openEuler
```

2. Restart the system.
3. Enter BIOS to enable Secure Boot:

```
Setup > Security > Secure Boot > Enable
```

4. Set the Secure Boot mode to custom:

```
Setup > Security > Secure Boot Certificate Configuration > Secure Boot Mode > Custom
```

5. Import the Secure Boot certificate:

```
Setup > Security > Secure Boot Certificate Configuration > Options Related to Secure Boot Custom Mode > DB Options > Import Signature > Add Signature by File > Select rsa.der > Save and exit.
```

6. Save the configuration and restart the system. The system is started successfully. Secure Boot is enabled.

```
mokutil --sb-state
SecureBoot enabled
```

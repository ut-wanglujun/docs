# Algorithm Library

## OpenSSL Cryptographic Interface

OpenSSL is a common cryptographic algorithm library software that supports SM2, SM3, and SM4 algorithms. You can invoke the encryption and decryption functions of SM series cryptographic algorithms through command lines or APIs.

### Prerequisites

OpenSSL 1.1.1m-6 or later

```
$ rpm -qa openssl
openssl-1.1.1m-6.oe2209.x86_64
```

### How to Use

#### Scenario 1: Using the CLI to Call Cryptographic Algorithms

1. SM2 public key algorithm

Generate an SM2 private key.

```
$ openssl ecparam -genkey -name SM2 -out priv.key
```

Generate a public key based on the private key.

```
$ openssl ec -in priv.key -pubout -out pub.key
read EC key
writing EC key
```

Use the SM2 algorithm to sign the file and set the message digest algorithm to SM3.

```
$ openssl dgst -sm3 -sign priv.key -out data.sig data
```

Use the public key to verify the signature.

```
$ openssl dgst -sm3 -verify pub.key -signature data.sig data
Verified OK
```

2. SM3 message digest algorithm

Use the SM3 algorithm for data digest.

```
$ openssl dgst -sm3 data
SM3(data)= a794922bb9f0a034257f6c7090a3e8429801a42d422c21f1473e83b7f7eac385
```

3. SM4 symmetric cipher algorithm

Use the SM4 algorithm to encrypt data. **-K** and **-iv** specify the key value and IV value used for encryption, respectively. Generally, the key value and IV value need to be randomly generated.

```
$ openssl enc -sm4 -in data -K 123456789ABCDEF0123456789ABCDEF0 -iv 123456789ABCDEF0123456789ABCDEF0 -out data.enc
```

Use the SM4 algorithm to decrypt data.

```
$ openssl enc -d -sm4 -in data.enc -K 123456789ABCDEF0123456789ABCDEF0 -iv 123456789ABCDEF0123456789ABCDEF0 -out data.raw
```

Compare the encrypted and decrypted data. The results are consistent.

```
$ diff data data.raw 
```

#### Scenario 2: Using APIs to Call Cryptographic Algorithms

You can directly install openssl-help and query the **man** manual.

```
$ yum install openssl-help
$ man sm2
$ man EVP_sm3
$ man EVP_sm4_cbc
```

## Kernel Cryptographic Interface

### Overview

The cryptographic algorithms of the Linux kernel is managed by the crypto framework. Different algorithm implementations can be registered and invoked in the crypto framework. Kernel 5.10 provided by openEuler supports ShangMi (SM) series cryptographic algorithms (SM2, SM3, and SM4). The SM2 and SM3 algorithms are compiled in the kernel by default, and the SM4 algorithm is provided as a kernel module.

### Prerequisites

Kernel 5.10.0-106 or later

```
# rpm -qa kernel
kernel-5.10.0-106.1.0.55.oe2209.x86_64
```

### How to Use

#### Scenario 1: Querying the Cryptographic Algorithms Supported by the Kernel

Use **/proc/crypto** to query the registered SM series cryptographic algorithms. By default, the SM2 and SM3 algorithms are loaded.

```
$ cat /proc/crypto | grep sm3 -A8
name         : sm3
driver       : sm3-generic
module       : kernel
priority     : 100
refcnt       : 1
selftest     : passed
internal     : no
type         : shash
blocksize    : 64
digestsize   : 32

$ cat /proc/crypto | grep sm2 -A6
name         : sm2
driver       : sm2-generic
module       : kernel
priority     : 100
refcnt       : 1
selftest     : passed
internal     : no
type         : akcipher
```

By default, the SM4 algorithm is not loaded. You need to insert the corresponding module first.

```
$ modprobe sm4-generic
$ cat /proc/crypto | grep sm4 -A8
name         : sm4
driver       : sm4-generic
module       : sm4_generic
priority     : 100
refcnt       : 1
selftest     : passed
internal     : no
type         : cipher
blocksize    : 16
min keysize  : 16
max keysize  : 16
```

#### Scenario 2: Calling Algorithm APIs

The method of calling SM series cryptographic algorithms is the same as that of calling other algorithms of the same type. For details, see the Linux kernel document.

```
https://www.kernel.org/doc/html/v5.10/crypto/userspace-if.html
```

#### Scenario 3: Optimizing Algorithm Performance Through Instruction Sets

The crypto framework allows registration of algorithm implementations related to the architecture. The algorithm performance can be optimized through a specific instruction set. Currently, the kernel 5.10 of openEuler supports algorithm performance optimization using the following instruction sets.

| Driver                            | Instruction Set            | Priority|
| -------------------------------- | ---------------------- | ------ |
| sm4-neon(ecb/cbc/cfb/ctr)        | ARM64-NEON      | 200    |
| sm3-avx                          | x86-AVX         | 300    |
| sm4-aesni-avx (ecb/cbc/cfb/ctr)  | x86-AVX         | 400    |
| sm4-aesni-avx 2(ecb/cbc/cfb/ctr) | x86-AVX2        | 500    |

When multiple instances of the same algorithm are registered, the default algorithm is selected based on the registered priority of each algorithm instance. A larger **priority** value indicates a higher priority. The priority of a pure software algorithm (with the suffix **-generic**) is fixed to **100**. By default, the performance optimization through instruction sets is disabled for the SM series cryptographic algorithms and is provided for users in the form of a kernel module. For example, to enable the AVX instruction set optimization of the SM3 algorithm, do as follows:

```
$ modprobe sm3-avx
$ cat /proc/crypto | grep sm3 -A8
name         : sm3
driver       : sm3-avx
module       : sm3_avx_x86_64
priority     : 300
refcnt       : 1
selftest     : passed
internal     : no
type         : shash
blocksize    : 64
digestsize   : 32

......
```

#### Notes

1. The prerequisite for enabling algorithm instruction set optimization is that the CPU supports the corresponding instruction set. You can query the instruction set supported by the CPU by calling **/proc/cpuinfo**.
2. Calling a specific instruction set has certain overhead. Therefore, it cannot be ensured that the performance optimized by the instruction set is higher than that of software implementation in all scenarios.
3. The optimization through some instruction sets has certain restrictions. For example, the NEON instruction set has optimization effect only in the encryption mode that supports parallel computing.

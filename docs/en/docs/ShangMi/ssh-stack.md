# SSH Stack

## Overview

The OpenSSH component is a Secure Shell Protocol (SSH) component implemented based on libcrypto of OpenSSL in C language. The main function is remote login to ensure the integrity and reliability of encrypted information over an unsecured network. The SSH server and client configuration items provided by openEuler involve key exchange, public key authentication, symmetric encryption, and integrity authentication. The values of these configuration items can be ShangMi (SM) Cipher Suites (including SM2, SM3, and SM4 algorithms).

## Prerequisites

OpenSSH 8.8p1-5 or later

```
$ rpm -qa | grep openssh
openssh-8.8p1-5.oe2209.x86_64
openssh-server-8.8p1-5.oe2209.x86_64
openssh-clients-8.8p1-5.oe2209.x86_64
```

## How to Use

### Scenario 1: Remote Login

1. On the client, call **ssh-keygen** to generate a user key, which is saved as **\~/.ssh/id_sm2** and **\~/.ssh/id_sm2.pub** by default. Then, send **\~/.ssh/id_sm2.pub** from the client to the server. (You can also run the **ssh-copy-id** command to send the file.)

```
$ ssh-keygen -t sm2 -m PEM
```

2. On the server, call **ssh-keygen** to generate a host key and add the public key sent by the client to the authorized key file list. (If you run the **ssh-copy-id** command, the public key is automatically written.)

```
$ ssh-keygen -t sm2 -m PEM -f /etc/ssh/ssh_host_sm2_key
$ cat /path/to/id_sm2.pub >> ~/.ssh/authorized_keys
```

3. On the server, modify the **/etc/ssh/sshd_config** file to support login using SM series cryptographic algorithms. The following table lists the SM configuration items.

| Description              | Configuration Item                 | SM Value   |
|---------------------|------------------------|---------------|
| Authentication key for the host key and public key (configurable only on the server)| HostKeyAlgorithms      | /etc/ssh/ssh_host_sm2_key           |
| Host key and public key authentication algorithm         | HostKeyAlgorithms      | sm2           |
| Key exchange algorithm             | KexAlgorithms          | sm2-sm3       |
| Symmetric cryptographic algorithm             | Ciphers                | sm4-ctr       |
| Integrity check algorithm            | MACs                   | hmac-sm3      |
| User public key authentication algorithm           | PubkeyAcceptedKeyTypes | sm2           |
| Authentication key for the user public key (configurable only on the client)   | IdentityFile           | ~/.ssh/id_sm2 |
| Hash algorithm used for printing key fingerprints      | FingerprintHash        | sm3           |

4. On the client, configure the SM series cryptographic algorithms to complete the login. You can enable the SM Cipher Suites on the client by running commands or modifying the configuration file. The following shows how to log in using the CLI:

```
ssh -o PreferredAuthentications=publickey -o HostKeyAlgorithms=sm2 -o PubkeyAcceptedKeyTypes=sm2 -o Ciphers=sm4-ctr -o MACs=hmac-sm3 -o KexAlgorithms=sm2-sm3 -i ~/.ssh/id_sm2 [remote-ip]
```

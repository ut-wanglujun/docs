# Kernel Module Signing

## Overview

The kernel module signing facility is an important mechanism for protecting Linux kernel security. Signature information is added to the end of the kernel module file in a certain format, and the system checks whether the signature matches the public key preset in the kernel when the kernel module is loaded. In this way, the authenticity and integrity of the kernel module file are ensured.

## Prerequisites

1. The openEuler kernel compilation environment has been prepared. For details, see https://gitee.com/openeuler/kernel/wikis/kernel.
2. In openEuler kernel 5.10, the ShangMi (SM) series cryptographic algorithms are supported for kernel module signing. You are advised to select the latest kernel 5.10 source code for compilation.
3. The SM2 private key and certificate used for kernel module signing have been generated. The reference commands using OpenSSL are as follows:

```
# Generate a certificate configuration file. (Other fields in the configuration file can be defined as required.)
$ echo 'subjectKeyIdentifier=hash' > mod.cfg
# Generate a private key for SM2 signing.
$ openssl ecparam -genkey -name SM2 -out mod.key
# Generate a signing request.
$ openssl req -new -sm3 -key mod.key -out mod.csr
# Generate an SM2 certificate.
$ openssl x509 -req -days 3650 -extfile mod.cfg -signkey mod.key -in mod.csr -out mod.crt
```

## How to Use

### Scenario 1: Automatic Signing

Write the certificate and private key to the **mod.pem** file.

```
$ cat /path/to/mod.key > mod.pem
$ cat /path/to/mod.crt >> mod.pem
```

Use the SM3 algorithm to sign the kernel module in the kernel compilation options.

```
$ make openeuler_defconfig
$ make menuconfig
```

Choose **Enable loadable module support** > **Sign modules with SM3** on the GUI.

```
Which hash algorithm should modules be signed with? (Sign modules with SM3)
```

Configure **Cryptographic API** > **Certificates for signature checking** to read the private key and certificate used for kernel signing from **mod.pem**.

```
(mod.pem) File name or PKCS#11 URI of module signing key
```

Build the kernel.

```
$ make -j64
$ make modules_install
$ make install
```

Run the **modinfo** command to check the signature information of the kernel module.

```
$ modinfo /usr/lib/modules/5.10.0/kernel/crypto/sm4.ko 
filename:       /usr/lib/modules/5.10.0/kernel/crypto/sm4.ko
license:        GPL v2
description:    Generic SM4 library
srcversion:     371050FDB8BF9878D9B5B9B
depends:        
retpoline:      Y
intree:         Y
name:           sm4
vermagic:       5.10.0 SMP mod_unload modversions 
sig_id:         PKCS#7
signer:         Internet Widgits Pty Ltd
sig_key:        33:0B:96:3E:1F:C1:CA:28:98:72:F5:AE:FF:3F:A4:F3:50:5D:E1:87
sig_hashalgo:   sm3
signature:      30:45:02:21:00:81:96:8D:40:CE:7F:7D:AE:3A:4B:CC:DC:9A:F2:B4:
		16:87:3E:C3:DC:77:ED:BC:6E:F5:D8:F3:DD:77:2B:D4:05:02:20:3B:
		39:5A:89:9D:DC:27:83:E8:D8:B4:75:86:FF:33:2B:34:33:D0:90:76:
		32:4D:36:88:84:34:31:5C:83:63:6B
```

### Scenario 2: Manual Signing

Call **sign_file** in the kernel source code directory to sign the specified kernel module.

```
$ ./scripts/sign-file sm3 /path/to/mod.key /path/to/mod.crt <module_file>
```

Other steps are the same as those in scenario 1.

### Scenario 3: Module Loading Verification

Add **module.sig_enforce** to the kernel startup parameters to enable forcible signature verification for the kernel module.

```
linux   /vmlinuz-5.10.0-106.1.0.55.oe2209.x86_64 root=/dev/mapper/openeuler-root ro resume=/dev/mapper/openeuler-swap rd.lvm.lv=openeuler/root rd.lvm.lv=openeuler/swap crashkernel=512M module.sig_enforce
```

After the system is restarted, only the kernel modules that pass the certificate verification can be loaded.

```
# insmod /usr/lib/modules/5.10.0/kernel/crypto/sm4.ko
```

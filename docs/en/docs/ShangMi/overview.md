# Overview

The ShangMi (SM) features for the openEuler OS aims to enable SM series cryptographic algorithms for key security features of the OS and provide cryptographic services such as the SM series cryptographic algorithm library, certificates, and secure transmission protocols for upper-layer applications.

Currently, the following SM features are supported:

1. SM2, SM3, and SM4 algorithms are supported in the user-mode algorithm libraries, such as OpenSSL and libgcrypt.
2. SM2, SM3, and SM4 cipher suites are supported in OpenSSH.
3. The SM Transport Layer Cryptography Protocol (TLCP) stack is supported in OpenSSL.
4. SM3 and SM4 algorithms are supported for disk encryption (dm-crypt/cryptsetup).
5. The SM3 algorithm is supported for password encryption in user identity authentication (pam/libuser/shadow).
6. The SM3 algorithm is supported for data digest in intrusion detection (AIDE).
7. SM2, SM3, and SM4 algorithms are supported in the kernel cryptographic framework (crypto) and algorithm performance optimization using instruction sets such as AVX/CE/NEON is allowed.
8. The SM3 message digest algorithm and SM2 certificate are supported in Integrity Measurement Architecture and Extended Verification Module (IMA/EVM) of the kernel.
9. The SM2 certificate is supported in kernel module signing and module signature verification.
10. SM4-CBC and SM4-GCM algorithms are supported in Kernel Transport Layer Security (KTLS).
11. SM3 and SM4 algorithms are supported in Kunpeng Accelerator Engine (KAE).
12. The SM3 algorithm and SM2 certificated signature are supported for UEFI secure boot.
# Certificates

## Overview

A ShangMi (SM) certificate is a digital certificate that complies with standards such as *Public Key Cryptographic Algorithm SM2 Based on Elliptic Curves* and *Digital Certificate Format Based on SM2 Algorithm*. The OpenSSL software released by openEuler supports SM certificates.

## Prerequisites

OpenSSL 1.1.1m-6 or later

```
$ rpm -qa openssl
openssl-1.1.1m-6.oe2209.x86_64
```

## How to Use

### Scenario 1: Generating an SM Certificate

1. Generate a private key for SM2 signing.

```
$ openssl ecparam -genkey -name SM2 -out sm2.key
```

2. Generate a signing request.

```
$ openssl req -new -sm3 -key sm2.key -out sm2.csr
```

3. Generate an SM certificate. You can use **-extfile** to specify the certificate configuration file.

```
$ openssl x509 -req -days 3650 -signkey sm2.key -in sm2.csr -out sm2.crt
```

4. View the certificate information.

```
$ openssl x509 -text -in sm2.crt
```

### Scenario 2: Building a Certificate Chain

#### Using the x509 Command (Generally Used for Function Tests)

1. Generate a private key for SM2 signing and a signing request.

```
$ openssl ecparam -genkey -name SM2 -out sm2.key
$ openssl req -new -sm3 -key sm2.key -out sm2.csr
```

2. Generate a level-2 certificate based on the level-1 certificate. You can use **-extfile** to specify the certificate configuration file.

```
$ openssl x509 -req -sm3 -CAcreateserial -CA ca.crt -CAkey ca.key -in sm2.csr -out sm2.crt
```

#### Using the CA Configuration File (Generally Used in Formal Scenarios)

1. Prepare the configuration file for generating the certificate. You can use **apps/openssl.cnf** in the OpenSSL source code directory.

2. Generate a self-signed CA certificate.

```
$ openssl ecparam -name SM2 -out SM2.pem
$ openssl req -config ./openssl.cnf -nodes -keyout CA.key -newkey ec:SM2.pem -new -out CA.csr
$ openssl x509 -sm3 -req -days 30 -in CA.csr -extfile ./openssl.cnf -extensions v3_ca -signkey CA.key -out CA.crt
```

3. Generate a level-2 certificate.

```
$ openssl req -config ./openssl.cnf -nodes -keyout SS.key -newkey ec:SM2.pem -new -out SS.csr
$ openssl x509 -sm3 -req -days 30 -in SS.csr -CA CA.crt -CAkey CA.key -extfile ./openssl.cnf -extensions v3_req -out SS.crt -CAcreateserial
```

### Scenario 3: Generating a TLCP Communication Certificate

For details, see chapter "TLCP Stack."

# File Integrity Protection

## Kernel Integrity Measurement Architecture (IMA)

IAM is a mandatory access control subsystem provided by the Linux kernel. It measures and verifies file integrity by adding check hooks to file access system calls.

### Prerequisites

1. The openEuler kernel compilation environment has been prepared. For details, see https://gitee.com/openeuler/kernel/wikis/kernel.
2. In openEuler kernel 5.10, the ShangMi (SM) series cryptographic algorithms are supported for kernel module signing. You are advised to select the latest kernel 5.10 source code for compilation.
3. The kernel SM2 root certificate has been generated.

```
# Generate a certificate configuration file. (Other fields in the configuration file can be defined as required.)
$ echo 'subjectKeyIdentifier=hash' > ca.cfg
# Generate a private key for SM2 signing.
$ openssl ecparam -genkey -name SM2 -out ca.key
# Generate a signing request.
$ openssl req -new -sm3 -key ca.key -out ca.csr
# Generate an SM2 certificate.
$ openssl x509 -req -days 3650 -extfile ca.cfg -signkey ca.key -in ca.csr -out ca.crt
```

4. The level-2 IMA certificate has been generated.

```
# Create a certificate configuration file.
echo 'subjectKeyIdentifier=hash' > ima.cfg
echo 'authorityKeyIdentifier=keyid,issuer' >> ima.cfg
# Generate a private key.
openssl ecparam -genkey -name SM2 -out ima.key
# Generate a signing request.
openssl req -new -sm3 -key ima.key -out ima.csr
# Generate a level-2 certificate based on the level-1 certificate.
openssl x509 -req -sm3 -CAcreateserial -CA ca.crt -CAkey ca.key -extfile ima.cfg -in ima.csr -out ima.crt
# Convert to the DER format.
openssl x509 -outform DER -in ima.crt -out x509_ima.der
```

5. The root certificate has been placed in the kernel source code directory, and **CONFIG_SYSTEM_TRUSTED_KEYS** has been modified to compile the certificate to the kernel trusted key.

```
$ cp /path/to/ca.crt .
$ make openeuler_defconfig
$ cat .config | grep CONFIG_SYSTEM_TRUSTED_KEYS
CONFIG_SYSTEM_TRUSTED_KEYS="ca.crt"
```

6. The kernel has been compiled and installed.

```
$ make -j64
$ make modules_install
$ make install
```

### How to Use

#### Scenario 1: Native IMA

##### IMA-Measurement

Configure the IMA policy and message digest algorithm, disable the IMA-appraisal mode, and restart the system.

```
ima_policy=tcb ima_hash=sm3 ima_appraise=off
```

Check the measurement log. It is found that the IMA measures all protected files and the message digest algorithm is SM3.

```
cat /sys/kernel/security/ima/ascii_runtime_measurements
10 601989730f01fb4688bba92d0ec94340cd90757f ima-sig sm3:0000000000000000000000000000000000000000000000000000000000000000 boot_aggregate 
10 dc0a98316b03ab15edd2b8daae75a0d64bca7c56 ima-sig sm3:3c62ee3c13ee32d7a287e04c843c03ebb428a5bb3dd83561efffe9b08444be22 /usr/lib/systemd/systemd 
10 1d0a5140e3924e2542963ad887a80def0aa8acac ima-sig sm3:4d3b83e143bd9d5288ef099eff4d01444947516d680165c6dd08cd5900768032 /usr/lib64/ld-linux-x86-64.so.2
......
```

##### IMA-Appraisal (Hash)

Configure the IMA policy and message digest algorithm, enable the IMA-appraisal fix mode, and restart the system.

```
ima_policy=appraise_tcb ima_hash=sm3 ima_appraise=fix
```

**appraise_tcb** indicates to appraise all files owned by the **root** user.

Perform an open operation on all files to be appraised to automatically mark the .ima extension.

```
find / -fstype ext4 -type f -uid 0 -exec dd if='{}' of=/dev/null count=0 status=none \;
```

After the marking is complete, you can see that all files with the .ima extension of the SM3 message digest algorithm are marked.

```
getfattr -m - -d -e hex /bin/bash
getfattr: Removing leading '/' from absolute path names
# file: bin/bash
security.ima=0x0411a794922bb9f0a034257f6c7090a3e8429801a42d422c21f1473e83b7f7eac385
security.selinux=0x73797374656d5f753a6f626a6563745f723a7368656c6c5f657865635f743a733000

openssl dgst -sm3 /bin/bash
SM3(/bin/bash)= a794922bb9f0a034257f6c7090a3e8429801a42d422c21f1473e83b7f7eac385
```

Enable the enforce mode and restart the system. The system can run properly.

```
ima_policy=appraise_tcb ima_hash=sm3 ima_appraise=enforce
```

##### IMA-Appraisal (Signing)

**Prerequisites**

1. The SM root certificate has been preset in the kernel.
2. The ima-evm-utils software package whose version is later than or equal to the specified version has been installed.

```
$ rpm -qa ima-evm-utils
ima-evm-utils-1.3.2-4.oe2209.x86_64
```

Generate a level-2 IMA certificate.

```
# Create a certificate configuration file.
echo 'subjectKeyIdentifier=hash' > ima.cfg
echo 'authorityKeyIdentifier=keyid,issuer' >> ima.cfg
# Generate a private key.
openssl ecparam -genkey -name SM2 -out ima.key
# Generate a signing request.
openssl req -new -sm3 -key ima.key -out ima.csr
# Generate a level-2 certificate based on the level-1 certificate.
openssl x509 -req -sm3 -CAcreateserial -CA ca.crt -CAkey ca.key -extfile ima.cfg -in ima.csr -out ima.crt
# Convert to the DER format.
openssl x509 -outform DER -in ima.crt -out x509_ima.der
```

Place the IMA certificate in the **/etc/keys** directory and run the **dracut** command to create initrd again.

```
mkdir -p /etc/keys
cp x509_ima.der /etc/keys
echo 'install_items+=" /etc/keys/x509_ima.der "' >> /etc/dracut.conf
dracut -f
```

Sign the files to be protected. For example, sign all executable files of the **root** user in the **/usr/bin** directory.

```
find /usr/bin -fstype ext4 -type f -executable -uid 0 -exec evmctl -a sm3 ima_sign --key /path/to/ima.key '{}' \;
```

Enable the enforce mode and restart the system. The system can run properly.

```
ima_policy=appraise_tcb ima_hash=sm3 ima_appraise=enforce
```

Check the protection effect of the signing mode.

```
# getfattr -m - -d /bin/echo
getfattr: Removing leading '/' from absolute path names
# file: bin/echo
security.ima=0sAwIRNJFkBQBIMEYCIQDLBg/bYlrkBqSaXNQMyK7rhiZj+qRiKdu+0fqW8lSmPQIhAJY2qSZJ0HgSu7kygydrS4MCC0KTK59nUkvISenZAUCo
security.selinux="system_u:object_r:bin_t:s0"

# echo 123 >> /bin/echo
-bash: /bin/echo: Permission denied
```

**Note:**

For each file under the IMA protection, you need to use either the hash mode or the signing mode to mark the integrity information. Generally, files that may change (such as data files and configuration files) are marked in hash mode, and files that do not change (such as executable files and dynamic link libraries) are marked in signing mode.

#### Scenario 2: IMA Digest Lists Mode

##### Generating SM3 Digest Lists

Configure **-a sm3** when calling **gen_digest_lists** to generate SM3 digest lists.

```
gen_digest_lists -a sm3 -t metadata -f compact -i l:policy -o add -p -1 -m immutable -i I:/usr/bin/bash -d <output_dir> -i i:
gen_digest_lists -a sm3 -t metadata -f compact -i l:policy -o add -p -1 -m immutable -i I:/usr/bin/bash -d <output_dir> -i i: -T
```

##### Configuring the SM3 Message Digest Algorithm

The overall procedure is the same as that for enabling the IMA Digest Lists feature. The only difference is that the **ima_hash** startup parameter is set to **sm3**. The following gives an example:

```
# Log mode
ima_template=ima-sig ima_policy="exec_tcb|appraise_exec_tcb|appraise_exec_immutable" initramtmpfs ima_hash=sm3 ima_appraise=log evm=allow_metadata_writes evm=x509 ima_digest_list_pcr=11 ima_appraise_digest_list=digest
# enforce mode
ima_template=ima-sig ima_policy="exec_tcb|appraise_exec_tcb|appraise_exec_immutable" initramtmpfs ima_hash=sm3 ima_appraise=enforce-evm evm=allow_metadata_writes evm=x509 ima_digest_list_pcr=11 ima_appraise_digest_list=digest
```

For details about other steps, see **Administrator Guide** > **Trusted Computing** > **Initial Deployment in the Digest Lists Scenario**.

After the configuration is complete, restart the system and query the measurement log. The default algorithm in the measurement log is changed to SM3.

```
cat /sys/kernel/security/ima/ascii_runtime_measurements
......
11 9e32183b5b1da72c6ff4298a44026e3f9af510c9 ima-sig sm3:5a2d81cd135f41e73e0224b9a81c3d8608ccde8caeafd5113de959ceb7c84948 /usr/bin/upload_digest_lists 
11 f3b9264761dbaeaf637d08b86cc3655e8f3380f7 ima-sig sm3:cc6faecee9976c12279dab1627a78ef36f6998c65779f3b846494ac5fe5493a1 /usr/libexec/rpm_parser 
11 dc0a98316b03ab15edd2b8daae75a0d64bca7c56 ima-sig sm3:3c62ee3c13ee32d7a287e04c843c03ebb428a5bb3dd83561efffe9b08444be22 /usr/lib/systemd/systemd 
......
```

##### Verifying Digest Lists Using the SM2 Certificates

**Prerequisites**

1. The SM root certificate has been preset in the kernel.
2. The digest-list-tools and ima-evm-utils software packages of the specified versions or later have been installed.

```
$ rpm -qa ima-evm-utils
ima-evm-utils-1.3.2-4.oe2209.x86_64
$ rpm -qa digest-list-tools
digest-list-tools-0.3.95-10.oe2209.x86_64
```

**Procedure**

1. Generate level-2 IMA and EVM certificates (sub-certificates of the SM root certificate preset in the kernel).

```
# Create a certificate configuration file.
echo 'subjectKeyIdentifier=hash' > ima.cfg
echo 'authorityKeyIdentifier=keyid,issuer' >> ima.cfg
# Generate a private key.
openssl ecparam -genkey -name SM2 -out ima.key
# Generate a signing request.
openssl req -new -sm3 -key ima.key -out ima.csr
# Generate a level-2 certificate based on the level-1 certificate.
openssl x509 -req -sm3 -CAcreateserial -CA ca.crt -CAkey ca.key -extfile ima.cfg -in ima.csr -out ima.crt
# Convert to the DER format.
openssl x509 -outform DER -in ima.crt -out x509_ima.der
openssl x509 -outform DER -in ima.crt -out x509_evm.der
```

2. Place the IMA and EVM certificates in the **/etc/keys** directory and run the **dracut** command to create initrd again.

```
mkdir -p /etc/keys
cp x509_ima.der /etc/keys
cp x509_evm.der /etc/keys
echo 'install_items+=" /etc/keys/x509_ima.der /etc/keys/x509_evm.der "' >> /etc/dracut.conf
dracut -f -e xattr
```

3. Enable the IMA Digest Lists function. After the restart, check whether the certificates are imported to the IMA and EVM key rings.

```
cat /proc/keys
......
024dee5e I------     1 perm 1f0f0000     0     0 keyring   .evm: 1
......
3980807f I------     1 perm 1f0f0000     0     0 keyring   .ima: 1
......
```

4. Sign the IMA digest lists using the private keys corresponding to the IMA and EVM certificates. The signed IMA digest lists can be imported to the kernel.

```
# Use **evmctl** to sign the digest lists.
evmctl ima_sign  --key /path/to/ima.key -a sm3 0-metadata_list-compact-tree-1.8.0-2.oe2209.x86_64
# Check the extension after signing.
getfattr -m - -d 0-metadata_list-compact-tree-1.8.0-2.oe2209.x86_64 
# file: 0-metadata_list-compact-tree-1.8.0-2.oe2209.x86_64
security.ima=0sAwIRNJFkBQBHMEUCIQCzdKVWdxw1hoVm9lgZB6sl+sxapptUFNjqHt5XZD87hgIgBMuZqBdrcNm7fXq/reQw7rzY/RN/UXPrIOxrVvpTouw=
security.selinux="unconfined_u:object_r:admin_home_t:s0"
# Import the signed digest lists file to the kernel.
echo /root/tree/etc/ima/digest_lists/0-metadata_list-compact-tree-1.8.0-2.oe2209.x86_64 > /sys/kernel/security/ima/digest_list_data
# Check the measurement log. You can view the import record of the digest lists.
cat /sys/kernel/security/ima/ascii_runtime_measurements
11 43b6981f84ba2725d05e91f19577cedb004adffb ima-sig sm3:b9430bbde2b7f30e935d91e29ab6778b6a825a2c3e5e7255895effb8747b7c1a /root/tree/etc/ima/digest_lists/0-metadata_list-compact-tree-1.8.0-2.oe2209.x86_64 0302113491640500473045022100b374a556771c35868566f6581907ab25facc5aa69b5414d8ea1ede57643f3b86022004cb99a8176b70d9bb7d7abfade430eebcd8fd137f5173eb20ec6b56fa53a2ec
```

**Notes:**

1. By default, the hash algorithm used in the digest lists provided by openEuler is SHA256. When the IMA digest lists measurement algorithm is set to SM3, you must remove the digest lists from the **/etc/ima/digest_lists** directory, generate new digest lists, and sign the digest lists. Otherwise, an error occurs during file integrity check. The procedure is as follows:

```
# Reset the SELinux tag of a disk (perform this operation when IMA extension verification and SELinux are enabled).
fixfiles -F restore
# Generate digest lists for all files (you can also specify the files).
gen_digest_lists -a sm3 -t metadata -f compact -i l:policy -o add -p -1 -m immutable -i I:/ -d /etc/ima/digest_lists -i i:
# (Optional) Change the name of the generated digest lists file.
mv /etc/ima/digest_lists/0-metadata_list-compact- /etc/ima/digest_lists/0-metadata_list-compact-everything-sm3
# Sign
evmctl ima_sign --key /path/to/ima.key -a sm3 /etc/ima/digest_lists/0-metadata_list-compact-everything-sm3
# Re-create initrd.
dracut -f -e xattr
```

2. For the software packages released by openEuler, the IMA digest lists file is provided by default. The default message digest algorithm is SHA256. Therefore, when the message digest algorithm is changed to SM3, the digest lists released by openEuler cannot be imported, and the following message may be displayed during software package installation:

```
Cannon parse /etc/ima/digest_lists/0-metadata_list-rpm-......
```



# Lightweight Intrusion Detection (AIDE)

AIDE is a lightweight intrusion detection tool. It checks file integrity to detect malicious intrusions to the system in a timely manner. The AIDE database can use hash algorithms such as SHA256 and SHA512 to create a verification code or hash value for each file in ciphertext. The AIDE provided by openEuler supports the SM3 algorithm in addition to the open source software.

## Prerequisites

AIDE 0.17.4-1 or later

```
$ rpm -qa aide
aide-0.17.4-1.oe2209.x86_64
```

## How to Use

Add the SM3 algorithm to the **/etc/aide.conf** configuration file.

```
......
FIPSR = p+i+n+u+g+s+m+c+acl+selinux+xattrs+sha256+sm3
......
DATAONLY =  p+n+u+g+s+acl+selinux+xattrs+sha256+sm3
......
```

3. Initialize the database and save the database as the benchmark.

Initialize the database.

```
$ aide -c /etc/aide.conf -i
```

The example output is as follows:

```
AIDE initialized database at /var/lib/aide/aide.db.new.gz

Number of entries:      64249

---------------------------------------------------
The attributes of the (uncompressed) database(s):
---------------------------------------------------

/var/lib/aide/aide.db.new.gz
 MD5       : a7y5ErdpBAezV2iGdaVleg==
 SHA1      : u7W7jxomFtZn8rwMlkIRCN0r7iQ=
 SHA256    : 88Kw5b2yJ9bejwO+NqT6lyAieno+K0+W
             BPVBjXcUl08=
 SHA512    : WyOIgRxk9SeSoktF6BYVV0tRL7nGNDKQ
             A9QyxVCgzg+PwPMV7tzxmwOZI/dB64pP
             vQ/D2jqJdf3NS2PHMI4yvg==
 RMD160    : qTEPs2SIxPm3iEwsCnwvp9hR4s4=
 TIGER     : 0HgLucmhCcB56bxOMj+j1Kuja8UIsFRg
 CRC32     : VKE1TA==
 WHIRLPOOL : JSA35/NmkMOkUWEpcZJf3PR1UUz5WcLG
             AmBKPkao3fzQUsLMTJizCV4CwAE0G/Yc
             KX0mpW5vx+gk3njya8rAvA==
 GOST      : yKjiytOwRr3bJcFsxnJ310t1FY6YE3HB
             YNT8XP93xpc=
 STRIBOG256: 9bzS+5j4ZAoU/P7v3tkKOWn4ZfggcX28
             9dLQVhaiJtQ=
 STRIBOG512: 9LLXgqsRIRiXP2WOrOJt1qhx6psfbACd
             un+GTVmu441quX4zaaPIIG9lzDMBAqMg
             hZx5DlxsQj3YjMezSUsXLg==
 SM3       : Vwii+uw3Ge5Hh3eo1KOombxn2jWgyYRX
             ZdyCRZqWZ/E=


End timestamp: 2022-08-12 09:01:25 +0800 (run time: 2m 43s)
```


Save the database as the benchmark.

```
$ mv /var/lib/aide/aide.db.new.gz /var/lib/aide/aide.db.gz
```

### Scenario 1: Detecting Changes of Protected Files

```
$ aide -c /etc/aide.conf --check
---------------------------------------------------
Detailed information about changes:
---------------------------------------------------

File: /boot/config-5.10.0-106.3.0.57.oe2209.aarch64
 Size      : 182936                           | 182938
 Mtime     : 2022-08-04 08:00:00 +0800        | 2022-08-12 09:05:34 +0800
 Ctime     : 2022-08-11 01:42:44 +0800        | 2022-08-12 09:05:34 +0800
 SHA256    : ae0fOzf7U+e/evTZKpk6JQa00kvSkc5J | gOlhcUgnZWhcyJYMEPxCYccXwFr9lERX
             vMTX5Ysh+1k=                     | KK3O/ytfR/g=
 SHA512    : zAPIxIAM7YvJPjwl/PH23leBb/HiO0Rf | p+WxVOZ6DX323rHDRF864w297yh7POk6
             PlRME7yvpzFZk/5BrNe2ofQWR/0sFu1m | 11dOzahlKTWpAKaexC/u+4REiCzjl1rm
             JsDSy8m57wzCpJA9iUFq1g==         | eb/kd3Xgp1LoKwn49mtqxw==
 SM3       : CW0GnITxNeGeYOCAm4xfu78Vqm+wLp/Z | GWq/3nXL16tMYyxyFD/HTZbvJi2h+ttg
             cOmXmIKJT4Q=                     | 6d8XmSHu26A=
```

### Scenario 2: Updating the Database

Update the database. After the update, the database file is **/var/lib/aide/aide.db.new.gz**.

```
$ aide -c /etc/aide.conf --update
---------------------------------------------------
Detailed information about changes:
---------------------------------------------------

File: /boot/config-5.10.0-106.3.0.57.oe2209.aarch64
 Size      : 182936                           | 182938
 Mtime     : 2022-08-04 08:00:00 +0800        | 2022-08-12 09:05:34 +0800
 Ctime     : 2022-08-11 01:42:44 +0800        | 2022-08-12 09:05:34 +0800
 SHA256    : ae0fOzf7U+e/evTZKpk6JQa00kvSkc5J | gOlhcUgnZWhcyJYMEPxCYccXwFr9lERX
             vMTX5Ysh+1k=                     | KK3O/ytfR/g=
 SHA512    : zAPIxIAM7YvJPjwl/PH23leBb/HiO0Rf | p+WxVOZ6DX323rHDRF864w297yh7POk6
             PlRME7yvpzFZk/5BrNe2ofQWR/0sFu1m | 11dOzahlKTWpAKaexC/u+4REiCzjl1rm
             JsDSy8m57wzCpJA9iUFq1g==         | eb/kd3Xgp1LoKwn49mtqxw==
 SM3       : CW0GnITxNeGeYOCAm4xfu78Vqm+wLp/Z | GWq/3nXL16tMYyxyFD/HTZbvJi2h+ttg
             cOmXmIKJT4Q=                     | 6d8XmSHu26A=
```

### Scenario 3: Comparing Databases

Configure two databases in **/etc/aide.conf**.

```
# The location of the database to be read.
database_in=file:@@{DBDIR}/aide.db.gz
database_new=file:@@{DBDIR}/aide.db.new.gz
```

Compare the databases.

```
$ aide -c /etc/aide.conf --compare
---------------------------------------------------
Detailed information about changes:
---------------------------------------------------

File: /boot/config-5.10.0-106.3.0.57.oe2209.aarch64
 Size      : 182936                           | 182938
 Mtime     : 2022-08-04 08:00:00 +0800        | 2022-08-12 09:05:34 +0800
 Ctime     : 2022-08-11 01:42:44 +0800        | 2022-08-12 09:05:34 +0800
 SHA256    : ae0fOzf7U+e/evTZKpk6JQa00kvSkc5J | gOlhcUgnZWhcyJYMEPxCYccXwFr9lERX
             vMTX5Ysh+1k=                     | KK3O/ytfR/g=
 SHA512    : zAPIxIAM7YvJPjwl/PH23leBb/HiO0Rf | p+WxVOZ6DX323rHDRF864w297yh7POk6
             PlRME7yvpzFZk/5BrNe2ofQWR/0sFu1m | 11dOzahlKTWpAKaexC/u+4REiCzjl1rm
             JsDSy8m57wzCpJA9iUFq1g==         | eb/kd3Xgp1LoKwn49mtqxw==
 SM3       : CW0GnITxNeGeYOCAm4xfu78Vqm+wLp/Z | GWq/3nXL16tMYyxyFD/HTZbvJi2h+ttg
             cOmXmIKJT4Q=                     | 6d8XmSHu26A=

---------------------------------------------------
The attributes of the (uncompressed) database(s):
---------------------------------------------------

/var/lib/aide/aide.db.gz
 MD5       : a7y5ErdpBAezV2iGdaVleg==
 SHA1      : u7W7jxomFtZn8rwMlkIRCN0r7iQ=
 SHA256    : 88Kw5b2yJ9bejwO+NqT6lyAieno+K0+W
             BPVBjXcUl08=
 SHA512    : WyOIgRxk9SeSoktF6BYVV0tRL7nGNDKQ
             A9QyxVCgzg+PwPMV7tzxmwOZI/dB64pP
             vQ/D2jqJdf3NS2PHMI4yvg==
 RMD160    : qTEPs2SIxPm3iEwsCnwvp9hR4s4=
 TIGER     : 0HgLucmhCcB56bxOMj+j1Kuja8UIsFRg
 CRC32     : VKE1TA==
 WHIRLPOOL : JSA35/NmkMOkUWEpcZJf3PR1UUz5WcLG
             AmBKPkao3fzQUsLMTJizCV4CwAE0G/Yc
             KX0mpW5vx+gk3njya8rAvA==
 GOST      : yKjiytOwRr3bJcFsxnJ310t1FY6YE3HB
             YNT8XP93xpc=
 STRIBOG256: 9bzS+5j4ZAoU/P7v3tkKOWn4ZfggcX28
             9dLQVhaiJtQ=
 STRIBOG512: 9LLXgqsRIRiXP2WOrOJt1qhx6psfbACd
             un+GTVmu441quX4zaaPIIG9lzDMBAqMg
             hZx5DlxsQj3YjMezSUsXLg==
 SM3       : Vwii+uw3Ge5Hh3eo1KOombxn2jWgyYRX
             ZdyCRZqWZ/E=

/var/lib/aide/aide.db.new.gz
 MD5       : sKt4dVDKY/8A9EY/X4Ue2A==
 SHA1      : hagLXMv7G+KbEKh861kjjFSYpRw=
 SHA256    : HTHF7k5U294ECjCLneoZ3o8bH6PYgY5u
             AzoIyCacZp4=
 SHA512    : 5gWi7K/ztWMl7H+PK1doV/tWDHmaE2m/
             ndRXGR7b5J3v82Jv2HeJPoOt5A4Z/9FH
             5H+uCLYaHwRleyalyy5Wew==
 RMD160    : uMM1HtAbfz+G3Y9Z+rVR4qjdqcQ=
 TIGER     : OTHdXNQOxnHnOl6C9M3czSC42+SeZAZA
 CRC32     : T9G1Tw==
 WHIRLPOOL : FRMnQ2wHgylsTmpKE8RwdUvkzXucHwu1
             W9ZkUrxoXeci2g7jIgoMmpoeDPhH73qz
             nZ7fKj1lStSpiUGD5KPeWA==
 GOST      : haeO5dhT+t34C1GJf+2dc3q1GMN71FqB
             kqoiODo+j2o=
 STRIBOG256: lgZUZhhd9JfMOXgNzYptapqagwgmvdM+
             7uWzJsmOxoY=
 STRIBOG512: PA6jksprS37xQzHm1ZIvLR9ROa+FxoiF
             /xbAe0pSi4lMXXzABrPKkjyK0WtjxFvx
             07Poj2iDwNNcUJWekbaEXA==
 SM3       : R5/HXng5MNvrjoCh8/JzrWle1IO8ggsR
             P5i2ePX5BpY=
```

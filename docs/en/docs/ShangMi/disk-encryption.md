# Disk Encryption

## Overview

Disk encryption protects the storage confidentiality of important data. Data is encrypted based on a specified encryption algorithm and then written to disks. This feature mainly involves the user-mode tool cryptsetup and the kernel-mode module dm-crypt. Currently, the disk encryption feature provided by the openEuler OS supports ShangMi (SM) series cryptographic algorithms. Parameters are as follows:

- Encryption modes: luks2 and plain;
- Key length: 256 bits;
- Message digest algorithm: SM3;
- Encryption algorithm: sm4-xts-plain64.

## Prerequisites

1. Kernel 5.10.0-106 or later

```
$ rpm -qa kernel
kernel-5.10.0-106.1.0.55.oe2209.x86_64
```

2. cryptsetup 2.4.1-1 or later

```
$ rpm -qa cryptsetup
cryptsetup-2.4.1-1.oe2209.x86_64
```

## How to Use

A disk is formatted into a disk in a specified encryption mode and mapped to **/dev/mapper** as a dm device. Subsequent disk read and write operations are performed through the dm device. Data encryption and decryption are performed in kernel mode and are not perceived by users. The procedure is as follows:

1. Format the disk and map the disk as a dm device.

a. luks2 mode

Set the encryption mode to luks2, encryption algorithm to sm4-xts-plain64, key length to 256 bits, and message digest algorithm to SM3.

```
# cryptsetup luksFormat /dev/sdd -c sm4-xts-plain64 --key-size 256 --hash sm3
```

b. plain mode

Set the encryption mode to plain, encryption algorithm to sm4-xts-plain64, key length to 256 bits, and message digest algorithm to SM3.

```
# cryptsetup plainOpen /dev/sdd crypt1 -c sm4-xts-plain64 --key-size 256 --hash sm3
```

2. After the mapping is successful, run the **lsblk** command to view the device information.

```
# lsblk
NAME             MAJ:MIN RM  SIZE RO TYPE  MOUNTPOINTS
......
sdd                8:48   0   50G  0 disk
└─crypt1         253:3    0   50G  0 crypt
......
```

3. Perform I/O read and write operations on the encrypted device.

Deliver I/Os to raw disks.

```
# dd if=/dev/random of=/dev/mapper/crypt1 bs=4k count=10240
```

Deliver I/Os through the file system.

```
# mkfs.ext4 /dev/mapper/crypt1
# mount /dev/mapper/crypt1 /mnt/crypt/
# dd if=/dev/random of=/mnt/crypt/tmp bs=4k count=10240
```

4. Disable device mapping.

If a file system is mounted, unmount it first.

```
# umount /mnt/crypt
```

Closes a device.

```
# cryptsetup luksClose crypt1
```

# OmniVirt

OmniVirt is a developer tool set incubated by the technical operation team and infrastructure team of the openEuler community. It integrates virtualization technologies (such as LXD, HyperV, and Virtualization Framework) in mainstream desktop operating systems (OSs). It utilizes VMs and container images officially released by the openEuler community to provide developers with unified development resource (such as VMs and containers) provisioning and management experience on Windows, macOS, and Linux, improving the convenience and stability of using the openEuler development environment on mainstream desktop OSs, as well as developer experience.

## Background

The convenience and stability of development resources (such as VMs and containers) provided by mainstream desktop OSs are important factors that affect the experience of openEuler developers, especially for individuals and university developers with limited development resources. Common VM management platforms have many limitations. For example, VirtualBox requires developers to download a large ISO image and install the OS, WSL cannot provide a real openEuler kernel, most VM management software does not fully support Apple Sillicon chips, and many software needs to be paid, greatly reducing developers' work efficiency.

OmniVirt provides a convenient, easy-to-use, and unified developer tool set on mainstream desktop OSs such as Windows, macOS, and Linux (planned). It supported the x86_64 and AArch64 hardware architectures, including Apple Sillicon chips. It also supports virtual hardware acceleration capabilities for different platforms, providing developers with high-performance development resources. OmniVirt allows users to use VMs and container images (planned) released by the openEuler community, Daily Build images provided by the openEuler community, and other custom images that meet requirements, providing developers with multiple choices.

## Quick Start

For macOS users, see [Installing and Running OmniVirt on macOS][1].

For Windows users, see [Installing and Running OmniVirt in Windows][2].

[1]: ./mac-user-manual.md
[2]: ./win-user-manual.md

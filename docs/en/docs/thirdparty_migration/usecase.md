# HA Use Cases

This section describes how to get started with the HA cluster and add an instance. If you are not familiar with HA cluster installation, see [Installing and Deploying an HA Cluster](./installha.md).

## Quick Start Guide

The following operations use the management platform newly developed by the community as an example.

### Login Page

The user name is `hacluster`, and the password is the one set on the host by the user.

![](./figures/HA-api.png)

### Home page

After logging in to the system, the main page is displayed. The main page consists of the side navigation bar, the top operation area, the resource node list area, and the node operation floating area.

The following describes the features and usage of the four areas in detail.

![](./figures/HA-home-page.png)

#### Navigation bar

The side navigation bar consists of two parts: the name and logo of the HA cluster software, and the system navigation. The system navigation consists of three parts: **System**, **Cluster Configurations**, and **Tools**. **System** is the default option and the corresponding item to the home page. It displays the information and operation entries of all resources in the system. **Preference Settings** and **Heartbeat Configurations** are set under **Cluster Configurations**. **Log Download** and **Quick Cluster Operation** are set under **Tools**. These two items are displayed in a pop-up box after you click them.

#### Top Operation Area

The current login user is displayed statically. When you hover the mouse cursor on the user icon, the operation menu items are displayed, including **Refresh Settings** and **Log Out**. After you click **Refresh Settings**, the **Refresh Settings** dialog box is displayed with the **Refresh Settings** option. You can set the automatic refresh modes for the system, the options are **Do not refresh automatically**, **Refresh every 5 seconds**, and **Refresh every 10 seconds**. By default, **Do not refresh automatically** is selected. Click **Log Out** to log out and jump to the login page. After that, a re-login is required if you want to continue to access the system.

![](./figures/HA-refresh.png)

#### Resource Node List Area

The resource node list displays the resource information such as **Resource Name**, **Status**, **Resource Type**, **Service**, and **Running Node** of all resources in the system, and the node information such as all nodes in the system and the running status of the nodes. In addition, you can **Add**, **Edit**, **Start**, **Stop**, **Clear**, **Migrate**, **Migrate Back**, **Delete**, and **Associate** the resources.

#### Node Operation Floating Area

By default, the node operation floating area is collapsed. When you click a node in the heading of the resource node list, the node operation area is displayed on the right, as shown in the preceding figure. This area consists of the collapse button, the node name, the stop button, and the standby button, and provides the stop and standby operations. Click the arrow in the upper left corner of the area to collapse the area.

### Preference Settings

The following operations can be performed using command lines. The following is a simple example. For more command details, run the `pcs --help` command.

- Through the CLI

    ```shell
    # pcs property set stonith-enabled=false
    # pcs property set no-quorum-policy=ignore
    ```

    Run the following command to view all configurations:

    ```shell
    pcs property
    ```

    ![](./figures/HA-firstchoice-cmd.png)

- Through the GUI
    Clicking **Preference Settings** in the navigation bar, the **Preference Settings** dialog box is displayed. Change the values of **No Quorum Policy** and **Stonith Enabled** from the default values to the values shown in the following figure. Then, click OK.

    ![](./figures/HA-firstchoice.png)

### Add Resource

#### Adding Common Resources

1. Click **Add Common Resource**. The **Create Resource** dialog box is displayed.
    All mandatory configuration items of a resource are displayed on the **Basic** page. After you select a resource type on the **Basic** page, other mandatory and optional configuration items of the resource are displayed.

2. Enter the resource configuration information.
    A gray text area is displayed on the right of the dialog box to describe the current configuration item. After all mandatory parameters are set, click **OK** to create a common resource or click **Cancel** to cancel the add operation.
 The optional configuration items on the **Instance Attribute**, **Meta Attribute**, or **Operation Attribute** page are optional. If they are not configured, the resource creation process is not affected. You can modify them as required. Otherwise, the default values are used.

The following uses Apache as an example to describe how to add resources through the CLI and GUI.

- Through the CLI

    ```shell
    # pcs resource create httpd ocf:heartbeat:apache
    ```

    Check the resource running status:

    ```shell
    # pcs status
    ```

    ![](./figures/HA-pcs-status.png)

- Through the GUI

1. Enter the resource name and resource type, as shown in the following figure.

    ![](./figures/HA-add-resource.png)

2. If the following information is displayed, the resource is successfully added and started, and runs on a node, for example, ha1.

    ![](./figures/HA-apache-suc.png)
3. Access the Apache page.

    ![](./figures/HA-apache-show.png)

#### Adding Group Resources

>**Note:**
> Adding group resources requires at least one common resource in the cluster.

1. Click **Add Group Resource**. The **Create Resource** dialog box is displayed.
    All the parameters on the **Basic** tab page are mandatory. After setting the parameters, click **OK** to add the resource or click **Cancel** to cancel the add operation.

    ![](./figures/HA-group.png)

    >**Notes:**
    > Group resources are started in the sequence of child resources. Therefore, you need to select child resources in sequence.

2. If the following information is displayed, the resource is added successfully.

    ![](./figures/HA-group-suc.png)

#### Adding Clone Resources

1. Click **Add Clone Resource**. The **Create Resource** dialog box is displayed.
    On the **Basic** page, enter the object to be cloned. The resource name is automatically generated. After entering the object name, click **OK** to add the resource, or click **Cancel** to cancel the add operation.

    ![](./figures/HA-clone.png)

2. If the following information is displayed, the resource is added successfully.

    ![](./figures/HA-clone-suc.png)

### Editing Resources

- Starting a resource: Select a target resource from the resource node list. The target resource must not be running. Start the resource.
- Stopping a resource: Select a target resource from the resource node list. The target resource must be running. Stop the resource.
- Clearing a resource: Select a target resource from the resource node list. Clear the resource.
- Migrating a resource: Select a target resource from the resource node list. The resource must be a common resource or group resource in the running status. Migrate the resource to migrate it to a specified node.
- Migrating back a resource: Select a target resource from the resource node list. The resource must be a migrated resource. Migrate back the resource to clear the migration settings of the resource and migrate the resource back to the original node.
After you click **Migrate Back**, the status change of the resource item in the list is the same as that when the resource is started.
- Deleting a resource: Select a target resource from the resource node list. Delete the resource.

### Setting Resource Relationships

Resource relationships are used to set restrictions for the target resources. There are three types resource restrictions: resource location, resource collaboration, and resource order.

- Resource location: sets the running level of the resource on the nodes in the cluster to determine the node where the resource runs during startup or switchover. The running levels are Master Node and Slave 1 in descending order.
- Resource collaboration: indicates whether the target resource and other resources in the cluster run on the same node. **Same Node** indicates that this node must run on the same node as the target resource. **Mutually Exclusive** indicates that this node cannot run on the same node as the target resource.
- Resource order: Set the order in which the target resource and other resources in the cluster are started. **Front Resource** indicates that this resource must be started before the target resource. **Follow-up Resource** indicates that this resource can be started only after the target resource is started.

## HA MySQL Configuration Example

### Configuring the Virtual IP Address

1. On the home page, choose **Add** > **Add Common Resource**, and set the parameters as follows:

    ![](./figures/HA-vip.png)

2. The resource is successfully created and started, and runs on a node, for example, ha1.
3. The IP address can be pinged and connected. After login, you can perform various operations normally. Resources can be switched to ha2 and can be accessed normally. See the following figure.
    ![](./figures/HA-vip-suc.png)

### Configuring NFS Storage

Perform the following steps to configure another host as the NFS server:

1. Install the software package.

    ```shell
    # yum install -y nfs-utils rpcbind
    ```

2. Disable the firewall.

    ```shell
    # systemctl stop firewalld && systemctl disable firewalld
    ```

3. Modify the /etc/selinux/config file to change the status of SELinux to disabled.

    ```shell
    # SELINUX=disabled
    ```

4. Start services.

    ```shell
    # systemctl start rpcbind && systemctl enable rpcbind
    # systemctl start nfs-server && systemctl enable nfs-server
    ```

5. Create a shared directory on the server.

    ```shell
    # mkdir -p /test
    ```

6. Modify the NFS configuration file.

    ```shell
    # vim /etc/exports
    # /test *(rw,no_root_squash)
    ```

7. Reload the service.

    ```shell
    # systemctl reload nfs
    ```

8. Install the software package on the client. Install MySQL first and then mount NFS to the MySQL data path.

    ```shell
    # yum install -y nfs-utils mariadb-server
    ```

9. On the home page, choose **Add** > **Add Common Resource** and configure the NFS resource as follows:

    ![](./figures/HA-nfs.png)

10. The resource is successfully created and started, and runs on a node, for example, ha1. The NFS is mounted to the `/var/lib/mysql` directory. The resource is switched to ha2. The NFS is unmounted from ha1 and automatically mounted to ha2. See the following figure.

    ![](./figures/HA-nfs-suc.png)

### Configuring MySQL

1. On the home page, choose **Add** > **Add Common Resource** and configure the MySQL resource as follows:

    ![](./figures/HA-mariadb.png)

2. If the following information is displayed, the resource is successfully added:

    ![](./figures/HA-mariadb-suc.png)

### Adding the Preceding Resources as a Group Resource

1. Add the three resources in the resource startup sequence.

    On the home page, choose **Add** > **Add Group Resource** and configure the group resource as follows:

    ![](./figures/HA-group-new.png)

2. The group resource is successfully created and started. If the command output is the same as that of the preceding common resources, the group resource is successfully added.

    ![](./figures/HA-group-new-suc.png)

3. Use ha1 as the standby node and migrate the group resource to the ha2 node. The system is running properly.

    ![](./figures/HA-group-new-suc2.png)

# Usage Instructions

- [Usage Instructions](#usage-instructions)
  - [Precautions](#precautions)
  - [Upgrade](#upgrade)
  - [Rollback](#rollback)
    - [Application Scenarios](#application-scenarios)
    - [Manual Rollback](#manual-rollback)
    - [KubeOS-based Rollback](#kubeos-based-rollback)

## Precautions

1. KubeOS upgrades the container OS in an atomic manner, where all software packages are upgraded at the same time. By default, single-package upgrade is not supported.
2. KubeOS supports container OSs with two partitions. Partitions more than two are not supported.
3. You can view the upgrade logs of a single node in the **/var/log/messages** file on the node.
4. Strictly follow the upgrade and rollback procedures described in this document. If the steps are performed in a wrong sequence, the system may fail to be upgraded or rolled back.
5. Upgrade using a Docker image and mTLS two-way authentication are supported only in openEuler 22.09 or later.
6. Cross-major version upgrade is not supported.

## Upgrade

Create a custom object of the OS type in the cluster and set the corresponding fields. The OS type comes from the CRD object created in the installation and deployment sections. The following table describes the fields.

| Parameter           |Type | Description                                                    | How to Use| Mandatory (Yes/No)        |
| -------------- | ------ | ------------------------------------------------------------ | ----- | ---------------- |
| imagetype      | string | Type of the upgrade image          | The value must be `docker` or `disk`. Other values are invalid. This parameter is valid only in upgrade scenarios.|Yes              |
| opstype        | string | Operation, that is, upgrade or rollback| The value must be `upgrade` or `rollback`. Other values are invalid.|Yes              |
| osversion      | string | OS version of the image used for upgrade or rollback         | The value must be a KubeOS version, for example, `KubeOS 1.0.0`.|Yes              |
| maxunavailable | int    | Number of nodes to be upgraded or rolled back at the same time| If the value of `maxunavailable` is greater than the actual number of nodes in the cluster, the deployment can be performed. The upgrade or rollback is performed based on the actual number of nodes in the cluster.|Yes              |
| dockerimage    | string | Docker image used for upgrade              | The value must be in the *repository/name:tag* format. This parameter is valid only when the Docker image is used for upgrade.|Yes              |
| imageurl       | string | Address of the disk image used for the upgrade| `imageurl` contains the protocol and only HTTP or HTTPS is supported. For example, `https://192.168.122.15/update.img` is valid only when a disk image is used for upgrade.|Yes              |
| checksum       | string | Checksum (SHA-256) value for disk image verification during the upgrade.                     | This parameter is valid only when a disk image is used for upgrade.|Yes              |
| flagSafe       | bool   | Whether `imageurl` specifies a secure HTTP address                | The value must be `true` or `false`. This parameter is valid only when `imageurl` specifies an HTTP address.|Yes              |
| mtls           | bool   | Whether HTTPS two-way authentication is used for the connection to the `imageurl` address.    | The value must be `true` or `false`. This parameter is valid only when `imageurl` specifies an HTTPS address.|Yes              |
| cacert         | string | Root certificate file used for HTTPS or HTTPS two-way authentication                      | This parameter is valid only when `imageurl` specifies an HTTPS address.| This parameter is mandatory when `imageurl` specifies an HTTPS address.|
| clientcert     | string | Client certificate file used for HTTPS two-way authentication                         | This parameter is valid only when HTTPS two-way authentication is used.|This parameter is mandatory when `mtls` is set to `true`.|
| clientkey      | string | Client public key used for HTTPS two-way authentication                             | This parameter is valid only when HTTPS two-way authentication is used.|This parameter is mandatory when `mtls` is set to `true`.|

The address specified by `imageurl` contains the protocol. Only the HTTP or HTTPS protocol is supported. If `imageurl` is set to an HTTPS address, secure transmission is used. If `imageurl` is set to an HTTP address, set `flagSafe` to `true`, because the image can be downloaded only when the address is secure. If `imageurl` is set to an HTTP address but `flagSafe` is not set to `true`, the address is insecure by default. The image will not be downloaded, and a message is written to the log of the node to be upgraded indicating that the address is insecure.

You are advised to set `imageurl` to an HTTPS address. In this case, ensure that the required certificate has been installed on the node to be upgraded. If the image server is maintained by yourself, you need to sign the certificate and ensure that the certificate has been installed on the node to be upgraded. Place the certificate in the **/etc/KubeOS/certs** directory of KubeOS. The administrator specifies the address and must ensure the security of the address. An intranet address is recommended.

The container OS image provider must check the validity of the image to ensure that the downloaded container OS image is from a reliable source.

Compile the YAML file for deploying the OS as a custom resource (CR) instance in the cluster. The following is an example YAML file for deploying the CR instance:

- Upgrade using a disk image

  ```
  apiVersion: upgrade.openeuler.org/v1alpha1
  kind: OS
  metadata:
    name: os-sample
  spec:
    imagetype: disk
    opstype: upgrade
    osversion: edit.os.version
    maxunavailable: edit.node.upgrade.number
    dockerimage: ""
    imageurl: edit.image.url
    checksum: image.checksum
    flagSafe: imageurl.safety
    mtls: imageurl use mtls or not
    cacert:  ca certificate 
    clientcert:  client certificate 
    clientkey:  client certificate key 
  ```

- Upgrade using a Docker image

  ``` shell
  apiVersion: upgrade.openeuler.org/v1alpha1
  kind: OS
  metadata:
    name: os-sample
  spec:
    imagetype: docker
    opstype: upgrade
    osversion: edit.os.version
    maxunavailable: edit.node.upgrade.number
    dockerimage: dockerimage like repository/name:tag
    imageurl: ""
    checksum: ""
    flagSafe: false
    mtls: true
  ```

  Before using a Docker image to perform the upgrade, create the image first. For details about how to create a Docker image, see **KubeOS Image Creation**.

Assume that the YAML file is **upgrade_v1alpha1_os.yaml**.

Check the OS version of the node that is not upgraded.

```
kubectl get nodes -o custom-columns='NAME:.metadata.name,OS:.status.nodeInfo.osImage'
```

Run the following command to deploy the CR instance in the cluster. The node is upgraded based on the configured parameters.

```
kubectl apply -f upgrade_v1alpha1_os.yaml
```

Check the node OS version again to determine whether the node upgrade is complete.

```
kubectl get nodes -o custom-columns='NAME:.metadata.name,OS:.status.nodeInfo.osImage'
```

> ![](./public_sys-resources/icon-note.gif)**NOTE**:
>
> If you need to perform the upgrade again, modify the `imageurl`, `osversion`, `checksum`, `maxunavailable`, `flagSafe`, or `dockerimage` parameters in **upgrade_v1alpha1_os.yaml**.

## Rollback

### Application Scenarios

- If a node cannot be started, you can only manually roll back the container OS to the previous version that can be properly started.
- If a node can be started and run the system, you can manually or use KubeOS (similar to the upgrade) to roll back the container OS. You are advised to use KubeOS.

### Manual Rollback

Manually restart the node and select the second boot option to roll back the container OS. Manual rollback can only roll back the container OS to the version before the upgrade.

### KubeOS-based Rollback

- Roll back to any version.
  - Modify the YAML configuration file (for example, **upgrade_v1alpha1_os.yaml**) of the CR instance of the OS and set the corresponding fields to the image information of the target source version. The OS type comes from the CRD object created in the installation and deployment sections. For details about the fields and examples, see the previous section.

  - After the YAML is modified, run the update command. After the custom object is updated in the cluster, the node performs rollback based on the configured field information.

    ```
    kubectl apply -f upgrade_v1alpha1_os.yaml
    ```

- Roll back to the previous version.

  - Modify the **upgrade_v1alpha1_os.yaml** file. Set **osversion** to the previous version and **opstype** to **rollback** to roll back to the previous version (that is, switch to the previous partition). Example YAML:

    ```
    apiVersion: upgrade.openeuler.org/v1alpha1
    kind: OS
    metadata:
      name: os-sample
    spec:
      imagetype: ""
      opstype: rollback
      osversion: KubeOS pervious version
      maxunavailable: 2
      dockerimage: ""
      imageurl: ""
      checksum: ""
      flagSafe: false
      mtls:true
    ```

  - After the YAML is modified, run the update command. After the custom object is updated in the cluster, the node performs rollback based on the configured field information.

    ```
    kubectl apply -f upgrade_v1alpha1_os.yaml
    ```

    After the update is complete, the node rolls back the container OS based on the configuration information.

- Check the OS version of the container on the node to determine whether the rollback is successful.

  ```
  kubectl get nodes -o custom-columns='NAME:.metadata.name,OS:.status.nodeInfo.osImage'
  ```

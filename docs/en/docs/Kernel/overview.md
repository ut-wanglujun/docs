## Overview

This feature allows you to allocate memory with corresponding reliability as required and mitigates the impact of some possible UCEs or CEs to some extent. In this way, the overall service reliability does not deteriorate when partial memory mirroring (a RAS feature called address range mirroring) is used.
# Desktop Environment User Guide

This document describes how to install and use four common desktop environments (UKUI, DDE, Xfce, and GNOME), which provide a user-friendly, secure, and reliable GUI for better user experience.

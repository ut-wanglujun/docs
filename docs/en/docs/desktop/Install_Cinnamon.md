# Installing Cinnamon on openEuler

Cinnamon is the most commonly used desktop environment running in Unix-like operating systems. It is also a desktop environment with complete functions, simple operations, user-friendly interfaces, and integrated use and development capabilities. It is also a formal desktop planned by GNU.

For users, Cinnamon is a suite that integrates the desktop environment and applications. For developers, Cinnamon is an application development framework, consisting of a large number of function libraries. Applications written in Cinnamon can run properly even if users do not run the Cinnamon desktop environment.

Cinnamon contains basic software such as the file manager, application store, and text editor, and advanced applications and tools such as system sampling analysis, system logs, software engineering IDE, web browser, simple virtual machine monitor, and developer document browser.

You are advised to create an administrator during the installation.

1. Configure the source and update the system.
[Download](https://openeuler.org/en/) the openEuler ISO file, install the system, and update the software source. (You need to configure the Everything source and EPOL source. The following command is used to install the Cinnamon in the minimum installation system.)

```
sudo dnf update
```

2. Install the font library.

```
sudo dnf install dejavu-fonts liberation-fonts gnu-*-fonts google-*-fonts
```

3. Install the Xorg.

```
sudo dnf install xorg-*
```
Unnecessary packages may be installed during the installation. You can run the following commands to install necessary Xorg packages:

```
sudo dnf install xorg-x11-apps xorg-x11-drivers xorg-x11-drv-ati \
	xorg-x11-drv-dummy xorg-x11-drv-evdev xorg-x11-drv-fbdev xorg-x11-drv-intel \
	xorg-x11-drv-libinput xorg-x11-drv-nouveau xorg-x11-drv-qxl \
	xorg-x11-drv-synaptics-legacy xorg-x11-drv-v4l xorg-x11-drv-vesa \
	xorg-x11-drv-vmware xorg-x11-drv-wacom xorg-x11-fonts xorg-x11-fonts-others \
	xorg-x11-font-utils xorg-x11-server xorg-x11-server-utils xorg-x11-server-Xephyr \
	xorg-x11-server-Xspice xorg-x11-util-macros xorg-x11-utils xorg-x11-xauth \
	xorg-x11-xbitmaps xorg-x11-xinit xorg-x11-xkb-utils
```

4. Install Cinnamon and components.

```
sudo dnf install cinnamon cinnamon-control-center cinnamon-desktop \
	cinnamon-menus cinnamon-screensaver cinnamon-session \
	cinnamon-settings-daemon  cinnamon-themes cjs \
	nemo nemo-extensions  muffin cinnamon-translations inxi \
	perl-XML-Dumper xapps mint-x-icons mint-y-icons mintlocale \
	python3-plum-py caribou mozjs78 python3-pam \
	python3-tinycss2 python3-xapp tint2 gnome-terminal \
	lightdm lightdm-gtk
```

5. Enable LightDM to automatically start upon system startup.

```
sudo systemctl enable lightdm
```

6. Set the system to log in to the GUI by default.

```
sudo systemctl set-default graphical.target
```
7. Reboot.

```
sudo reboot
```

# Memory Fabric User Guide

- [Introduction](#introduction)
- [Deployment](#deployment)
- [Startup](#startup)
- [Interfaces](#interfaces)

## Introduction

The Memory Fabric memory pool suite builds high-performance distributed memory resource pool based on memory medium and new network technologies, and provides services for applications through semantics such as BigMemory, MemPlog, and MemKV.

Memory Fabric uses and manages resource such as DRAM, persistent memory, and RAM drives, and provides microsecond-level latency based on new interconnection protocols (RDMA, XCL, and UBUS). By reducing the software latency and node CPU overhead, Memory Fabric improves the overall performance. (Pools of directly connect drives are not supported.)

## Deployment

Memory Fabric supports multiple deployment modes for multi-scenario application integration based on the node types, resource distribution, and application requirements. Deployment modes include:

- Co-deployed clients and servers when computing and Memory Fabric resources are on the same node. (Node1 and Node2 in [Figure 1](#fig17349154610267))
- Independently deployed clients ands server when computing and Memory Fabric resources are on different nodes. (Node3 and Node4 in [Figure 1](#fig17349154610267))
- Hybrid deployment of co-deployed clients and servers and independently deployed clients and servers.
- Multiple clients and servers in one node. (Node2 in [Figure 1](#fig17349154610267))
- Inter-process communication (IPC) for intra-node communication between clients and servers, and remote procedure call (RPC) for connection to remote servers.
- Servers connected to the cluster management service for unified node numbering an resource information broadcasting.

**Figure 1** Integrated deployment <a name="fig17349154610267"></a>
![](images/IntegratedDeployment.png)

## Startup

To start Memory Fabric, you need to start the server and client. Start the server on the node first, perform the cluster registration and monitoring procedures, and initiate resource registration (the total capacity is obtained from the configuration file) and communication link establishment. After the initialization, multiple local clients can connect to the server and provide services for external systems.

## Interfaces

<a name="table104mcpsimp"></a>

<table><tbody><tr id="row109mcpsimp"><td class="cellrowborder" valign="top" width="77%"><p id="p111mcpsimp"><a name="p111mcpsimp"></a><a name="p111mcpsimp"></a>Interface </p>
</td>
<td class="cellrowborder" valign="top" width="23%"><p id="p113mcpsimp"><a name="p113mcpsimp"></a><a name="p113mcpsimp"></a>Description</p>
</td>
</tr>
<tr id="row114mcpsimp"><td class="cellrowborder" valign="top" width="77%"><p id="p116mcpsimp"><a name="p116mcpsimp"></a><a name="p116mcpsimp"></a>int BM_Init(char *ockPath, int flags);</p>
<p id="p117mcpsimp"><a name="p117mcpsimp"></a><a name="p117mcpsimp"></a>int BM_InitWithExtAttr(char*ockPath, int flags, void *attr);</p>
<p id="p118mcpsimp"><a name="p118mcpsimp"></a><a name="p118mcpsimp"></a>int BM_Exit(void);</p>
</td>
<td class="cellrowborder" valign="top" width="23%"><p id="p119mcpsimp"><a name="p119mcpsimp"></a><a name="p119mcpsimp"></a>Initializes </p>
<p id="p120mcpsimp"><a name="p120mcpsimp"></a><a name="p120mcpsimp"></a>Initializes with attributes </p>
<p id="p121mcpsimp"><a name="p121mcpsimp"></a><a name="p121mcpsimp"></a>Exits</p>
</td>
</tr>
<tr id="row122mcpsimp"><td class="cellrowborder" valign="top" width="77%"><p id="p124mcpsimp"><a name="p124mcpsimp"></a><a name="p124mcpsimp"></a>int BM_CreateNSSec(const char*poolName, const char *ns, int attribute, const char*secId);</p>
<p id="p125mcpsimp"><a name="p125mcpsimp"></a><a name="p125mcpsimp"></a>int BM_DestroyNSSec(const char *poolName, const char*ns, const char *secId);</p>
<p id="p126mcpsimp"><a name="p126mcpsimp"></a><a name="p126mcpsimp"></a>int BM_QueryNSCache(QueryNsCache*buff, size_t buffLen);</p>
</td>
<td class="cellrowborder" valign="top" width="23%"><p id="p128mcpsimp"><a name="p128mcpsimp"></a><a name="p128mcpsimp"></a>Creates, destroys, and queries namespaces.</p>
</td>
</tr>
<tr id="row129mcpsimp"><td class="cellrowborder" valign="top" width="77%"><p id="p131mcpsimp"><a name="p131mcpsimp"></a><a name="p131mcpsimp"></a>int BM_AllocSec(BmInfo *bminfo, size_t size, int flags, BmAffinity* affinity);</p>
<p id="p132mcpsimp"><a name="p132mcpsimp"></a><a name="p132mcpsimp"></a>int BM_FreeSec(BmInfo *bminfo, int flags);</p>
<p id="p133mcpsimp"><a name="p133mcpsimp"></a><a name="p133mcpsimp"></a>int BM_BatchFreeSec(char*bmIdArray[], int num, int flags, const char *ns, const char*secId);</p>
</td>
<td class="cellrowborder" valign="top" width="23%"><p id="p135mcpsimp"><a name="p135mcpsimp"></a><a name="p135mcpsimp"></a>Applies for and releases BigMemory objects</p>
</td>
</tr>
<tr id="row136mcpsimp"><td class="cellrowborder" valign="top" width="77%"><p id="p138mcpsimp"><a name="p138mcpsimp"></a><a name="p138mcpsimp"></a>int BM_ExpandSec(BmInfo *bminfo, size_t size, int flags);</p>
</td>
<td class="cellrowborder" valign="top" width="23%"><p id="p140mcpsimp"><a name="p140mcpsimp"></a><a name="p140mcpsimp"></a>Expanses BigMemory capacity</p>
</td>
</tr>
<tr id="row141mcpsimp"><td class="cellrowborder" valign="top" width="77%"><p id="p143mcpsimp"><a name="p143mcpsimp"></a><a name="p143mcpsimp"></a>int BM_MapSec(BmInfo*bminfo, int prot, int flags, void **ptr);</p>
<p id="p144mcpsimp"><a name="p144mcpsimp"></a><a name="p144mcpsimp"></a>int BM_Unmap(char *bmId, void *ptr);</p>
</td>
<td class="cellrowborder" valign="top" width="23%"><p id="p146mcpsimp"><a name="p146mcpsimp"></a><a name="p146mcpsimp"></a>Maps BigMemory to continuous virtual space and unmaps</p>
</td>
</tr>
<tr id="row147mcpsimp"><td class="cellrowborder" valign="top" width="77%"><p id="p149mcpsimp"><a name="p149mcpsimp"></a><a name="p149mcpsimp"></a>int BM_CopySec(BmInfo *srcBM, size_t srcOffset, BmInfo *dstBM, size_t dstOffset, size_t length, int flags);</p>
</td>
<td class="cellrowborder" valign="top" width="23%"><p id="p151mcpsimp"><a name="p151mcpsimp"></a><a name="p151mcpsimp"></a>Copies between BigMemory objects </p>
</td>
</tr>
<tr id="row152mcpsimp"><td class="cellrowborder" valign="top" width="77%"><p id="p154mcpsimp"><a name="p154mcpsimp"></a><a name="p154mcpsimp"></a>int BM_SpecificObjSwapInSec(DiskFileDesc *file, BmInfo *bminfo, int flags, size_t offset, size_t length);</p>
</td>
<td class="cellrowborder" valign="top" width="23%"><p id="p156mcpsimp"><a name="p156mcpsimp"></a><a name="p156mcpsimp"></a>Swaps file content into the BigMemory object</p>
</td>
</tr>
<tr id="row157mcpsimp"><td class="cellrowborder" valign="top" width="77%"><p id="p159mcpsimp"><a name="p159mcpsimp"></a><a name="p159mcpsimp"></a>int BM_ReadSec(BmInfo *bminfo, size_t offset, size_t length, void *buffer, int flags);</p>
<p id="p160mcpsimp"><a name="p160mcpsimp"></a><a name="p160mcpsimp"></a>int BM_WriteSec(BmInfo *bminfo, size_t offset, size_t length, const void *buffer, int flags);</p>
</td>
<td class="cellrowborder" valign="top" width="23%"><p id="p162mcpsimp"><a name="p162mcpsimp"></a><a name="p162mcpsimp"></a>Reads and write BigMemory </p>
</td>
</tr>
<tr id="row163mcpsimp"><td class="cellrowborder" valign="top" width="77%"><p id="p165mcpsimp"><a name="p165mcpsimp"></a><a name="p165mcpsimp"></a>int BM_GatherSec(intptr_t *inputInfo, BmOperatorCode operatorCode, int flags, BmInfo *bminfo, size_t *bmLen);</p>
</td>
<td class="cellrowborder" valign="top" width="23%"><p id="p167mcpsimp"><a name="p167mcpsimp"></a><a name="p167mcpsimp"></a>Performs aggregation</p>
</td>
</tr>
</tbody>
</table>

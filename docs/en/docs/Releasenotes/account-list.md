# Account List

| User Name| Default Password     | Function          | User Status| Login Mode          | Remarks                                                        |
| ------ | ------------- | ------------------ | -------- | ------------------ | ------------------------------------------------------------ |
| root   | openEuler12#$ | Default user of the VM image| Enabled    | Remote login          | This account is used to log in to the VM installed using the openEuler VM image.                     |
| root   | openEuler#12  | GRUB2 login         | Enabled    | Local login and remote login| GRand UnifiedBootloader (GRUB) is used to boot different systems, such as Windows and Linux.<br>GRUB2 is an upgraded version of GRUB. When the system is started, you can modify startup parameters on the GRUB2 GUI. To ensure that the system startup parameters are modified with authorization, you need to encrypt the GRUB2 GUI. The GRUB2 GUI can be modified only when you enter the correct GRUB2 password.|

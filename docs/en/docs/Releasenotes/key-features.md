# Key Features<a name="ZH-CN_TOPIC_0228254580"></a>

## OpenEuler 22.09 is built based on Linux Kernel 5.10 and absorbs beneficial features and innovative features of later versions from the community

- **Kmesh**: Kmesh is a high-performance mesh data plane based on the programmable kernel. It provides high-performance service communication capabilities in service mesh scenarios.

- **NFS multipathing**：This feature allows an NFS mount point to be accessed by using multiple IP addresses, increasing the access performance and fault switching capability of NFS. The NFS and sunrpc kernel modules are modified to implement this feature.

## WASM Security Sandbox Engine

Framework as a Service (FaaS) is a new computing paradigm of cloud computing. With FaaS, users can quickly build applications and services of any type. FaaS features agile development, auto scaling, pay-per-use, and O&M-free. Although the conventional container-based FaaS solution can decouple customized computing capabilities from CDN services and implement fast iteration and update, in scenarios with high concurrency and heavy traffic, the cold start speed and memory overhead of containers cannot meet the requirements of quick execution and processing of tens of thousands of instances on a single node in service scenarios.
This feature provides the security sandbox capability based on the WebAssembly (WASM) technology. Functions are deployed in the WASM security sandbox to implement function isolation. This solves the problems of slow container cold start and high memory overhead in high-concurrency scenarios.
**WASM function management framework**

- Supports listening and processing highly concurrent function requests.
- Supports function lifecycle management.
- Compatible with OCI container images and manages local function image resources.
  **WASM lightweight coroutine scheduling framework**
- Abstracts the execution context of WASM instances, supports lightweight and high-performance user-mode coroutine scheduling models, and supports multiple WASM instance execution models such as JIT and AOT.

## HPC Running Base, Data-Control Separated HCK

Most HPC service features comply with the Bulk Synchronous Parallel Computing (BSP) model, with parallel computing + communication + synchronization. The system noise has a great impact on the performance of such services. The system noise refers to non-application computing tasks executed during service running, including system/user-mode daemon processes, kernel daemon processes, memory management, system scheduling overhead, non-computing tasks of service application, overhead (cache misses, page faults) caused by resource competition, and more. According to the analysis on system noise, the longer the system noise length and the shorter the noise interval, the greater the impact on the HPC application performance. The longer the application synchronization time and the larger the number of running nodes, the greater the impact on the HPC application performance. For an enterprise-level system, the operation scale is large, and the system noise has a significant impact on the performance. The HCK greatly reduces the system noise through the data-control separation design.
The separated data-control base provides the following functions:

- **CPU isolation management**: Reserves specified CPUs during system startup.
- **Task domain management**: Creates task domains and configures affinity when processes are running on isolated CPUs.
- **Topology filtering**: Controls the visibility range when some interfaces under proc and sysfs obtain the CPU topology.

## StratoVirt Standard Virtualization Enhancement

StratoVirt is an enterprise-grade virtualization platform designed for cloud data centers. "Strato" is abbreviated from "stratosphere", which is the layer of Earth's atmosphere immediately above the troposphere. It means a light protective layer that protects services on the openEuler platform.
**StratoVirt has the following advantages**:

- **Robust security**: The memory safe language Rust is used, modules are combined as required to minimize the attack surface, and physical isolation of multiple tenants is supported.
- **Lightweight and low overhead**: When the simplified device model is used, the startup time is less than 50 ms, and the memory overhead is less than 4 MB.
- **Software and hardware collaboration**: Supports x86 VT-x and Kunpeng Kunpeng-V.
- **Fast scaling**: StratoVirt helps realize device scaling within milliseconds, providing flexible resource scaling capabilities for lightweight workloads.
- **Multi-scenario support**: One architecture supports multiple application scenarios, such as serverless, secure containers, and standard VMs.

## HybridSched Virtualized Hybrid Scheduling

Low resource utilization of cloud data centers is a common problem in the industry. Improving resource utilization has become an important technical issue. Deploying services based on priorities (hybrid deployment) is a typical effective method to improve resource utilization. The core technology of hybrid deployment is resource isolation and control.
HybridSched is a full-stack solution for hybrid deployment of VMs, including enhanced OpenStack cluster scheduling capability, new single-node QoS management component Skylark, and kernel-mode basic resource isolation capability. Skylark is a QoS-aware resource scheduler in scenarios where high- and low-priority VMs are deployed together. It improves physical machine resource utilization while ensuring the QoS of high-priority VMs.

- **Enhanced cluster scheduling**: The OpenStack Nova capability is enhanced to support priority-based semantic scheduling.
- **Power consumption control**: The CPU bandwidth of low-priority VMs is limited to reduce the power consumption of the entire system and ensure the QoS of high-priority VMs.
- **Cache and memory bandwidth control**: Limits the LLC and memory bandwidth of low-priority VMs. Currently, only static allocation is supported.
- **CPU interference control**: Supports CPU time slice preemption in μs, SMT interference isolation, and anti-priority inversion.

## Third-party Application Support

- **OpenStack Yoga**: The OpenStack version is updated to the latest stable version Yoga released in April 2022, and the OpenStack-Helm component is supported in the OpenStack Yoga version of openEuler.
- **OpenStack deployment tool opensd**: Supports basic deployment of OpenStack Yoga on openEuler 22.09.
- **Hybrid deployment of OpenStack Yoga on VMs**: The high-priority and low-priority VM technology is introduced to OpenStack Nova. VMs that have different requirements on CPU, I/O, and memory resources are deployed and migrated to the same compute node in scheduling, fully utilizing node resources.
- **File backup and restoration**: Provide functions such as system backup, file backup, customized restoration, and one-click backup and restoration, greatly reducing O&M costs.

## Desktop Environments

More desktop environments are provided to ensure better development experience.

- **DDE** is upgraded and supports the update tool, control center, file management, system monitoring tool, and log collection tool.
- The **Cinnamon** desktop environment is added to support desktop, file management, and control center.
- *kiran-desktop 2.3*, Controls center plug-in integration, adds volume/sound setting, adds network setting plug-in, and the Qt theme plug-in.

# OS Installation<a name="ZH-CN_TOPIC_0225731123"></a>

## Release Files<a name="section19865103114280"></a>

The openEuler release files include [ISO release package](http://repo.openeuler.org/openEuler-22.09/ISO/), [VM images](http://repo.openeuler.org/openEuler-22.09/virtual_machine_img/), [container images](http://repo.openeuler.org/openEuler-22.09/docker_img/), [embedded images](http://repo.openeuler.org/openEuler-22.09/embedded_img/), and [repo sources](http://repo.openeuler.org/openEuler-22.09/). [Table 1](#table8396719144315) describes the ISO release packages. [Table 3](#table1276911538154) describes the container images. [Table 5](#table953512211576) describes the repo sources, which are convenient for online use.

**Table 1** ISO release packages

<a name="table8396719144315"></a>

<table><thead align="left"><tr id="row19396819194314"><th class="cellrowborder" valign="top" width="38.53%" id="mcps1.2.3.1.1"><p id="p999543016432"><a name="p999543016432"></a><a name="p999543016432"></a>Name</p>
</th>
<th class="cellrowborder" valign="top" width="61.47%" id="mcps1.2.3.1.2"><p id="p7396719104315"><a name="p7396719104315"></a><a name="p7396719104315"></a>Description</p>
</th>
</tr>
</thead>
<tbody><tr id="row739615199431"><td class="cellrowborder" valign="top" width="38.53%" headers="mcps1.2.3.1.1 "><p id="p16396111912430"><a name="p16396111912430"></a><a name="p16396111912430"></a>openEuler-22.09-aarch64-dvd.iso</p>
</td>
<td class="cellrowborder" valign="top" width="61.47%" headers="mcps1.2.3.1.2 "><p id="p177033394133"><a name="p177033394133"></a><a name="p177033394133"></a>Base installation ISO file of the AArch64 architecture, including the core components for running the minimum system.</p>
</td>
</tr>
<tr id="row83961519184313"><td class="cellrowborder" valign="top" width="38.53%" headers="mcps1.2.3.1.1 "><p id="p153964194439"><a name="p153964194439"></a><a name="p153964194439"></a>openEuler-22.09-everything-aarch64-dvd.iso</p>
</td>
<td class="cellrowborder" valign="top" width="61.47%" headers="mcps1.2.3.1.2 "><p id="p1765163817452"><a name="p1765163817452"></a><a name="p1765163817452"></a>Full installation ISO file of the AArch64 architecture, including all components for running the entire system.</p>
</td>
</tr>
<tr id="row83968192435"><td class="cellrowborder" valign="top" width="38.53%" headers="mcps1.2.3.1.1 "><p id="p19396319184313"><a name="p19396319184313"></a><a name="p19396319184313"></a>openEuler-22.09-everything-debug-aarch64-dvd.iso</p>
</td>
<td class="cellrowborder" valign="top" width="61.47%" headers="mcps1.2.3.1.2 "><p id="p0157139181912"><a name="p0157139181912"></a><a name="p0157139181912"></a>ISO file for openEuler debugging in the AArch64 architecture, including the symbol table information required for debugging.</p>
</td>
</tr>
<tr id="row521955165312"><td class="cellrowborder" valign="top" width="38.53%" headers="mcps1.2.3.1.1 "><p id="p92191359537"><a name="p92191359537"></a><a name="p92191359537"></a>openEuler-22.09-x86_64-dvd.iso</p>
</td>
<td class="cellrowborder" valign="top" width="61.47%" headers="mcps1.2.3.1.2 "><p id="p6846145114195"><a name="p6846145114195"></a><a name="p6846145114195"></a>Base installation ISO file of the x86_64 architecture, including the core components for running the minimum system.</p>
</td>
</tr>
<tr id="row1964171015310"><td class="cellrowborder" valign="top" width="38.53%" headers="mcps1.2.3.1.1 "><p id="p3964710125311"><a name="p3964710125311"></a><a name="p3964710125311"></a>openEuler-22.09-everything-x86_64-dvd.iso</p>
</td>
<td class="cellrowborder" valign="top" width="61.47%" headers="mcps1.2.3.1.2 "><p id="p101571442012"><a name="p101571442012"></a><a name="p101571442012"></a>Full installation ISO file of the x86_64 architecture, including all components for running the entire system.</p>
</td>
</tr>
<tr id="row342917865317"><td class="cellrowborder" valign="top" width="38.53%" headers="mcps1.2.3.1.1 "><p id="p242914815315"><a name="p242914815315"></a><a name="p242914815315"></a>openEuler-22.09-everything-debuginfo-x86_64-dvd.iso</p>
</td>
<td class="cellrowborder" valign="top" width="61.47%" headers="mcps1.2.3.1.2 "><p id="p6680177162617"><a name="p6680177162617"></a><a name="p6680177162617"></a>ISO file for openEuler debugging in the x86_64 architecture, including the symbol table information required for debugging.</p>
</td>
</tr>
<tr id="row34221536125311"><td class="cellrowborder" valign="top" width="38.53%" headers="mcps1.2.3.1.1 "><p id="p13423336175314"><a name="p13423336175314"></a><a name="p13423336175314"></a>openEuler-22.09-source-dvd.iso</p>
</td>
<td class="cellrowborder" valign="top" width="61.47%" headers="mcps1.2.3.1.2 "><p id="p108431625162112"><a name="p108431625162112"></a><a name="p108431625162112"></a>ISO file of the openEuler source code.</p>
</td>
</tr>
<tr id="row34221536125311"><td class="cellrowborder" valign="top" width="38.53%" headers="mcps1.2.3.1.1 "><p id="p13423336175314"><a name="p13423336175314"></a><a name="p13423336175314"></a>openEuler-21.09-edge-aarch64-dvd.iso</p>
</td>
<td class="cellrowborder" valign="top" width="61.47%" headers="mcps1.2.3.1.2 "><p id="p108431625162112"><a name="p108431625162112"></a><a name="p108431625162112"></a>Edge ISO file in the AArch64 architecture, including the core components for running the minimum system.</p>
</td>
</tr>
<tr id="row34221536125311"><td class="cellrowborder" valign="top" width="38.53%" headers="mcps1.2.3.1.1 "><p id="p13423336175314"><a name="p13423336175314"></a><a name="p13423336175314"></a>openEuler-21.09-edge-x86_64-dvd.iso</p>
</td>
<td class="cellrowborder" valign="top" width="61.47%" headers="mcps1.2.3.1.2 "><p id="p108431625162112"><a name="p108431625162112"></a><a name="p108431625162112"></a>Edge ISO file in the x86_64 architecture, including the core components for running the minimum system.</p>
</td>
</tr>
<tr id="row34221536125311"><td class="cellrowborder" valign="top" width="38.53%" headers="mcps1.2.3.1.1 "><p id="p13423336175314"><a name="p13423336175314"></a><a name="p13423336175314"></a>openEuler-21.09-Desktop-aarch64-dvd.iso</p>
</td>
<td class="cellrowborder" valign="top" width="61.47%" headers="mcps1.2.3.1.2 "><p id="p108431625162112"><a name="p108431625162112"></a><a name="p108431625162112"></a>Desktop ISO file in the AArch64 architecture, including the minimum software set for running the development desktop.</p>
</td>
</tr>
<tr id="row34221536125311"><td class="cellrowborder" valign="top" width="38.53%" headers="mcps1.2.3.1.1 "><p id="p13423336175314"><a name="p13423336175314"></a><a name="p13423336175314"></a>openEuler-21.09-Desktop-x86_64-dvd.iso</p>
</td>
<td class="cellrowborder" valign="top" width="61.47%" headers="mcps1.2.3.1.2 "><p id="p108431625162112"><a name="p108431625162112"></a><a name="p108431625162112"></a>Desktop ISO file in the x86_64 architecture, including the minimum software set for running the development desktop.</p>
</td>
</tr>
</tbody>
</table>

**Table 2** VM images

<a name="table1995101714610"></a>

<table><thead align="left"><tr id="row149511174464"><th class="cellrowborder" valign="top" width="36.9%" id="mcps1.2.3.1.1"><p id="p2095111718462"><a name="p2095111718462"></a><a name="p2095111718462"></a>Name</p>
</th>
<th class="cellrowborder" valign="top" width="63.1%" id="mcps1.2.3.1.2"><p id="p129511177462"><a name="p129511177462"></a><a name="p129511177462"></a>Description</p>
</th>
</tr>
</thead>
<tbody><tr id="row11951121712462"><td class="cellrowborder" valign="top" width="36.9%" headers="mcps1.2.3.1.1 "><p id="p151011740115214"><a name="p151011740115214"></a><a name="p151011740115214"></a>openEuler-22.09-aarch64.qcow2.xz</p>
</td>
<td class="cellrowborder" valign="top" width="63.1%" headers="mcps1.2.3.1.2 "><p id="p9865159194813"><a name="p9865159194813"></a><a name="p9865159194813"></a>VM image of openEuler in the AArch64 architecture.</p>
</td>
</tr>
<tr id="row1695171774614"><td class="cellrowborder" valign="top" width="36.9%" headers="mcps1.2.3.1.1 "><p id="p10510154855215"><a name="p10510154855215"></a><a name="p10510154855215"></a>openEuler-22.09-x86_64.qcow2.xz</p>
</td>
<td class="cellrowborder" valign="top" width="63.1%" headers="mcps1.2.3.1.2 "><p id="p1895111177468"><a name="p1895111177468"></a><a name="p1895111177468"></a>VM image of openEuler in the x86_64 architecture.</p>
</td>
</tr>
</tbody>
</table>

>![](./public_sys-resources/icon-note.gif) **NOTE**  
>The default password of the **root** user of the VM image is **openEuler12#$**. Change the password upon the first login.

**Table 3** Container images

<a name="table1276911538154"></a>

<table><thead align="left"><tr id="row1777075317151"><th class="cellrowborder" valign="top" width="31.509999999999998%" id="mcps1.2.3.1.1"><p id="p1277018533152"><a name="p1277018533152"></a><a name="p1277018533152"></a>Name</p>
</th>
<th class="cellrowborder" valign="top" width="68.49%" id="mcps1.2.3.1.2"><p id="p677015331515"><a name="p677015331515"></a><a name="p677015331515"></a>Description</p>
</th>
</tr>
</thead>
<tbody><tr id="row1277035311157"><td class="cellrowborder" valign="top" width="31.509999999999998%" headers="mcps1.2.3.1.1 "><p id="p17686637201813"><a name="p17686637201813"></a><a name="p17686637201813"></a>open<span id="ph11656938181410"><a name="ph11656938181410"></a><a name="ph11656938181410"></a>E</span>uler-docker.aarch64.tar.xz</p>
</td>
<td class="cellrowborder" valign="top" width="68.49%" headers="mcps1.2.3.1.2 "><p id="p3770165314159"><a name="p3770165314159"></a><a name="p3770165314159"></a>A<span id="ph61191628111518"><a name="ph61191628111518"></a><a name="ph61191628111518"></a>Container image of openEuler in the AArch64 architecture.</p>
</td>
</tr>
<tr id="row1277055310150"><td class="cellrowborder" valign="top" width="31.509999999999998%" headers="mcps1.2.3.1.1 "><p id="p2686337111816"><a name="p2686337111816"></a><a name="p2686337111816"></a>open<span id="ph21198416145"><a name="ph21198416145"></a><a name="ph21198416145"></a>E</span>uler-docker.x86_64.tar.xz</p>
</td>
<td class="cellrowborder" valign="top" width="68.49%" headers="mcps1.2.3.1.2 "><p id="p9770135311518"><a name="p9770135311518"></a><a name="p9770135311518"></a>Container image of openEuler in the x86_64 architecture.</p>
</td>
</tr>
</tbody>
</table>

**Table 4** Embedded images

| Name                                   | Description                                                  |
| -------------------------------------- | ------------------------------------------------------------ |
| arm64/aarch64-std/zImage               | Kernel image that supports QEMU in the AArch64 architecture. |
| arm64/aarch64-std/\*toolchain-22.09.sh | Development and compilation toolchain in the AArch64 architecture. |
| arm64/aarch64-std/\*rootfs.cpio.gz     | File system that supports QEMU in the AArch64 architecture.  |
| arm32/arm-std/zImage                   | Kernel image that supports QEMU in the ARM architecture.     |
| arm32/arm-std/\*toolchain-22.09.sh     | Development and compilation toolchain in the ARM architecture. |
| arm32/arm-std/\*rootfs.cpio.gz         | File system that supports QEMU in the ARM architecture.      |
| source-list/manifest.xml               | Manifest of source code used for building.                   |

**Table 5** Repo sources

<a name="table953512211576"></a>

<table><thead align="left"><tr id="row95351027573"><th class="cellrowborder" valign="top" width="28.000000000000004%" id="mcps1.2.3.1.1"><p id="p1753512213572"><a name="p1753512213572"></a><a name="p1753512213572"></a>Name</p>
</th>
<th class="cellrowborder" valign="top" width="72%" id="mcps1.2.3.1.2"><p id="p253520295717"><a name="p253520295717"></a><a name="p253520295717"></a>Description</p>
</th>
</tr>
</thead>
<tbody><tr id="row115351221577"><td class="cellrowborder" valign="top" width="28.000000000000004%" headers="mcps1.2.3.1.1 "><p id="p95359213577"><a name="p95359213577"></a><a name="p95359213577"></a>ISO</p>
</td>
<td class="cellrowborder" valign="top" width="72%" headers="mcps1.2.3.1.2 "><p id="p553512125711"><a name="p553512125711"></a><a name="p553512125711"></a>Stores ISO images.</p>
</td>
</tr>
<tr id="row155369225718"><td class="cellrowborder" valign="top" width="28.000000000000004%" headers="mcps1.2.3.1.1 "><p id="p1453620295717"><a name="p1453620295717"></a><a name="p1453620295717"></a>OS</p>
</td>
<td class="cellrowborder" valign="top" width="72%" headers="mcps1.2.3.1.2 "><p id="p1953614214577"><a name="p1953614214577"></a><a name="p1953614214577"></a>Stores basic software package sources.</p>
</td>
</tr>
<tr id="row8536142115720"><td class="cellrowborder" valign="top" width="28.000000000000004%" headers="mcps1.2.3.1.1 "><p id="p169307504113"><a name="p169307504113"></a><a name="p169307504113"></a>debuginfo</p>
</td>
<td class="cellrowborder" valign="top" width="72%" headers="mcps1.2.3.1.2 "><p id="p893085010119"><a name="p893085010119"></a><a name="p893085010119"></a>Stores debugging package sources.</p>
</td>
</tr>
<tr id="row1834462641115"><td class="cellrowborder" valign="top" width="28.000000000000004%" headers="mcps1.2.3.1.1 "><p id="p13344026151113"><a name="p13344026151113"></a><a name="p13344026151113"></a>docker_img</p>
</td>
<td class="cellrowborder" valign="top" width="72%" headers="mcps1.2.3.1.2 "><p id="p1534415264118"><a name="p1534415264118"></a><a name="p1534415264118"></a>Stores container images.</p>
</td>
</tr>
<tr id="row16791828125815"><td class="cellrowborder" valign="top" width="28.000000000000004%" headers="mcps1.2.3.1.1 "><p id="p137919284589"><a name="p137919284589"></a><a name="p137919284589"></a><span id="ph9676303584"><a name="ph9676303584"></a><a name="ph9676303584"></a>virtual_machine_img</span></p>
</td>
<td class="cellrowborder" valign="top" width="72%" headers="mcps1.2.3.1.2 "><p id="p479117286581"><a name="p479117286581"></a><a name="p479117286581"></a><span id="ph19963182719110"><a name="ph19963182719110"></a><a name="ph19963182719110"></a>Stores VM images.</span></p>
</td>
</tr>
<tr id="row20986143316445"><td class="cellrowborder" valign="top" width="28.000000000000004%" headers="mcps1.2.3.1.1 "><p id="p5184193919445"><a           name="p5184193919445"></a><a name="p5184193919445"></a><span id="ph9676303545"><a         name="ph9676303545"></a><a name="ph9676303545"></a>embedded_img</span></p>
</td>
<td class="cellrowborder" valign="top" width="72%" headers="mcps1.2.3.1.2 "><p id="p479117286545"><a name="p479117286545"></a><a name="p479117286545"></a><span id="p1018423974945"><a name="p1018423974945"></a><a name="p1018423974945"></a>Stores embedded images.</span></p>
</td>
</tr>
<tr id="row157551217215"><td class="cellrowborder" valign="top" width="28.000000000000004%" headers="mcps1.2.3.1.1 "><p id="p108641479116"><a name="p108641479116"></a><a name="p108641479116"></a>everything</p>
</td>
<td class="cellrowborder" valign="top" width="72%" headers="mcps1.2.3.1.2 "><p id="p168641547816"><a name="p168641547816"></a><a name="p168641547816"></a>Stores full software package sources.</p>
</td>
</tr>
<tr id="row2053613245715"><td class="cellrowborder" valign="top" width="28.000000000000004%" headers="mcps1.2.3.1.1 "><p id="p19239377410"><a name="p19239377410"></a><a name="p19239377410"></a>extras</p>
</td>
<td class="cellrowborder" valign="top" width="72%" headers="mcps1.2.3.1.2 "><p id="p2923113716419"><a name="p2923113716419"></a><a name="p2923113716419"></a>Stores extended software package sources.</p>
</td>
</tr>
<tr id="row17445193315911"><td class="cellrowborder" valign="top" width="28.000000000000004%" headers="mcps1.2.3.1.1 "><p id="p644543385918"><a name="p644543385918"></a><a name="p644543385918"></a>source</p>
</td>
<td class="cellrowborder" valign="top" width="72%" headers="mcps1.2.3.1.2 "><p id="p19445733115918"><a name="p19445733115918"></a><a name="p19445733115918"></a>Stores source code software package.</p>
</td>
</tr>
<tr id="row153622185717"><td class="cellrowborder" valign="top" width="28.000000000000004%" headers="mcps1.2.3.1.1 "><p id="p85361024579"><a name="p85361024579"></a><a name="p85361024579"></a>update</p>
</td>
<td class="cellrowborder" valign="top" width="72%" headers="mcps1.2.3.1.2 "><p id="p125364285717"><a name="p125364285717"></a><a name="p125364285717"></a>Stores update software package sources.</p>
</td>
</tr>
<tr id="row20986143316493"><td class="cellrowborder" valign="top" width="28.000000000000004%" headers="mcps1.2.3.1.1 "><p id="p5184193919496"><a name="p5184193919496"></a><a name="p5184193919496"></a>EPOL</p>
</td>
<td class="cellrowborder" valign="top" width="72%" headers="mcps1.2.3.1.2 "><p id="p1018423974917"><a name="p1018423974917"></a><a name="p1018423974917"></a>Stores extended openEuler package sources.</p>
</td>
</tr>
</tbody>
</table>

## Minimum Hardware Specifications<a name="zh-cn_topic_0182825778_section1542202114014"></a>

[Table 6](#zh-cn_topic_0182825778_tff48b99c9bf24b84bb602c53229e2541) lists the minimum hardware specifications for installing openEuler 22.09-LTS.

**Table 6** Minimum hardware requirements

<a name="zh-cn_topic_0182825778_tff48b99c9bf24b84bb602c53229e2541"></a>

<table><thead align="left"><tr id="zh-cn_topic_0182825778_r36f08b63edea4973a8228200caa2a50b"><th class="cellrowborder" valign="top" width="21.89%" id="mcps1.2.3.1.1"><p id="zh-cn_topic_0182825778_aef3575d97cdf4dcfb65f8d0c8d2d4a76"><a name="zh-cn_topic_0182825778_aef3575d97cdf4dcfb65f8d0c8d2d4a76"></a><a name="zh-cn_topic_0182825778_aef3575d97cdf4dcfb65f8d0c8d2d4a76"></a><strong id="zh-cn_topic_0182825778_abf63bde6a66a4ce5b21d81948fcafe36"><a name="zh-cn_topic_0182825778_abf63bde6a66a4ce5b21d81948fcafe36"></a><a name="zh-cn_topic_0182825778_abf63bde6a66a4ce5b21d81948fcafe36"></a>Component</strong></p>
</th>
<th class="cellrowborder" valign="top" width="78.11%" id="mcps1.2.3.1.2"><p id="zh-cn_topic_0182825778_a919d3bb266c8432fb33c51fa8f3a4fc3"><a name="zh-cn_topic_0182825778_a919d3bb266c8432fb33c51fa8f3a4fc3"></a><a name="zh-cn_topic_0182825778_a919d3bb266c8432fb33c51fa8f3a4fc3"></a><strong id="zh-cn_topic_0182825778_a9386cf027c1e47d99651159bb62130e7"><a name="zh-cn_topic_0182825778_a9386cf027c1e47d99651159bb62130e7"></a><a name="zh-cn_topic_0182825778_a9386cf027c1e47d99651159bb62130e7"></a>Minimum Hardware Specification</strong></p>
</th>
</tr>
</thead>
<tbody><tr id="zh-cn_topic_0182825778_ra68eff5c33a84bb2be6672a48a643d26"><td class="cellrowborder" valign="top" width="21.89%" headers="mcps1.2.3.1.1 "><p id="zh-cn_topic_0182825778_ac0a50d2069ab444cafff180647772df4"><a name="zh-cn_topic_0182825778_ac0a50d2069ab444cafff180647772df4"></a><a name="zh-cn_topic_0182825778_ac0a50d2069ab444cafff180647772df4"></a>CPU</p>
</td>
<td class="cellrowborder" valign="top" width="78.11%" headers="mcps1.2.3.1.2 "><p id="zh-cn_topic_0182825778_p202681030132314"><a name="zh-cn_topic_0182825778_p202681030132314"></a><a name="zh-cn_topic_0182825778_p202681030132314"></a>Kunpeng 920 (AArch64)/ x86_64 (later than Skylake)</p>
<p id="p267183805010"><a name="p267183805010"></a><a name="p267183805010"></a>x86-64 (later than Skylake) </p>
</td>
</tr>
<tr id="zh-cn_topic_0182825778_rf2a5d43b74894a0882b7c17bdfeb697f"><td class="cellrowborder" valign="top" width="21.89%" headers="mcps1.2.3.1.1 "><p id="zh-cn_topic_0182825778_ad00611ec129a41a9841fb579eece7804"><a name="zh-cn_topic_0182825778_ad00611ec129a41a9841fb579eece7804"></a><a name="zh-cn_topic_0182825778_ad00611ec129a41a9841fb579eece7804"></a>Memory</p>
</td>
<td class="cellrowborder" valign="top" width="78.11%" headers="mcps1.2.3.1.2 "><p id="zh-cn_topic_0182825778_a94efe642b8694e5a85747e123b951efc"><a name="zh-cn_topic_0182825778_a94efe642b8694e5a85747e123b951efc"></a><a name="zh-cn_topic_0182825778_a94efe642b8694e5a85747e123b951efc"></a>≥ 8 GB</p>
</td>
</tr>
<tr id="zh-cn_topic_0182825778_rd2c1ebd93ea64e85a5f3fc88dc5ba456"><td class="cellrowborder" valign="top" width="21.89%" headers="mcps1.2.3.1.1 "><p id="zh-cn_topic_0182825778_afd36954546334c1681b5a391bbc386ae"><a name="zh-cn_topic_0182825778_afd36954546334c1681b5a391bbc386ae"></a><a name="zh-cn_topic_0182825778_afd36954546334c1681b5a391bbc386ae"></a>Hard drive</p>
</td>
<td class="cellrowborder" valign="top" width="78.11%" headers="mcps1.2.3.1.2 "><p id="zh-cn_topic_0182825778_p1224172312719"><a name="zh-cn_topic_0182825778_p1224172312719"></a><a name="zh-cn_topic_0182825778_p1224172312719"></a>≥ 120 GB</p>
</td>
</tr>
</tbody>
</table>

## Hardware Compatibility

[Table 7](#zh-cn_topic_0227922427_table39822012) describes the typical configurations of servers and components supported by openEuler. openEuler will support more servers in the future. Partners and developers are welcome to participate in the contribution and verification. For details about the servers supported by openEuler, see [Compatibility List](https://www.openeuler.org/en/compatibility/).

**Table 7** Supported servers and configurations

<a name="zh-cn_topic_0227922427_table39822012"></a>

<table><thead align="left"><tr id="zh-cn_topic_0227922427_row17270681"><th class="cellrowborder" valign="top" width="8.200820082008201%" id="mcps1.2.6.1.1"><p id="p1239010167198"><a name="p1239010167198"></a><a name="p1239010167198"></a>Vendor</p>
</th>
<th class="cellrowborder" valign="top" width="16.28162816281628%" id="mcps1.2.6.1.2"><p id="zh-cn_topic_0227922427_p56747887"><a name="zh-cn_topic_0227922427_p56747887"></a><a name="zh-cn_topic_0227922427_p56747887"></a>Server Name</p>
</th>
<th class="cellrowborder" valign="top" width="18.71187118711871%" id="mcps1.2.6.1.3"><p id="zh-cn_topic_0227922427_p33176134"><a name="zh-cn_topic_0227922427_p33176134"></a><a name="zh-cn_topic_0227922427_p33176134"></a>Server Model</p>
</th>
<th class="cellrowborder" valign="top" width="17.211721172117212%" id="mcps1.2.6.1.4"><p id="p19313161314450"><a name="p19313161314450"></a><a name="p19313161314450"></a>Component</p>
</th>
<th class="cellrowborder" valign="top" width="39.59395939593959%" id="mcps1.2.6.1.5"><p id="p183840814519"><a name="p183840814519"></a><a name="p183840814519"></a>Configuration</p>
</th>
</tr>
</thead>
<tbody><tr id="zh-cn_topic_0227922427_row7284856"><td class="cellrowborder" rowspan="4" valign="top" width="8.200820082008201%" headers="mcps1.2.6.1.1 "><p id="p111661925121918"><a name="p111661925121918"></a><a name="p111661925121918"></a>Huawei</p>
</td>
<td class="cellrowborder" rowspan="4" valign="top" width="16.28162816281628%" headers="mcps1.2.6.1.2 "><p id="zh-cn_topic_0227922427_p34571797"><a name="zh-cn_topic_0227922427_p34571797"></a><a name="zh-cn_topic_0227922427_p34571797"></a>TaiShan 200</p>
</td>
<td class="cellrowborder" rowspan="4" valign="top" width="18.71187118711871%" headers="mcps1.2.6.1.3 "><p id="zh-cn_topic_0227922427_p53202458"><a name="zh-cn_topic_0227922427_p53202458"></a><a name="zh-cn_topic_0227922427_p53202458"></a>2280 (balanced model)</p>
</td>
<td class="cellrowborder" valign="top" width="17.211721172117212%" headers="mcps1.2.6.1.4 "><p id="p632634144516"><a name="p632634144516"></a><a name="p632634144516"></a>CPU</p>
</td>
<td class="cellrowborder" valign="top" width="39.59395939593959%" headers="mcps1.2.6.1.5 "><p id="p269564774512"><a name="p269564774512"></a><a name="p269564774512"></a>HiSilicon Kunpeng 920</p>
</td>
</tr>
<tr id="row127460329457"><td class="cellrowborder" valign="top" headers="mcps1.2.6.1.1 "><p id="p7326641174519"><a name="p7326641174519"></a><a name="p7326641174519"></a>Memory</p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.2.6.1.2 "><p id="p4695124774516"><a name="p4695124774516"></a><a name="p4695124774516"></a>4 x 32 GB 2933MHz</p>
</td>
</tr>
<tr id="row3713103715458"><td class="cellrowborder" valign="top" headers="mcps1.2.6.1.1 "><p id="p13326114115457"><a name="p13326114115457"></a><a name="p13326114115457"></a>RAID card</p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.2.6.1.2 "><p id="p13696154764514"><a name="p13696154764514"></a><a name="p13696154764514"></a>LSI SAS3508</p>
</td>
</tr>
<tr id="row1371312378455"><td class="cellrowborder" valign="top" headers="mcps1.2.6.1.1 "><p id="p732611416456"><a name="p732611416456"></a><a name="p732611416456"></a>Network</p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.2.6.1.2 "><p id="p13696114718455"><a name="p13696114718455"></a><a name="p13696114718455"></a>TM210</p>
</td>
</tr>
<tr id="zh-cn_topic_0227922427_row36446907"><td class="cellrowborder" rowspan="4" valign="top" width="8.200820082008201%" headers="mcps1.2.6.1.1 "><p id="p1831103919198"><a name="p1831103919198"></a><a name="p1831103919198"></a>Huawei</p>
</td>
<td class="cellrowborder" rowspan="4" valign="top" width="16.28162816281628%" headers="mcps1.2.6.1.2 "><p id="p1245317822418"><a name="p1245317822418"></a><a name="p1245317822418"></a>FusionServer Pro</p>
</td>
<td class="cellrowborder" rowspan="4" valign="top" width="18.71187118711871%" headers="mcps1.2.6.1.3 "><p id="p1345214816241"><a name="p1345214816241"></a><a name="p1345214816241"></a>2288H V5 (rack server)</p>
</td>
<td class="cellrowborder" valign="top" width="17.211721172117212%" headers="mcps1.2.6.1.4 "><p id="p1235016319467"><a name="p1235016319467"></a><a name="p1235016319467"></a>CPU</p>
</td>
<td class="cellrowborder" valign="top" width="39.59395939593959%" headers="mcps1.2.6.1.5 "><p id="p148932077463"><a name="p148932077463"></a><a name="p148932077463"></a>Intel(R) Xeon(R) Gold 5118 CPU @ 2.30GHz</p>
</td>
</tr>
<tr id="row12326548454"><td class="cellrowborder" valign="top" headers="mcps1.2.6.1.1 "><p id="p1035012313461"><a name="p1035012313461"></a><a name="p1035012313461"></a>Memory</p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.2.6.1.2 "><p id="p13893379461"><a name="p13893379461"></a><a name="p13893379461"></a>4 x 32 GB 2400MHz</p>
</td>
</tr>
<tr id="row206435916456"><td class="cellrowborder" valign="top" headers="mcps1.2.6.1.1 "><p id="p23509319462"><a name="p23509319462"></a><a name="p23509319462"></a>RAID card</p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.2.6.1.2 "><p id="p16893107144611"><a name="p16893107144611"></a><a name="p16893107144611"></a>LSI SAS3508</p>
</td>
</tr>
<tr id="row5647591455"><td class="cellrowborder" valign="top" headers="mcps1.2.6.1.1 "><p id="p1835019312469"><a name="p1835019312469"></a><a name="p1835019312469"></a>Network</p>
</td>
<td class="cellrowborder" valign="top" headers="mcps1.2.6.1.2 "><p id="p189318714460"><a name="p189318714460"></a><a name="p189318714460"></a>X722</p>
</td>
</tr>
</tbody>
</table>

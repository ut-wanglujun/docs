# Installation and Deployment

## Software

* OS: openEuler 23.03

## Hardware

* x86_64
* Arm

## Preparing the Environment

Install the openEuler operating system. For details, see the *openEuler 23.03 Installation Guide*.

## Installing NFS Multipathing

```shell
Insert the .ko file of NFS multipathing.
modprobe nfs_multipath

View the .ko file of NFS multipathing.
lsmod | grep nfs_multipath
nfs_multipath          36864  1
nfs                   544768  3 nfs_multipath,nfsv3
sunrpc                704512  30 nfs_multipath,lockd,nfsv3,nfs_acl,nfs
If `nfs_multipath` exists, the installation is successful.
```

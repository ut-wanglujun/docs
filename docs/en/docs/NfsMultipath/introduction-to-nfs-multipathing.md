# Introduction to NFS Multipathing

## Introduction

Network File System (NFS) is a distributed file system protocol originally developed by Sun Microsystems (Sun) in 1984. It allows users on NFS clients to access files on NFS servers over computer networks. As the NFS service is widely used in industries such as finance, EDA, AI, and container, higher requirements are imposed on NFS performance and reliability. Traditional NFS has the following disadvantages:

* A single mount point on a single host can be accessed only by using one client IP address and one server IP address. When multiple physical links exist between the client and server, the performance of these links cannot be fully utilized.
* If a single link of a single mount point is faulty, link failover cannot be performed. As a result, host services will be interrupted.

NFS multipathing is developed to solve the preceding defects encountered during the use of traditional NFS. It is proposed that multiple links be established between the client and the server under a single mount point to support I/O transmission over multiple links, improving the performance of a single mount point. In addition, the link status is periodically checked to ensure fast I/O failover upon a link fault.

NFS multipathing provides the following functions:

* NFSv3 supports the Round Robin link selection algorithm to balance the performance of multiple links.
* NFSv3 and NFSv4 support fast link failover, improving NFS reliability.
* Provides an interface for registering link path selection algorithms and allows NFS server developers to customize path selection algorithms.
* Supports periodic link availability detection.
* Allows the link status to be viewed.

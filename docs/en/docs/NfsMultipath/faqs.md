# FAQs

## 1. Failed to Mount NFS Multipathing and Query the .ko File of NFS Multipathing by Running the lsmod Command

```shell
Error message:
mount.nfs: an incorrect mount option was specified
The .ko file of NFS multipathing cannot be queried by running the lsmod | grep nfs_multipath command.

Possible cause: NFS multipathing is not installed.

Solution: Install NFS multipathing by referring to section "Installation and Deployment."
```

## 2. Failed to Mount NFS Multipathing Links

```shell
Error message:
mount.nfs: an incorrect mount option was specified
Error message in the /var/log/messages file:
Mar 18 23:38:22 localhost kernel: [  635.016793] [MULTIPATH:nfs_multipath_parse_ip_range] iplist for remote reached 8, more than supported limit 8

Possible cause: The number of specified IP addresses exceeds 8.

Solution: The number of specified IP addresses must be fewer than or equal to 8.
```

# Usage

## Mounting Multipath Links Using Parameters

```shell
Command for mounting multipath links
mount -t nfs -o localaddrs=1.1.1.1~2.2.2.2,remoteaddrs=1.1.1.10~2.2.2.10 1.1.1.1:/test /mnt/fs1

The optional parameters localaddrs and remoteaddrs are added to specify multipathing links.
localaddrs indicates the IP address of the client.
remoteaddrs indicates the IP address of the server.
An IP range or multiple IP addresses can be configured. Use hyphens (-) to indicate an IP range and tilde (~) to indicate multiple IP addresses.
For example, remoteaddrs=1.1.1.1-1.1.1.3 specifies three server addresses: 1.1.1.1, 1.1.1.2, and 1.1.1.3.
localaddrs=1.1.1.1~1.1.1.3 specifies two client addresses: 1.1.1.1 and 1.1.1.3.

Both localaddrs and remoteaddrs are optional. You can specify either or both of them. If neither of them is specified, the multipathing feature is disabled.
```

## Querying Multipathing Information

```shell
Command for querying multipathing information
cat /proc/fs/nfs/mutipath/conn_info
The command output is similar to the following:
===============================Id:4 count 5 proto 3 start========================
index:0, client_ip:1.1.1.1, server_ip:1.1.1.10, status:connect, load:3
index:1, client_ip:1.1.1.1, server_ip:1.1.1.11, status:connect, load:3
index:2, client_ip:1.1.1.1, server_ip:1.1.1.12, status:connect, load:3
index:3, client_ip:1.1.1.1, server_ip:1.1.1.13, status:disconnect, load:0
index:4, client_ip:1.1.1.1, server_ip:1.1.1.14, status:disconnect, load:0
=========================================Id:4 end================================
status indicates the link status. connect indicates that the link is available, and disconnect indicates that the link is unavailable.
load indicates the number of NFS requests sent over the link.
```

## Precautions

* A maximum of eight client or server IP addresses are supported. If more than eight IP addresses are specified, the links cannot be mounted.
* A maximum of eight multipath links are supported.

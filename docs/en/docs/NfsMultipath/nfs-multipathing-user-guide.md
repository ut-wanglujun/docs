
# NFS Multipathing User Guide

This document describes how to install, deploy, and use the multipathing feature of Network File System (NFS) clients.

This document is intended for community developers, open source enthusiasts, and partners who use the openEuler operating system and want to know and use the NFS multipathing feature. Users must have basic NFS knowledge.

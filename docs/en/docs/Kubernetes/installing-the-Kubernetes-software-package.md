# Installing the Kubernetes Software Package


```bash
$ dnf install -y docker conntrack-tools socat
```

After the EPOL source is configured, you can directly install Kubernetes through DNF.

```bash
$ rpm -ivh kubernetes*.rpm
```



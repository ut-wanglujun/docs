# Tailoring and Customization Tool Usage Guide

This document describes the tailoring and customization tool of openEuler, including the introduction, installation, and usage.
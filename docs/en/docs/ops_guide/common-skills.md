# Common Skills
-   [Common Skills](#common-skills)
    -   [Configuring the Network](#configuring-the-network)
    -   [Managing RPM Packages](#managing-rpm-packages) 
    -   [Configuring SSH](#configuring-ssh)
## Configuring the Network

1. Configure the IP address.

Run the **ip** command to configure an address for the interface. **interface-name** indicates the name of the NIC.

```
ip addr [ add | del ] address dev interface-name
```

2. Configure a static IP address.

```shell
# Configure the IP address.
ip address add 192.168.0.10/24 dev enp3s0

# Run the following command as the root user to query the configuration result:
ip addr show dev enp3s0

# The result is as follows:
2: enp3s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
link/ether 52:54:00:aa:ad:4a brd ff:ff:ff:ff:ff:ff
inet 192.168.202.248/16 brd 192.168.255.255 scope global dynamic noprefixroute enp3s0
valid_lft 9547sec preferred_lft 9547sec
inet 192.168.0.10/24 scope global enp3s0
valid_lft forever preferred_lft forever
inet6 fe80::32e8:cc22:9db2:f4d4/64 scope link noprefixroute
valid_lft forever preferred_lft forever
```

3. Configure a static route.

Run the **ip route add** command to add a static route to the routing table and run the **ip route del** command to delete a static route. The common format of the **ip route** command is as follows:

```shell
ip route [ add | del | change | append | replace ] destination-address
```

- To add a static route to the host address, run the following command as the **root** user:

```shell
ip route add 192.168.2.1 via 10.0.0.1 [dev interface-name]
```

- To add a static route to the network, run the following command as the **root** user:

```shell
ip route add 192.168.2.0/24 via 10.0.0.1 [dev interface-name]
```

4. Configure the network using the ifcfg file.

Modify the **ifcfg-enp4s0** file generated in the **/etc/sysconfig/network-scripts/ directory** as the **root** user. The following is an example:

```shell
TYPE=Ethernet
PROXY_METHOD=none
BROWSER_ONLY=no
BOOTPROTO=none
IPADDR=192.168.0.10
PREFIX=24
DEFROUTE=yes
IPV4_FAILURE_FATAL=no
IPV6INIT=yes
IPV6_AUTOCONF=yes
IPV6_DEFROUTE=yes
IPV6_FAILURE_FATAL=no
IPV6_ADDR_GEN_MODE=stable-privacy
NAME=enp4s0static
UUID=xx
DEVICE=enp4s0
ONBOOT=yes
```

## Managing RPM Packages

The full name of RPM is Red Hat Package Manager, which is intended to manage Red Hat software packages. It is used in mainstream distributions such as openEuler, Fedora, Red Hat, Mandriva, SUSE and YellowDog, and distributions developed based on these distributions.

  RPM installs the required software to a set of management programs on the Linux host in database record mode. The software to be installed is compiled and packaged, and the default database record in the packaged software records the dependencies required for the software installation. When a user installs the software on a Linux host, RPM checks whether the dependencies on the Linux host meets the requirements based on the data recorded in it.

- If yes, install the software.
- If no, do not install the software. During the installation, all software information is written into the RPM database for subsequent query, verification, and uninstallation.

![zh-cn_other_0000001337581224](./images/zh-cn_other_0000001337581224.jpeg)

1. Default installation path of the RPM packages

Generally, RPM uses the default installation path. (The default installation path can be queried by running a command and will be described in detail in subsequent sections.) All installation files are distributed to the directories listed in the following table by type.

Table 1 RPM installation paths and their meanings

|Installation Path|Description|
|--|--|
|/etc/|Configuration file installation directory|
|/usr/bin/|Installation directory of the executable commands|
|/usr/lib/|Path for storing the function library used by the program|
|/usr/share/doc|Location where the basic software user manual is saved|
|/usr/share/man/|Path for saving the help file|

Note: You can manually specify the installation path of RPM, but this method is not recommended. After the installation path is manually specified, all installation files are installed in the specified path, and the command for querying the installation path in the system cannot be used. The command can be identified by the system only after being manually configured.

2. rpm command options

**Checking the RPM Signature of the Software Package**

Before installing the RPM package on a Linux host, check the PGP signature. After ensuring that the signature integrity and source are correct, run the **rpm --checksig** command to verify the validity:

```
rpm --checksig nano-2.3.1-10.el7.x86_64.rpm
```



**Installing RPM Packages**

To install RPM packages in Linux, use the **-i** option in the **rpm** command.

```
rpm -ivh nano-2.3.1-10.el7.x86_64.rpm
```

-   **-i**: installs the software package.
-   **-v**: displays detailed information.
-   **-h**: lists flags during suite installation.



**Querying an Installed RPM Package**

To query an RPM package (dnf) installed in the Linux system, use the **-q** option in the **rpm** command.

```
rpm -q dnf
```

-   **-q**: query operation

If the specified package is not installed, the following error message is displayed:

![zh-cn_image_0000001387961737](./images/zh-cn_image_0000001387961737.png)



**Querying All Installed RPM Packages**

To query all RPM packages installed in Linux, use the **-qa** option in the **rpm** command.

![](./images/zh-cn_image_0000001388241577.png)

Note: When using the **-qa** option, use the pipe character (|) together to improve the search accuracy.



**Querying Details About an Installed RPM Package**

Use the **-qi** option in the **rpm** command to query the details of an RPM package installed in the system.

![zh-cn_image_0000001337422904](./images/zh-cn_image_0000001337422904.png)



**Querying All Files in an RPM Package**

To query the file list of an RPM package, use the **-qlp** option in the **rpm** command.

![zh-cn_image_0000001387902849](./images/zh-cn_image_0000001387902849.png)



**Querying RPM Package Dependencies**

To query the list of dependency packages compiled by a specified RPM package, use the **-qRp** option in the **rpm** command.

![zh-cn_image_0000001337424024](./images/zh-cn_image_0000001337424024.png)



**Verifying All Installed RPM Packages**

To verify an installed RPM package, use the **-Va** option in the **rpm** command to compare the information about the files installed in the package with the information about the files obtained from the package metadata stored in the RPM database.

![zh-cn_image_0000001388184025](./images/zh-cn_image_0000001388184025.png)

Table 2 Output fields of the **rpm -Va** command and their meanings

|Field|Description|
|--|--|
|S|The file length changes.|
|M|The access permission or type of a file changes.|
|5|The MD5 checksum changes.|
|D|The attributes of a device node change.|
|L|The symbolic link of a file changes.|
|U|The owner of a file, subdirectory, or device node changes.|
|G|The group of a file, subdirectory, or device node changes.|
|T|The last modification time of a file changes.|



**Querying the RPM Package of a Specific File**

To query an RPM package that provides a specific binary file on Linux, use the **-qf** option in the **rpm** command.

![zh-cn_image_0000001337424304](./images/zh-cn_image_0000001337424304.png)

**Querying Files in an Installed RPM Package**

To query the list of installation files of an RPM package, use the **-ql** option in the **rpm** command.

![zh-cn_image_0000001388024321](./images/zh-cn_image_0000001388024321.png)

**Querying the Recently Installed RPM Packages**

Linux is a multi-user OS. During the use of Linux, other users may have installed some software packages. To query the recently installed packages in the system, use the **-qa --last** options in the **rpm** command.

![zh-cn_image_0000001337584296](./images/zh-cn_image_0000001337584296.png)

**Querying Only the Documents of the Installed RPM Packages**

You can obtain the help information of any command from the **Linux Man** page (path for storing **/usr/share/doc/Package\_Name-Version\_Number/docs\*** documents). To query the list of documents associated with the installed RPM packages, use the **-qdf** option in the **rpm** command and enter the binary file path.

![zh-cn_image_0000001387784693](./images/zh-cn_image_0000001387784693.png)

**Upgrading an Installed RPM Package**

You can easily upgrade the installed RPM package to the latest version by using the **-Uvh** option and the **rpm** command.

![zh-cn_image_0000001388024397](./images/zh-cn_image_0000001388024397.png)

Note: When the installed RPM package is upgraded, the old RPM package is deleted and the new RPM package is installed.

**Removing an Installed RPM Package**

To remove an RPM package installed on the system, use the **-ev** or **-e** option in the **rpm** command.

```shell
rpm -ev pkgship
```

**Rebuilding the Damaged RPM Database**

When you try to update the system using the **yum update** command, you may receive an error message indicating that the RPM database is damaged. If you receive this message, use the **--rebuilddb** option in the **rmp** command to rebuild the database.

```shell
rm /var/lib/rpm/__db*
rpm --rebuilddb
```

**Checking Whether Vulnerabilities in Specific Packages Have Been Fixed**

You can use the **--changelog** option in the **rpm** command and enter the corresponding CVE ID.

```shell
rpm -q --changelog python-2.6.6 | grep -i "CVE-2019-9636"
```

**Importing the RPM GPG Key**

By default, when a new repository is added to the Linux system, the GPG key is automatically imported. You can also use **--import** in the **rpm** command to manually import the RPM GPG key to check the integrity of a package when downloading it from the repository.

```shell
rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-OpenEuler-22.03-LTS
```
3. dnf commands

Table 3 dnf commands

|Command|Description|
|--|--|
|repolist|Displays the configured software repository source.|
|install|Installs one or more software packages on Linux.|
|upgrade|Upgrades one or more software packages on Linux.|
|list|Lists a software package or a group of software packages.|
|info|Displays detailed information about a package or package group.|
|updateinfo|Displays the bulletin information about a package.|
|search|Searches for the specified character string in the software package details.|
|check-update|Checks for software package update.|
|remove|Removes one or more software packages from the system.|
|reinstall|Reinstalls a package.|
|downgrade|Downgrades a software package.|
|autoremove|Removes all unnecessary software packages that are installed due to dependency relationships.|
|distro-sync|Synchronizes the installed software package to the latest available version.|
|makecache|Creates a metadata cache.|
|repository-package|Runs commands on all software packages in a specified repository.|
|provides|Searches for the software package that provides the specified content.|
|group|Displays or uses group information.|
|history|Displays or uses transaction history.|
|clean|Deletes cached data.|

**Displaying Configured Software Repositories**

By default, the **--enabled** option is added to display the enabled software repositories.

![zh-cn_image_0000001337427216](./images/zh-cn_image_0000001337427216.png)

-   **--all**: displays all software repositories.
-   **--disabled**: displays disabled software repositories.
-   **--enabled**: displays enabled repositories (default).

Installing One or More Software Packages

You can run the **install** command to install RPM packages.

![zh-cn_image_0000001337747132](./images/zh-cn_image_0000001337747132.png)

Conflicting packages or packages that cannot be installed may exist during software package installation. You can add **--allowerasing** to the command to replace the conflicting packages or **--skip-broken** to skip the packages that cannot be installed.

![zh-cn_image_0000001387787605](./images/zh-cn_image_0000001387787605.png)

When dnf is used to install a software package, add **--installroot** to set the root directory for installing the software package.

![zh-cn_image_0000001387907229](./images/zh-cn_image_0000001387907229.png)

If you need to temporarily specify a repository source for installation, you can add the **--setopt=reposdir=** option to specify the loading directory of the repository source.

![zh-cn_image_0000001337427392](./images/zh-cn_image_0000001337427392.png)

If interactive confirmation is not required during installation, you can add **-y** or **--assumeyes** to enable all software packages to be installed to automatically answer **Yes**.

![zh-cn_image_0000001387907229](./images/zh-cn_image_0000001387907229.png)

To install an RPM package by specifying a specific repository source, you can specify the **--repo** or **--enablerepo** option. To achieve the same effect, you can also use the **--disablerepo** option to disable the matched repository source. You are advised to use the **--repo** option to install the RPM package.

![zh-cn_image_0000001388187325](./images/zh-cn_image_0000001388187325.png)

**Reinstalling a Software Package**

You can run the **reinstall** command to reinstall a software package in the system.

![zh-cn_image_0000001387908045](./images/zh-cn_image_0000001387908045.png)

**Upgrading One or More Software Packages**

- You can use the **upgrade** command to upgrade one or more software packages on Linux.

![zh-cn_image_0000001388028161](./images/zh-cn_image_0000001388028161.png)

- You can also run the **update** command to upgrade one or more software packages.

![zh-cn_image_0000001337268560](./images/zh-cn_image_0000001337268560.png)


**Downgrading a Software Package**

If a compatibility problem occurs because the version of a software package is too late, you can downgrade the software package.

![zh-cn_image_0000001337748300](./images/zh-cn_image_0000001337748300.png)

**Listing a Package or a Group of Packages**

You can run the **list** command to list the software packages installed in the system and the software packages in the configured repository.

![zh-cn_image_0000001337268820](./images/zh-cn_image_0000001337268820.png)

You can add options to filter the displayed package list.

-   **--all**: displays all software packages (default).
-   **--available**: displays only available software packages.
-   **-- installed**: displays only installed software packages.
-   **--extras**: displays only additional software packages.
-   **--updates**: displays only the software packages to be upgraded.
-   -**-upgrades**: displays only the software packages to be upgraded.
-   **--autoremove**: displays only the software packages to be removed.
-   **--recent**: displays the software packages that have been changed recently.

**Querying Details About a Software Package**

You can run the **info** command to query details about a software package.

![zh-cn_image_0000001388188365](./images/zh-cn_image_0000001388188365.png)

**Searching for a Software Package**

If you need to install a software package in the system but you are not sure about the full name of the software package, you can run the **search** command to search for the matched package.

![zh-cn_image_0000001387908453](./images/zh-cn_image_0000001387908453.png)

**Uninstalling One or More Software Packages**

You can run the **remove** command to remove an expired or duplicate software package.

![zh-cn_image_0000001388028537](./images/zh-cn_image_0000001388028537.png)

-   **--duplicates**: removes installed (duplicate) software packages.
-   **--oldinstallonly**: removes expired installation-only software packages.

**Automatically Removing Software Packages Installed Due to Dependency Relationships**

You can run the **autoremove** command to remove unnecessary software packages that are installed due to dependency relationships.

![zh-cn_image_0000001337748528](./images/zh-cn_image_0000001337748528.png)

## Configuring SSH

1. Introduction to the SSH service

Secure Shell (SSH) is a reliable protocol that ensures the security of remote login sessions and other network services. The SSH protocol can effectively prevent information leakage during remote management. SSH encrypts transferred data to prevent domain name server (DNS) spoofing and IP spoofing. OpenSSH was created as an open source alternative to the proprietary SSH protocol.

2. Configuring the SSH Service

```shell
# Open and modify the **/etc/ssh/sshd_config** file.
vi /etc/ssh/sshd_config

# Restart the SSH service.
systemctl restart sshd

# Check the SSH service status.
systemctl status sshd
```

3. Main options in the SSH service configuration file

```shell
# Specify the SSH protocol version.
Protocol 2

# Allowed users
AllowUsers xxx

# Denied users
DenyUser root

# Configure session timeout.
ClientAliveInterval 120

# Disable SSH root login.
PermitRootLogin no

# Configure or change the SSH port number.
Port 1234

# Disable SSH password authentication.
PasswordAuthentication no
```

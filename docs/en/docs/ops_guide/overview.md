# O&M Guide

This document describes how to operate and maintain the openEuler operating system (OS), including performance monitoring, information collection, emergency handling, and tools.

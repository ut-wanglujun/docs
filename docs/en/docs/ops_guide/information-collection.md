# Information Collection
-   [Information Collection](#information-collection)
    -   [Querying OS Information](#querying-os-information)
    -   [Querying Hardware Information](#querying-hardware-information)
    -   [Querying Software Information](#querying-software-information) 
    -   [Viewing OS Logs](#viewing-os-logs)
## Querying OS Information
1. Query the OS version.
```shell
cat /etc/openEuler-latest
```

![zh-cn_image_0000001336729664](./images/zh-cn_image_0000001336729664.png)

```shell
cat /etc/os-release
```

![zh-cn_image_0000001337051916](./images/zh-cn_image_0000001337051916.jpg)

```shell
cat /etc/openEuler-release
```

![zh-cn_image_0000001387691985](./images/zh-cn_image_0000001387691985.png)

2. Query the kernel version.

```shell
uname -a
```

![zh-cn_image_0000001337212144](./images/zh-cn_image_0000001337212144.jpg)

## Querying Hardware Information

1. Query CPU statistics.

```shell
lscpu
```

![zh-cn_image_0000001387692269](./images/zh-cn_image_0000001387692269.jpg)

2. View CPU parameters.

```shell
cat /proc/cpuinfo
```

![zh-cn_image_0000001387293085](./images/zh-cn_image_0000001387293085.png)

3. View system memory information.

```shell
cat /proc/meminfo
```

![zh-cn_image_0000001387692893](./images/zh-cn_image_0000001387692893.png)

4. View memory and hard drive information.

```shell
dmidecode -t memory
```

![zh-cn_image_0000001337053248](./images/zh-cn_image_0000001337053248.png)

5. View hard drive and partition distribution.

```shell
lsblk
```

![zh-cn_image_0000001387413509](./images/zh-cn_image_0000001387413509.png)

6. View details about hard drives and partitions.

```shell
fdisk -l
```

![zh-cn_image_0000001337533690](./images/zh-cn_image_0000001337533690.png)

7. View NIC information.

```shell
lspci | grep -i 'eth'
```

![zh-cn_image_0000001387413793](./images/zh-cn_image_0000001387413793.png)

8. View all network interfaces.

```shell
ip a or ifconfig -a
```

![zh-cn_image_0000001387855149](./images/zh-cn_image_0000001387855149.png)

9. View details about a network interface.

```shell
ethtool enp7s0 (enp7s0 is used as an example.)
```

![zh-cn_image_0000001387415629](./images/zh-cn_image_0000001387415629.png)

10. View PCI information.

```shell
lspci
```

![zh-cn_image_0000001337696078](./images/zh-cn_image_0000001337696078.png)

11. View the device tree.

```shell
lspci -t
```

![zh-cn_image_0000001337536842](./images/zh-cn_image_0000001337536842.png)

12. View BIOS information.

```shell
dmidecode -t bios
```

![zh-cn_image_0000001387857005](./images/zh-cn_image_0000001387857005.png)

##  Querying Software Information

1. Query details about a software package.

```shell
rpm -qi (systemd is used as an example.)
```

![zh-cn_image_0000001387755969](./images/zh-cn_image_0000001387755969.png)

2. View the modules provided by a software package.

```shell
rpm -q --provides (systemd is used as an example.)
```

![zh-cn_image_0000001337579708](./images/zh-cn_image_0000001337579708.png)

3. View all installed software packages.

```shell
rpm -qa (systemd is used as an example.)
```

![zh-cn_image_0000001337419960](./images/zh-cn_image_0000001337419960.png)

4. View the list of software packages.

```shell
rpm -ql (python3-rpm is used as an emple.)
```

![zh-cn_image_0000001387780357](./images/zh-cn_image_0000001387780357.png)

## Viewing OS Logs

1. View the information and error logs after the system is started.

```shell
cat  /var/log/messages
```

![zh-cn_image_0000001388020197](./images/zh-cn_image_0000001388020197.png)

2. View the security-related logs.

```shell
cat /var/log/secure
```

![zh-cn_image_0000001337580216](./images/zh-cn_image_0000001337580216.png)

3. View the email-related logs.

```shell
cat /var/log/maillog
```

![zh-cn_image_0000001337740252](./images/zh-cn_image_0000001337740252.png)

4. View the logs related to scheduled tasks.

```shell
cat /var/log/cron
```

![zh-cn_image_0000001337420372](./images/zh-cn_image_0000001337420372.png)

5. View the logs related to UUCP and news devices.

```shell
cat /var/log/spooler
```

![zh-cn_image_0000001337260780](./images/zh-cn_image_0000001337260780.png)

6. View the logs related to the startup and stop of the daemon process.

```shell
cat /var/log/boot.log
```

![zh-cn_image_0000001337740540](./images/zh-cn_image_0000001337740540.png)

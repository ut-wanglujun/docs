# Troubleshooting
-   [Troubleshooting(#troubleshooting)
    -   [Triggering kdump Restart](#triggering-kdump-restart)
    -   [Performing Forcible Restart](#performing-forcible-restart)
    -   [Restarting the Network](#restarting-the-network)
    -   [Repairing the File System](#repairing-the-file-system) 
    -   [Manually Dropping Cache](#manually-dropping-cache)
    -   [Rescue Mode and Single-User Mode](#rescue-mode-and-single-user mode)

## Triggering kdump Restart
```shell
# Write 1 to the sysrq file to enable the SysRq function. After this function is enabled, the kernel will respond to any operation.
echo 1 > /proc/sys/kernel/sysrq

# Make the system crash.
echo c > /proc/sysrq-trigger
```

## Performing Forcible Restart
You can use either of the following methods to forcibly restart the OS:
- Manually restart the OS.

```shell
reboot -f
```

- Forcibly power on and off the OS through iBMC.

![en-us_image_0000001372249333](./images/en-us_image_0000001372249333.png)

## Restarting the Network

openEuler uses NetworkManager to manage the network. Run the following command to restart the network:

```shell
systemctl restart NetworkManager
```

## Repairing the File System

After the OS is forcibly powered off and then powered on, the file system may be damaged. When the OS is started, it automatically checks and repairs the file system. If the file system fails to be repaired, you need to run the **fsck** command to scan for and repair the file system.

```shell
# In this case, the system enters the rescue mode. Check which file system is damaged in the log.
journalctl -xb
# Check whether the partition has been mounted before the repair.
cat /proc/mounts
# Uninstall the directory.
umount xx
# If the directory cannot be uninstalled, kill the process that occupies the directory.
lsof | grep xxx
kill xxx
# Run the fsck command to rectify the fault. Enter yes or no when prompted.
fsck -y /dev/xxx
```

## Manually Dropping Cache

```shell
#Different values of N can achieve different clearance purposes. According to the Linux kernel document, run the sync command before clearing data. (The drop operation does not release any dirty objects. The sync command writes all unwritten system buffers to drives, including modified inodes, delayed block I/Os, and read/write mapping files. In this way, dirty objects can be reduced so that more objects can be released.)
echo N > /proc/sys/vm/drop_caches

#Release the page caches.
echo 1 > /proc/sys/vm/drop_caches

#Release dentries and inodes.
echo 2 > /proc/sys/vm/drop_caches

#Release the page caches, dentries, and inodes.
echo 3 > /proc/sys/vm/drop_caches
```

## Rescue Mode and Single-User Mode

- Single-user mode

  On the login page, enter **e** to go to the grub page, add **init=/bin/sh** to the **linux** line, and press **Ctrl**+**X**.

  1. Run the **mount -o remount,rw /** command.
  2. Perform operations such as changing the password.
  3. Enter **exit** to exit.

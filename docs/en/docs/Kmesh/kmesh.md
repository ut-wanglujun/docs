# Kmesh User Guide

This document describes how to install, deploy, and use Kmesh, a high-performance service mesh data plane software provided by openEuler.

This document is intended for developers, open source enthusiasts, and partners who use the openEuler operating system (OS) and want to learn and use Kmesh. Users must have basic knowledge of the Linux OS.

# Installation and Deployment

## Software

* OS: openEuler 23.03

## Hardware

* x86_64

## Preparing the Environment

* Install the openEuler OS by referring to the *openEuler 23.03 Installation Guide*.

* Root permissions are required for installing Kmesh.

## Installing Kmesh

* Install the Kmesh software package.

```shell
[root@openEuler ~]# yum install Kmesh
```

* Check whether the installation is successful. If the command output contains the name of the software package, the installation is successful.

```shell
[root@openEuler ~]# rpm -q Kmesh
```

## Deploying Kmesh

### Cluster Mode

Before starting Kmesh, configure the IP address of the control plane program (for example, Istiod IP address) in the cluster.

```json
    "clusters": [
      {
        "name": "xds-grpc",
        "type" : "STATIC",
        "connect_timeout": "1s",
        "lb_policy": "ROUND_ROBIN",
        "load_assignment": {
          "cluster_name": "xds-grpc",
          "endpoints": [{
            "lb_endpoints": [{
              "endpoint": {
                "address":{
                  "socket_address": {
                          "protocol": "TCP",
                          "address": "192.168.0.1",# Configure the control plane IP address (for example, Istiod IP address).
                          "port_value": 15010
                     }
                }
              }
            }]
          }]
```

### Local Mode

Before starting Kmesh, modify `kmesh.service` to disable ADS.

```shell
[root@openEuler ~]# vim /usr/lib/systemd/system/kmesh.service
ExecStart=/usr/bin/kmesh-daemon -enable-kmesh -enable-ads=false
[root@openEuler ~]# systemctl daemon-reload
```

When the Kmesh service is started, the kmesh-daemon program is invoked. For details about how to use the kmesh-daemon program, see [Using kmesh-daemon](./usage.md).

### Starting Kmesh

```shell
# Start the Kmesh service.
[root@openEuler ~]# systemctl start kmesh.service
# Check the Kmesh running status.
[root@openEuler ~]# systemctl status kmesh.service
```

### Stopping Kmesh

```shell
# Stop the Kmesh service.
[root@openEuler ~]# systemctl stop kmesh.service
```

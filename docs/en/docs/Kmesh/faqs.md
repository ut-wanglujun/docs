# FAQs

## 1. If the Kmesh Service Is Started in the Cluster Mode and Control Plane IP Address Is Not Configured, Kmesh Reports an Error and Exits

![](./figures/not_set_cluster_ip.png)

Possible cause: In cluster mode, the Kmesh service communicates with the control plane program to obtain configuration information. Therefore, you need to configure the correct IP address of the control plane program.

Solution: Configure the correct IP address of the control plane program by referring to the cluster mode in [Installation and Deployment](./Installation and Deployment.md).

## 2. Kmesh Reports "get kube config error!" When Started

![](./figures/get_kubeconfig_error.png)

Possible cause: In cluster mode, the Kmesh service automatically obtains the IP address of the control plane program based on Kubernetes configurations. If the **kubeconfig** path is not configured in the system, Kmesh will fail to obtain the configurations and reports "get kube config error!" (If the IP address of the control plane program has been correctly configured in the Kmesh configuration file, ignore this problem.)

Solution: Configure **kubeconfig** as follows:

```shell
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
```

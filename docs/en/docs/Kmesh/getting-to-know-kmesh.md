# Getting to Know Kmesh

## Introduction

As more and more applications become cloud-native, the scale of cloud applications and application SLA requirements put high demands on cloud infrastructure.

Kubernetes-based cloud infrastructure can help applications achieve agile deployment and management, but it lacks the application traffic orchestration ability. The emergence of service mesh has effectively compensated for Kubernetes shortcomings, allowing Kubernetes to completely realize agile cloud application development and O&M. However, as the application of service mesh gradually deepens, the current sidecar-based mesh architecture has obvious performance defects in the data plane, and the following problems have become a consensus in the industry:

* High latency
    Take the Istio service mesh for example. The single-hop access delay of a service is increased by 2.65 ms, which cannot meet the requirements of latency-sensitive applications.

* High overhead
    In Istio, each sidecar consumes 50 MB memory and occupies 2 CPU cores. This causes high overhead in large-scale clusters and decreases the deployment density of service containers.

Based on the programmable kernel, Kmesh moves mesh traffic management down to the OS level and shortens the data path from 3 hops to 1. This greatly improves the latency performance of the mesh data plane and helps services innovate quickly.

## Architecture

![](./figures/kmesh-arch.png)

Main components of Kmesh include:

* kmesh-controller：
    The management program of Kmesh, which manages the Kmesh lifecycle, XDS interconnection, and O&M observation.

* kmesh-api：
    The external API layer of Kmesh, including APIs for converted XDS orchestration and O&M observation.

* kmesh-runtime：
    The runtime for orchestration of traffic in layer 3 to layer 7, which is implemented in the kernel.

* kmesh-orchestration：
    Orchestration of traffic in layer 3 to layer 7 based on eBPF, implementing functions such as routing, gray release, and load balancing.

* kmesh-probe：
    O&M observation probe, which provides end-to-end observation.

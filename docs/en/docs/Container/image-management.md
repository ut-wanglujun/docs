# Image Management

- [Image Management](#image-management)
    - [Docker Image Management](#docker-image-management)
        - [Logging In to a Registry](#logging-in-to-a-registry)
        - [Logging Out of a Registry](#logging-out-of-a-registry)
        - [Pulling Images from a Registry](#pulling-images-from-a-registry)
        - [Deleting Images](#deleting-images)
        - [Adding an Image Tag](#adding-an-image-tag)
        - [Loading Images](#loading-images)
        - [Listing Images](#listing-images)
        - [Inspecting Images](#inspecting-images)
        - [Two-Way Authentication](#two-way-authentication)
        - [Importing rootfs](#importing-rootfs)
        - [Exporting rootfs](#exporting-rootfs)
    - [Embedded Image Management](#embedded-image-management)
        - [Loading Images](#loading-images-1)
        - [Listing Images](#listing-images-1)
        - [Inspecting Images](#inspecting-images-1)
        - [Deleting Images](#deleting-images-1)

## Docker Image Management

### Logging In to a Registry

#### Description

The  **isula login**  command is run to log in to a registry. After successful login, you can run the  **isula pull**  command to pull images from the registry. If the registry does not require a password, you do not need to run this command before pulling images.

#### Usage

```shell
isula login [OPTIONS] SERVER
```

#### Parameters

For details about the parameters in the **login** command, see **Appendix** > **Command Line Parameters** > **Table 1 login command parameters**.

#### Example

```shell
$ isula login -u abc my.csp-edge.com:5000

Login Succeeded
```

### Logging Out of a Registry

#### Description

The  **isula logout**  command is run to log out of a registry. If you run the  **isula pull**  command to pull images from the registry after logging out of the system, the image will fail to be pulled because you are not authenticated.

#### Usage

```shell
isula logout SERVER
```

#### Parameters

For details about the parameters in the **logout** command, see **Appendix** > **Command Line Parameters** > **Table 2 logout command parameters**.

#### Example

```shell
$ isula logout my.csp-edge.com:5000
Logout Succeeded
```

### Pulling Images from a Registry

#### Description

Pull images from a registry to the local host.

#### Usage

```shell
isula pull [OPTIONS] NAME[:TAG]
```

#### Parameters

For details about the parameters in the **pull** command, see **Appendix** > **Command Line Parameters** > **Table 3 pull command parameters**.

#### Example

```shell
$ isula pull localhost:5000/official/busybox
Image "localhost:5000/official/busybox" pulling
Image "localhost:5000/official/busybox@sha256:bf510723d2cd2d4e3f5ce7e93bf1e52c8fd76831995ac3bd3f90ecc866643aff" pulled
```

### Deleting Images

#### Description

Delete one or more images.

#### Usage

```shell
isula rmi [OPTIONS] IMAGE [IMAGE...]
```

#### Parameters

For details about the parameters in the **rmi** command, see **Appendix** > **Command Line Parameters** > **Table 4 rmi command parameters**.

#### Example

```shell
$ isula rmi rnd-dockerhub.huawei.com/official/busybox
Image "rnd-dockerhub.huawei.com/official/busybox" removed
```

### Adding an Image Tag

#### Description

Add an image tag.

#### Usage

```shell
isula tag SOURCE_IMAGE[:TAG] TARGET_IMAGE[:TAG]
```

#### Parameters

For details about the parameters in the **tag** command, see **Appendix** > **Command Line Parameters** > **Table 8 tag command parameters**.

#### Example

```shell
isula tag busybox:latest test:latest
```

### Loading Images

#### Description

Load images from a .tar package. The .tar package must be exported by using the  **docker save**  command or must be in the same format.

#### Usage

```shell
isula load [OPTIONS]
```

#### Parameters

For details about the parameters in the **load** command, see **Appendix** > **Command Line Parameters** > **Table 5 load command parameters**.

#### Example

```shell
$ isula load -i busybox.tar
Load image from "/root/busybox.tar" success
```

### Listing Images

#### Description

List all images in the current environment.

#### Usage

```shell
isula images [OPTIONS]
```

#### Parameters

For details about the parameters in the **images** command, see **Appendix** > **Command Line Parameters** > **Table 6 images command parameters**.

#### Example

```shell
$ isula images
REPOSITORY                                   TAG        IMAGE ID             CREATED              SIZE
busybox                                      latest     beae173ccac6         2021-12-31 03:19:41  1.184MB
```

### Inspecting Images

#### Description

After the configuration information of an image is returned, you can use the  **-f**  parameter to filter the information as needed.

#### Usage

```shell
isula inspect [options] CONTAINER|IMAGE [CONTAINER|IMAGE...]
```

#### Parameters

For details about the parameters in the **inspect** command, see **Appendix** > **Command Line Parameters** > **Table 7 inspect command parameters**.

#### Example

```shell
$ isula inspect -f "{{json .image.id}}" rnd-dockerhub.huawei.com/official/busybox
"e4db68de4ff27c2adfea0c54bbb73a61a42f5b667c326de4d7d5b19ab71c6a3b"
```

### Two-Way Authentication

#### Description

After this function is enabled, iSulad and image repositories communicate over HTTPS. Both iSulad and image repositories verify the validity of each other.

#### Usage

The corresponding registry needs to support this function and iSulad needs to be configured as follows:

1. Modify iSulad configuration \(default path:  **/etc/isulad/daemon.json**\) and set  **use-decrypted-key**  to  **false**.
2. Place related certificates in the folder named after the registry in the  **/etc/isulad/certs.d**  directory. For details about how to generate certificates, visit the official Docker website:
    - [https://docs.docker.com/engine/security/certificates/](https://docs.docker.com/engine/security/certificates/)
    - [https://docs.docker.com/engine/security/https/](https://docs.docker.com/engine/security/https/)

3. Run the  **systemctl restart isulad**  command to restart iSulad.

#### Parameters

Parameters can be configured in the  **/etc/isulad/daemon.json**  file or carried when iSulad is started.

```shell
isulad --use-decrypted-key=false
```

#### Example

Set  **use-decrypted-key**  to  **false**.

```shell
$ cat /etc/isulad/daemon.json
{
    "group": "isulad",
    "graph": "/var/lib/isulad",
    "state": "/var/run/isulad",
    "engine": "lcr",
    "log-level": "ERROR",
    "pidfile": "/var/run/isulad.pid",
    "log-opts": {
        "log-file-mode": "0600",
        "log-path": "/var/lib/isulad",
        "max-file": "1",
        "max-size": "30KB"
    },
    "log-driver": "stdout",
    "hook-spec": "/etc/default/isulad/hooks/default.json",
    "start-timeout": "2m",
    "storage-driver": "overlay2",
    "storage-opts": [
        "overlay2.override_kernel_check=true"
    ],
    "registry-mirrors": [
        "docker.io"
    ],
    "insecure-registries": [
        "rnd-dockerhub.huawei.com"
    ],
    "pod-sandbox-image": "",
    "image-opt-timeout": "5m",
    "native.umask": "secure",
    "network-plugin": "",
    "cni-bin-dir": "",
    "cni-conf-dir": "",
    "image-layer-check": false,
    "use-decrypted-key": false,
    "insecure-skip-verify-enforce": false
}
```

Place the certificate in the corresponding directory.

```shell
$ pwd
/etc/isulad/certs.d/my.csp-edge.com:5000
$ ls
ca.crt  tls.cert  tls.key
```

Restart iSulad.

```shell
systemctl restart isulad
```

Run the  **pull**  command to download images from the registry:

```shell
$ isula pull my.csp-edge.com:5000/busybox
Image "my.csp-edge.com:5000/busybox" pulling
Image "my.csp-edge.com:5000/busybox@sha256:f1bdc62115dbfe8f54e52e19795ee34b4473babdeb9bc4f83045d85c7b2ad5c0" pulled
```

### Importing rootfs

#### Description

Import a .tar package that contains rootfs as an image. Generally, the .tar package is exported by running the **export** command or a .tar package that contains rootfs in compatible format. Currently, the .tar, .tar.gz, .tgz, .bzip, .tar.xz, and .txz formats are supported. Do not use the TAR package in other formats for import.

#### Usage

```shell
isula import file REPOSITORY[:TAG]
```

After the import is successful, the printed character string is the image ID generated by the imported rootfs.

#### Parameters

For details about the parameters in the **import** command, see **Appendix** > **Command Line Parameters** > **Table 9 import command parameters**.

#### Example

```shell
$ isula import busybox.tar test
sha256:441851e38dad32478e6609a81fac93ca082b64b366643bafb7a8ba398301839d
$ isula images
REPOSITORY      TAG        IMAGE ID            CREATED                  SIZE
test            latest     441851e38dad        2020-09-01 11:14:35      1.168 MB
```

### Exporting rootfs

#### Description

Export the content of the rootfs of a container as a TAR package. The exported TAR package can be imported as an image by running the **import** command.

#### Usage

```shell
isula export [OPTIONS] [ID|NAME]
```

#### Parameters

For details about the parameters in the **export** command, see **Appendix** > **Command Line Parameters** > **Table 10 export command parameters**.

#### Example

```shell
$ isula run -tid --name container_test test sh
d7e601c2ef3eb8d378276d2b42f9e58a2f36763539d3bfcaf3a0a77dc668064b
$ isula export -o rootfs.tar d7e601c
$ ls
rootfs.tar
```

## Embedded Image Management

### Loading Images

#### Description

Load images based on the  **manifest**  files of embedded images. The value of  **--type**  must be set to  **embedded**.

#### Usage

```shell
isula load [OPTIONS] --input=FILE --type=TYPE
```

#### Parameters

For details about the parameters in the **load** command, see **Appendix** > **Command Line Parameters** > **Table 5 load command parameters**.

#### Example

```shell
$ isula load -i test.manifest --type embedded
Load image from "/root/work/bugfix/tmp/ci_testcase_data/embedded/img/test.manifest" success
```

### Listing Images

#### Description

List all images in the current environment.

#### Usage

```shell
isula images [OPTIONS]
```

#### Parameters

For details about the parameters in the **images** command, see **Appendix** > **Command Line Parameters** > **Table 6 images command parameters**.

#### Example

```shell
$ isula images
REPOSITORY                                   TAG        IMAGE ID             CREATED              SIZE
busybox                                      latest     beae173ccac6         2021-12-31 03:19:41  1.184MB
```

### Inspecting Images

#### Description

After the configuration information of an image is returned, you can use the  **-f**  parameter to filter the information as needed.

#### Usage

```shell
isula inspect [options] CONTAINER|IMAGE [CONTAINER|IMAGE...]
```

#### Parameters

For details about the parameters in the **inspect** command, see **Appendix** > **Command Line Parameters** > **Table 7 inspect command parameters**.

#### Example

```shell
$ isula inspect -f "{{json .created}}" test:v1
"2018-03-01T15:55:44.322987811Z"
```

### Deleting Images

#### Description

Delete one or more images.

#### Usage

```shell
isula rmi [OPTIONS] IMAGE [IMAGE...]
```

#### Parameters

For details about the parameters in the **rmi** command, see **Appendix** > **Command Line Parameters** > **Table 4 rmi command parameters**.

#### Example

```shell
$ isula rmi test:v1
Image "test:v1" removed
```

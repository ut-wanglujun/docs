# Container User Guide

## Overview

openEuler provides software packages of iSulad and Docker container engines.

The following container forms are provided for different application scenarios:

-   Common containers applicable to most common scenarios
-   Secure containers applicable to strong isolation and multi-tenant scenarios
-   System containers applicable to scenarios where the systemd is used to manage services

This document describes how to install and use the container engines and how to deploy and use containers in different forms.

## Intended Audience

This document is intended for openEuler users who need to install containers. You can better understand this document if you:

-   Be familiar with basic Linux operations.
-   Have a basic understanding of containers.

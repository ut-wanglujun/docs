# Installation and Running

This chapter describes how to obtain a pre-built image and how to run an image.

<!-- TOC -->

- [Installation and Running](#installation-and-running)
  - [Obtaining the Image](#obtaining-the-image)
  - [Image Content](#image-content)
  - [Running the Image](#running-the-image)
    - [Simplified Running Scenario](#simplified-running-scenario)
    - [Shared File System Enabled Scenario](#shared-file-system-enabled-scenario)
    - [Network Enabled Scenario](#network-enabled-scenario)
    <!-- /TOC -->

## Obtaining the Image
The released pre-built images support only the ARM and AArch64 architectures, and are compatible only with the ARM virt-4.0 platform of QEMU. You can obtain the images through the following links:

- [qemu_arm](https://repo.openeuler.org/openEuler-22.09/embedded_img/arm32/arm-std/) for ARM Cortex A15 processor of 32-bit ARM architecture.
- [qemu_aarch64](https://repo.openeuler.org/openEuler-22.09/embedded_img/arm64/aarch64-std/) for ARM Cortex A57 processor of 64-bit AArch64 architecture.

You can deploy an openEuler Embedded image on a physical bare-metal server, cloud server, container, or VM as long as the environment supports QEMU 5.0 or later.

## Image Content

The downloaded image consists of the following parts:

- Kernel image **zImage**, which is built based on Linux 5.10 of the openEuler community. You can obtain the kernel configurations through the following links:
  - [ARM(Cortex a15)](https://gitee.com/openeuler/yocto-embedded-tools/blob/openEuler-22.09/config/arm/defconfig-kernel)
  - [ARM(Cortex a57)](https://gitee.com/openeuler/yocto-embedded-tools/blob/openEuler-22.09/config/arm64/defconfig-kernel) for the AArch64 architecture. The image provides the image self-decompression function in addition. For details, see the corresponding [patch](https://gitee.com/openeuler/yocto-embedded-tools/blob/openEuler-22.09/patches/arm64/0001-arm64-add-zImage-support-for-arm64.patch).

- Root file system image:
  - **openeuler-image-qemu-xxx.cpio.gz**, which is the image of the standard root file system. It has received necessary security hardening and includes various software packages, such as audit, cracklib, OpenSSH, Linux PAM, shadow and software packages supported by iSula.
  
- Software Development Kit (SDK)
  
  -   **openeuler-glibc-x86_64-xxxxx.sh**: The self-extracting installation package of openEuler Embedded SDK. The SDK contains tools, libraries, and header files for developing user-mode applications and kernel modules.

## Running the Image

You can run the image to experience the functions of openEuler Embedded, and develop basic embedded Linux applications.

![](./public_sys-resources/icon-note.gif) **Note:**

- You are advised to use QEMU 5.0 or later to run the image. Some additional functions (the network and shared file system) depend on the virtio-net and virtio-fs features of QEMU. If these features are not enabled in QEMU, errors may occur during image running. In this case, you may need to recompile QEMU from the source code.

- When running the image, you are advised to place the kernel image and root file system image in the same directory.

Download and install QEMU by referring to the [QEMU official website](https://www.qemu.org/download/#linux), or download and build from [source](https://www.qemu.org/download/#source). Use the following command to verify the installation:

``` 
qemu-system-aarch64 --version
```

### Simplified Running Scenario

In this scenario, the network and shared file system are not enabled in QEMU. You can use this scenario to experience the functions.

1. **Start QEMU.**

   For the ARM architecture (ARM Cortex A15), run the following command:

   ```
   qemu-system-arm -M virt-4.0 -m 1G -cpu cortex-a15 -nographic -kernel zImage -initrd <openeuler-image-qemu-xxx.cpio.gz>
   ```

   For the AArch64 architecture (ARM Cortex A57), run the following command:

   ```
   qemu-system-aarch64 -M virt-4.0 -m 1G -cpu cortex-a57 -nographic -kernel zImage -initrd <openeuler-image-qemu-xxx.cpio.gz>
   ```

   ![](./public_sys-resources/icon-note.gif) **Note:**   
   

The standard root file system image is securely hardened and requires you to set a password for the **root** user during the first startup. The password must comply with the follow requirements:

1. Must contain at least eight characters.
   
2. Must contain digits, letters, and special characters.
   
   @#$%^&*+|\\=~`!?,.:;-_'"(){}[]/>< 
   
   For example, **openEuler@2021**.



2. **Check whether QEMU is started successfully.**

   The shell of openEuler Embedded will be displayed after QEMU is successfully started and logged in.

### Shared File System Enabled Scenario

The shared file system allows the host machine of QEMU to share files with openEuler Embedded. In this way, programs that are cross-compiled on the host machine can run on openEuler Embedded after being copied to the shared directory.

Assume that the **/tmp** directory of the host machine is used as the shared directory, and a **hello_openeuler.txt** file is created in the directory in advance. To enable the shared file system function, perform the following steps:

1. **Start QEMU.**

   For the ARM architecture (ARM Cortex A15), run the following command:

   ```
   qemu-system-arm -M virt-4.0 -m 1G -cpu cortex-a15 -nographic -kernel zImage -initrd <openeuler-image-qemu-xxx.cpio.gz>  -device virtio-9p-device,fsdev=fs1,mount_tag=host -fsdev local,security_model=passthrough,id=fs1,path=/tmp
   ```

   For the AArch64 architecture (ARM Cortex A57), run the following command:

   ```
   qemu-system-aarch64 -M virt-4.0 -m 1G -cpu cortex-a57 -nographic -kernel zImage -initrd <openeuler-image-qemu-xxx.cpio.gz> -device virtio-9p-device,fsdev=fs1,mount_tag=host -fsdev local,security_model=passthrough,id=fs1,path=/tmp
   ```

2. **Mount the file system.**

   After you start and log in to openEuler Embedded, run the following commands to mount the shared file system:

   ```
   cd /tmp
   mkdir host
   mount -t 9p -o trans=virtio,version=9p2000.L host /tmp/host
   ```

   That is, mount the 9p file system to the **/tmp/host** directory of openEuler Embedded to implement sharing mapping.

3. **Check whether the file system is shared successfully.**

   In openEuler Embedded, run the following commands:

   ```
   cd /tmp/host
   ls
   ```
   If **hello_openeuler.txt** is discovered, the file system is shared successfully.

### Network Enabled Scenario

The virtio-net of QEMU and the virtual NIC of the host machine allow for the network communication between the host machine and openEuler Embedded. In addition to sharing files using virtio-fs, you can transfer files between the host machine and openEuler Embedded using the network, for example, the **scp** command.

1. **Start QEMU.**

   For the ARM architecture (ARM Cortex A15), run the following command:

   ```
   qemu-system-arm -M virt-4.0 -m 1G -cpu cortex-a15 -nographic -kernel zImage -initrd <openeuler-image-qemu-xxx.cpio.gz> -device virtio-net-device,netdev=tap0 -netdev tap,id=tap0,script=/etc/qemu-ifup
   ```

   For the AArch64 architecture (ARM Cortex A57), run the following command:

   ```
   qemu-system-aarch64 -M virt-4.0 -m 1G -cpu cortex-a57 -nographic -kernel zImage -initrd <openeuler-image-qemu-xxx.cpio.gz> -device virtio-net-device,netdev=tap0 -netdev tap,id=tap0,script=/etc/qemu-ifup
   ```

2. **Create a vNIC on the host machine.**

   You can create a **/qemu-ifup** script in the **/etc** directory and run the script to create a **tap0** vNIC on the host machine. The script details are as follows:

   ```
   #!/bin/bash
   ifconfig $1 192.168.10.1 up
   ```
   **root** permissions are required for running the script.

   ```
   chmod a+x qemu-ifup
   ```

   Use the **qemu-ifup** script to create a **tap0** vNIC on the host machine. The IP address of the vNIC is **192.168.10.1**.

3. **Configure the NIC of openEuler Embedded.**

   Log in to openEuler Embedded and run the following command:
   ```
   ifconfig eth0 192.168.10.2
   ```

4. **Check whether the network connection is normal.**

   In openEuler Embedded, run the following command:
   ```
   ping 192.168.10.1
   ```

   If the IP address can be pinged, the network connection between the host machine and openEuler Embedded is normal.

   >![](./public_sys-resources/icon-note.gif) **Note:**   
   >
   >If you need openEuler Embedded to access the Internet through the host machine, create a bridge on the host machine. For details, see the related documents.

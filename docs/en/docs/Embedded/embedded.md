# openEuler Embedded User Guide

openEuler Embedded is a Linux distribution for embedded scenarios based on the openEuler community version aiming to build a high-quality Linux-centric embedded software platform. openEuler Embedded will keep consistent with other openEuler versions in terms of kernel and software versions, with kernel configurations, software package combinations and configurations, and feature patches optimized for embedded scenarios.

This document describes how to obtain pre-built images, run the images, develop basic embedded Linux applications based on the images, and build openEuler Embedded.

# openEuler Embedded 22.09 Release Notes

openEuler Embedded 22.09 is the second innovation release of openEuler Embedded. This section describes the main features of this version.

## Kernel

- The kernel is upgraded to 5.10.0-106.18.0.

- The kernel supports Preempt-RT patches.

- The kernel supports Raspberry Pi 4B patches.

## Software Packages

- More than 140 software packages are supported. For details, see [Supported Software Packages](https://openeuler.gitee.io/yocto-meta-openeuler/features/software_package_description.html).

## Feature Highlights

- The multi-OS hybrid deployment capability is enhanced. The Raspberry Pi 4B hybrid deployment instance is added. The service-oriented hybrid deployment function is added. Zephyr can be accessed through the Linux shell CLI. For details, see [Multi-OS Hybrid Deployment Framework](https://openeuler.gitee.io/yocto-meta-openeuler/features/mcs.html).

- The distributed soft bus capability is enhanced. The distributed soft bus-based openEuler and OpenHarmony device authentication and interconnection are supported. Southbound Wi-Fi transmission media are supported. For details, see [Distributed Soft Bus](https://openeuler.gitee.io/yocto-meta-openeuler/features/distributed_soft_bus.html).

- Security hardening. For details, see [Security Hardening Description](https://openeuler.gitee.io/yocto-meta-openeuler/security_hardening/index.html).

- Preempt-RT-based soft real-time. For details, see [Soft Real-Time System Introduction](https://openeuler.gitee.io/yocto-meta-openeuler/features/preempt_rt.html).

## Build System

- The NativeSDK is added for containerized build. For details, see [Container Build Guide](./container-build-guide.md).

Container Build Guide
==============================

The openEuler Embedded build process is based on the openEuler OS, and requires many system tools and build tools to be installed. To help developers quickly set up a build environment, the OS and tools on which the build process depends are encapsulated into a container. In this way, developers can avoid the time-consuming environment preparation process and focus on development.

<!-- TOC -->

  - [Environment Preparation](#environment-preparation)
    - [Installing Docker](#installing-docker)
    - [Obtaining the Container Image](#obtaining-the-container-image)
    - [Preparing the Container Build Environment](#preparing-the-container-build-environment)
  - [Version Build](#version-build)
    - [Downloading Source Code](#downloading-source-code)
    - [Compiling the Build](#compiling-the-build)
    - [Build Result](#build-result)
    <!-- /TOC -->

## Environment Preparation

Use Docker to create a container environment. The software and hardware requirements of Docker are as follows:

-   OS: openEuler 20.03/22.03, Ubuntu 20.04/22.04, Debian 11, and SUSE 12.05 are recommended.
-   Kernel: Linux 3.8 or later is recommended.
-   Driver: The kernel must include a proper storage driver, for example, Device Mapper, AUFS, vfs, btrfs, or ZFS.
-   Architecture: 64-bit architecture (currently only x86-64 and AMD64).

### Installing Docker

-------------

1. Check whether Docker has been installed in the current environment.

   Run the following command. If the Docker version is displayed, Docker has been installed in the current environment. You can use it directly.

   ``` {.sourceCode .console}
   docker version
   ```

2. If Docker is not installed, install it by referring to the [official document](http://docs.docker.com/engine/install/).

   Install Docker on openEuler by referring to the installation guide for CentOS.

   For example, run the following command to install Docker on openEuler:

   ``` 
   sudo yum install docker
   ```

### Obtaining the Container Image

---------------

Run the `docker pull` command to pull the image from Huawei Cloud to the host machine:

``` 
docker pull swr.cn-north-4.myhuaweicloud.com/openeuler-embedded/openeuler-container
```

By default, the latest image is downloaded. You can also specify the image version to be downloaded based on the version to be compiled. The command is as follows:

```
docker pull [Container Image Name]:[Tag]
# example: docker pull swr.cn-north-4.myhuaweicloud.com/openeuler-embedded/openeuler-container:latest
```

Container images

| Container Image Name                                         | Tag       | For Image Branch    | Kernel Version | Libc Version |
| ------------------------------------------------------------ | --------- | ------------------- | -------------- | ------------ |
| swr.cn-north-4.myhuaweicloud.com/openeuler -embedded/openeuler-container | latest    | master              | 21.03          | 2.31         |
| swr.cn-north-4.myhuaweicloud.com/openeuler -embedded/openeuler-container | 22.09     | openEuler-22.09     | 21.03          | 2.31         |
| swr.cn-north-4.myhuaweicloud.com/openeuler -embedded/openeuler-container | 22.03-lts | openEuler-22.03-LTS | 22.03 LTS      | 2.34         |
| swr.cn-north-4.myhuaweicloud.com/openeuler -embedded/openeuler-container | 21.09     | openEuler-21.09     | 21.03          | 2.31         |

> ![](./public_sys-resources/icon-note.gif) **NOTE**
>
> - To build openEuler images of different branches or versions, you need to use different containers. The **For Image Branch** column shows the mapping.
> - To be compatible with the host tool and native SDK of Yocto Poky, the built-in libc 2.31 container is used. Therefore, the C library version is earlier than 22.03.

### Preparing the Container Build Environment

-------------------

#### 1. Start a container.

Run the `docker run` command to start a container. To ensure that the container can run in the background and access the Internet after being started, you are advised to run the following command to start the container:

``` 
docker run -idt --network host swr.cn-north-4.myhuaweicloud.com/openeuler-embedded/openeuler-container bash
```

Parameter description:

-   **-i**: keeps the standard input open.
-   **-d**: starts a container in daemon mode in the background.
-   **-t**: allocates a pseudo-tty and binds it to the standard input of the container.
-   **\--network**: connects a container to the network of the host machine.
-   **swr.cn-north-4.myhuaweicloud.com/openeuler-embedded/openeuler-container**: specifies the name of the image (image_name:image_version).
-   **bash**: method for accessing a container.

#### 2. Check the ID of the started container.

``` 
docker ps
```

#### 3. Enter the container.

``` 
docker exec -it <container_id> bash
```

After the build environment is ready, you can build in the container.

## Version Build

### Downloading Source Code

1. Obtain the source code download script.

   ``` 
   git clone https://gitee.com/openeuler/yocto-meta-openeuler.git -b <For Image Branch> -v /usr1/openeuler/src/yocto-meta-openeuler
   #example: git clone https://gitee.com/openeuler/yocto-meta-openeuler.git -b master -v /usr1/openeuler/src/yocto-meta-openeuler
   ```

   > ![](./public_sys-resources/icon-note.gif) **NOTE**
   >
   > - For details about **&lt;For Image Branch&gt;**, see the content in the third column of the container image list.
   >
   > - The full code required for build is obtained from the yocto-meta-openeuler repository. Therefore, if you want to build the code of the corresponding version (such as openEuler 22.09 or openEuler 22.03 LTS), download the yocto-meta-openeuler repository of the corresponding branch.
   > - Different containers are required for building openEuler images of different branches or versions.

2. Download the source code using the script.

   ``` 
   cd /usr1/openeuler/src/yocto-meta-openeuler/scripts
   sh download_code.sh /usr1/openeuler/src
   ```
   
   > ![](./public_sys-resources/icon-note.gif) **NOTE**   
   >
   > 22.09 and later versions support **/usr1/openeuler/src/yocto-meta-openeuler/script/oe_helper.sh**. You can run the **source oe_helper.sh** command to download the code by referring to the **usage** description.

### Compiling the Build

-   Compilation architecture: aarch64-std, aarch64-pro, arm-std, or raspberrypi4-64
-   Build directory: **/usr1/build**
-   Source code directory: **/usr1/openeuler/src**
-   Path of the compiler: **/usr1/openeuler/gcc/openeuler\_gcc\_arm64le**

>![](./public_sys-resources/icon-note.gif) **NOTE**  
>- Use different compilers for different compilation architectures. aarch64-std, aarch64-pro, and raspberrypi4-64 use the openeuler\_gcc\_arm64le compiler, and arm-std uses the openeuler\_gcc\_arm32le compiler.
>- The following uses the aarch64-std architecture as an example.

1. Change the owner group of the **/usr1** directory to **openeuler**. Otherwise, permission issues may occur when switching to the **openeuler** user.

   ``` 
   chown -R openeuler:users /usr1
   ```

2. Switch to the **openeuler** user.

   ``` 
   su openeuler
   ```

3. Go to the path where the build script is stored and run the script.

   ``` 
   # Go to the directory where the compilation initialization scripts are stored.
   cd /usr1/openeuler/src/yocto-meta-openeuler/scripts
   ```
   ```
   # For versions earlier than 22.03, skip this command. (You must run this command in 22.09 and later versions.)
   # Initialize the container build dependency tool (poky nativesdk).
   . /opt/buildtools/nativesdk/environment-setup-x86_64-pokysdk-linux
   ```
   ```
   # Initialize the compilation environment using the compilation initialization script.
   source compile.sh aarch64-std /usr1/build /usr1/openeuler/gcc/openeuler_gcc_arm64le
   bitbake openeuler-image
   ```

   > ![](./public_sys-resources/icon-note.gif) **NOTE**   
   >
   > 22.09 and later versions support **/usr1/openeuler/src/yocto-meta-openeuler/script/oe_helper.sh**. You can run the **source oe_helper.sh** command to download the code by referring to the **usage** description.

### Build Result

By default, the files are generated in the **output** directory of the build directory. For example, the built files of the aarch64-std example are generated in the **/usr1/build/output** directory, as shown in the following table:

| Filename                                                  | Description                         |
| --------------------------------------------------------- | ----------------------------------- |
| Image-\*                                                  | openEuler Embedded image            |
| openeuler-glibc-x86\_64-openeuler-image-\*-toolchain-\*.sh | openEuler Embedded SDK toolchain    |
| openeuler-image-qemu-aarch64-\*.rootfs.cpio.gz             | openEuler Embedded file system      |
| zImage                                                    | openEuler Embedded compressed image |

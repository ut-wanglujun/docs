# Trusted Computing

<!-- TOC -->

- [Trusted Computing](#trusted-computing)
    - [Trusted Computing Basics](#trusted-computing-basics)
        - [What Is Trusted Computing](#what-is-trusted-computing)
    - [Kernel Integrity Measurement Architecture (IMA)](#kernel-integrity-measurement-architecture-ima)
        - [Overview](#overview)
        - [Constraints](#constraints)
        - [Application Scenario](#application-scenario)
        - [Procedure](#procedure)
        - [FAQs](#faqs)
        - [Appendixes](#appendixes)
    - [Remote Attestation (Kunpeng Security Library)](#remote-attestation-kunpeng-security-library)
        - [Introduction](#introduction)
        - [Software Architecture](#software-architecture)
        - [Installation and Configuration](#installation-and-configuration)
        - [Options](#options)
        - [API Definition](#api-definition)
        - [FAQs](#faqs-1)

<!-- /TOC -->

## Trusted Computing Basics

### What Is Trusted Computing

The definition of being trusted varies with international organizations.

1. Trusted Computing Group (TCG):

   An entity that is trusted always achieves the desired goal in an expected way.

2. International Organization for Standardization (ISO) and International Electrotechnical Commission (IEC) (1999):

   The components, operations, or processes involved in computing are predictable under any conditions and are resistant to viruses and a certain degree of physical interference.

3. IEEE Computer Society Technical Committee on Dependable Computing:

   Being trusted means that the services provided by the computer system can be proved to be reliable, and mainly refers to the reliability and availability of the system.

In short, being trusted means that the system operates according to a pre-determined design and policy.

A trusted computing system consists of a root of trust, a trusted hardware platform, operating system (OS), and application. The basic idea of the system is to create a trusted computing base (TCB) first, and then establish a trust chain that covers the hardware platform, OS, and application. In the trust chain, authentication is performed from the root to the next level, extending trust level by level and building a secure and trusted computing environment.

![](./figures/trusted_chain.png)

Unlike the traditional security mechanism that eliminates viruses without solving the root of the problem, trusted computing adopts the whitelist mechanism to allow only authorized kernels, kernel modules, and applications to run on the system. The system will reject the execution of a program that is unknown or has been changed.

## Kernel Integrity Measurement Architecture (IMA)

### Overview

#### IMA

The integrity measurement architecture (IMA) is a subsystem in the kernel. The IMA can measure files accessed through **execve()**, **mmap()**, and **open()** systems based on user-defined policies. The measurement result can be used for **local or remote attestation**, or can be compared with an existing reference value to **control the access to files**.

According to the Wiki definition, the function of the kernel integrity subsystem includes three parts:

- Measure: Detects accidental or malicious modifications to files, either remotely or locally.
- Appraise: Measures a file and compares it with a reference value stored in the extended attribute to control the integrity of the local file.
- Audit: Writes the measurement result into system logs for auditing.

Figuratively, IMA measurement is an observer that only records modification without interfering in it, and IMA appraisal is more like a strict security guard that rejects any unauthorized access to programs.

#### EVM

The extended verification module (EVM) is used to calculate a hash value based on the security extended attributes of a file in the system, including **security.ima** and **security.selinux**. Then this value is signed by the key stored in the TPM or other trusted environments. The signature value is stored in **security.evm** and cannot be tampered with. If the value is tampered with, the signature verification fails when the file is accessed again.

In summary, the EVM is used to provide offline protection for security extended attributes by calculating the digest of the attributes and signing and storing them in **security.evm**.

#### IMA Digest Lists

IMA Digest Lists are an enhancement of the original kernel integrity protection mechanism provided by openEuler. It replaces the original IMA mechanism to protect file integrity.

Digest lists are binary data files in a special format. Each digest list corresponds to an RPM package and records the hash values of protected files (executable files and dynamic library files) in the RPM package.

After the startup parameters are correctly configured, the kernel maintains a hash table (invisible to the user space) and provides interfaces (**digest\_list\_data** and **digest\_list\_data\_del**) that update the hash table using **securityfs**. The digest lists are signed by the private key when they are built. When uploaded to the kernel through the interface, the digest lists need to be verified by the public key in the kernel.

![](./figures/ima_digest_list_update.png)

When IMA appraisal is enabled, each time an executable file or dynamic library file is accessed, the hook in the kernel is invoked to calculate the hash values of the file content and extended attributes and search in the kernel hash table. If the calculated hash values match the one in the table, the file is allowed to be executed. Otherwise, the access is denied.

![1599719649188](./figures/ima_verification.png)

The IMA Digest Lists extension provided by the openEuler kernel provides higher security, performance, and usability than the native IMA mechanism of the kernel community, facilitating the implementation of the integrity protection mechanism in the production environment.

- **A complete trust chain for high security**

  The native IMA mechanism requires that the file extended attribute be generated and marked in advance on the live network. When the file is accessed, the file extended attribute is used as a reference value, resulting in an incomplete trust chain.

  The IMA Digest Lists extension saves the reference digest value of the file in the kernel space. During the construction, the reference digest value of the file is carried in the released RPM package in the form of a digest list. When the RPM package is installed, the digest list is imported and the signature is verified, ensuring that the reference value comes from the software publisher and implementing a complete trust chain.

- **Superior performance**

  The trusted platform module (TPM) chip is a low-speed chip, making the PCR extension operation a performance bottleneck in the IMA measurement scenario. To shatter this bottleneck, the Digest Lists extension reduces unnecessary PCR extension operations while ensuring security, providing 65% higher performance than the native IMA mechanism.

  In the IMA appraisal scenario, the Digest Lists extension performs signature verification in the startup phase to prevent signature verification from being performed each time the file is accessed. This helps deliver a 20% higher file access performance in the operation phase than that in the native IMA appraisal scenario.

- **Fast deployment and smooth upgrade**

  When the native IMA mechanism is deployed for the first time or the software package is updated, you need to switch to the fix mode, manually mark the extended attributes of the file, and then restart the system to enter the enforcing mode. In this way, the installed program can be accessed normally.

  The Digest Lists extension can be used immediately after the installation is completed. In addition, the RPM package can be directly installed or upgraded in the enforcing mode without restarting the system or manually marking the extended attributes of the file. This minimizes user perception during the operation, allowing for quick deployment and smooth upgrade on the live network.

Note: The IMA Digest Lists extension advances the signature verification of the native IMA to the startup phase. This causes the assumption that the memory in the kernel space cannot be tampered with. As a result, the IMA depends on other security mechanisms (secure startup of kernel module and dynamic memory measurement) to protect the integrity of the kernel memory.

However, either the native IMA mechanism of the community or the IMA Digest Lists extension is only a link in the trust chain of trusted computing, and cannot ensure the system security alone. Security construction is always a systematic project that builds in-depth defense.

### Constraints

1. The current IMA appraisal mode can only protect immutable files in the system, including executable files and dynamic library files.
2. The IMA provides integrity measurement at the application layer. The security of the IMA depends on the reliability of the previous links.
3. Currently, the IMA does not support the import of the third-party application digest lists.
4. The startup log may contain `Unable to open file: /etc/keys/x509_ima.der`. This error is reported from the open source community and does not affect the use of the IMA digest lists feature.
5. In the ARM version, audit errors may occur when the log mode is enabled for the IMA. This occurs because the modprobe loads the kernel module before the digest lists are imported, but does not affect the normal functions.

### Application Scenario

#### IMA Measurement

The purpose of IMA measurement is to detect unexpected or malicious modifications to system files. The measurement result can be used for local or remote attestation.

If a TPM chip exists in the system, the measurement result is extended to a specified PCR register of the TPM chip. Due to the unidirectional PCR extension and the hardware security of the TPM chip, a user cannot modify the extended measurement result, thereby ensuring authenticity of the measurement result.

The file scope and triggering conditions of IMA measurement can be configured by the user using the IMA policy.

By default, IMA is disabled. However, the system searches for the **ima-policy** policy file in the `/etc/ima/` path. If the file is found, the system measures the files in the system based on the policy during startup. If you do not want to manually compile the policy file, you can configure the `ima_policy=tcb` in the startup parameters using the default policy. For details about more policy parameters, see the section *IMA Startup Parameters* in *Appendix*.

You can check the currently loaded IMA policy in the `/sys/kernel/security/ima/policy` file. The IMA measurement log is located in the `/sys/kernel/security/ima/ascii_runtime_measurements` file, as shown in the following figure:

```shell
$ head /sys/kernel/security/ima/ascii_runtime_measurements
10 ddee6004dc3bd4ee300406cd93181c5a2187b59b ima-ng sha1:9797edf8d0eed36b1cf92547816051c8af4e45ee boot_aggregate
10 180ecafba6fadbece09b057bcd0d55d39f1a8a52 ima-ng sha1:db82919bf7d1849ae9aba01e28e9be012823cf3a /init
10 ac792e08a7cf8de7656003125c7276968d84ea65 ima-ng sha1:f778e2082b08d21bbc59898f4775a75e8f2af4db /bin/bash
10 0a0d9258c151356204aea2498bbca4be34d6bb05 ima-ng sha1:b0ab2e7ebd22c4d17d975de0d881f52dc14359a7 /lib64/ld-2.27.so
10 0d6b1d90350778d58f1302d00e59493e11bc0011 ima-ng sha1:ce8204c948b9fe3ae67b94625ad620420c1dc838 /etc/ld.so.cache
10 d69ac2c1d60d28b2da07c7f0cbd49e31e9cca277 ima-ng sha1:8526466068709356630490ff5196c95a186092b8 /lib64/libreadline.so.7.0
10 ef3212c12d1fbb94de9534b0bbd9f0c8ea50a77b ima-ng sha1:f80ba92b8a6e390a80a7a3deef8eae921fc8ca4e /lib64/libc-2.27.so
10 f805861177a99c61eabebe21003b3c831ccf288b ima-ng sha1:261a3cd5863de3f2421662ba5b455df09d941168 /lib64/libncurses.so.6.1
10 52f680881893b28e6f0ce2b132d723a885333500 ima-ng sha1:b953a3fa385e64dfe9927de94c33318d3de56260 /lib64/libnss_files-2.27.so
10 4da8ce3c51a7814d4e38be55a2a990a5ceec8b27 ima-ng sha1:99a9c095c7928ecca8c3a4bc44b06246fc5f49de /etc/passwd
```

From left to right, the content of each record indicates:

1. PCR: PCR register for extending measurement results (The default value is 10. This register is valid only when the TPM chip is installed in the system.)
2. Template hash value: hash value that is finally used for extension, combining the file content hash and the length and value of the file path
3. Template: template of the extended measurement value, for example, **ima-ng**
4. File content hash value: hash value of the measured file content
5. File path: path of the measured file

#### IMA Appraisal

The purpose of IMA appraisal is to control access to local files by comparing the reference value with the standard reference value.

IMA uses the security extension attributes **security.ima** and **security.evm** to store the reference values of file integrity measurement.

- **security.ima**: stores the hash value of the file content
- **security.evm**: stores the hash value signature of a file extended attribute

When a protected file is accessed, the hook in the kernel is triggered to verify the integrity of the extended attributes and content of the file.

1. Use the public key in the kernel keyring to verify the signature value in the extended attribute of the **security.evm** file, and compare this signature value with the hash value of the extended attribute of the current file. If they match, the extended attribute of the file is complete (including **security.ima**).
2. When the extended attribute of the file is complete, the system compares the extended attribute of the file **security.ima** with the digest value of the current file content. If they match, the system allows for the access to the file.

Likewise, the file scope and trigger conditions for IMA appraisal can be configured by users using IMA policies.

#### IMA Digest Lists

Currently, the IMA Digest Lists extension supports the following three combinations of startup parameters:

- IMA measurement mode:

  ```shell
  ima_policy=exec_tcb ima_digest_list_pcr=11
  ```

- IMA appraisal log mode + IMA measurement mode:

  ```shell
  ima_template=ima-sig ima_policy="exec_tcb|appraise_exec_tcb|appraise_exec_immutable" initramtmpfs ima_hash=sha256 ima_appraise=log evm=allow_metadata_writes evm=x509 ima_digest_list_pcr=11 ima_appraise_digest_list=digest
  ```

- IMA appraisal enforcing mode + IMA measurement mode:

  ```shell
  ima_template=ima-sig ima_policy="exec_tcb|appraise_exec_tcb|appraise_exec_immutable" initramtmpfs ima_hash=sha256 ima_appraise=enforce-evm evm=allow_metadata_writes evm=x509 ima_digest_list_pcr=11 ima_appraise_digest_list=digest
  ```

### Procedure

#### Initial Deployment in the Native IMA Scenario

When the system is started for the first time, you need to configure the following startup parameters:

```shell
ima_appraise=fix ima_policy=appraise_tcb
```

In the `fix` mode, the system can be started when no reference value is available. `appraise_tcb` corresponds to an IMA policy. For details, see *IMA Startup Parameters* in the *Appendix*.

Next, you need to access all the files that need to be verified to add IMA extended attributes to them:

```shell
time find / -fstype ext4 -type f -uid 0 -exec dd if='{}' of=/dev/null count=0 status=none \;
```

This process takes some time. After the command is executed, you can see the marked reference value in the extended attributes of the protected file.

```shell
$ getfattr -m - -d /sbin/init
# file: sbin/init
security.ima=0sAXr7Qmun5mkGDS286oZxCpdGEuKT
security.selinux="system_u:object_r:init_exec_t"
```

Configure the following startup parameters and restart the system:

```shell
ima_appraise=enforce ima_policy=appraise_tcb
```

#### Initial Deployment in the Digest Lists Scenario

1. Set kernel parameters to enter the log mode.

   Add the following parameters to edit the `/boot/efi/EFI/euleros/grub.cfg` file:

   ```shell
   ima_template=ima-sig ima_policy="exec_tcb|appraise_exec_tcb|appraise_exec_immutable" initramtmpfs ima_hash=sha256 ima_appraise=log evm=allow_metadata_writes evm=x509 ima_digest_list_pcr=11 ima_appraise_digest_list=digest
   ```

   Run the `reboot` command to restart the system and enter the log mode. In this mode, integrity check has been enabled, but the system can be started even if the check fails.

2. Install the dependency package.

   Run the **yum** command to install **digest-list-tools** and **ima-evm-utils**. Ensure that the versions are not earlier than the following:

   ```shell
   $ yum install digest-list-tools ima-evm-utils
   $ rpm -qa | grep digest-list-tools
   digest-list-tools-0.3.93-1.oe1.x86_64
   $ rpm -qa | grep ima-evm-utils
   ima-evm-utils-1.2.1-9.oe1.x86_64
   ```

3. If the **plymouth** package is installed, you need to add `-a` to the end of the **cp** command in line 147 in the `/usr/libexec/plymouth/plymouth-populate-initrd` script file:

   ```shell
       ...
       ddebug "Installing $_src"
    cp -a --sparse=always -pfL "$PLYMOUTH_SYSROOT$_src" "${initdir}/$target"
   }
   ```

4. Run `dracut` to generate **initrd** again:

   ```shell
   dracut -f -e xattr
   ```

   Edit the `/boot/efi/EFI/euleros/grub.cfg` file by changing **ima\_appraise=log** to **ima\_appraise=enforce-evm**.

   ```shell
   ima_template=ima-sig ima_policy="exec_tcb|appraise_exec_tcb|appraise_exec_immutable" initramtmpfs ima_hash=sha256 ima_appraise=enforce-evm evm=allow_metadata_writes evm=x509 ima_digest_list_pcr=11 ima_appraise_digest_list=digest
   ```

   Run the **reboot** command to complete the initial deployment.

#### Building Digest Lists on OBS

Open Build Service (OBS) is a compilation system that was first used for building software packages in openSUSE and supports distributed compilation of multiple architectures.

Before building a digest list, ensure that your project contains the following RPM packages from openEuler:

- digest-list-tools
- pesign-obs-integration
- selinux-policy
- rpm
- openEuler-rpm-config

Add **Project Config** in the deliverable project:

```shell
Preinstall: pesign-obs-integration digest-list-tools selinux-policy-targeted
Macros:
%__brp_digest_list /usr/lib/rpm/openEuler/brp-digest-list %{buildroot}
:Macros
```

- The following content is added to **Preinstall**: **digest-list-tools** for generating the digest list; **pesign-obs-integration** for generating the digest list signature; **selinux-policy-targeted**, ensuring that the SELinux label in the environment is correct when the digest list is generated.
- Define the macro **%\_\_brp\_digest\_list** in Macros. The RPM runs this macro to generate a digest list for the compiled binary file in the build phase. This macro can be used as a switch to control whether the digest list is generated in the project.

After the configuration is completed, OBS automatically performs full build. In normal cases, the following two files are added to the software package:

- **/etc/ima/digest\_lists/0-metadata\_list-compact-\[package name]-\[version number]**
- **/etc/ima/digest\_lists.tlv/0-metadata\_list-compact\_tlv-\[package name]-\[version number]**

#### Building Digest Lists on Koji

Koji is a compilation system of the Fedora community. The openEuler community will support Koji in the future.

### FAQs

1. Why does the system fail to be started, or commands fail to be executed, or services are abnormal after the system is started in enforcing mode?

    In enforcing mode, IMA controls file access. If the content or extended attributes of a file to be accessed are incomplete, the access will be denied. If key commands that affect system startup cannot be executed, the system cannot be started.

    Check whether the following problems exist:

    - **Check whether the digest list is added to initrd.**

      Check whether the **dracut** command is executed to add the digest list to the kernel during the initial deployment. If the digest list is not added to **initrd**, the digest list cannot be imported during startup. As a result, the startup fails.

    - **Check whether the official RPM package is used.**

      If a non-official openEuler RPM package is used, the RPM package may not carry the digest list, or the private key for signing the digest list does not match the public key for signature verification in the kernel. As a result, the digest list is not imported to the kernel.

    If the cause is not clear, enter the log mode and find the cause from the error log:

    ```shell
    dmesg | grep appraise
    ```

2. Why access control is not performed on system files in enforcing mode?

   When the system does not perform access control on the file as expected, check whether the IMA policy in the startup parameters is correctly configured:

   ```shell
   $ cat /proc/cmdline
   ...ima_policy=exec_tcb|appraise_exec_tcb|appraise_exec_immutable...
   ```

   Run the following command to check whether the IMA policy in the current kernel has taken effect:

   ```shell
   cat /sys/kernel/security/ima/policy
   ```

   If the policy file is empty, it indicates that the policy fails to be set. In this case, the system does not perform access control.

3. After the initial deployment is completed, do I need to manually run the **dracut** command to generate **initrd** after installing, upgrading, or uninstalling the software package?

   No. The **digest\_list.so** plug-in provided by the RPM package can automatically update the digest list at the RPM package granularity, allowing users to be unaware of the digest list.

### Appendixes

#### Description of the IMA securityfs Interface

The native IMA provides the following **securityfs** interfaces:

> Note: The following interface paths are in the `/sys/kernel/security/` directory.

| Path                           | Permission | Description                                                  |
| ------------------------------ | ---------- | ------------------------------------------------------------ |
| ima/policy                     | 600        | IMA policy interface                                         |
| ima/ascii_runtime_measurement  | 440        | IMA measurement result in ASCII code format                  |
| ima/binary_runtime_measurement | 440        | IMA measurement result in binary format                      |
| ima/runtime_measurement_count  | 440        | Measurement result statistics                                |
| ima/violations                 | 440        | Number of IMA measurement result conflicts                   |
| evm                            | 660        | EVM mode, that is, the mode for verifying the integrity of extended attributes of files |

The values of `/sys/kernel/security/evm` are as follows:

- 0: EVM uninitialized.
- 1: Uses HMAC (symmetric encryption) to verify the integrity of extended attributes.
- 2: Uses the public key signature (asymmetric encryption) to verify the integrity of extended attributes.
- 6: Disables the integrity check of extended attributes (This mode is used for openEuler).

The additional **securityfs** interfaces provided by the IMA Digest Lists extension are as follows:

| Path                     | Permission | Description                                                |
| ------------------------ | ---------- | ---------------------------------------------------------- |
| ima/digests_count        | 440        | Total number of digests (IMA+EVM) in the system hash table |
| ima/digest_list_data     | 200        | New interfaces in the digest list                          |
| ima/digest_list_data_del | 200        | Interfaces deleted from the digest list                    |

#### IMA Policy Syntax

Each IMA policy statement must start with an **action** represented by the keyword action and be followed by a **filtering condition**:

- **action**: indicates the action of a policy. Only one **action** can be selected for a policy.

  > Note: You can **ignore the word action** and directly write **dont\_measure** instead of **action=dont\_measure**.

- **func**: indicates the type of the file to be measured or authenticated. It is often used together with **mask**. Only one **func** can be selected for a policy.

    - **FILE\_CHECK** can be used only with **MAY\_EXEC**, **MAY\_WRITE**, and **MAY\_READ**.
    - **MODULE\_CHECK**, **MMAP\_CHECK**, and **BPRM\_CHECK** can be used only with **MAY\_EXEC**.
    - A combination without the preceding matching relationships does not take effect.

- **mask**: indicates the operation upon which files will be measured or appraised. Only one **mask** can be selected for a policy.

- **fsmagic**: indicates the hexadecimal magic number of the file system type, which is defined in the `/usr/include/linux/magic.h` file.

  > Note: By default, all file systems are measured unless you use the **dont\_measure/dont\_appraise** to mark a file system not to be measured.

- **fsuid**: indicates the UUID of a system device. The value is a hexadecimal string of 16 characters.

- **objtype**: indicates the file type. Only one file type can be selected for a policy.

  > Note: **objtype** has a finer granularity than **func**. For example, **obj\_type=nova\_log\_t** indicates the nova log file.

- **uid**: indicates the user (represented by the user ID) who performs operations on the file. Only one **uid** can be selected for a policy.

- **fowner**: indicates the owner (represented by the user ID) of the file. Only one **fowner** can be selected for a policy.

The values and description of the keywords are as follows:

| Keyword       | Value              | Description                                                  |
| ------------- | ------------------ | ------------------------------------------------------------ |
| action        | measure            | Enables IMA measurement                                      |
|               | dont_measure       | Disables IMA measurement                                     |
|               | appraise           | Enables IMA appraisal                                        |
|               | dont_appraise      | Disables IMA appraisal                                       |
|               | audit              | Enables audit                                                |
| func          | FILE_CHECK         | File to be opened                                            |
|               | MODULE_CHECK       | Kernel module file to be loaded                              |
|               | MMAP_CHECK         | Dynamic library file to be mapped to the memory space of the process |
|               | BRPM_CHECK         | File to be executed (excluding script files opened by programs such as `/bin/hash`) |
|               | POLICY_CHECK       | File to be loaded as a supplement to the IMA policy          |
|               | FIRMWARE_CHECK     | Firmware to be loaded into memory                            |
|               | DIGEST_LIST_CHECK  | Digest list file to be loaded into the kernel                |
|               | KEXEC_KERNEL_CHECK | kexec kernel to be switched to                               |
| mask          | MAY_EXEC           | Executes a file                                              |
|               | MAY_WRITE          | Writes data to a file This operation is not recommended because it is restricted by open source mechanisms such as echo and vim (the essence of modification is to create a temporary file and then rename it). The IMA measurement of **MAY\_WRITE** is not triggered each time the file is modified. |
|               | MAY_READ           | Reads a file                                                 |
|               | MAY_APPEND         | Extends file attributes                                      |
| fsmagic       | fsmagic=xxx        | Hexadecimal magic number of the file system type             |
| fsuuid        | fsuuid=xxx         | UUID of a system device. The value is a hexadecimal string of 16 characters. |
| fowner        | fowner=xxx         | User ID of the file owner                                    |
| uid           | uid=xxx            | ID of the user who operates the file                         |
| obj_type      | obj_type=xxx_t     | File type (based on the SELinux tag)                         |
| pcr           | pcr=<num>          | Selects the PCR used to extend the measurement values in the TPM. The default value is 10. |
| appraise_type | imasig             | Signature-based IMA appraisal                                |
|               | meta_immutable     | Evaluates the extended attributes of the file based on signatures (supporting the digest list). |

> Note: **PATH\_CHECK** is equivalent to **FILE\_CHECK**, and **FILE\_MMAP** is equivalent to **MMAP\_CHECK**. They are not mentioned in this table.

#### IMA Native Startup Parameters

The following table lists the kernel startup parameters of the native IMA.

| Parameter        | Value        | Description                                                  |
| ---------------- | ------------ | ------------------------------------------------------------ |
| ima_appraise     | off          | Disables the IMA appraisal mode. The integrity check is not performed when the file is accessed and no new reference value is generated for the file. |
|                  | enforce      | Enables the IMA appraisal enforcing mode to perform the integrity check when the file is accessed. That is, the file digest value is calculated and compared with the reference value. If the comparison fails, the file access is rejected. In this case, the IMA generates a new reference value for the new file. |
|                  | fix          | Enables the IMA repair mode. In this mode, the reference value of a protected file can be updated. |
|                  | log          | Enables the IMA appraisal log mode to perform the integrity check when the file is accessed. However, commands can be executed even if the check fails, and only logs are recorded. |
| ima_policy       | tcb          | Measures all file execution, dynamic library mapping, kernel module import, and device driver loading. The file read behavior of the root user is also measured. |
|                  | appraise_tcb | Evaluates all files whose owner is the root user.            |
|                  | secure_boot  | Evaluates the kernel module import, hardware driver loading, kexec kernel switchover, and IMA policies. The prerequisite is that these files have IMA signatures. |
| ima_tcb          | None         | Equivalent to **ima\_policy=tcb**.                           |
| ima_appraise_tcb | None         | Equivalent to **ima\_policy=appraise\_tcb**.                 |
| ima_hash         | sha1/md5/... | IMA digest algorithm. The default value is sha1.             |
| ima_template     | ima          | IMA measurement extension template                           |
|                  | ima-ng       | IMA measurement extension template                           |
|                  | ima-sig      | IMA measurement extension template                           |
| integrity_audit  | 0            | Basic integrity audit information (default)                  |
|                  | 1            | Additional integrity audit information                       |

> Note: The **ima\_policy** parameter can specify multiple values at the same time, for example, **ima\_policy=tcb\|appraise\_tcb**. After the system is started, the IMA policy of the system is the sum of the policies for the two parameters.

The IMA policy for the `ima_policy=tcb` startup parameter is as follows:

```console
# PROC_SUPER_MAGIC = 0x9fa0
dont_measure fsmagic=0x9fa0
# SYSFS_MAGIC = 0x62656572
dont_measure fsmagic=0x62656572
# DEBUGFS_MAGIC = 0x64626720
dont_measure fsmagic=0x64626720
# TMPFS_MAGIC = 0x01021994
dont_measure fsmagic=0x1021994
# DEVPTS_SUPER_MAGIC=0x1cd1
dont_measure fsmagic=0x1cd1
# BINFMTFS_MAGIC=0x42494e4d
dont_measure fsmagic=0x42494e4d
# SECURITYFS_MAGIC=0x73636673
dont_measure fsmagic=0x73636673
# SELINUX_MAGIC=0xf97cff8c
dont_measure fsmagic=0xf97cff8c
# SMACK_MAGIC=0x43415d53
dont_measure fsmagic=0x43415d53
# CGROUP_SUPER_MAGIC=0x27e0eb
dont_measure fsmagic=0x27e0eb
# CGROUP2_SUPER_MAGIC=0x63677270
dont_measure fsmagic=0x63677270
# NSFS_MAGIC=0x6e736673
dont_measure fsmagic=0x6e736673
measure func=MMAP_CHECK mask=MAY_EXEC
measure func=BPRM_CHECK mask=MAY_EXEC
measure func=FILE_CHECK mask=MAY_READ uid=0
measure func=MODULE_CHECK
measure func=FIRMWARE_CHECK
```

The IMA policy for the `ima_policy=tcb_appraise` startup parameter is as follows:

```console
# PROC_SUPER_MAGIC = 0x9fa0
dont_appraise fsmagic=0x9fa0
# SYSFS_MAGIC = 0x62656572
dont_appraise fsmagic=0x62656572
# DEBUGFS_MAGIC = 0x64626720
dont_appraise fsmagic=0x64626720
# TMPFS_MAGIC = 0x01021994
dont_appraise fsmagic=0x1021994
# RAMFS_MAGIC
dont_appraise fsmagic=0x858458f6
# DEVPTS_SUPER_MAGIC=0x1cd1
dont_appraise fsmagic=0x1cd1
# BINFMTFS_MAGIC=0x42494e4d
dont_appraise fsmagic=0x42494e4d
# SECURITYFS_MAGIC=0x73636673
dont_appraise fsmagic=0x73636673
# SELINUX_MAGIC=0xf97cff8c
dont_appraise fsmagic=0xf97cff8c
# SMACK_MAGIC=0x43415d53
dont_appraise fsmagic=0x43415d53
# NSFS_MAGIC=0x6e736673
dont_appraise fsmagic=0x6e736673
# CGROUP_SUPER_MAGIC=0x27e0eb
dont_appraise fsmagic=0x27e0eb
# CGROUP2_SUPER_MAGIC=0x63677270
dont_appraise fsmagic=0x63677270
appraise fowner=0
```

The IMA policy for the `ima_policy=secure_boot` startup parameter is as follows:

```console
appraise func=MODULE_CHECK appraise_type=imasig
appraise func=FIRMWARE_CHECK appraise_type=imasig
appraise func=KEXEC_KERNEL_CHECK appraise_type=imasig
appraise func=POLICY_CHECK appraise_type=imasig
```

#### IMA Digest List Startup Parameters

The kernel startup parameters added to the IMA digest list feature are as follows:

| Parameter                | Value                   | Description                                                  |
| ------------------------ | ----------------------- | ------------------------------------------------------------ |
| integrity                | 0                       | Disables the IMA feature (by default)                        |
|                          | 1                       | Enables the IMA feature                                      |
| ima_appraise             | off                     | Disables the IMA appraisal mode                              |
|                          | enforce-evm             | Enables the IMA appraisal forced mode to perform the integrity check when the file is accessed and control the access. |
| ima_appraise_digest_list | digest                  | When the EVM is disabled, the abstract list is used for IMA appraise. The abstract list protects both the content and extended attributes of the file. |
|                          | digest-nometadata       | If the EVM digest value does not exist, the integrity check is performed only based on the IMA digest value (the file extended attribute is not protected). |
| evm                      | fix                     | Allows for any modification to the extended attribute (even if the modification causes the failure to verify the integrity of the extended attribute). |
|                          | ignore                  | Allowed to modify the extended attribute only when it does not exist or is incorrect. |
| ima_policy               | exec_tcb                | IMA measurement policy. For details, see the following policy description. |
|                          | appraise_exec_tcb       | IMA appraisal policy. For details, see the following policy description. |
|                          | appraise_exec_immutable | IMA appraisal policy. For details, see the following policy description. |
| ima_digest_list_pcr      | 11                      | Uses PCR 11 instead of PCR 10, and uses only the digest list for measurement. |
|                          | +11                     | The PCR 10 measurement is reserved. When the TPM chip is available, the measurement result is written to the TPM chip. |
| initramtmpfs             | None                    | Adds the support for **tmpfs**.                              |

The IMA policy for the `ima_policy=exec_tcb` startup parameter is as follows:

```console
dont_measure fsmagic=0x9fa0
dont_measure fsmagic=0x62656572
dont_measure fsmagic=0x64626720
dont_measure fsmagic=0x1cd1
dont_measure fsmagic=0x42494e4d
dont_measure fsmagic=0x73636673
dont_measure fsmagic=0xf97cff8c
dont_measure fsmagic=0x43415d53
dont_measure fsmagic=0x27e0eb
dont_measure fsmagic=0x63677270
dont_measure fsmagic=0x6e736673
measure func=MMAP_CHECK mask=MAY_EXEC
measure func=BPRM_CHECK mask=MAY_EXEC
measure func=MODULE_CHECK
measure func=FIRMWARE_CHECK
measure func=POLICY_CHECK
measure func=DIGEST_LIST_CHECK
measure parser
```

The IMA policy for the `ima_policy=appraise_exec_tcb` startup parameter is as follows:

```console
appraise func=MODULE_CHECK appraise_type=imasig
appraise func=FIRMWARE_CHECK appraise_type=imasig
appraise func=KEXEC_KERNEL_CHECK appraise_type=imasig
appraise func=POLICY_CHECK appraise_type=imasig
appraise func=DIGEST_LIST_CHECK appraise_type=imasig
dont_appraise fsmagic=0x9fa0
dont_appraise fsmagic=0x62656572
dont_appraise fsmagic=0x64626720
dont_appraise fsmagic=0x858458f6
dont_appraise fsmagic=0x1cd1
dont_appraise fsmagic=0x42494e4d
dont_appraise fsmagic=0x73636673
dont_appraise fsmagic=0xf97cff8c
dont_appraise fsmagic=0x43415d53
dont_appraise fsmagic=0x6e736673
dont_appraise fsmagic=0x27e0eb
dont_appraise fsmagic=0x63677270
```

The IMA policy for the `ima_policy=appraise_exec_immutable` startup parameter is as follows:

```console
appraise func=BPRM_CHECK appraise_type=imasig appraise_type=meta_immutable
appraise func=MMAP_CHECK
appraise parser appraise_type=imasig
```

#### IMA Kernel Compilation Options

The native IMA provides the following compilation options:

| Compilation Option               | Description                                             |
| -------------------------------- | ------------------------------------------------------- |
| CONFIG_INTEGRITY                 | IMA/EVM compilation switch                              |
| CONFIG_INTEGRITY_SIGNATURE       | Enables IMA signature verification                      |
| CONFIG_INTEGRITY_ASYMMETRIC_KEYS | Enables IMA asymmetric signature verification           |
| CONFIG_INTEGRITY_TRUSTED_KEYRING | Enables IMA/EVM key ring                                |
| CONFIG_INTEGRITY_AUDIT           | Compiles the IMA audit module                           |
| CONFIG_IMA                       | IMA compilation switch                                  |
| CONFIG_IMA_WRITE_POLICY          | Allows updating the IMA policy in the running phase     |
| CONFIG_IMA_MEASURE_PCR_IDX       | Allows specifying the PCR number of the IMA measurement |
| CONFIG_IMA_LSM_RULES             | Allows configuring LSM rules                            |
| CONFIG_IMA_APPRAISE              | IMA appraisal compilation switch                        |
| IMA_APPRAISE_BOOTPARAM           | Enables IMA appraisal startup parameters                |
| CONFIG_EVM                       | EVM compilation switch                                  |

The additional compilation options provided by the IMA Digest Lists extension are as follows:

| Compilation Option | Description                         |
| ------------------ | ----------------------------------- |
| CONFIG_DIGEST_LIST | Enables the IMA Digest List feature |

#### IMA Performance Reference Data

The following figure compares the performance when IMA is disabled, native IMA is enabled, and IMA digest list is enabled.

![img](./figures/ima_performance.png)

#### Impact of IMA on the kdump Service

When the IMA enforce mode is enabled and kexec system call verification is configured in the policy, kdump may fail to be started.

```console
appraise func=KEXEC_KERNEL_CHECK appraise_type=imasig
```

Cause of the kdump startup failure: After IMA is enabled, file integrity needs to be verified. Therefore, the **kexec_file_load** system call is restricted when kdump loads kernel image files. You can modify **KDUMP_FILE_LOAD** in the **/etc/sysconfig/kdump** configuration file to enable the **kexec_file_load** system call.

```console
KDUMP_FILE_LOAD="on"
```

At the same time, the **kexec_file_load** system call itself also verifies the signature of the file. Therefore, the loaded kernel image file must contain the correct secure boot signature, and the current kernel must contain the corresponding verification certificate.

#### IMA Root Certificate Configuration

Currently, openEuler uses the RPM key to sign the IMA digest list. To ensure that the IMA function is available out of the box, openEuler imports the RPM root certificate (PGP certificate) to the kernel by default during kernel compilation. Currently, there are two PGP certificates, namely, the OBS certificate used in the earlier version and the openEuler certificate used in the switchover of openEuler 22.03 LTS SP1.

```shell
$ cat /proc/keys | grep PGP
1909b4ad I------     1 perm 1f030000     0     0 asymmetri private OBS b25e7f66: PGP.rsa b25e7f66 []
2f10cd36 I------     1 perm 1f030000     0     0 asymmetri openeuler <openeuler@compass-ci.com> fb37bc6f: PGP.rsa fb37bc6f []
```

The current kernel does not support the import of the PGP sub-public key, and the switched openEuler certificate uses the sub-key signature. Therefore, the openEuler kernel preprocesses the certificate before compilation, extracts the sub-public key, and imports it to the kernel. For details, see the [process_pgp_certs.sh](https://gitee.com/src-openeuler/kernel/blob/openEuler-22.03-LTS-SP1/process_pgp_certs.sh) script file in the code repository of the kernel software package.

If the user does not use the IMA digest list function or uses other keys to implement signature/verification, you can remove the related code and configure the kernel root certificate by yourself.

## Remote Attestation (Kunpeng Security Library)

### Introduction

This project develops basic security software components running on Kunpeng processors. In the early stage, the project focuses on trusted computing fields such as remote attestation to empower security developers in the community.

### Software Architecture

On the platform without TEE enabled, this project can provide the platform remote attestation feature, and its software architecture is shown in the following figure:

![img](./figures/RA-arch-1.png)

On the platform that has enabled TEE, this project can provide TEE remote attestation feature, and its software architecture is shown in the following figure:

![img](./figures/RA-arch-2.png)

### Installation and Configuration

1. Run the following command to use the RPM package of the Yum installation program:

    ```shell
    yum install kunpengsecl-ras kunpengsecl-rac kunpengsecl-rahub kunpengsecl-qcaserver kunpengsecl-attester kunpengsecl-tas kunpengsecl-devel
    ```

2. Prepare the database environment. Go to the `/usr/share/attestation/ras` directory and run the p`prepare-database-env.sh` script to automatically configure the database environment.

3. The configuration files required for program running are stored in three paths: current path `./config.yaml`, home path `${HOME}/.config/attestation/ras(rac)(rahub)(qcaserver)(attester)(tas)/config.yaml`, and system path `/etc/attestation/ras(rac)(rahub)(qcaserver)(attester)(tas)/config.yaml`.

4. (Optional) To create a home directory configuration file, run the `prepare-ras(rac)(hub)(qca)(attester)(tas)conf-env.sh` script in `/usr/share/attestation/ras(rac)(rahub)(qcaserver)(attester)(tas)` after installing the RPM package.

### Options

#### RAS Boot Options

Run the `ras` command to start the RAS program. Note that you need to provide the ECDSA public key in the current directory and name it `ecdsakey.pub`. Options are as follows:

```console
  -H  --https         HTTP/HTTPS mode switch. The default value is https(true), false=http.
  -h  --hport         RESTful API port listened by RAS in HTTPS mode.
  -p, --port string   Client API port listened by RAS.
  -r, --rest string   RESTful API port listened by RAS in HTTP mode.
  -T, --token         Generates a verification code for test and exits.
  -v, --verbose       Prints more detailed RAS runtime log information.
  -V, --version       Prints the RAS version and exits.
```

#### RAC Boot Options

Run the `sudo raagent` command to start the RAC program. Note that the sudo permission is required to enable the physical TPM module. Options are as follows:

```console
  -s, --server string   Specifies the RAS service port to be connected.
  -t, --test            Starts in test mode.
  -v, --verbose         Prints more detailed RAC runtime log information.
  -V, --version         Prints the RAC version and exits.
  -i, --imalog          Specifies the path of the IMA file.
  -b, --bioslog         Specifies the path of the BIOS file.
  -T, --tatest          Starts in TA test mode.
```

**Note:**
>1.To use TEE remote attestation feature, you must start RAC not in TA test mode. And place the uuid, whether to use TCB, mem_hash and img_hash of the TA to be attestated sequentially in the **talist** file under the RAC execution path. At the same time, pre install the **libqca.so** and **libteec.so** library provided by the TEE team. The format of the **talist** file is as follows:
>```
>e08f7eca-e875-440e-9ab0-5f381136c600 false ccd5160c6461e19214c0d8787281a1e3c4048850352abe45ce86e12dd3df9fde 46d5019b0a7ffbb87ad71ea629ebd6f568140c95d7b452011acfa2f9daf61c7a
>```
>2.To not use TEE remote attestation feature, you must  copy the **libqca.so** and **libteec.so** library in `${DESTDIR}/usr/share/attestation/qcaserver` path to `/usr/lib` or `/usr/lib64` path, and start RAC in TA test mode.

#### QCA Boot Options

Run the `${DESTDIR}/usr/bin/qcaserver` command to start the QCA program. Note that to start QTA normally, the full path of qcaserver must be used, and the CA path parameter in QTA needs to be kept the same as the path. Options are as follows:

```console
  -C, --scenario int    Sets the application scenario of the program, The default value is sce_no_as(0), 1=sce_as_no_daa, 2=sce_as_with_daa.
  -S, --server string   Specifies the open server address/port.
```

#### ATTESTER Boot Options

Run the `attester` command to start the ATTESTER program. Options are as follows:

```console
  -B, --basevalue string   Sets the base value file read path
  -M, --mspolicy int       Sets the measurement strategy, which defaults to -1 and needs to be specified manually. 1=compare only img-hash values, 2=compare only hash values, and 3=compare both img-hash and hash values at the same time.
  -S, --server string      Specifies the address of the server to connect to.
  -U, --uuid int           Specifies the trusted apps to verify.
  -V, --version            Prints the program version and exit.
  -T, --test               Reads fixed nonce values to match currently hard-coded trusted reports.
```

#### TAS Boot Options

Run the `tas` command to start the TAS program. Options are as follows:

```console
  -T, --token         Generates a verification code for test and exits.
```

**Note:**
>1.To enable the TAS, you must configure the private key for TAS. Run the following command to modify the configuration file in the home directory:
>```shell
>$ cd ${HOME}/.config/attestation/tas
>$ vim config.yaml
> # The values of the following DAA_GRP_KEY_SK_X and DAA_GRP_KEY_SK_Y are for testing purposes only.
> # Be sure to update their contents to ensure safety before normal use.
>tasconfig:
>  port: 127.0.0.1:40008
>  rest: 127.0.0.1:40009
>  akskeycertfile: ./ascert.crt
>  aksprivkeyfile: ./aspriv.key
>  huaweiitcafile: ./Huawei IT Product CA.pem
>  DAA_GRP_KEY_SK_X: 65a9bf91ac8832379ff04dd2c6def16d48a56be244f6e19274e97881a776543c65a9bf91ac8832379ff04dd2c6def16d48a56be244f6e19274e97881a776543c
>  DAA_GRP_KEY_SK_Y: 126f74258bb0ceca2ae7522c51825f980549ec1ef24f81d189d17e38f1773b56126f74258bb0ceca2ae7522c51825f980549ec1ef24f81d189d17e38f1773b56
>```
>Then enter `tas` to start TAS program.
>
>2.In an environment with TAS, in order to improve the efficiency of QCA's certificate configuration process, not every boot needs to access the TAS to generate the certificate, but through the localized storage of the certificate. That is, read the certification path configured in `config.yaml` on QCA side, check if a TAS-issued certificate has been saved locally through the `func hasAKCert(s int) bool` function. If the certificate is successfully read, there is no need to access TAS. If the certificate cannot be read, you need to access TAS and save the certificate returned by TAS locally.

### API Definition

#### RAS APIs

To facilitate the administrator to manage the target server, RAS and the user TA in the TEE deployed on the target server, the following APIs are designed for calling:

| API                                               | Method                      |
| ---------------------------------                 | --------------------------- |
| /                                                 | GET                         |
| /{id}                                             | GET、POST、DELETE           |
| /{from}/{to}                                      | GET                         |
| /{id}/reports                                     | GET                         |
| /{id}/reports/{reportid}                          | GET、DELETE                 |
| /{id}/basevalues                                  | GET                         |
| /{id}/newbasevalue                                | POST                        |
| /{id}/basevalues/{basevalueid}                    | GET、POST、DELETE           |
| /{id}/ta/{tauuid}/status                          | GET                         |
| /{id}/ta/{tauuid}/tabasevalues                    | GET                         |
| /{id}/ta/{tauuid}/tabasevalues/{tabasevalueid}    | GET、POST、DELETE           |
| /{id}/ta/{tauuid}/newtabasevalue                  | POST                        |
| /{id}/ta/{tauuid}/tareports                       | GET                         |
| /{id}/ta/{tauuid}/tareports/{tareportid}          | GET、POST、DELETE           |
| /{id}/basevalues/{basevalueid}                    | GET、DELETE                 |
| /version                                          | GET                         |
| /config                                           | GET、POST                   |
| /{id}/container/status                            | GET                         |
| /{id}/device/status                               | GET                         |

The usage of the preceding APIs is described as follows:

To query information about all servers, use `/`.

```shell
curl -X GET -H "Content-Type: application/json" http://localhost:40002/
```

***
To query detailed information about a target server, use the GET method of `/{id}`. **{id}** is the unique ID allocated by RAS to the target server.

```shell
curl -X GET -H "Content-Type: application/json" http://localhost:40002/1
```

***
To modify information about the target server, use the POST method of `/{id}`. `$AUTHTOKEN` is the identity verification code automatically generated by running the `ras -T` command.

```go
type clientInfo struct {
  Registered   *bool `json:"registered"`  // Registration status of the target server
  IsAutoUpdate *bool `json:"isautoupdate"`// Target server base value update policy
}
```

```shell
curl -X POST -H "Authorization: $AUTHTOKEN" -H "Content-Type: application/json" http://localhost:40002/1 -d '{"registered":false, "isautoupdate":false}'
```

***
To delete a target server, use the DELETE method of `/{id}`.

**Note:**
>This method does not delete all information about the target server. Instead, it sets the registration status of the target server to `false`.

```shell
curl -X DELETE -H "Authorization: $AUTHTOKEN" -H "Content-Type: application/json" http://localhost:40002/1
```

***
To query information about all servers in a specified range, use the GET method of `/{from}/{to}`.

```shell
curl -X GET -H "Content-Type: application/json" http://localhost:40002/1/9
```

***
To query all trust reports of the target server, use the GET method of `/{id}/reports`.

```shell
curl -X GET -H "Content-Type: application/json" http://localhost:40002/1/reports
```

***
To query details about a specified trust report of the target server, use the GET method of `/{id}/reports/{reportid}`. **{reportid}** indicates the unique ID assigned by RAS to the trust report of the target server.

```shell
curl -X GET -H "Content-Type: application/json" http://localhost:40002/1/reports/1
```

***
To delete a specified trust report of the target server, use the DELETE method of `/{id}/reports/{reportid}`.

**Note:**
>This method will delete all information about the specified trusted report, and the report cannot be queried through the API.

```shell
curl -X DELETE -H "Authorization: $AUTHTOKEN" -H "Content-Type: application/json" http://localhost:40002/1/reports/1
```

***
To query all base values of the target server, use the GET method of `/{id}/reports/{reportid}`.

```shell
curl -X GET -H "Content-Type: application/json" http://localhost:40002/1/basevalues
```

***
To add a base value to the target server, use the POST method of `/{id}/newbasevalue`.

```go
type baseValueJson struct {
  BaseType   string `json:"basetype"`   // Base value type
  Uuid       string `json:"uuid"`       // ID of a container or device
  Name       string `json:"name"`       // Base value name
  Enabled    bool   `json:"enabled"`    // Whether the base value is available
  Pcr        string `json:"pcr"`        // PCR value
  Bios       string `json:"bios"`       // BIOS value
  Ima        string `json:"ima"`        // IMA value
  IsNewGroup bool   `json:"isnewgroup"` // Whether this is a group of new reference values
}
```

```shell
curl -X POST -H "Authorization: $AUTHTOKEN" -H "Content-Type: application/json" http://localhost:40002/1/newbasevalue -d '{"name":"test", "basetype":"host", "enabled":true, "pcr":"testpcr", "bios":"testbios", "ima":"testima", "isnewgroup":true}'
```

***
To query details about a specified base value of a target server, use the get method of `/{id}/basevalues/{basevalueid}`. **{basevalueid}** indicates the unique ID allocated by RAS to the specified base value of the target server.

```shell
curl -X GET -H "Content-Type: application/json" http://localhost:40002/1/basevalues/1
```

***
To change the availability status of a specified base value of the target server, use the POST method of `/{id}/basevalues/{basevalueid}`.

```shell
curl -X POST -H "Content-type: application/json" -H "Authorization: $AUTHTOKEN" http://localhost:40002/1/basevalues/1 -d '{"enabled":true}'
```

***
To delete a specified base value of the target server, use the DELETE method of `/{id}/basevalues/{basevalueid}`.

**Note:**
>This method will delete all the information about the specified base value, and the base value cannot be queried through the API.

```shell
curl -X DELETE -H "Authorization: $AUTHTOKEN" -H "Content-Type: application/json" http://localhost:40002/1/basevalues/1
```

To query the trusted status of a specific user TA on the target server, use the GET method of the `"/{id}/ta/{tauuid}/status"` interface. Where {id} is the unique identification number assigned by RAS to the target server, and {tauuid} is the identification number of the specific user TA.

```shell
curl -X GET -H "Content-type: application/json" -H "Authorization: $AUTHTOKEN" http://localhost:40002/1/ta/test/status
```

***
To query all the baseline value information of a specific user TA on the target server, use the GET method of the `"/{id}/ta/{tauuid}/tabasevalues"` interface.

```shell
curl -X GET -H "Content-type: application/json" http://localhost:40002/1/ta/test/tabasevalues
```

***
To query the details of a specified base value for a specific user TA on the target server, use the GET method of the `"/{id}/ta/{tauuid}/tabasevalues/{tabasevalueid}"` interface. where {tabasevalueid} is the unique identification number assigned by RAS to the specified base value of a specific user TA on the target server.

```shell
curl -X GET -H "Content-type: application/json" http://localhost:40002/1/ta/test/tabasevalues/1
```

***
To modify the available status of a specified base value for a specific user TA on the target server, use the `POST` method of the `"/{id}/ta/{tauuid}/tabasevalues/{tabasevalueid}"` interface.

```shell
curl -X POST -H "Content-type: application/json" -H "Authorization: $AUTHTOKEN"  http://localhost:40002/1/ta/test/tabasevalues/1 --data '{"enabled":true}'
```

***
To delete the specified base value of a specific user TA on the target server, use the `DELETE` method of the `"/{id}/ta/{tauuid}/tabasevalues/{tabasevalueid}"` interface.

**Note:**
>This method will delete all information about the specified base value, and the base value cannot be queried through the API.

```shell
curl -X DELETE -H "Content-type: application/json" -H "Authorization: $AUTHTOKEN" -k http://localhost:40002/1/ta/test/tabasevalues/1
```

***
To add a baseline value to a specific user TA on the target server, use the `POST` method of the `"/{id}/ta/{tauuid}/newtabasevalue"` interface.

```go
type tabaseValueJson struct {
  Uuid      string `json:"uuid"`       // the identification number of the user TA
  Name      string `json:"name"`       // base value name
  Enabled   bool   `json:"enabled"`    // whether a baseline value is available
  Valueinfo string `json:"valueinfo"`  // mirror hash value and memory hash value
}
```

```shell
curl -X POST -H "Content-Type: application/json" -H "Authorization: $AUTHTOKEN" -k http://localhost:40002/1/ta/test/newtabasevalue -d '{"uuid":"test", "name":"testname", "enabled":true, "valueinfo":"test info"}'
```

***
To query the target server for all trusted reports for a specific user TA, use the `GET` method of the `"/{id}/ta/{tauuid}/tareports"` interface.

```shell
curl -X GET -H "Content-type: application/json" http://localhost:40002/1/ta/test/tareports
```

***
To query the details of a specified trusted report for a specific user TA on the target server, use the `GET` method of the `"/{id}/ta/{tauuid}/tareports/{tareportid}"` interface. Where {tareportid} is the unique identification number assigned by RAS to the specified trusted report of a specific user TA on the target server.

```shell
curl -X GET -H "Content-type: application/json" http://localhost:40002/1/ta/test/tareports/2
```

***
To delete the specified trusted report of a specific user TA on the target server, use the `DELETE` method of the `"/{id}/ta/{tauuid}/tareports/{tareportid}"` interface.

**Note:**
>This method will delete all information of the specified trusted report, and the report cannot be queried through the API.

```shell
curl -X DELETE -H "Content-type: application/json" http://localhost:40002/1/ta/test/tareports/2
```

***
To obtain the version information of the program, use the GET method of `/version`.

```shell
curl -X GET -H "Content-Type: application/json" http://localhost:40002/version
```

***
To query the configuration information about the target server, RAS, or database, use the GET method of `/config`.

```shell
curl -X GET -H "Content-Type: application/json" http://localhost:40002/config
```

***
To modify the configuration information about the target server, RAS, or database, use the POST method of /config.

```go
type cfgRecord struct {
  // Target server configuration
    HBDuration      string `json:"hbduration" form:"hbduration"`
    TrustDuration   string `json:"trustduration" form:"trustduration"`
    DigestAlgorithm string `json:"digestalgorithm" form:"digestalgorithm"`
  // RAS configuration
    MgrStrategy     string `json:"mgrstrategy" form:"mgrstrategy"`
    ExtractRules    string `json:"extractrules" form:"extractrules"`
    IsAllupdate     *bool  `json:"isallupdate" form:"isallupdate"`
    LogTestMode     *bool  `json:"logtestmode" form:"logtestmode"`
}
```

```shell
curl -X POST -H "Authorization: $AUTHTOKEN" -H "Content-Type: application/json" http://localhost:40002/config -d '{"hbduration":"5s","trustduration":"20s","DigestAlgorithm":"sha256"}'
```

#### TAS APIs

To facilitate the administrator's management of TAS for remote control, the following API is designed for calling:

| API                 | Method            |
| --------------------| ------------------|
| /config             | GET、POST         |

To query the configuration information, use the GET method of the `/config` interface.

```shell
curl -X GET -H "Content-Type: application/json" http://localhost:40009/config
```

***
To modify the configuration information, use the POST method of the `/config` interface.

```shell
curl -X POST -H "Content-Type: application/json" -H "Authorization: $AUTHTOKEN" http://localhost:40009/config -d '{"basevalue":"testvalue"}'
```

**Note:**
>Currently, only the base value in the configuration information of TAS is supported for querying and modifying.

### FAQs

1. Why cannot RAS be started after it is installed?

    > In the current RAS design logic, after the program is started, it needs to search for the `ecdsakey.pub` file in the current directory and read the file as the identity verification code for accessing the program. If the file does not exist in the current directory, an error is reported during RAS boot.
    >> Solution 1: Run the `ras -T` command to generate a test token. The `ecdsakey.pub` file is generated.  
    >> Solution 2: After deploying the oauth2 authentication service, save the verification public key of the JWT token generator as `ecdsakey.pub`.

2. Why cannot RAS be accessed through REST APIs after it is started?

    > RAS is started in HTTPS mode by default. Therefore, you need to provide a valid certificate for RAS to access it. However, RAS started in HTTP mode does not require a certificate.
